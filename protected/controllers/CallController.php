<?php

class CallController extends Controller
{
    public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        public function accessRules()
        {

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolida or role=:rolidd or role=:rolidi or role=:rolidu or role=:rolidp';
    	$criteria1->params = array(':rolida'=>'academic',':rolidd'=>'director',':rolidi'=>'ilcmanager',':rolidu'=>'unit',':rolidp'=>'partner');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
        
        return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','setnoty','checknum','nlist','setcall','cal','setst','updatest','newm','updatem','getcity','getilc','getcalilc'),
    		'users'=>$modad,
    	),
        
        array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('archive','calla','closecall','setdetail','updatedetail','setai','getcmy','editfeed','updatefeed'),
    		'users'=>$modad,
    	),
            
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        
        public function actionGetcity()
        {
            if(Yii::app()->request->isPostRequest) {  
            $st1=  trim($_POST['st1']);
            $nl= new Nlist();
            $nl->getcity($st1);
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
         public function actionGetilc()
	{
           if(Yii::app()->request->isPostRequest) {  
             $ci1=  trim($_POST['ci1']);
            $nl= new Nlist();
            $nl->getilc($ci1);
        }  else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
	}
	public function actionIndex()
	{
             $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
     $crt=new CDbCriteria();
        $crt->condition='ilcid=:r';
        $crt->params=array(':r'=>$ilcid);
        $rest= ManagerIlc::model()->find($crt);
        $ilcmid=$rest->ilcmid;       
           
    //$this->render('interview',array('man'=>$ilcmid));
            $cr1=new CDbCriteria();
            $res1= Ilc::model()->findAll($cr1);
            if($rolid=='unit' || $rolid=='partner'){
            Yii::app()->user->setState('man',$ilcmid);
            }
            $this->render('index',array('res'=>$res1,'man'=>$ilcmid));
	}
        
        public function actionArchive()
	{
            $cr1=new CDbCriteria();
            $res1= Ilc::model()->findAll($cr1);
            $this->render('archive',array('res'=>$res1));
	}
        
     public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
          $nl->setnoty1();
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
        }
          public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {  
  
              $nl= new Nlist();
            $nl->checknum1();
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
        
   public function actionNlist() {
         if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
       $nl->nlist1();
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       public function actionGetcmy() {
        if(Yii::app()->request->isPostRequest) {  
           $yea=  trim($_POST['yea']);
           $mon=  trim($_POST['mon']);
        $y=intval($yea);
        $m=intval($mon);
        $userid=Yii::app()->user->getState('user_id');
        $rolid = Yii::app()->user->getState("rolid");
        
        if($rolid=='unit' || $rolid=='partner'){
            
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            if($m!=0 && $y!=0)
            {
            $crq->condition="month=:m and year=:y and userid=:i and closed=1";
            $crq->params=array(":m"=>$m,":y"=>$y,":i"=>$userid);
            }
            else if($m==0 && $y!=0)
               {
            $crq->condition="year=:y and userid=:i and closed=1";
            $crq->params=array(":y"=>$y,":i"=>$userid);
            } 
            $queries=  Call::model()->findAll($crq);
            $n=  sizeof($queries);  
        }
        
        if($rolid=='ilcmanager'){
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            if($m!=0 && $y!=0)
            {
            $crq->condition="month=:m and year=:y and ilcmid=:i and closed=1";
            $crq->params=array(":m"=>$m,":y"=>$y,":i"=>$userid);
            }
            else if($m==0 && $y!=0)
               {
            $crq->condition="year=:y and ilcmid=:i and closed=1";
            $crq->params=array(":y"=>$y,":i"=>$userid);
            } 
            $queries=  Call::model()->findAll($crq);
            $n=  sizeof($queries);   
            
        }
        
        if($rolid=='academic' || $rolid=='director'){
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            if($m!=0 && $y!=0)
            {
            $crq->condition="month=:m and year=:y and closed=1";
            $crq->params=array(":m"=>$m,":y"=>$y);
            }
            else if($m==0 && $y!=0)
               {
            $crq->condition="year=:y and closed=1";
            $crq->params=array(":y"=>$y);
            } 
            $queries=  Call::model()->findAll($crq);
            $n=  sizeof($queries);   
        }
        
        if($n==0) {
            ?> <p>No Calls were done in this month !</p> <?
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Manager's Status</b></td>
                    </tr>  
                 <?
                foreach ($queries as $q){
                ?>
                    <tr>
                        <td><?=$q->topic ?></td>
                        <td><?=$q->chour ?>:<?=$q->cmin ?><br><strong><?=$q->cdate ?></strong></td>
                        <td><?=$q->date1 ?></td>
                        <td><?=$q->cstatus ?></td>
                    </tr>
            <?
            }
            ?>
                </table>
           </div>
            <?
          } 
          
 }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
    }
        public function actionSetcall(){
              if(Yii::app()->request->isPostRequest) {  
            $topic=  trim($_POST['topic']);
            $datep=trim($_POST['datep']);
            $h=trim($_POST['h']);
            $m=trim($_POST['m']);
            $uid=Yii::app()->user->getState("user_id");
            $ecount=0;
            
            $cru=new CDbCriteria();
            $cru->condition='userid=:r';
            $cru->params=array(':r'=>$uid);
            $resu=  Users::model()->find($cru);
            $ilcid=$resu->ilcid;
            
            $cri=new CDbCriteria();
            $cri->condition='ilcid=:u';
            $cri->params=array(":u"=>$ilcid);
            $ilcinfo= Ilc::model()->find($cri);
            
            $crum=new CDbCriteria();
            $crum->condition='ilcid=:r';
            $crum->params=array(':r'=>$ilcid);
            $resum= ManagerIlc::model()->find($crum);
            $ilcmid=$resum->ilcmid;
            
            $callid=uniqid();
            $da=  explode("-", $datep);
            $yy=intval($da[0]);
            $mm=intval(date('m'));
            $ww=intval(date('W'));
            
            $en=new Call();
            
              $ecc=$en->count("ilcid=:ilc and year=:y and week=:w", array(":ilc"=>$ilcid,":y"=>$yy,":w"=>$ww));
                if($ecc<=5)
            {
                $ecc2=$en->count("cdate=:c and chour=:h and cmin=:m", array(":c"=>$datep,":h"=>$h,":m"=>$m));
                if($ecc2==0) {
             
            $rw=new Report();
            $week=$rw->getweekno($datep);
            
            $en->callid=  $callid;
            
            $en->userid= $uid;
            $en->topic=$topic;
            $en->ilcid=$ilcid;
            $en->ilcmid=$ilcmid;
            $en->cdate= $datep;
            $en->chour=$h;
            $en->cmin=$m;
            $en->date1=date('Y-m-d');
            $en->closed=0;
            $en->weekmo=$week;
            $en->month=  intval($da[1]);
            $en->year=  intval($da[0]);
            $en->week=intval($rw->getweekno(date('Y-m-d')));
            $en->state=$ilcinfo->state;
            $en->city=$ilcinfo->city;
            $en->isNewRecord=true;
            $en->save(FALSE);
            
            
            
            $mst="A call has been scheduled on ".$datep." at ".$h.':'.$m;
            $qn=new Notifications();
            $qn->queryid=$callid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            $usr=new Nlist();
            $il=$usr->getilcmid($ilcid);
            $ilcmid=$il->ilcmid;
            
            $usr=new Nlist();
            $uu=$usr->getuser($ilcmid);
            $uname=$uu->username;
            $name=$uu->name;
            
            
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Call Scheduled";
            $msg="Dear $name,<br>
                A call has been scheduled on $datep at $h:$m<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            ///////////////////////////////////////////// 
            }
            }     
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
       
        }
        public function actiongetcalilc(){
           
             if(Yii::app()->request->isPostRequest) {    
            $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           $st=  trim($_POST['st']);
             $ci=  trim($_POST['ci']);
               $ilc1=  trim($_POST['ilc1']);
             
           if($rolid=='unit' )
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and closed=0 and userid=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Scheduled Calls"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                       
                        <td><?=$i->cstatus ?></td>
                        <td>
                           
                           <? if($i->closed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->callid ?>" name="<?=$i->callid ?>" value="Close" onclick="editfeed(this.id)" />
                            
                        <? } ?>
                        </td>
                    </tr>
                   <?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     
            if($rolid=='partner'){
         $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and closed=0 and userid=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Scheduled Calls"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                       
                        <td><?=$i->cstatus ?></td>
                        <td>
                           
                           <? if($i->closed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->callid ?>" name="<?=$i->callid ?>" value="Close" onclick="closecall(this.id)" />
                            
                        <? } ?>
                        </td>
                    </tr><?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
               $crq=new CDbCriteria();
               $crq->condition='closed=0 and ilcid=:i';
               $crq->params=array(':i'=>$ilc1);
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p> <? echo "No Current call from this ILC"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                  if(in_array($i->ilcid, $ii)) {  
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                       <td>
                         <? if($i->cstatus!=""){
                          ?> 
                            <?=$i->cstatus ?>
                        <? } 
                        else if($i->cstatus=="") {
                        ?> 
                         <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->callid ?>" name="<?=$i->callid ?>" value="Set status" onclick="setst(this.id)" />
                        <? } ?>
                       </td>
                       <td> <?
                         $edate1=  strtotime($i->cdate);
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d<=24 ) {
                    ?>
                         <br>
                         <button class="btn-warning">Active</button>
                    <?
                }
                else{
                    ?>
                        <button class="btn-danger">Escalated</button><?
                }?>   
                        </td>
                    </tr><?
                } }
        ?>
          </table>
        </div>
             <?
            }
            
        }
        
           if($rolid=='academic'){
               $crq=new CDbCriteria();
               if($ci=="0" && $ilc1=="0")
               {
                   $crq->condition='closed=0 and state=:s';
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($ilc1=="0"){
                    $crq->condition='closed=0 and state=:s and city=:c';
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition='closed=0 and ilcid=:i';
                    $crq->params=array(':i'=>$ilc1);
               }
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p> <? echo "No calls schehuled from this ILC"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Action</b></td>
                        <td><b>Status</b></td>
                        <td></td>
                    </tr>        
               <?
                foreach ($en as $i){
                $edate1=  strtotime($i->cdate);
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>24 ) {
                ?>
                <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <td>
                          <button type="button" id="<?=$i->callid ?>" name="<?=$i->callid ?>" style="text-align:left;background-color:transparent;border:0;font-size:14px !important; padding:4px;border-radius:5px;text-decoration:underline;color:black" onclick="newm(this.id)">Appoint NEW Manager</button>
                        </td>
                        <td><?=$i->cstatus ?></td>
                        <td><? if($d>24 && $d<=48 ) {  ?>
                            
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            &nbsp;
                            <? } 
                            else if ($d>48 || $d==48){ ?>
                             <button class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <? } ?>
                             
                        </td>
                    </tr><? 
                
                } } ?>
                </table>
                </div>
                <?
           }
        }
        
           if($rolid=='director'){
            $crq=new CDbCriteria();
               if($ci=="0" && $ilc1=="0")
               {
                   $crq->condition='closed=0 and state=:s';
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($ilc1=="0"){
                    $crq->condition='closed=0 and state=:s and city=:c';
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition='closed=0 and ilcid=:i';
                    $crq->params=array(':i'=>$ilc1);
               }
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p> <? echo "No Calls from this ILC"; ?></p> <?
                }
                else 
                { ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td></td>
                    </tr>  
                 <?
                foreach ($en as $i){
                   $edate1=  strtotime($i->cdate);
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>48 ) {
                
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <? 
                        if($d>48 && $d<=72)
                        { ?>
                        <td><?=$i->cstatus ?></td>
                          <td><button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button></td> 
                        <? } 
                        else if($d>72 || $d==72){ ?>
                        <td><button class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button></td>
                         <? } ?>
                       
                    </tr>
                   <?
                }}
        ?>
          </table>
        </div>
                <?
                }
        }
        
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
     }
        public function actionCal($deer){
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit' )
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and closed=0 and userid=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Scheduled Calls"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                       
                        <td><?=$i->cstatus ?></td>
                        <td>
                           
                           <? if($i->closed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->callid ?>" name="<?=$i->callid ?>" value="Close" onclick="editfeed(this.id)" />
                            
                        <? } ?>
                        </td>
                    </tr>
                   <?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     
            if($rolid=='partner'){
         $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and closed=0 and userid=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Scheduled Calls"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                       
                        <td><?=$i->cstatus ?></td>
                        <td>
                           
                           <? if($i->closed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->callid ?>" name="<?=$i->callid ?>" value="Close" onclick="closecall(this.id)" />
                            
                        <? } ?>
                        </td>
                    </tr><?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     
           if($rolid=='ilcmanager')
           {
               ///////////////reassigned manager///////////////
               $crq=new CDbCriteria();
               $crq->condition='ilcmid=:i and close=0 and callid<>:c';
               $crq->params=array(':i'=>$userid,':c'=>'-');
               $rc= Assignesc::model()->findAll($crq);
               $nc=sizeof($rc);
               ////////////////////////////////////////////////
               
               
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
               $crq=new CDbCriteria();
               $crq->condition='closed=0';
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Call Scheduled"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>  
                 <?
                 /////////////reassigned list/////////////
                 foreach ($rc as $c){
                    $callid=$c->callid;
                    $crq=new CDbCriteria();
                    $crq->condition='callid=:c';
                    $crq->params=array(':c'=>$callid);
                    $crq->order='date1 desc';
                    $i= Call::model()->find($crq);
                     
                  ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                       <td>
                         <? if($i->cstatus!=""){
                          ?> 
                            <?=$i->cstatus ?>
                        <? } 
                        else if($i->cstatus=="") {
                        ?> 
                         <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->callid ?>" name="<?=$i->callid ?>" value="Set status" onclick="setst(this.id)" />
                        <? }
                        
                        ?>
                       </td>
                       <td>
                        <button class="btn-danger">Reassigned</button>                   
                        <?
                         $edate1=$i->cdate;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d<=24 ) {
                    ?>
                         <br>
<!--                         <button class="btn-warning">Active</button>-->
                    <?
                }
                else{
                    ?>
                     <!--   <button class="btn-danger">Escalated</button>--><?
                } ?>   
                        </td>
                    </tr><?
                }
                 ////////////////////////////////////////
                 
                foreach ($en as $i){
                  if(in_array($i->ilcid, $ii)) {  
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                       <td>
                         <? if($i->cstatus!=""){
                          ?> 
                            <?=$i->cstatus ?>
                        <? } 
                        else if($i->cstatus=="") {
                        ?> 
                         <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->callid ?>" name="<?=$i->callid ?>" value="Set status" onclick="setst(this.id)" />
                        <? } ?>
                       </td>
                       <td> <?
                         $edate1= strtotime($i->cdate);
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
//                if($currtime<$edate1)
//                    $datediff=0;
		$d=round($datediff/(60 * 60 * 24))*24;  // echo "$d<br>"; 
                if($d<=24 ) {
                  ?>
                         <br>
                       
                         <button class="btn-warning">Active</button>
                    <?
                }
                else{
                    ?>
                        <button class="btn-danger">Escalated</button><?
                }?>   
                        </td>
                    </tr><?
                } }
        ?>
          </table>
        </div>
             <?
            }
            
        }
        
           if($rolid=='academic'){
            $crq=new CDbCriteria();
               $crq->condition='closed=0';
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No calls schehuled"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Action</b></td>
                        <td><b>Status</b></td>
                        <td></td>
                    </tr>        
               <?
                foreach ($en as $i){
                $callid=$i->callid;    
                $edate1=  strtotime($i->cdate);
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>24 || $d==24) {
                ?>
                <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <td>
                          <button class="btn btn" type="button" id="<?=$i->callid ?>" name="<?=$i->callid ?>" style="border:1px solid black;text-align:left;font-size:14px !important; padding:4px;border-radius:5px;color:black" onclick="newm(this.id)">
                              Appoint NEW Manager</button>
                           
                        </td>
                        <td><?=$i->cstatus ?></td>
                        <td><?
                            $crq=new CDbCriteria();
                            $crq->condition='callid=:c and close=0';
                            $crq->params=array(':c'=>$callid);
                            $rc= Assignesc::model()->find($crq);
                            $nc=sizeof($rc);
                            if($rc!=0){
                                ?><button class="btn-danger" style="margin-bottom:5px">Reassigned</button><? 
                            }
                        if($d>24 && $d<=48 ) {  ?>
                            
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            &nbsp;
                            <? } 
                            else if ($d>48 || $d==48){ ?>
                             <button class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <?
                            
                            
                            }
                            ?>
                             
                        </td>
                    </tr><? 
                
                } } ?>
                </table>
                </div>
                <?
           }
        }
        
           if($rolid=='director'){
            $crq=new CDbCriteria();
               $crq->condition='closed=0';
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Calls"; ?></p> <?
                }
                else 
                { ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Status</b></td>
                        <td></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                   $edate1=  strtotime($i->date1);
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>48 ) {
                
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <? 
                        if($d>48 && $d<=72)
                        { ?>
                        <td><?=$i->cstatus ?></td>
                          <td><button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button></td> 
                        <? } 
                        else if($d>72 || $d==72){ ?>
                        <td><button class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button></td>
                         <? } ?>
                       
                    </tr>
                   <?
                }}
        ?>
          </table>
        </div>
                <?
                }
        }
        
        
     }
     
        public function actionEditfeed(){
         if(Yii::app()->request->isPostRequest) {  
            $id=  trim($_POST['id']);
    $ilcid=Yii::app()->user->getState('ilc_id');
    $cru=new CDbCriteria();
    $cru->condition='ilcid=:u';
    $cru->params=array(":u"=>$ilcid);
    $manager= ManagerIlc::model()->find($cru);
    $ilcmid=$manager->ilcmid;
    
     ?>
         <div class="row">            
            <div class="col-md-12">      
                <div class="input-group mb-3">
                    <input type="hidden" value="<?=$id ?>" id="qqid">
                    <input type="hidden" value="<?=$ilcmid ?>" id="ilcmid">
                    <p>How much satisfied are you with the reply?</p>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rating</span>
                    </div>
                    <select type="number" class="form-control"  id="rating" aria-describedby="basic-addon1" onchange="revshowf()">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                </div>
            </div>
             <div class="col-md-12" id="revshow" >  
                 <br style="clear:both">
               <textarea  style="width:100% !important"  id="review" placeholder="What could have been better?"   aria-describedby="basic-addon1"></textarea>
                <br style="clear:both">
            </div>
            
        </div>
        <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatefeed()"/>
        </div>    
         <?
         }       else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
 
        public function actionUpdatefeed(){
              if(Yii::app()->request->isPostRequest) {  
            $qid=  trim($_POST['qid']);
            $ilcmid= trim($_POST['ilcmid']);
            $rating= trim($_POST['rating']);
            $review=trim($_POST['review']);
            $asc=new Assignesc();
            $callcount=intval($asc->count("callid=:c", array(":c"=>$callid)));
            
            if($callcount>0){
                $qq=new Assignesc();
                $qq->updateAll(array('rating'=>$rating), "callid=:q", array(':q'=>$qid));
            }
            else{
                $qq=new Call();
                $qq->updateAll(array('cfeed'=>$rating), "callid=:q", array(':q'=>$qid));
            }
      }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
     
     
     
     
     }
     
        public function actionCalla(){
           if(Yii::app()->request->isPostRequest) {  
            $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit')
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and closed=1 and userid=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Scheduled Calls"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Manager's Status</b></td>
                    </tr>  
                 <?
                foreach ($en as $i){
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <td><?=$i->cstatus ?></td>
                    </tr>
            
            <?
            }
            ?>
          </table>
           </div>
            <?
       }
     }
            
            if($rolid=='partner')
                {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and closed=1 and userid=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Scheduled Calls"; ?></p> <?
                }
                else 
                {
                ?>
               <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Manager's Status</b></td>
                    </tr>  
                 <?
                foreach ($en as $i){
                ?>
                    <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <td><?=$i->cstatus ?></td>
                    </tr>
            
            <?
            }
            ?>
          </table>
           </div>
            <?
       }
     }
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
               $crq=new CDbCriteria();
               $crq->condition='closed=1';
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No calls"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                   <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Manager's Status</b></td>
                    </tr>    
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                if(in_array($ilid,$ii)){
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                ?>
                <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <td><?=$i->cstatus ?></td>
                    </tr>
                
                
                <? }
               }
               ?>
                </table>
              </div>
                <?
            }
            
        }
        
           if($rolid=='academic'){
            $crq=new CDbCriteria();
               $crq->condition='closed=1';
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Calls"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Manager's Status</b></td>
                    </tr>     
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $edate1=$i->date1;
                
                ?>
                
                <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <td><?=$i->cstatus ?></td>
                    </tr>
                
                
                <? 
               
               }
               ?>
                </table>
                </div>
                <?
            }
        }
        
           if($rolid=='director'){
            $crq=new CDbCriteria();
               $crq->condition='closed=1';
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Calls"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Created On</b></td>
                        <td><b>Manager's Status</b></td>
                    </tr>     
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $edate1=$i->date1;
                
                ?>
                <tr>
                        <td><?=$i->topic ?></td>
                        <td><?=$i->chour ?>:<?=$i->cmin ?><br><strong><?=$i->cdate ?></strong></td>
                        <td><?=$i->date1 ?></td>
                        <td><?=$i->cstatus ?></td>
                </tr>
                
                <? 
                
               }
               ?>
                </table>
                    </div>
                        <?
            }
        }
        
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
     }
   
        public function actionSetst(){ 
              if(Yii::app()->request->isPostRequest) {  
            $enqid=  trim($_POST['enqid']);
        $cru=new CDbCriteria(); 
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $en= Enquiry::model()->find($cru);
        
     ?>
         <div class="row">            
            <div class="col-md-12"> 
                <p>Set status for this call</p>
                <input type="hidden" id="enqid" value="<?=$enqid ?>">   
               <textarea class="form-control" id="st1" name="st1" rows="3" placeholder="Add some text"></textarea>
            </div>
             
         </div>
         <br>
         <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatest()"/>
         </div>
            
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionSetdetail(){
         if(Yii::app()->request->isPostRequest) {  
    
            $enqid=  trim($_POST['enqid']);
        $rolid=Yii::app()->user->getState('rolid');    
        
        $cru=new CDbCriteria(); 
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $en= Enquiry::model()->find($cru);
        $parid=$en->parentid;
        
        $crp=new CDbCriteria(); 
        $crp->condition='parentid=:u';
        $crp->params=array(":u"=>$parid);
        $pa= EnquiryParent::model()->find($crp);
        
        
     ?>
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Father's Details</strong>
         </div>
         <div class="row">
            <div class="col-md-6"> 
                <input type="hidden" id="parid1" value="<?=$parid ?>">
                <input type="text" class="enin" id="femail" value="<?=($pa->parent_f_email=='')?'':$pa->parent_f_email ?>"  placeholder="Email ID">  
            </div>
             <div class="col-md-6"> 
                <input type="text" class="enin" id="focc" value="<?=($pa->f_occupation=='')?'':$pa->f_occupation ?>" placeholder="Occupation">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="fphn" value="<?=($pa->parent_f_phone=='')?'':$pa->parent_f_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                 <select type="text" class="enin" id="fcall"  placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?>">
                    <?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?> </option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Call at your own risk">Call at your own risk</option>
                 </select> 
            </div>
         </div>
         <br style="clear:both">
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Mother's Details</strong>
         </div>
         <div class="row">
             <div class="col-md-6"> 
                <input type="text" class="enin" id="mname" value="<?=($pa->parent_m_name=='')?'':$pa->parent_m_name ?>" placeholder="Name">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="memail" value="<?=($pa->parent_m_email=='')?'':$pa->parent_m_email ?>" placeholder="Email ID">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="mphn" value="<?=($pa->parent_m_phone=='')?'':$pa->parent_m_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                <select type="text" class="enin" id="mcall" placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?>"><?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?></option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Mother's busy call father">Mother's busy call father.</option>
                 </select>  
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
            <div class="col-md-12"> 
              <textarea  class="enin" id="add" placeholder="Parent Address"><?=(trim($pa->parent_add)=='')?'':trim($pa->parent_add) ?></textarea>
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                 <strong style="margin-left:1%;font-size:16px !important">
                     How have parents heard about Edify Kids Baguiati?</strong>
                 <div id="hhear">
                 <input type="hidden" value="<?=$pa->how_hear ?>" id="how">    
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Hoardings">
                    <span style="font-size:16px!important">Hoardings</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Pamphlets" >
                    <span style="font-size:16px!important">Pamphlets</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Newspaper insert">
                    <span style="font-size:16px!important">Newspaper insert</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Just Dial Services">
                    <span style="font-size:16px!important">Just Dial Services</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Cable TV Ad">
                  <span style="font-size:16px!important">Cable TV Ad</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Internet">
                   <span style="font-size:16px!important">Internet</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Word of mouth">
                    <span style="font-size:16px!important">Word of mouth</span>&nbsp;
                </div>
                </div>
             </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                <input type="text" class="enin" id="hoarding" value="<?=($pa->hoarding=='')?'':$pa->hoarding ?>" placeholder="Where have you seen the hoarding?">
              </div>
         </div>
         
         <div class="row">
             <div class="col-md-12">
                 <textarea class="enin" id="feed" rows="3" placeholder="Parent Feedback"><?=(trim($pa->parent_feedback)=='')?'':trim($pa->parent_feedback) ?></textarea>
              </div>
         </div>
         
         
          <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatedetail()"/>
         </div>
            
        <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionSetai(){
        if(Yii::app()->request->isPostRequest) {  
            $enqid=  trim($_POST['enqid']);
            $rolid=Yii::app()->user->getState('rolid');    
        
        $cru=new CDbCriteria(); 
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $en= Enquiry::model()->find($cru);
        $parid=$en->parentid;
        
        $crp=new CDbCriteria(); 
        $crp->condition='parentid=:u';
        $crp->params=array(":u"=>$parid);
        $pa= EnquiryParent::model()->find($crp);
        
        ?>
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Father's Details</strong>
         </div>
         <div class="row">
            <div class="col-md-6"> 
                <input type="hidden" id="parid1" value="<?=$parid ?>">
                <input type="text" class="enin" id="femail" value="<?=($pa->parent_f_email=='')?'':$pa->parent_f_email ?>"  placeholder="Email ID">  
            </div>
             <div class="col-md-6"> 
                <input type="text" class="enin" id="focc" value="<?=($pa->f_occupation=='')?'':$pa->f_occupation ?>" placeholder="Occupation">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="fphn" value="<?=($pa->parent_f_phone=='')?'':$pa->parent_f_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                 <select type="text" class="enin" id="fcall"  placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?>">
                    <?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?> </option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Call at your own risk">Call at your own risk</option>
                 </select> 
            </div>
         </div>
         <br style="clear:both">
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Mother's Details</strong>
         </div>
         <div class="row">
             <div class="col-md-6"> 
                <input type="text" class="enin" id="mname" value="<?=($pa->parent_m_name=='')?'':$pa->parent_m_name ?>" placeholder="Name">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="memail" value="<?=($pa->parent_m_email=='')?'':$pa->parent_m_email ?>" placeholder="Email ID">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="mphn" value="<?=($pa->parent_m_phone=='')?'':$pa->parent_m_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                <select type="text" class="enin" id="mcall" placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?>"><?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?></option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Mother's busy call father">Mother's busy call father.</option>
                 </select>  
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
            <div class="col-md-12"> 
              <textarea  class="enin" id="add" placeholder="Parent Address"><?=(trim($pa->parent_add)=='')?'':trim($pa->parent_add) ?></textarea>
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                 <strong style="margin-left:1%;font-size:16px !important">
                     How have parents heard about Edify Kids Baguiati?</strong>
                 <div id="hhear">
                 <input type="hidden" value="<?=$pa->how_hear ?>" id="how">    
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Hoardings">
                    <span style="font-size:16px!important">Hoardings</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Pamphlets" >
                    <span style="font-size:16px!important">Pamphlets</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Newspaper insert">
                    <span style="font-size:16px!important">Newspaper insert</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Just Dial Services">
                    <span style="font-size:16px!important">Just Dial Services</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Cable TV Ad">
                  <span style="font-size:16px!important">Cable TV Ad</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Internet">
                   <span style="font-size:16px!important">Internet</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Word of mouth">
                    <span style="font-size:16px!important">Word of mouth</span>&nbsp;
                </div>
                </div>
             </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                <input type="text" class="enin" id="hoarding" value="<?=($pa->hoarding=='')?'':$pa->hoarding ?>" placeholder="Where have you seen the hoarding?">
              </div>
         </div>
         
         <div class="row">
             <div class="col-md-12">
                 <textarea class="enin" id="feed" rows="3" placeholder="Parent Feedback"><?=(trim($pa->parent_feedback)=='')?'':trim($pa->parent_feedback) ?></textarea>
              </div>
         </div>
         
            
        <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionNewm(){
              if(Yii::app()->request->isPostRequest) {  
            $callid=  trim($_POST['callid']);
        $cru=new CDbCriteria(); 
        $cru->condition='callid=:u';
        $cru->params=array(":u"=>$callid);
        $en= Call::model()->find($cru);
        
     ?>
        <div class="row">            
            <div class="col-md-12">  
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2">NEW Manager</span>
                    </div>
                    <input type="hidden" id="callid" value="<?=$callid ?>">
                    <select class="form-control" id="ilcmid" >
                        <option value="0" selected>Choose NEW Manager</option>
                        <?
                        $crq=new CDbCriteria(); 
                        $crq->select='*';
                        $crq->condition="role=:r";
                        $crq->params=array(':r'=>'ilcmanager');
                        $ilc=Users::model()->findAll($crq);
        
                        foreach($ilc as $i){
                            $iname=$i->name;
                            $iid=$i->userid;
                            ?>
                            <option value="<?=$iid ?>"><?=$iname ?></option>
                            <? } ?>
                    </select>
                </div>
</div>
         </div>
         <br>
         <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatem()"/>
         </div>
            
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUpdatest(){
          if(Yii::app()->request->isPostRequest) {  
            $enqid=  trim($_POST['enqid']);
        $st1=  trim($_POST['st1']);
            $qq=new Call();
            $qq->updateAll(array('cstatus'=>$st1), "callid=:q", array(':q'=>$enqid));
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUpdatedetail(){
           if(Yii::app()->request->isPostRequest) { 
            $parid=trim($_POST['parid']);
            $femail=trim($_POST['femail']);
            $fphn=trim($_POST['fphn']);
            $focc=trim($_POST['focc']);
            $fcall=trim($_POST['fcall']);
            $mname=trim($_POST['mname']);
            $mphn=trim($_POST['mphn']);
            $memail=trim($_POST['memail']);
            $mcall=trim($_POST['mcall']);
            $add=trim($_POST['add']);
            $hhear=trim($_POST['hhear']);
            $hoarding=trim($_POST['hoarding']);
            $feed=  trim($_POST['feed']);


            $qq=new EnquiryParent();
            $qq->updateAll(array(
                'f_occupation'=>$focc,
                'parent_f_phone'=>$fphn,
                'parent_f_email'=>$femail,
                'parent_f_calltime'=>$fcall,
                'parent_m_name'=>$mname,
                'parent_m_email'=>$memail,
                'parent_m_calltime'=>$mcall,
                'parent_m_phone'=>$mphn,
                'parent_add'=>$add,
                'how_hear'=>$hhear,
                'hoarding'=>$hoarding,
                'parent_feedback'=>$feed,
                ),
                    "parentid=:q", array(':q'=>$parid));
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUpdatem(){
       if(Yii::app()->request->isPostRequest) {  
         $callid=  trim($_POST['callid'])   ;
         $ilcmid=  trim($_POST['ilcmid']);
         $ilcmname=  trim($_POST['ilcmname']);
         
         $cdate=date('Y-m-d');    
        $cru=new CDbCriteria();
        $cru->condition='callid=:u';
        $cru->params=array(":u"=>$callid);
        $enq=Call::model()->find($cru);
        $uid=$enq->userid;    
        $rw=new Report(); 
        $ilcm=$enq->ilcmid;
        
        $qq=new Call();
        $qq->updateAll(array('cdate'=>$cdate), "callid=:q", array(':q'=>$callid)); 
           
        if($ilcmid!=$enq->ilcmid){
            $q2=new Assignesc();
            $q2->asid=uniqid();
            $q2->intid='-';
            $q2->callid=$callid;
            $q2->ilcmid=$ilcmid;
            $q2->ilcid=$enq->ilcid;
            $q2->week=$rw->getweekno(date('Y-m-d'));
            $q2->month= intval(date('m'));
            $q2->year=intval(date('Y'));
            $q2->close=0;
            $q2->city=$enq->city;;
            $q2->state=$enq->state;
            $q2->isNewRecord=true;
            $q2->save(FALSE);
        }
        else{
           $qq1=new Call();
            $qq1->updateAll(array('ilcmid'=>$ilcmid), "callid=:q", array(':q'=>$callid));  
        }
          
        
        $crum=new CDbCriteria();
        $crum->condition='ilcmid=:u';
        $crum->params=array(":u"=>$ilcmid);
        $enqm= ManagerIlc::model()->find($crum);
        $nm=  sizeof($enqm);
        
         
        $mst="Academic head has assigned a new ILC Manager ".$ilcmname." for the call scheduled on ".$enq->cdate." at ".$enq->chour.":".$enq->cmin;
        $qn=new Notifications();
        $qn->queryid=$callid;
        $qn->notid=  uniqid();
        $qn->role="unit";
        $qn->ilc=$enq->ilcid;
        $qn->message=$mst;
        $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
        $qn->count1=3;
        $qn->isNewRecord=true;
        $qn->save(FALSE);
    
        $qn=new Notifications();
        $qn->queryid=$callid;
        $qn->notid=  uniqid();
        $qn->role="partner";
        $qn->ilc=$enq->ilcid;
        $qn->message=$mst;
        $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
        $qn->count1=3;
        $qn->isNewRecord=true;
        $qn->save(FALSE);
         
            $usr=new Nlist();
            $uu=$usr->getuser($ilcm);
            $uname=$uu->username;
            $name=$uu->name;
            
            
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Manager reassigned";
            $msg="Dear $name,<br>
                Academic head has assigned a new ILC Manager $ilcmname for the call scheduled on $enq->cdate at $enq->chour:$enq->cmin<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
        
        
        
        if($nm!=0){
            
        $qn=new Notifications();
        $qn->queryid=$callid;
        $qn->notid=  uniqid();
        $qn->role="ilcmanager";
        $qn->ilc=$enqm->ilcid;
        $qn->message=$mst;
        $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
        $qn->count1=3;
        $qn->isNewRecord=true;
        $qn->save(FALSE);
        
            
            
            $usr=new Nlist();
            $uu=$usr->getuser($ilcmid);
            $uname=$uu->username;
            $name=$uu->name;
            
            
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Manager reassigned";
            $msg="Dear $name,<br>
                Academic head has assigned you as new ILC Manager $ilcmname for the call scheduled on $enq->cdate at $enq->chour:$enq->cmin<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
        
        }
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        } 
        
        public function actionClosecall() {
       if(Yii::app()->request->isPostRequest) {  
            $callid=  trim($_POST['callid']);
            $cru=new CDbCriteria(); 
            $cru->condition='callid=:u';
            $cru->params=array(":u"=>$callid);
            $inter= Call::model()->find($cru);
            $ilcid=$inter->ilcid;
                        
            $qq=new Notifications();
            $qq->deleteAll("queryid=:q ", array(":q"=>$callid));
            
            $asc=new Assignesc();
            $callcount=intval($asc->count("callid=:c", array(":c"=>$callid)));
            
            if($callcount>0){
                $qq=new Assignesc();
            $qq->updateAll(array("close"=>1), 'callid=:q', array(":q"=>$callid));
            $qq=new Call();
                $qq->updateAll(array("closed"=>1,"clodate"=>date('Y-m-d')), 'callid=:q', array(":q"=>$callid)); 
            }
            else {
               $qq=new Call();
                $qq->updateAll(array("closed"=>1,"clodate"=>date('Y-m-d')), 'callid=:q', array(":q"=>$callid)); 
            }
             
            
          $mst="The scheduled call on ".$inter->cdate." has been closed";
            $qn=new Notifications();
            $qn->queryid=$callid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$inter->ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            $usr=new Nlist();
            $il=$usr->getilcmid($ilcid);
            $ilcmid=$il->ilcmid;
            
            $usr=new Nlist();
            $uu=$usr->getuser($ilcmid);
            $uname=$uu->username;
            $name=$uu->name;
            
            
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Call Closed";
            $msg="Dear $name,<br>
                The scheduled call on $inter->cdate has been closed<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
           }
        }