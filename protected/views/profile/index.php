<?php
/* @var $this ProfileController */

if(empty($userp))
    $userp=new Userphoto;
?>
<div class="container">
    <div class="row">
        <h1>Edit Profile</h1>
    </div>
    <input type="hidden" class="form-control"  id="uid" value="<?=$user->userid ?>"   aria-describedby="basic-addon1">
    <div class="row">
        <img src="<?= Yii::app()->request->baseUrl.'/userphoto/'.$userp->uphoto ?>" style="width:60px;height:60px">
    </div>  
   <br style="clear:both">
    <div class="row">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon2">Full Name</span>
            </div>
            <input type="text" class="form-control"  id="fullname" value="<?=$user->name ?>"   aria-describedby="basic-addon1">
        </div>
    </div>
    
    <div class="row">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon2">Username</span>
            </div>
            <input type="text" readonly="true" class="form-control"  id="username" value="<?=$user->username ?>"   aria-describedby="basic-addon1">
        </div>
    </div>
    
    <div class="row">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon2">Password</span>
            </div>
            <input type="password" class="form-control"  id="password" value=""   aria-describedby="basic-addon1">
        </div>
    </div>
        <div class="row">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon2">Re type Password</span>
            </div>
            <input type="password" class="form-control"  id="passwordb" value=""   aria-describedby="basic-addon1">
        </div>
    </div>
    <div class="row">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon2">Address</span>
            </div>
            <input type="text" readonly="true" class="form-control"  id="address" value="<?=$user->address ?>"   aria-describedby="basic-addon1">
        </div>
    </div>
    
    <div class="row">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon2">Phone</span>
            </div>
            <input type="text"  class="form-control"  id="phone" value="<?=$user->phone ?>"   aria-describedby="basic-addon1">
        </div>
    </div>
    
    

    
    <div class="row">
        <button class="btn btn-dark" onclick="updateprof()" >Update</button>&nbsp;
        <a href="<?=Yii::app()->request->baseUrl."/index.php/userphoto/create"?> " class="btn btn-success">Upload Photo</a>
    <div id="body1"></div>
    </div>
            
</div>
<script>
    function isNumeric(x) {
    return parseFloat(x).toString() === x.toString();
}
    function updateprof(){
    var uid=$("#uid").val();
    var fullname=$("#fullname").val();
    var password=$("#password").val();
    var passwordb=$("#passwordb").val();
    var phone=$("#phone").val();
      var numbers = /^[0-9]+$/;
   if(password=="" || password.length<5)
   {alert("Enter proper password! Password should be greater than 4 letters.");}
   else if(password.includes(" "))
   {alert("Password should not contain blank spaces.");}
   else if(password!=passwordb)
   {alert("Both passwords should match!");}
   else if(!isNumeric(phone)) {alert("Enter only numbers for phone...");}
   else {
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('&nbsp;Changes are being saved..');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
   //   window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updateprof"); ?>',
        data:{
          uid:uid,fullname:fullname,password:password,phone:phone
        },
    dataType:"html",
    cache:false
})//ajax
}
}
</script>



