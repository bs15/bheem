<?php
/* @var $this AcademicController */
$logged=Yii::app()->user->getState('logged');
 $userid=Yii::app()->user->getState('user_id');
  $rolid=Yii::app()->user->getState("rolid");
  
  $ii=array();
$this->breadcrumbs=array(
	'Academic',
);

$u=new Users();

$ni=0;
$crm=new CDbCriteria();
        $crm->condition='ilcmid=:u';
        $crm->params=array(":u"=>$userid);
        $res=  ManagerIlc::model()->findAll($crm);
        foreach ($res as $r) {
            array_push($ii, $r->ilcid);
        }
        $ni=sizeof($ii);
        
$nu=0;
$np=0;
$nt=0;

$unit=new CDbCriteria();
$unit->condition='role=:r';
$unit->params=array(':r'=>'unit');
$ures=Users::model()->findAll($unit);
foreach($ures as $uu){
    if(in_array($uu->ilcid,$ii)){
        $nu++;
    }
}

$unit=new CDbCriteria();
$unit->condition='role=:r';
$unit->params=array(':r'=>'partner');
$ures=Users::model()->findAll($unit);
foreach($ures as $uu){
    if(in_array($uu->ilcid,$ii)){
        $np++;
    }
}

$unit=new CDbCriteria();
$unit->condition='role=:r';
$unit->params=array(':r'=>'teacher');
$ures=Users::model()->findAll($unit);
foreach($ures as $uu){
    if(in_array($uu->ilcid,$ii)){
        $nt++;
    }
}
        

/////////////////////////query///////////////////
$qesc=0;
$ilcs=array();        
        ///////////////////////////////////////
        $crm=new CDbCriteria();
        $crm->condition='ilcmid=:u';
        $crm->params=array(":u"=>$userid);
        $res=  ManagerIlc::model()->findAll($crm);
        foreach ($res as $r) {
            array_push($ilcs, $r->ilcid);
        }
        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="query_closed=0";
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        foreach ($queries as $q) {
                $user=$q->userid;
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                if(in_array($ilc, $ilcs))
                {
                
                
                    $qesc++;
                }
            }
            
///////////////////////interview///////////////////   
$iesc=0;
            $crq=new CDbCriteria();
            $crq->condition='ilcmid=:u';
            $crq->params=array(':u'=>$userid);
            $ilcs= ManagerIlc::model()->findAll($crq);
            
            
            
            $ii=array();
            foreach($ilcs as $il){
                array_push($ii,$il->ilcid);
            }
                
                $crq1=new CDbCriteria();
                $crq1->condition='iclosed=0';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                foreach($interviews as $i){
                    
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);
                
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                } 
                    
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                if(in_array($ilc,$ii)){
                 $iesc++;
                 }
                 
                }
                 $crq1=new CDbCriteria();
                $crq1->condition='close=0 and ilcmid=:u and intid<>:i';
                $crq1->params=array(':u'=>$userid,':i'=>'-');
                $ki= Assignesc::model()->findAll($crq1);
                $nk=sizeof($ki);
                
                foreach($ki as $i){
                    $iesc++;
                }
                
/////////////////////enquiry///////////////////////
$eesc=0;
$crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
               $crq=new CDbCriteria();
               $crq->condition='eclosed=0';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                
                    $eesc++;
                }
                }
                
/////////////////////////////admission//////////////////////////
$adm=0;
$crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
                
               $crq=new CDbCriteria();
               $crq->condition='aclosed=0';
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                   $adm++;
               }}
               
/////////////////////////////call//////////////////////////
                $cal=0;
                $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
               }
                
               $crq=new CDbCriteria();
               $crq->condition='closed=0';
               $crq->order='date1 desc';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               foreach ($en as $i){
                
                $ilid=$i->ilcid;
                if(in_array($ilid,$ii)){
                   $cal++;
                    }
                }
                
                $crq=new CDbCriteria();
               $crq->condition='close=0 and ilcmid=:i and callid<>:c';
               $crq->params=array(':i'=>$userid,':c'=>'-');
               $en= Assignesc::model()->findAll($crq);
               $n=sizeof($en);
               foreach ($en as $i){
                $cal++;
               }
                ?>
<div class="bdy3"  >
    <br style="clear:both">        
    <div class="row">
        <div class="col-md-2">
        <div class="box box-red">
            <? 
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="query_date desc";
            $queries=  Query::model()->findAll($crq);
            $n=  sizeof($queries);
            ?>
            <a href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/query"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Query<br>Module</h4>
            <p><span style="font-size:20px !important"><?=$qesc ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-green">
           
            <a href="<?=Yii::app()->request->baseUrl."/index.php/admission/index"?>" style="text-decoration:none;color:white">
            <h4 class="text-white">Admission <br>Module</h4>
            <p><span style="font-size:20px !important"><?=$adm ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-blue">
            <? 
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            $enq= Enquiry::model()->findAll($crq);
            $n= sizeof($enq);
            ?>
            <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Enquiry<br> Module</h4>
            <p><span style="font-size:20px !important"><?=$eesc ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-pink">
           
            <a href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/interview"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Interview<br>Module</h4>
            <p><span style="font-size:20px !important"><?=$iesc ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-lav">
           
            <a href="<?=Yii::app()->request->baseUrl."/index.php/call/index"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Call<br>Module</h4>
            <p><span style="font-size:20px !important"><?=$cal ?></span> Notifications</p>
            </a>
        </div>
        </div>
    </div> 
        
  <div class="dashbrd">
      <div class="container">
          <div class="row">
          <div class="col-md-4">
              <div  id="holcal">
                  <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Calender
                      <a href="<?=Yii::app()->request->baseUrl."/index.php/holiday/index"?>" style="float:right;color:black">
                      <i class="glyphicon glyphicon-th"></i>
                      </a>
                  </h5>
                  
           <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcalenderilcm'), // URL to get event
                /////////////////////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getcalenderilcmmodal/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            //////////////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>   
            <ul class="hpal">
                                    <li><div class="bred"></div>Holidays</li>
                                    <li><div class="bora"></div>Enquiry</li>
                                    <li><div class="byel"></div>Interview</li>
                                    <li><div class="bbla"></div>Leaves</li>
                                    <li><div class="bpur"></div>Calls</li>
                                </ul>
            </div>
          </div>
              
              <div class="col-md-5 ilc-back" style="height:450px;overflow: scroll">
                  <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Current Notifications
                      <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications" style="float:right;color:black">
                      <i class="glyphicon glyphicon-th-list"></i>
                      </a>
                  </h5> <?
                  $cr=new CDbCriteria();
        $cr->condition='role=:r ';
        $cr->params=array(':r'=>'ilcmanager');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        
        $cr1=new CDbCriteria();
        $cr1->condition='ilcmid=:r ';
        $cr1->params=array(':r'=>$userid);
        $res1= ManagerIlc::model()->findAll($cr1);
        
        $ilcs=array();
        foreach($res1 as $r1){
            array_push($ilcs, $r1->ilcid);
        }
        
        $n3=  sizeof($res);
        foreach($res as $r){
            if(in_array($r->ilc,$ilcs)){
            ?>
           <span style="font-size:13px !important;margin-bottom:3%"><?=date('d-M-Y',$r->date1) ?></span>
            <p style="font-size:14px !important;">
           <?=$r->message ?></p>
                
            
            <?
        } } 
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n6=  sizeof($res);
        foreach($res as $r){
            ?>
           <span style="font-size:13px !important;margin-bottom:3%"><?=date('d-M-Y',$r->date1) ?></span>
            <p style="font-size:14px !important;">
           <?=$r->message ?></p>
                
            
            <?
        }
        ?>
              </div>
              
              <!-- <div class="col-md-3 ">
                  
                  <div class="iframe-back">
                      <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Resources
                      
                  </h5>
                      <div class="box box-white" style="margin:4%;width:90%">
                          
                          <span class="badge badge-success pull-left" style="margin:0;"><?=$ni ?></span>
                          <h4> &nbsp;|&nbsp;ILC</h4>
                      </div>
                       
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-danger pull-left" style="margin:0;"><?=$nu ?></span>
                          <h4> &nbsp;|&nbsp;Coordinators</h4>
                      </div>
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-primary pull-left" style="margin:0;"><?=$np ?></span>
                          <h4> &nbsp;|&nbsp;Partners</h4>
                      </div>
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-dark pull-left" style="margin:0;"><?=$nt ?></span>
                          <h4> &nbsp;|&nbsp;Teachers</h4>
                      </div>
                  </div>
              </div> -->
          </div>
      </div>
            
        </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
         /////////////////////////
var imgurl='<?=Yii::app()->request->baseUrl ?>/images/face.jpg';
$('img').error(function(){
        $(this).attr('src', imgurl);
});
//////////////////////
    });
</script>