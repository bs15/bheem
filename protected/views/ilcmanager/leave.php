<div class="row">
    <div class="col-md-12">
        <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
               Internal  Management > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/leave">
                    <span style="color:blue">Leave Management </span>
                </a>
            </span>
        </div> 
    </div>
   
    <div class="row">
        <div class="col-md-4">
            <div style="padding:6px">
                <h3>Add Personal Leave / block </h3>
                <div id="qer" class="alert alert-danger"></div>
                <div class="row">
                    
                    <div class="col-md-12">
                        <input type="text" class="enin" id="datepicker" placeholder="Choose Date">
                    </div>
                    <div class="col-md-12">
                        <input type="text" class="enin" id="htopic" placeholder="Specify reason for leave">
                    </div>
                    <div class="col-md-2">
                        <button type="button" onclick="sethol()" class="btn-green">SET</button>
                    </div>
                </div>
            </div>
            
            <br style="clear:both">
            <h3 class="">List of Holidays / blocked days</h3>
            <ul class="hpal">
                <li><div class="bred"></div>&nbsp;Disapproved</li>
                <li><div class="bgre"></div>&nbsp;Accepted</li>
                <li><div class="by"></div>&nbsp;Pending</li>
                </ul>
            <br>
            <div  id="hol">
                
            </div>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <br style="clear:both">
           <div  id="holcal">
                  <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Calender
                      <a href="<?=Yii::app()->request->baseUrl."/index.php/holiday/index"?>" style="float:right;color:black">
                      <i class="glyphicon glyphicon-th"></i>
                      </a>
                  </h5>
                  
           <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcalenderilcm'), // URL to get event
                /////////////////////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getcalenderilcmmodal/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            //////////////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>   
            <ul class="hpal">
                                    <li><div class="bred"></div>Holidays</li>
                                    <li><div class="bora"></div>Enquiry</li>
                                    <li><div class="byel"></div>Interview</li>
                                    <li><div class="bbla"></div>Leaves</li>
                                    <li><div class="bpur"></div>Calls</li>
                                </ul>
            </div>
        </div>
    </div>

            
        
        
<script>            
 $(document).ready(function(){
  $("#qer").hide(); 
  $("#qer2").hide();
  $("#datepicker").datepicker({
  dateFormat: "yy-mm-dd" ,minDate: 0
    });
  getq();  
 // getwhol();
 $('.fc-day-content').css('font-size','12px');
});
function getq() {
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#hol").show().html('Loading...........');
        },
                
        success:function(html){
        $("#hol").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("hol"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function sethol() {
    var htopic=$("#htopic").val();
    var hdate= $("#datepicker").val();
    
     if(htopic=="" || htopic==null)
     {
         $("#qer").show().html("Please Specify your reason");
     }
     else if(hdate=="" || hdate==null)
     {
         $("#qer").show().html("Select Leave Date");
     }
     else 
     {
        $.ajax({
         beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Setting Leave');
        },
                
        success:function(html){
        //$("#qer").empty().append(html);
       getq();
        window.location.href=window.location.href;
        
   },
              error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("sethol"); ?>',
        data:{
          htopic:htopic,hdate:hdate,
        },
    dataType:"html",
    cache:false
})//ajax
            
            
                 
                 
                 ///////////////////////////////////////////////////
     }
}    
</script>