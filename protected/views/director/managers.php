<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br style="clear:both">
            <h1>All ILC Managers</h1>
            <br style="clear:both">
            
            <div class="table table-responsive table-font table-striped">
                <table>
                    <tr>
                        <th>Photo</th>
                        <th>Name</th>
                        <th>Details</th>
                    </tr>
                
            <?
               $cru=new CDbCriteria();
                $cru->condition='role=:u';
                $cru->params=array(":u"=>'ilcmanager');
                $uinfo=  Users::model()->findAll($cru);
                
                 
                foreach($uinfo as $i){
                $uid=$i->userid; 
                $ilcid= $i->ilcid;
                
                $crup=new CDbCriteria();
                $crup->condition='userid=:u';
                $crup->params=array(":u"=>$uid);
                $up= Userphoto::model()->find($crup);
                
                    
                    
                    ?>
                    <tr>
                        <td>
                            <img src="<?= Yii::app()->request->baseUrl.'/userphoto/'.$up->uphoto ?>" class="uph">
                        </td>
                        <td>
                            <?=$i->name ?>
                        </td>
                        <td>
                            <a class="btn-dark text-white" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/managerprofile?q=<?=$uid?>" style="text-decoration:none;padding:3px">
                  View Details
                  </a> 
                        </td>
                    </tr>
           
            
                    <?
                }
                ?>
                </table>        
        </div>
    </div>
</div>
</div>