<?php

class DirectorController extends Controller
{
    public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        
        public function accessRules()
        {

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid';
    	$criteria1->params = array(':rolid'=>'director');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'updatea' actions
    		'actions'=>array('index','ilcmanager','createilcm','getilcm','delilcm','getcity','getilc','passreset'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('unit','teacher','createilcm2','getilcm2','setnoty','getnoty','checknum','nlist','uploadpost'),
    		'users'=>$modad,
    	),
          array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('queryhistory','getquery3','getchat,','getqueryilc','getquery','queryesc','queryescview','sendchat','getchat','suggestilc','searchbyilc','getqmy'),
    		'users'=>$modad,
    	),
            
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('interview','interview2','getintilc','intlist1','intlist2','getimy','searchbyilc1','suggestilc1','mcal','managers','userpass'),
                'users'=>$modad,
    	),
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        public function actionGetcity()
        { 
                if(Yii::app()->request->isPostRequest) {   
            $st1=  trim($_POST['st1']);
            $nl= new Nlist();
            $nl->getcity($st1);
              }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
         public function actionGetilc()
	{   
             if(Yii::app()->request->isPostRequest) {   
             $ci1=  trim($_POST['ci1']);
            $nl= new Nlist();
            $nl->getilc($ci1);
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
	}
        public function actionInterview() {
            $this->render('interview');
        }
        public function actionUserpass() {
            $cru=new CDbCriteria();
            $cru->condition="role='ilcmanager' or role='unit' or role='partner' or role='teacher'";
            $users=  Users::model()->findAll($cru);
            
            $this->render("userpass",array("users"=>$users));
        }
        public function actionPassreset() {
              if(Yii::app()->request->isPostRequest) { 
                  $uid=trim($_POST['uid']);
                  $pass=trim($_POST['pass']);
                  $usd=new Userdef();
                  $us=new Users();
                  $n=  intval($usd->count("userid=:u", array(":u"=>$uid)));
                  if($n==0)
                  {
                      $usd->userid=$uid;
                      $usd->defpass=$pass;
                      $usd->isNewRecord=TRUE;
                      $usd->save(FALSE);
                  }
                  else {
                      $usd->updateAll(array("defpass"=>$pass),"userid=:u", array(":u"=>$uid));
                  }
                  $us->updateAll(array("password"=>md5($pass)), "userid=:u", array(":u"=>$uid));
              } 
               else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }

        public function actionGetqueryilc(){
            if(Yii::app()->request->isPostRequest) {  
            $il=  trim($_POST['il']);
            $ci=  trim($_POST['ci']);
            $st=  trim($_POST['st']);
            
            $crq=new CDbCriteria();
               if($ci=="0" && $il=="0")
               {
                    $crq->condition="query_closed=0 and state=:s";
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($il=="0"){
                    $crq->condition="query_closed=0 and state=:s and city=:c";
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition="query_closed=0 and ilc_id=:i";
                   $crq->params=array(':i'=>$il);
               }    
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        
        if($n==0) {
            ?> <p>No queries have been raised from this ILC</p>
       <? } 
        else 
        {
           ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Address</b></td>
                        <td><b>Query Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Status</b></td>
                        <td><b>Chat</b></td>
                    </tr>
            <? $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $ilcid=$q->ilc_id;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilcid);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                if($d>72 || $d==72){
                    $qflag=1;
                
                ?>
                <tr>
                    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
                    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
                    <td><?=$q->query_topic ?></td>
                    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                    <td>    
                        <?
                        if($d>=72 && $d<96)
                        {
                        ?>
                        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</span>
                        <?
                        }
                        else if($d>96 || $d==96){
                        ?>
                        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
                        <? } ?>
                    </td>
                    <td>
                        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                    </td>
                </tr>
                <?
                }   
            }
            ?>
                </table>
           </div>
            <?
            
            if($qflag==0){
                ?><p>No queries have been escalated yet!</p><?
                //echo "<br>".$d."hours";
            }
          
        } 
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
 
        public function actionGetintilc() {
              if(Yii::app()->request->isPostRequest) {   
            $ci=  trim($_POST['ci']);
            $st=  trim($_POST['st']);
            $il= trim($_POST['il']);
            $crq=new CDbCriteria();
                  if($ci=="0" && $il=="0")
               {
                   $crq->condition='iclosed=0 and state=:s';
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($ilc1=="0"){
                    $crq->condition='iclosed=0 and state=:s and city=:c';
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition='iclosed=0 and ilcid=:i';
                    $crq->params=array(':i'=>$il);
               }
                $crq->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq);
                $n=sizeof($interviews);
                
                if($n==0) {
                    ?>
            <p>No interviews have escalated from this ILC </p>
            <?
                 }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                    </tr>
            <?
                foreach($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);  
                    
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                
                $crid=$i->userid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$crid);
                $user2= Users::model()->find($crq1);
                $uname1=$user2->name;
                $rolex=$user2->role;
                
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                } 
                if($rolex=="unit"){
                    $rolex="Coordinator";
                }
                else if($rolex=="partner"){
                    $rolex="Partner";
                }
                
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                $crq=new CDbCriteria();
                $crq->condition='ilcid=:u';
                $crq->params=array(':u'=>$ilc);
                $ilcs= ManagerIlc::model()->find($crq);
                
                
                $idate=$i->idate;
                $idate1=intval(strtotime($idate));
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>72 || $d==72)
                {
                ?>
                    <tr>
                        <td><span style="text-transform:uppercase"><?=$ilcs->ilcname ?></span></td>
                        <td><?=$uname1 ."[".$rolex."]" ?> <?=$idate ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br>
                            
                            <a style="margin-top:3%;font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? 
                            if($is->communication=='' || $is->academic_knowledge=='' || $is->attitude=='' ){ ?>
                            <button id="<?=$intid ?>" onclick="manageint(this.id)" >Assign Manager</button> 
                           <? }
                            if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { 
                             
                                ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if ($istatus=='1' && $d>=72) {
                             ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            elseif ($istatus=='0' && $d>72) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            ?>
                        </td>
                    
                    </tr>
            

<?
 }
                
            } ?>
                </table>
            </div><?
        } 
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        
    }
    
        public function actionInterview2() {
            $this->render('interview2');
        }
          public function actionManagers() {
            $this->render("managers");
        }
        public function actionMcal() {
            $man=$_REQUEST['q'];
            Yii::app()->user->setState('man',$man);   
            $this->render('mcal',array('man'=>$man));
        }
        public function actionGetimy() {
        if(Yii::app()->request->isPostRequest) {  
        $yea=  trim($_POST['yea']);
        $mon=  trim($_POST['mon']);
        $y=  intval($yea);
        $m= intval($mon);
      
      
        $crq=new CDbCriteria();
        if($y!=0 && $m==0 )
        {
           $crq->condition='year=:y and iclosed=1';
        $crq->params=array(':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
         $crq->condition='year=:y and month=:m and iclosed=1';
        $crq->params=array(':m'=>$m,':y'=>$y);
        }
      $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p> No Interviews have been conducted yet</p> <?
        }
        else 
        {
            ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Resume</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
//                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                ?>
            <tr>
                 <td><?=$i->candidatename ?>
                            
                        </td>
                        <td>
                            <a style="font-size:14px !important; padding:4px;border-radius:2px;border:1px solid black" class="btn-light" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a></td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         
                    </tr>

<?
            } ?>
                </table>
                    </div><?
        } 
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
 
        public function actionGetqmy() {
         if(Yii::app()->request->isPostRequest) {  
            $yea=  trim($_POST['yea']);
            $mon= trim($_POST['mon']);
            $y=intval($yea);
        $m=intval($mon);
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="month=:m and year=:y and ilc_approved=1 and query_closed=1";
        $crq->params=array(":m"=>$m,":y"=>$y);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?> <p style="margin-top:8%">No Queries were raised in this month !</p> <?
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <? foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
                    <?
                }
            
            ?>
                </table>
           </div>
            <?
          } 
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 
 }
        public function actionIntlist1() {
                 if(Yii::app()->request->isPostRequest) { 
            $crq1=new CDbCriteria();
                $crq1->condition='iclosed=0';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                if($n==0) {
                 }
                else 
                {
                    ?>
<div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr>
            <?
                foreach($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);     
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                
                $crid=$i->userid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$crid);
                $user2= Users::model()->find($crq1);
                $uname1=$user2->name;
                $rolex=$user2->role;
                
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                } 
                if($rolex=="unit"){
                    $rolex="Coordinator";
                }
                else if($rolex=="partner"){
                    $rolex="Partner";
                }
                
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                $crq=new CDbCriteria();
                $crq->condition='ilcid=:u';
                $crq->params=array(':u'=>$ilc);
                $ilcs= ManagerIlc::model()->find($crq);
                
                $idate=$i->idate;
                $idate1=intval(strtotime($idate));
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>72|| $d==72)
                {
                ?>
                    <tr>
                    <td><span style="text-transform:uppercase"><?=$ilcs->ilcname ?></span></td>
                        <td><?=$uname1 ."[".$rolex."]" ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     ?>
                            
                        </td>
                        <td> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if($istatus=='0' && $d<=48){
                            ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Approve" onclick="approveint(this.id)" />
                            <br>  
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Reschedule" onclick="rescheduleint(this.id)" />
                             <? } 
                            if($istatus=='1' && $d<=48){
                              ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" value="Active"  />

                            <?   
                             }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            
                            ?>
                        </td>
                    
                    </tr>
            

<?
 }
                
            } ?>
                </table>
            </div>
<?
        } 
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
    }
        
        public function actionIntlist2() {
               if(Yii::app()->request->isPostRequest) {  
                
                $crq1=new CDbCriteria();
                $crq1->condition='iclosed=1';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                if($n==0) {
                ?> <p style="text-align:center;font-size: 12px !important"> <? echo "No scheduled interviews"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
                foreach($interviews as $i){
                 $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
                 $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }   
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                $crq=new CDbCriteria();
            $crq->condition='ilcid=:u';
            $crq->params=array(':u'=>$ilc);
            $ilcs= Ilc::model()->find($crq);
                
                ?>
                <tr>
                        <td><span style="text-transform:uppercase"><?=$ilcs->ins_name ?></span></td>
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey;color:grey" class="btn-inverse" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">Resume</a>
        
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         <? } ?>
                       
                    
                    </tr>
             

<?              
            
                
        
        } 
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
	public function actionIndex()
	{
	$uid = Yii::app()->user->getState("user_id");
        $cr = new CDbCriteria();
        $cr->condition='userid=:u';
        $cr->params=array(':u'=>$uid);
        $user=Users::model()->find($cr);
        $userp=Userphoto::model()->find($cr);
	$this->render('index',array('user'=>$user,'userp'=>$userp));	
	}
        public function actionGetquery($deer) {
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        { ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                
                ?>
                    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>    
        <?
        if($d>=24)
        {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <?
        }
        ?>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active Query":"Closed Query" ?></span>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
                    <?
                
            }?>
                </table>
            </div>
          <?
        }
 }
 
        public function actionQueryhistory() {
            $this->render('queryhistory');
        }
        
        public function actionGetquery3() {
        if(Yii::app()->request->isPostRequest) {  

        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="ilc_approved=1 and query_closed=1";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
            ?>
           <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                
                ?>
                    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr><?
                
            }?>
                </table>
           </div>
          <?
        } 
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }

 }
 
        public function actionSearchbyilc(){
         if(Yii::app()->request->isPostRequest) { 
            $silc=  trim($_POST['silc']);
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ilc_id=:s and query_closed=0";
        $crq->params=array(':s'=>$silc);
        $crq->order="query_date desc";
        
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
          ?>
             <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr> 
                <?
            $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                $qdate=$q->query_date;
                
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
              if($d>72 || $d==72){
                    $qflag=1;
                
                ?>
                    
                 <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>    
        <?
        if($d>=72 && $d<96)
        {
        ?>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</span>
        <?
        }
        else if($d>96 || $d==96){
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <? } ?>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr><?
                }   
            }
            ?>
                </table>
             </div>
                <?
            if($qflag==0){
                ?><p>No escalated queries</p><?
                //echo "<br>".$d."hours";
            }
          
        } 
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionSearchbyilc1(){ 
              if(Yii::app()->request->isPostRequest) {  
            $silc=  trim($_POST['silc']);
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ilcid=:s and iclosed=0";
        $crq->params=array(':s'=>$silc);
        $crq->order="idate1 desc";
        $queries=  Interview::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No escalated interviews";
        }
        else 
        { ?>
            <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr>
                    <?
                foreach ($queries as $q) {
                
                $intid=$q->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
                $qdate=$q->idate1;
                $ilc=$q->ilcid;
                
                $role1="";
                //$role="";
                $inid=$q->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role=:r';
                $cru->params=array(":u"=>$silc,":r"=>'unit');
                $uinfo=  Users::model()->find($cru);
                $unit=$uinfo->name;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$silc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>72){
                    
                //echo $d;
                ?>
                 <tr>
                     
                        <td><span style="text-transform:uppercase"><?=$ilcn ?></span></td>
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$q->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($q->idate)) ?>
                            <br>
                        <?=$q->ihour.":".$q->imin ?>
                        </td>
                        <td>
                           <?=$q->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     ?>
                            
                        </td>
                        <td> <?
                            $istatus=$q->istatus;
                            $idate=$q->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if($istatus=='0' && $d<=48){
                            ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Approve" onclick="approveint(this.id)" />
                            <br>  
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Reschedule" onclick="rescheduleint(this.id)" />
                             <? } 
                            if($istatus=='1' && $d<=48){
                              ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" value="Active"  />

                            <?   
                             }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            
                            ?>
                        </td>
                    
                    </tr>
               
            <?
           } 
            
            
            
          }
          ?>
              </div>
                  </table><?
        } 
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        public function actionQueryesc() {
            
           $cr1=new CDbCriteria();
           $res1= Ilc::model()->findAll($cr1);
            
             
            $this->render('queryesc',array('res'=>$res1));
        }
       //////////////////////upload post ///////////////////
            public function actionUploadpost() {
        $model = new QueryChat();
        $userid=Yii::app()->user->getState('user_id');
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
       // $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
                $model->queryid=trim($_POST['qid']);
                $model -> document = $fileName;
	
                
                $model->chatid=  uniqid();
         $model->senderid=$userid;
         $model->recid='-';
         $model->message='-';
         $model->date1=  strtotime(date('Y-m-d H:i:s'));
         
	if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../chatdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
            } 
            
        public function actionSuggestilc(){ 
             if(Yii::app()->request->isPostRequest) {  
            $silc1=  trim($_POST['silc1']);
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ins_name=:s or ins_name like :s1 or ins_name like :s2";
        $crq->params=array(':s'=>$silc1,':s1'=>$silc.'%',':s2'=>'%'.$silc.'%');
        $queries=  Ilc::model()->findAll($crq);
        ?>
        <ul class="slist">
            <? foreach($queries as $q){
                ?><li name="<?=$q->ins_name ?>" id="<?=$q->ilcid ?>-<?=$q->ins_name ?>" onclick="setilc1(this.id)">
                        <?=$q->ins_name ?>
                </li><?
            } ?>
        </ul>
        <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }

        }
        
        public function actionSuggestilc1(){
            if(Yii::app()->request->isPostRequest) {    
            $silc1=  trim($_POST['silc1']);
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ins_name=:s or ins_name like :s1 or ins_name like :s2";
        $crq->params=array(':s'=>$silc1,':s1'=>$silc.'%',':s2'=>'%'.$silc.'%');
        $queries=  Ilc::model()->findAll($crq);
        ?>
        <ul class="slist">
            <? foreach($queries as $q){
                ?><li name="<?=$q->ins_name ?>" id="<?=$q->ilcid ?>-<?=$q->ins_name ?>" onclick="setilc1(this.id)">
                        <?=$q->ins_name ?>
                </li><?
            } ?>
        </ul>
        <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        public function actionQueryescview(){
       if(Yii::app()->request->isPostRequest) {  

            $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="query_closed=0";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
            ?> <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr> <?
            $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                if($nc>0){
                    $qdate=$qchat->date1;
                }
                else{
                    $qdate=$q->query_date;
                }
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                //echo $d;
                if($d>72 || $d==72){
                    $qflag=1;
                
                ?>
                    
                 <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>    
        <?
        if($d>=72 && $d<96)
        {
        ?>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</span>
        <?
        }
        else if($d>96 || $d==96){
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <? } ?>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr><?
                }   
            }
            ?>
            </table>
        </div>
            <?
            if($qflag==0){
                ?><p>No escalated queries</p><?
                //echo "<br>".$d."hours";
            }
          
        } 
       }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 
 }
 
        public function actionGetchat() {
             if(Yii::app()->request->isPostRequest) {  
            $qid=  trim($_POST['qid']);
            $uid=  trim($_POST['uid']);
    $cr=new CDbCriteria();
    $cr->condition='queryid=:q';
    $cr->params=array(":q"=>$qid);
    $cr->order='date1 asc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       echo "no chats";
   }
 else {
     $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
     ?>
                <br>
                <p style="font-size:16px !important" ><b>Topic:</b> <?=$topic ?>  </p>
        <div id="chatui"> <?
        
       foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
         if($uinf->role=='unit'){
           ?>
                <div class="alert alert-success talk " style="float:left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='partner')
         {
           ?>
            <div class="alert alert-warning talk" style="float:left">
    <b style="font-size:14px !important"><?=$uinf->name ?>[Unit Partner]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
           <?  
         }
          if($uinf->role=='academic')
         {
           ?>
            <div class="alert alert-primary talk" style="float:left">
    <b style="font-size:14px !important"><?=$uinf->name ?>[Academic Head]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
           <?  
         }
          if($uinf->role=='director')
         {
           ?>
            <div class="alert alert-dark talk" style="float:right">
    <b style="font-size:14px !important"><?=$uinf->name ?>[Director]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
        </div>  
           <?  
         }
         
         
       } ?>
        </div> <?
   } 
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
  }
    


  public function actionSendchat() {
 if(Yii::app()->request->isPostRequest) {  
      $qid=  trim($_POST['qid']);
      $msg=  trim($_POST['msg']);
      $uid=Yii::app()->user->getState("user_id");
  
  $cru=new CDbCriteria();
        $cru->condition='userid=:r';
        $cru->params=array(':r'=>$uid);
        $resu=  Users::model()->find($cru);
        $ilcid=$resu->ilcid;
        
        
        $crt=new CDbCriteria();
        $crt->condition='qid=:r';
        $crt->params=array(':r'=>$qid);
        $rest=  Query::model()->find($crt);
        $qtopic=$rest->query_topic;
        $quser=$rest->userid;
        
        $crt=new CDbCriteria();
        $crt->condition='userid=:r';
        $crt->params=array(':r'=>$quser);
        $rest= Users::model()->find($crt);
        $uname=$rest->username;
        $name=$rest->name;
        
        
    $qc=new QueryChat();
    $qc->queryid=$qid;
    $qc->chatid=  uniqid();
    $qc->senderid=$uid;
    $qc->recid='-';
    $qc->message=$msg;
    $qc->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qc->isNewRecord=true;
    $qc->save(FALSE);
        
    $mst="There is a reply for the query '".substr($qtopic, 0, 100)."..'";
    $qn=new Notifications();
    $qn->queryid=$qid;
    $qn->notid=  uniqid();
    $qn->role="all";
    $qn->ilc=$ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Query Chat Reply";
            $msg="Dear $name,<br>
                Director has replied to your query <b>".substr($qtopic, 0, 100)."</b><br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }

 
   public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
          $nl->setnoty1();
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
        }
          public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {  
  
              $nl= new Nlist();
            $nl->checknum1();
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
        
   public function actionNlist() {
         if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
       $nl->nlist1();
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
