<?php

class AdmissionController extends Controller
{
	public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        public function accessRules()
        {

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolida or role=:rolidd or role=:rolidi or role=:rolidu or role=:rolidp';
    	$criteria1->params = array(':rolida'=>'academic',':rolidd'=>'director',':rolidi'=>'ilcmanager',':rolidu'=>'unit',':rolidp'=>'partner');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
        
        return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','archive','setnoty','checknum','nlist','adm2','getamy','getcity','getilc','getadmilc'),
    		'users'=>$modad,
    	),
        
        array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('adm','setai','updateai','uploadphoto','uploadphoto2','viewai','setfeed','updatefeed','closeadm','admar'),
    		'users'=>$modad,
    	),
            
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        public function actionGetcity()
        {
            if(Yii::app()->request->isPostRequest) {
            $st1=trim($_POST['st1']);
            $nl= new Nlist();
            $nl->getcity($st1);
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
         public function actionGetilc()
	{
            if(Yii::app()->request->isPostRequest) {
           $ci1=trim($_POST['ci1']);
             $nl= new Nlist();
            $nl->getilc($ci1);
            }
            else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
	}
	public function actionIndex()
	{
            $cr1=new CDbCriteria();
            $res1= Ilc::model()->findAll($cr1);
            $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
     $crt=new CDbCriteria();
        $crt->condition='ilcid=:r';
        $crt->params=array(':r'=>$ilcid);
        $rest= ManagerIlc::model()->find($crt);
        $ilcmid=$rest->ilcmid;       
           
    //$this->render('interview',array('man'=>$ilcmid));
        if($rolid=='unit' || $rolid=='partner'){
            Yii::app()->user->setState('man',$ilcmid);
            }
            $this->render('index',array('res'=>$res1,'man'=>$ilcmid));
	}
        
        public function actionArchive()
	{
            $cr1=new CDbCriteria();
            $res1= Ilc::model()->findAll($cr1);
            $this->render('archive',array('res'=>$res1));
	}
        
        public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
       
        public function actionAdm(){
            if(Yii::app()->request->isPostRequest) {
           
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit'  || $rolid=='partner')
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and userid=:uu and aclosed=0';
               $crq->params=array(':u'=>$ilcid,':uu'=>$userid);
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No  Admissions"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                <?
                foreach ($en as $i){
                    
                //$enqid=$i->enqid;
                $parid=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$parid);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                           <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>Receipt</u></a>
                         <? } ?> 
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? 
                            if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;margin-bottom:5%" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Add Details" onclick="setai(this.id)" />
                        <br>
                        <? if($i->aclosed==0 && $i->ilcfeed!='') { ?>
                            &nbsp;
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Close ticket" onclick="closeadm(this.id)" />
                            <? } ?>
                        </td>
                    </tr>
        <?
        }?>
                </table>
                </div>
            <?
       }
     }
     
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
                
               $crq=new CDbCriteria();
               $crq->condition='aclosed=0';
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Admissions"; ?></p> <?
                }
                else 
                {?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                    
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                ?>
                    <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr><? }
               }?>
                </table>
                    </div><?
            }
            
        }
        
           if($rolid=='academic' || $rolid=='director'){
            $crq=new CDbCriteria();
               $crq->condition='aclosed=0';
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p > 
                Currently no Admissions exist for this ILC</p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr><? 
                
               } ?>
                   </table>
                       </div><?
            }
          }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
    }
    
        public function actionGetadmilc(){
            if(Yii::app()->request->isPostRequest) {
           $st=trim($_POST['st']);
           $ci=trim($_POST['ci']);
           $ilc1=trim($_POST['ilc1']);
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit'  || $rolid=='partner')
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and userid=:uu and aclosed=0';
               $crq->params=array(':u'=>$ilcid,':uu'=>$userid);
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No  Admissions"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                <?
                foreach ($en as $i){
                    
                //$enqid=$i->enqid;
                $parid=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$parid);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                           <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>Receipt</u></a>
                         <? } ?> 
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? 
                            if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;margin-bottom:5%" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Add Details" onclick="setai(this.id)" />
                        <br>
                        <? if($i->aclosed==0 && $i->ilcfeed!='') { ?>
                            &nbsp;
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Close ticket" onclick="closeadm(this.id)" />
                            <? } ?>
                        </td>
                    </tr>
        <?
        }?>
                </table>
                </div>
            <?
       }
     }
     
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
                
               $crq=new CDbCriteria();
               $crq->condition='aclosed=0 and ilcid=:i';
               $crq->params=array(':i'=>$ilc1);
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Admissions"; ?></p> <?
                }
                else 
                {?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                    
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                ?>
                    <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr><? }
               }?>
                </table>
                    </div><?
            }
            
        }
        
           if($rolid=='academic' || $rolid=='director'){
           
               $crq=new CDbCriteria();
               if($ci=="0" && $ilc1=="0")
               {
                   $crq->condition='aclosed=0 and state=:s';
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($ilc1=="0"){
                    $crq->condition='aclosed=0 and state=:s and city=:c';
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition='aclosed=0 and ilcid=:i';
                    $crq->params=array(':i'=>$ilc1);
               }
               
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p > 
                Currently no Admissions exist for this ILC</p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr><? 
                
               } ?>
                   </table>
                       </div><?
            }
        }
            }
            else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
    }
    
    public function actionGetamy() {
      if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);

        $y=  intval($yea);
      $m= intval($mon);
      $userid=Yii::app()->user->getState('user_id');
      $ilcid=Yii::app()->user->getState('ilc_id');
      $rolid=Yii::app()->user->getState('rolid');
      
      
      if($rolid=='director'||$rolid=='academic'){
        $crq=new CDbCriteria();
        if($y!=0 && $m==0)
        {
            $crq->condition='year=:y and aclosed=1';
            $crq->params=array(':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
            $crq->condition='year=:y and month=:m and aclosed=1';
            $crq->params=array(':m'=>$m,':y'=>$y);
        }
        $crq->order='date1 desc';
        $interviews= Admission::model()->findAll($crq);
        $n=sizeof($interviews);
      }
      
      if($rolid=='ilcmanager'){
        $crqm=new CDbCriteria();
        $crqm->condition='ilcmid=:u';
        $crqm->params=array(':u'=>$userid);
        $ilcs= ManagerIlc::model()->findAll($crqm);
        $ii=array();
        foreach($ilcs as $il){
             array_push($ii,$il->ilcid);
         }
          
        $crq=new CDbCriteria();
        if($y!=0 && $m==0)
        {
            $crq->condition='year=:y and aclosed=1';
            $crq->params=array(':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
            $crq->condition='year=:y and month=:m and aclosed=1';
            $crq->params=array(':m'=>$m,':y'=>$y);
        }
        $crq->order='date1 desc';
        $interviews= Admission::model()->findAll($crq);
        $n=sizeof($interviews);
      }
      if($rolid=='unit' || $rolid=='partner'){
          $crq=new CDbCriteria();
           if($y!=0 && $m==0)
            {
                $crq->condition='userid=:u and year=:y and aclosed=1';
                $crq->params=array(':u'=>$userid,':y'=>$y);   
            }
        else if($y!=0 && $m!=0)
        {
         $crq->condition='userid=:u and year=:y and month=:m and aclosed=1';
        $crq->params=array(':u'=>$userid,':m'=>$m,':y'=>$y);
        }
        $crq->order='date1 desc';
        $interviews= Admission::model()->findAll($crq);
        $n=sizeof($interviews);
      }
        
        if($n==0) {
            ?> <p> No Admissions have been conducted yet</p> <?
        }
        else 
        {
        ?>
        <div class="table table-striped table-font">  
        <table>
            <tr class="bold">
                <td><b>Photo</b></td>
                <td><b>Child Info</b></td>
                <td><b>Parent Info</b></td>
                <td><b>Contact</b></td>
                <td><b>Call Feedback</b></td>
                <td><b>More Info</b></td>

            </tr>  <?
                foreach ($interviews as $i){
                    
                //$enqid=$i->enqid;
                $parid=$i->parentid;
                $ilcid=$i->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$parid);
                $ep=EnquiryParent::model()->find($cri); 
                
                if($rolid=='ilcmanager'){
                    if(in_array($ilcid,$ii)){
                        ?>
            <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr>
            <?
                    }
                }
                else{
                ?>
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr>

<?
                } } ?>
                </table>
                    </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
       public function actionAdm2(){
           if(Yii::app()->request->isPostRequest) {
           $ilcid1=trim($_POST['ilcid1']);
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit'  || $rolid=='partner')
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and userid=:uu and aclosed=0';
               $crq->params=array(':u'=>$ilcid,':uu'=>$userid);
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                   ?> <p >Currently no Admissions exist for this ILC</p> <?
                }
                else 
                {
                
                foreach ($en as $i){
                    
                //$enqid=$i->enqid;
                $parid=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$parid);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                <div class="card">
                    <div class="card-header">
                        <strong>Created on :</strong>&nbsp;<?=date('d-M-y',$i->date1) ?>
                        <span style="float:right">
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Add Details" onclick="setai(this.id)" />
                            <? if($i->aclosed==0 && $i->ilcfeed!='') { ?>
                            &nbsp;<input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Close" onclick="closeadm(this.id)" />
                            <? } ?>
                        </span>
                    </div>
                    <div class="card-body">
                        <p>
                           <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                            <?=$ep->child_name ?>, &nbsp;&nbsp;
                            <span style="text-transform:capitalize"><?=$ep->gender ?></span>&nbsp;
                            born on <?=$ep->child_dob ?> </p> 
                        <p>C/O <?=$ep->parent_f_name ?> </p>
                        <p style="font-size:14px;"><?=$ep->parent_add.', '.$ep->city ?></p>
                        <p style="font-size:14px;">Contact
                            <span style="color:grey;"><?=$ep->parent_f_email ?>&nbsp;|&nbsp;<?=$ep->parent_f_phone ?></span>
                        </p>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Admission Receipt</u></a>
                         <? } ?>
                        <? if($i->ilcfeed!=''){ ?>
                        <hr>
                        <p><strong>Manager's Feedback: </strong>
                           <?=$i->ilcfeed ?>
                        </p>
                        <? } ?>
                           
                    </div>
                </div>
            <br>
        <?
        }
       }
     }
     
           if($rolid=='ilcmanager')
           {
                $crq=new CDbCriteria();
               $crq->condition='aclosed=0 and ilcid=:i';
               $crq->params=array(":i"=>$ilcid1);
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Admissions"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
//                if(in_array($ilid,$ii)){
                ?>
               <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr><? } ?>
                    </table>
                        </div>
                            <?
             //  }
            }
            
        }
        
           if($rolid=='academic' || $rolid=='director'){
            $crq=new CDbCriteria();
               $crq->condition='aclosed=0 and ilcid=:i';
               $crq->params=array(":i"=>$ilcid1);
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                   ?> <p >Currently no Admissions exist for this ILC</p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr><? 
                
               }?>
                </table>
             </div>
             <?
            }
        }
           }
           else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
    }
    
       public function actionAdmar(){
           if(Yii::app()->request->isPostRequest) {
         
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit'  || $rolid=='partner')
           {
               $crq=new CDbCriteria();
               $crq->condition='aclosed=1 and userid=:u';
               $crq->params=array(":u"=>$userid);
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No  Admissions"; ?></p> <?
                }
                else 
                {
                ?>
                    <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  <?
                foreach ($en as $i){
                    
                //$enqid=$i->enqid;
                $parid=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$parid);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr>
        <?
        }
       }
     }
     
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
                
               $crq=new CDbCriteria();
               $crq->condition='aclosed=1';
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Admissions"; ?></p> <?
                }
                else 
                { ?>
                    <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                ?>
               <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr>
                <? }
               }
               ?>
                </table>
              </div>
               <?
            }
            
        }
        
           if($rolid=='academic' || $rolid=='director'){
            $crq=new CDbCriteria();
               $crq->condition='aclosed=1';
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Admissions"; ?></p> <?
                }
                else 
                {
                    ?>
                    <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Photo</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Call Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                
                <tr>
                        <td>
                            <? if($i->photo!="")
                           { ?>
                           <img src="<?= Yii::app()->request->baseUrl.'/studentphoto/'.$i->photo ?>" class="admpic">&nbsp;
                           <? } ?>
                        </td>
                        <td><?=$ep->child_name ?>,<br>
                        <?=$ep->gender ?> born on <?=$ep->child_dob ?> 
                        </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_add.', '.$ep->city ?>
                        </td>
                        <td><span class="color:lightblue" >
                            <?=$ep->parent_f_email ?></span><br>
                            <?=$ep->parent_f_phone ?>
                        </td>
                        <td>
                            <? if($i->ilcfeed=='') { ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="Call Feedback" onclick="setfeed(this.id)" />
                            <? }
                            else if($i->ilcfeed!=''){ ?>
                            <p><?=$i->ilcfeed ?></p> 
                           <? }
                            ?>
                        </td>
                        <td>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->adid ?>" name="<?=$i->adid ?>" value="View Details" onclick="viewai(this.id)" />
                        <br>
                        <? if($i->recphoto!=''){ ?>
                        <a target="_blank" style="color:black" href="<?= Yii::app()->request->baseUrl.'/recphoto/'.$i->recphoto ?>" ><u>View Receipt</u></a>
                         <? } ?> 
                        </td>
                    </tr><? 
                
                }?></table>
                    </div> <?
            }
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
    }

       public function actionSetai(){
        if(Yii::app()->request->isPostRequest) {
        $adid=trim($_POST['adid']);
        $rolid=Yii::app()->user->getState('rolid');    
        
        $cru=new CDbCriteria(); 
        $cru->condition='adid=:u';
        $cru->params=array(":u"=>$adid);
        $en= Admission::model()->find($cru);
        $parid=$en->parentid;
        
        $crp=new CDbCriteria(); 
        $crp->condition='parentid=:u';
        $crp->params=array(":u"=>$parid);
        $pa= EnquiryParent::model()->find($crp);
        
        ?>
        <input type="hidden" id="adid" value="<?=$en->adid ?>">
        <input type="hidden" id="parid" value="<?=$parid ?>">
        <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important;margin-bottom:4px;">ADMIN SECTION</strong>
         </div>
                
         <div class="row">
             <div class="col-md-6">
                 <div style="display: none;">
                     <input type="file" class="enin" placeholder="Choose Photo" id="file" onchange="upfile()"> 
                 </div>
                 <button type="button" class="enin" onclick="browsefile()">Upload Child's Photo</button>
             </div>
              <div class="col-md-6">
                 <div style="display: none;">
                     <input type="file" class="enin" placeholder="Choose Photo" id="file2" onchange="upfile2()"> 
                 </div>
                 <button type="button" class="enin" onclick="browsefile2()">Attach Admission Receipt</button>
             </div>
             
             
             
             <div class="col-md-6">
                 <input type="text" class="enin" id="recno" value="<?=($en->recno=='')?'':$en->recno ?>" placeholder="Receipt No." >
             </div>
             
              <div class="col-md-6">
                 <input type="text" class="enin" id="enrno" value="<?=($en->enrno=='')?'':$en->enrno ?>" placeholder="Enrolment No." >
             </div>
             <div class="col-md-6">
                 <input class="enin" id="datepicker" value="<?=($en->recdate=='' || $en->recdate=='0000-00-00')?'Receipt Date':$en->recdate ?>" placeholder="Receipt Date">
             </div>
             
            
             
             <div class="col-md-6">
                 <input class="enin" id="datepicker1" value="<?=($en->enrdate=='' || $en->enrdate=='0000-00-00')?'Enrolment Date':$en->enrdate ?>" placeholder="Enrolment Date">
             </div>
             <div class="col-md-6">
                 <select id="adtype" class="enin">
                     <option value="<?=($en->adtype=='')?'Admission Type':$en->adtype ?>"><?=($en->adtype=='')?'Admission Type':$en->adtype ?></option>
                     <option value="Preschool">Pre School</option>
                     <option value="Afterschool">After School</option>
                 </select>  
             </div>
             
         </div>
                <br style="clear:both">
         <div class="row"> 
             
             <strong style="margin-left:3%;font-size:16px !important;margin-bottom:4px;">PERSONAL DETAILS</strong>
         </div>
         <div class="row">
            <div class="col-md-6"> 
                
                 <input type="text" readonly class="enin"  value="<?=$pa->child_name ?>">  
            </div>
             <div class="col-md-6"> 
                <input type="text" class="enin" id="mto" value="<?=($en->m_tongue=='')?'':$en->m_tongue ?>" placeholder="Mother Tongue">  
            </div>
            
            <div class="col-md-6"> 
                <input type="text" readonly class="enin" value="<?=$pa->parent_f_name ?>" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" id="mname" class="enin" value="<?=($pa->parent_m_name=='')?'':$pa->parent_m_name ?>"placeholder="Mother's Name">  
            </div>
             
            <div class="col-md-6"> 
                <input type="text" id="focc" class="enin" value="<?=($pa->f_occupation=='')?'':$pa->f_occupation ?>" placeholder="Father's Occupation"  >  
            </div>
             
            <div class="col-md-6"> 
                <input type="text" id="mocc" class="enin" value="<?=($en->m_occupation=='')?'':$en->m_occupation ?>" placeholder="Mother's Occupation" >  
            </div>
             
            <div class="col-md-6"> 
                <input type="text" id="foa" class="enin" value="<?=($en->f_ofc_add=='')?'':$en->f_ofc_add ?>" placeholder="Office Address" >  
            </div> 
             
             <div class="col-md-6"> 
                <input type="text" id="moa" class="enin" value="<?=($en->m_ofc_add=='')?'':$en->m_ofc_add ?>" placeholder="Office Address" >  
            </div> 
            
             <div class="col-md-6"> 
                <input type="text" readonly class="enin" value="<?=$pa->parent_f_phone ?>" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" id="mphone" class="enin" value="<?=($pa->parent_m_phone=='')?'':$pa->parent_m_phone ?>" placeholder="Mother's Phone" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" readonly class="enin" value="<?=$pa->parent_f_email ?>" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" id="memail" class="enin" value="<?=($pa->parent_m_email=='')?'':$pa->parent_m_email ?>" placeholder="Mother's Email" >  
            </div>
        </div>
               
         <br style="clear:both">
         <div class="row"> 
             
             <strong style="margin-left:3%;font-size:16px !important;margin-bottom:4px;">MEDICAL DETAILS</strong>
         </div>
         <div class="row">
             <div class="col-md-12">
                 <span style="margin-left:1%;font-size:16px !important">
                     Vaccination done</span>
                 <div id="hhear">
                 <input type="hidden" value="<?$en->vaccine ?>" id="how">    
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Bcg">
                    <span style="font-size:16px!important">Bcg</span>&nbsp;
                </div>
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Polio">
                    <span style="font-size:16px!important">Polio</span>&nbsp;
                </div>
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Triple Antigen">
                    <span style="font-size:16px!important">Triple Antigen</span>&nbsp;
                </div>
             </div> 
         </div>
         <div class="col-md-6"> 
                <input type="text" class="enin" id="allergy" value="<?=($en->allergy=='')?'':$en->allergy ?>" placeholder="Other allergies" >  
         </div>
         <div class="col-md-6"> 
                <input type="text" class="enin" id="blood" value="<?=($en->blood=='')?'':$en->blood ?>" placeholder="Blood Group" >  
         </div>
         <div class="col-md-12"> 
                <input type="text" class="enin" id="omed" value="<?=($en->other_med=='')?'':$en->other_med ?>" placeholder="Any other Medical problems?" >  
         </div>
         </div>
         
         <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updateai()"/>
         </div>
         
            
        <?
        }
        else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
       public function actionSetfeed(){
           if(Yii::app()->request->isPostRequest) {
           $adid=trim($_POST['adid']);
          $cru=new CDbCriteria(); 
        $cru->condition='adid=:u';
        $cru->params=array(":u"=>$adid);
        $en= Admission::model()->find($cru);
        
        ?>
        <input type="hidden" id="adid2" value="<?=$en->adid ?>">
         <div class="col-md-12">
             <textarea class="form-control" style="width: 90%" id="feed" placeholder="Give your feedback after talking to the parents "><?=($en->ilcfeed=='')?'':$en->ilcfeed ?></textarea> 
             <br>   </div>
        <div class="input-group">
            <input type="button" class="btn btn-success" value="Send Feedback" onclick="updatefeed()"/>
         </div>
         
            
        <?
           }
           else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        } 
        
        public function actionViewai(){
        if(Yii::app()->request->isPostRequest) {
        $adid=trim($_POST['adid']);
        $rolid=Yii::app()->user->getState('rolid');    
        
        $cru=new CDbCriteria(); 
        $cru->condition='adid=:u';
        $cru->params=array(":u"=>$adid);
        $en= Admission::model()->find($cru);
        $parid=$en->parentid;
        
        $crp=new CDbCriteria(); 
        $crp->condition='parentid=:u';
        $crp->params=array(":u"=>$parid);
        $pa= EnquiryParent::model()->find($crp);
        
        ?>
        <input type="hidden" id="adid" value="<?=$en->adid ?>">
        <input type="hidden" id="parid" value="<?=$parid ?>">
        <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important;margin-bottom:4px;">ADMIN SECTION</strong>
         </div>
                
         <div class="row">
             <div class="col-md-6">
                 <div style="display: none;">
                     <input type="file" class="enin" placeholder="Choose Photo" id="file" onchange="upfile()"> 
                 </div>
                 <button type="button" class="enin" onclick="browsefile()">Upload Photo</button>
             </div>
             <div class="col-md-6">
                 <select id="adtype" class="enin">
                     <option value="<?=($en->adtype=='')?'Admission Type':$en->adtype ?>"><?=($en->adtype=='')?'Admission Type':$en->adtype ?></option>
                     <option value="Preschool">Pre School</option>
                     <option value="Afterschool">After School</option>
                 </select>  
             </div>
             
             <div class="col-md-6">
                 <input type="text" class="enin" id="recno" value="<?=($en->recno=='')?'':$en->recno ?>" placeholder="Receipt No." >
             </div>
             
             <div class="col-md-6">
                 <input class="enin" id="datepicker" value="<?=($en->recdate=='' || $en->recdate=='0000-00-00')?'Receipt Date':$en->recdate ?>" placeholder="Receipt Date">
             </div>
             
             <div class="col-md-6">
                 <input type="text" class="enin" id="enrno" value="<?=($en->enrno=='')?'':$en->enrno ?>" placeholder="Enrolment No." >
             </div>
             
             <div class="col-md-6">
                 <input class="enin" id="datepicker1" value="<?=($en->enrdate=='' || $en->enrdate=='0000-00-00')?'Enrolment Date':$en->enrdate ?>" placeholder="Enrolment Date">
             </div>
             
         </div>
                <br style="clear:both">
         <div class="row"> 
             
             <strong style="margin-left:3%;font-size:16px !important;margin-bottom:4px;">PERSONAL DETAILS</strong>
         </div>
         <div class="row">
            <div class="col-md-6"> 
                
                 <input type="text" readonly class="enin"  value="<?=$pa->child_name ?>">  
            </div>
             <div class="col-md-6"> 
                <input type="text" class="enin" id="mto" value="<?=($en->m_tongue=='')?'':$en->m_tongue ?>" placeholder="Mother Tongue">  
            </div>
            
            <div class="col-md-6"> 
                <input type="text" readonly class="enin" value="<?=$pa->parent_f_name ?>" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" id="mname" class="enin" value="<?=($pa->parent_m_name=='')?'':$pa->parent_m_name ?>"placeholder="Mother's Name">  
            </div>
             
            <div class="col-md-6"> 
                <input type="text" id="focc" class="enin" value="<?=($pa->f_occupation=='')?'':$pa->f_occupation ?>" placeholder="Father's Occupation"  >  
            </div>
             
            <div class="col-md-6"> 
                <input type="text" id="mocc" class="enin" value="<?=($en->m_occupation=='')?'':$en->m_occupation ?>" placeholder="Mother's Occupation" >  
            </div>
             
            <div class="col-md-6"> 
                <input type="text" id="foa" class="enin" value="<?=($en->f_ofc_add=='')?'':$en->f_ofc_add ?>" placeholder="Office Address" >  
            </div> 
             
             <div class="col-md-6"> 
                <input type="text" id="moa" class="enin" value="<?=($en->m_ofc_add=='')?'':$en->m_ofc_add ?>" placeholder="Office Address" >  
            </div> 
            
             <div class="col-md-6"> 
                <input type="text" readonly class="enin" value="<?=$pa->parent_f_phone ?>" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" id="mphone" class="enin" value="<?=($pa->parent_m_phone=='')?'':$pa->parent_m_phone ?>" placeholder="Mother's Phone" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" readonly class="enin" value="<?=$pa->parent_f_email ?>" >  
            </div>
             
             <div class="col-md-6"> 
                <input type="text" id="memail" class="enin" value="<?=($pa->parent_m_email=='')?'':$pa->parent_m_email ?>" placeholder="Mother's Email" >  
            </div>
        </div>
               
         <br style="clear:both">
         <div class="row"> 
             
             <strong style="margin-left:3%;font-size:16px !important;margin-bottom:4px;">MEDICAL DETAILS</strong>
         </div>
         <div class="row">
             <div class="col-md-12">
                 <span style="margin-left:1%;font-size:16px !important">
                     Vaccination done</span>
                 <div id="hhear">
                 <input type="hidden" value="<?$en->vaccine ?>" id="how">    
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Bcg">
                    <span style="font-size:16px!important">Bcg</span>&nbsp;
                </div>
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Polio">
                    <span style="font-size:16px!important">Polio</span>&nbsp;
                </div>
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Triple Antigen">
                    <span style="font-size:16px!important">Triple Antigen</span>&nbsp;
                </div>
             </div> 
         </div>
         <div class="col-md-6"> 
                <input type="text" class="enin" id="allergy" value="<?=($en->allergy=='')?'':$en->allergy ?>" placeholder="Other allergies" >  
         </div>
         <div class="col-md-6"> 
                <input type="text" class="enin" id="blood" value="<?=($en->blood=='')?'':$en->blood ?>" placeholder="Blood Group" >  
         </div>
         <div class="col-md-12"> 
                <input type="text" class="enin" id="omed" value="<?=($en->other_med=='')?'':$en->other_med ?>" placeholder="Any other Medical problems?" >  
         </div>
         </div>
         
         
        <?
        }
        else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUpdateai()
        {
            if(Yii::app()->request->isPostRequest) {
            $adid=trim($_POST['adid']);
            $parid=trim($_POST['parid']);
            $recno=trim($_POST['recno']);
            $enrno=trim($_POST['enrno']);
            $recdate=trim($_POST['recdate']);
            $enrdate=trim($_POST['enrdate']);
            $mto=trim($_POST['mto']);
            $mname=trim($_POST['mname']);
            $focc=trim($_POST['focc']);
            $mocc=trim($_POST['mocc']);
            $foa=trim($_POST['foa']);
            $moa=trim($_POST['moa']);
            $hhear=trim($_POST['hhear']);
            $mphone=trim($_POST['mphone']);
            $memail=trim($_POST['memail']);
            $allergy=trim($_POST['allergy']);
            $blood=trim($_POST['blood']);
            $omed=trim($_POST['omed']);
            $adtype1=trim($_POST['adtype1']);
           $qq=new EnquiryParent();
            $qq->updateAll(array(
                'f_occupation'=>$focc,
                'parent_m_name'=>$mname,
                'parent_m_email'=>$memail,
                'parent_m_phone'=>$mphone,
                 ),
                "parentid=:q", array(':q'=>$parid));
            
            $qq=new Admission();
            $qq->updateAll(array(
                'adtype'=>$adtype1,
                'recno'=>$recno,
                'enrno'=>$enrno,
                'recdate'=>$recdate,
                'enrdate'=>$enrdate,
                'm_tongue'=>$mto,
                'm_name'=>$mname,
                'm_occupation'=>$mocc,
                'f_ofc_add'=>$foa,
                'm_ofc_add'=>$moa,
                'vaccine'=>$hhear,
                'allergy'=>$allergy,
                'blood'=>$blood,
                'other_med'=>$omed,
                ),
                "adid=:q", array(':q'=>$adid));
            }
            else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUpdatefeed()        
            {
            if(Yii::app()->request->isPostRequest) {
           $feed=trim($_POST['feed']);
           $adid=trim($_POST['adid']);
           $qq=new Admission();
           $qq->updateAll(array(
                'ilcfeed'=>$feed,
                ),
                "adid=:q", array(':q'=>$adid));
            
            $cru=new CDbCriteria(); 
            $cru->condition='adid=:u';
            $cru->params=array(":u"=>$adid);
            $inter= Admission::model()->find($cru);
            $parid= $inter->parentid;
            $userid=$inter->userid;
            
            $cru=new CDbCriteria(); 
            $cru->condition='userid=:u';
            $cru->params=array(":u"=>$userid);
            $uu= Users::model()->find($cru);
            $uname= $uu->username;
            $name=$uu->name;
            
            $crp=new CDbCriteria(); 
            $crp->condition='parentid=:u';
            $crp->params=array(":u"=>$parid);
            $intep= EnquiryParent::model()->find($crp);
            $child=$intep->child_name;
            
            $mst="ILC manager have given the feedback regarding the admission of ".$child;
            $qn=new Notifications();
            $qn->queryid=$adid;
            $qn->notid=  uniqid();
            $qn->role="unit";
            $qn->ilc=$inter->ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            $qn=new Notifications();
            $qn->queryid=$adid;
            $qn->notid=  uniqid();
            $qn->role="partner";
            $qn->ilc=$inter->ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Manager's Feedback";
            $msg="Dear $name,<br>
                ILC manager have given the feedback regarding the admission of $child<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            }
            else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUploadphoto() {
            $adid=trim($_POST['adid']);
            
            $ad= new Admission(); // new object model
             if ($_FILES['file']['error'] > 0) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
   
    }
     else { 
    //    $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
             
               $ad->updateAll(array(
                'photo'=>$fileName,
            
                 ),
                "adid=:q", array(':q'=>$adid));
            
         
	//if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../studentphoto/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
       // }
      
}
        } 
        
        public function actionUploadphoto2() {
            $adid=trim($_POST['adid']);
            
            $ad= new Admission(); // new object model
             if ($_FILES['file']['error'] > 0) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
   
    }
     else { 
     //   $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
             
               $ad->updateAll(array(
                'recphoto'=>$fileName,
            
                 ),
                "adid=:q", array(':q'=>$adid));
            
         
	//if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../recphoto/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
       // }
      
        }
        } 
        
        public function actionCloseadm() {
            if(Yii::app()->request->isPostRequest) {
            $adid=trim($_POST['adid']);
            $cru=new CDbCriteria(); 
            $cru->condition='adid=:u';
            $cru->params=array(":u"=>$adid);
            $inter= Admission::model()->find($cru);
            $parid= $inter->parentid;
            
            $userid=$inter->userid;
            $ilcid=$inter->ilcid;
            
            $usr=new Nlist();
            $il=$usr->getilcmid($ilcid);
            $ilcmid=$il->ilcmid;
            
            $usr=new Nlist();
            $uu=$usr->getuser($ilcmid);
            $uname=$uu->username;
            $name=$uu->name;
            
            $crp=new CDbCriteria(); 
            $crp->condition='parentid=:u';
            $crp->params=array(":u"=>$parid);
            $intep= EnquiryParent::model()->find($crp);
            $child=$intep->child_name;

            $qq=new Admission();
            $qq->updateAll(array("aclosed"=>1,"clodate"=>date('Y-m-d')), 'adid=:q', array(":q"=>$adid));
            
            $qq=new Notifications();
            $qq->deleteAll("queryid=:q ", array(":q"=>$adid));
           
            $mst="The admission of ".$child." has been closed";
            $qn=new Notifications();
            $qn->queryid=$adid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$inter->ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Admission Closed";
            $msg="Dear $name,<br>
                The admission of $child has been closed<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            ///////////////////////////////////////////// 
            
            }
            else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
         
}