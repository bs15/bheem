<?php
/* @var $this CrockController */
/* @var $model Crock */

$this->breadcrumbs=array(
	'Crocks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Crock', 'url'=>array('index')),
	array('label'=>'Manage Crock', 'url'=>array('admin')),
);
?>

<h1>Create Crock</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>