<?php

/**
 * This is the model class for table "enquiry".
 *
 * The followings are the available columns in table 'enquiry':
 * @property string $enquiryid
 * @property string $enquiry_type
 * @property string $enquired_by
 * @property string $ilcid
 * @property string $ilcmid
 * @property string $parentid
 * @property string $enquiry_date
 * @property string $enquiry_time
 * @property string $estatus
 * @property string $etask
 * @property integer $eclosed
 * @property string $date1
 * @property integer $sfa
 * @property integer $pref
 * @property string $cdate
 * @property integer $month
 * @property integer $year
 */
class Enquiry extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Enquiry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'enquiry';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enquiryid, enquiry_type, enquired_by, ilcid, ilcmid, parentid, enquiry_date, enquiry_time, estatus, etask, eclosed, date1, sfa, pref, cdate, month, year, clodate,state,city', 'required'),
			array('id, eclosed, sfa, pref,week, month, year,esc', 'numerical', 'integerOnly'=>true),
			array('enquiryid, enquired_by, ilcid, ilcmid, parentid', 'length', 'max'=>100),
			array('date1', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, enquiryid, enquiry_type, enquired_by, ilcid, ilcmid, parentid, enquiry_date, enquiry_time, estatus, etask, eclosed, date1, sfa, pref, cdate,week, month, year, esc, clodate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enquiryid' => 'Enquiryid',
			'enquiry_type' => 'Enquiry Type',
			'enquired_by' => 'Enquired By',
			'ilcid' => 'Ilcid',
			'ilcmid' => 'Ilcmid',
			'parentid' => 'Parentid',
			'enquiry_date' => 'Enquiry Date',
			'enquiry_time' => 'Enquiry Time',
			'estatus' => 'Estatus',
			'etask' => 'Etask',
			'eclosed' => 'Eclosed',
			'date1' => 'Date1',
			'sfa' => 'Sfa',
			'pref' => 'Pref',
			'cdate' => 'Cdate',
                        'week' => 'Week',
			'month' => 'Month',
			'year' => 'Year',
                        'esc' => 'Esc',
                        'clodate' => 'Closed Date',
                        'state' => 'State',
                        'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('enquiryid',$this->enquiryid,true);
		$criteria->compare('enquiry_type',$this->enquiry_type,true);
		$criteria->compare('enquired_by',$this->enquired_by,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('ilcmid',$this->ilcmid,true);
		$criteria->compare('parentid',$this->parentid,true);
		$criteria->compare('enquiry_date',$this->enquiry_date,true);
		$criteria->compare('enquiry_time',$this->enquiry_time,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('etask',$this->etask,true);
		$criteria->compare('eclosed',$this->eclosed);
		$criteria->compare('date1',$this->date1,true);
		$criteria->compare('sfa',$this->sfa);
		$criteria->compare('pref',$this->pref);
		$criteria->compare('cdate',$this->cdate,true);
                $criteria->compare('week',$this->week,true);
		$criteria->compare('month',$this->month);
		$criteria->compare('year',$this->year);
                $criteria->compare('esc',$this->esc);
                $criteria->compare('clodate',$this->clodate);
                $criteria->compare('state',$this->state);
                $criteria->compare('city',$this->city);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}