<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
        // $model=new LoginForm;
            
		//$this->render('login',array('model'=>$model));
        $this->redirect($this->redirect(Yii::app()->request->baseUrl."/index.php/site/home"));
	}

  public function actionHome()
  {
    $rolid=Yii::app()->user->getState("rolid");
    $logged=Yii::app()->user->getState('logged');

    if($logged=="true"){
        if($rolid=="not_valid") {
              $this->redirect(Yii::app()->baseUrl."/index.php/site/logout"); 
        }
        else {
          $this->redirect(Yii::app()->baseUrl."/index.php/".$rolid."/index"); 
        }
    }            
    $this->render('home');
        // $this->redirect($this->redirect(Yii::app()->request->baseUrl."/index.php/site/home"));
  }
        
  public function actionLogf()
	{
		$this->render('logf');
                
  }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
        
        
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
        public function actionForgot()
        {
            $this->render('forgot');
        }
        public function actionDoreset()
        {
              if(Yii::app()->request->isPostRequest) 
                  {
                  $uname=  trim($_POST['umail']);
                  $uss=new Users();
               $c=  intval($uss->count("username=:u", array(':u'=>$uname)));
               if($c==0){
                   echo "Invalid email or email does not exist!";
               }
               else {
                   $r=  uniqid();
                   $re=new Passreset();
                   $nre=intval($re->count("email=:e", array(":e"=>$uname)));
                   if($nre==0) {
                   $re->isNewRecord=TRUE;
                   $re->resetid=$r;
                   $re->email=$uname;
                   $re->save(FALSE);
                         ////////////////send mail//////////////////
            $to=$uname;
            $url="http://tmp-mis.beanstalkedu.com/index.php/site/reset/?q=".$r;
            $subject="Beanstalk Manager Registration";
            $msg="Dear User,<br>
                Your password reset request has been accepted.<br>
                Please click on this <a target=\"_blank\" href=\"$url\">LINK</a> to reset password .<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
                   }
               }
              
                  } else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionPassreset() {
              if(Yii::app()->request->isPostRequest) 
                  { 
                  $m=  trim($_POST['um']);
                  $pass=trim($_POST['p1']);
                  $u=new Users();
                  $u->updateAll(array('password'=>md5($pass)), "username=:u", array(":u"=>$m));
                  $pr=new Passreset();
                  $pr->deleteAll("email=:e", array(":e"=>$m));
                  echo "Password updated!";
                  
                } else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }

        public function actionReset() {
            $rid=  trim($_REQUEST['q']);
            $crp=new CDbCriteria();
            $crp->condition="resetid=:r";
            $crp->params=array(":r"=>$rid);
            $rres=  Passreset::model()->find($crp);
            $this->render("reset",array("rres"=>$rres));
        }

        /**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) 	{			
                               ///////////////////////////role check/////////////////////
                            $rolid=Yii::app()->user->getState("rolid");
                             $uid=Yii::app()->user->getState("user_id");
                        //     die("role - ".$rolid);
                             switch($rolid){
                 case "admin":
                                {
                      Yii::app()->user->setState("temp_logged","");      
                                  //header("location: ../../spcl_admin/index.php");
//                                      Yii::import('application.controllers.Spcl_adminController'); // ConsolidateController is another controller in back controller folder
//                echo Spcl_adminController::index(); // test is action in ConsolidateController
                                   /* Yii::app()->runController('Super_admin/dashboard');*/
                        $this->redirect(Yii::app()->baseUrl."/index.php/admin/index");

                                  break;
                              
                                }
                          case "academic":
                                {
                      Yii::app()->user->setState("temp_logged","");      
                                  //header("location: ../../spcl_admin/index.php");
//                                      Yii::import('application.controllers.Spcl_adminController'); // ConsolidateController is another controller in back controller folder
//                echo Spcl_adminController::index(); // test is action in ConsolidateController
                                   /* Yii::app()->runController('Super_admin/dashboard');*/
                        $this->redirect(Yii::app()->baseUrl."/index.php/academic/index");

                                  break;
                              
                                }      
                                   case "ilcmanager":
                                {
                      Yii::app()->user->setState("temp_logged","");      
                                  //header("location: ../../spcl_admin/index.php");
//                                      Yii::import('application.controllers.Spcl_adminController'); // ConsolidateController is another controller in back controller folder
//                echo Spcl_adminController::index(); // test is action in ConsolidateController
                                   /* Yii::app()->runController('Super_admin/dashboard');*/
                        $this->redirect(Yii::app()->baseUrl."/index.php/ilcmanager/index");

                                  break;
                              
                                }  
                                   case "partner":
                                {
                      Yii::app()->user->setState("temp_logged","");      
                                  //header("location: ../../spcl_admin/index.php");
//                                      Yii::import('application.controllers.Spcl_adminController'); // ConsolidateController is another controller in back controller folder
//                echo Spcl_adminController::index(); // test is action in ConsolidateController
                                   /* Yii::app()->runController('Super_admin/dashboard');*/
                        $this->redirect(Yii::app()->baseUrl."/index.php/partner/index");

                                  break;
                              
                                }    
                                       case "unit":
                                {
                      Yii::app()->user->setState("temp_logged","");      
                                  //header("location: ../../spcl_admin/index.php");
//                                      Yii::import('application.controllers.Spcl_adminController'); // ConsolidateController is another controller in back controller folder
//                echo Spcl_adminController::index(); // test is action in ConsolidateController
                                   /* Yii::app()->runController('Super_admin/dashboard');*/
                        $this->redirect(Yii::app()->baseUrl."/index.php/unit/index");

                                  break;
                              
                                }    
                                  case "teacher":
                                {
                      Yii::app()->user->setState("temp_logged","");      
                                  //header("location: ../../spcl_admin/index.php");
//                                      Yii::import('application.controllers.Spcl_adminController'); // ConsolidateController is another controller in back controller folder
//                echo Spcl_adminController::index(); // test is action in ConsolidateController
                                   /* Yii::app()->runController('Super_admin/dashboard');*/
                        $this->redirect(Yii::app()->baseUrl."/index.php/teacher/index");

                                  break;
                              
                                }  
                                     case "director":
                                {
                      Yii::app()->user->setState("temp_logged","");      
                                  //header("location: ../../spcl_admin/index.php");
//                                      Yii::import('application.controllers.Spcl_adminController'); // ConsolidateController is another controller in back controller folder
//                echo Spcl_adminController::index(); // test is action in ConsolidateController
                                   /* Yii::app()->runController('Super_admin/dashboard');*/
                        $this->redirect(Yii::app()->baseUrl."/index.php/director/index");

                                  break;
                              
                                }     
                                  case "not_valid":
                                {
                                //{Yii::app()->runController('Site/logf');
                     $this->redirect(Yii::app()->baseUrl."/index.php/site/logf");

                                  break;
                                    
                                }
                                case "user":
                                {
                               Yii::app()->user->setState("temp_logged","");      
                                    //{Yii::app()->runController('Site/logf');
                     $this->redirect(Yii::app()->baseUrl."/index.php/user/index");

                                  break;
                                    
                                }
                                 case "trainer":
                                {
                               Yii::app()->user->setState("temp_logged","");      
                                    //{Yii::app()->runController('Site/logf');
                     $this->redirect(Yii::app()->baseUrl."/index.php/user/index");

                                  break;
                                    
                                }    
                                case "blank_uid":
                                {
                                    //Yii::app()->runController('Site/logout');
                                $this->redirect("logout");
                                  break;
                                    
                                }
                              
                               default:{
                                   $this->render('index');
                                   break;
                               }
                                
                                    } 
                            ///////////////////////////////////////////////////////////
                            $this->redirect(Yii::app()->user->returnUrl);
                            
                        }
                        
		}
		$rolid=Yii::app()->user->getState("rolid");
                $logged=Yii::app()->user->getState('logged');

                if($logged=="true"){
                    if($rolid=="not_valid") {
                          $this->redirect(Yii::app()->baseUrl."/index.php/site/logout"); 
                    }
                    else {
                   $this->redirect(Yii::app()->baseUrl."/index.php/".$rolid."/index"); 
                    }
                }
                // display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
            Yii::app()->user->logout();
            Yii::app()->session->clear();
            $this->redirect("home");
	}
       public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
}