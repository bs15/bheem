<?php
/* @var $this AcademicController */

$this->breadcrumbs=array(
	'Academic',
);

$u=new Users();
$nt=intval($u->count('role=:r',array(":r"=>'teacher')));
$nu=intval($u->count('role=:r',array(":r"=>'unit')));
$np=intval($u->count('role=:r',array(":r"=>'partner')));
$nm=intval($u->count('role=:r',array(":r"=>'ilcmanager')));

$ilc=new Ilc();
$ni=intval($ilc->count('id>:z',array(":z"=>'0')));

/////////////////////////query///////////////////
$qesc=0;
$crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="query_closed=0";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                
                    $qdate=$q->query_date;
                
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>24 || $d==24){
                    $qesc++;
                }
            }
            
///////////////////////interview///////////////////   
$iesc=0;
$crq1=new CDbCriteria();
                $crq1->condition='iclosed=0';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                 
                foreach($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);  
                    
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                $crq=new CDbCriteria();
                $crq->condition='ilcid=:u';
                $crq->params=array(':u'=>$ilc);
                $ilcs= ManagerIlc::model()->find($crq);
                
                
                $idate=$i->idate;
                $idate1=intval(strtotime($idate));
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>=48)
                { $iesc++;
                 }
                 
                }
                
/////////////////////enquiry///////////////////////
$eesc=0;
$crq=new CDbCriteria();
               $crq->condition='eclosed=0';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>24 ) {
                    $eesc++;
                }
                }
                
/////////////////////////////admission//////////////////////////
$adm=0;
$crq=new CDbCriteria();
               $crq->condition='aclosed=0';
               $crq->order='date1 desc';
               $en= Admission::model()->findAll($crq);
               $n=sizeof($en);
               foreach ($en as $i){
                   $adm++;
                }
                
/////////////////////////////call//////////////////////////
$cal=0;
$crq=new CDbCriteria();
               $crq->condition='closed=0';
               $en= Call::model()->findAll($crq);
               $n=sizeof($en);
               foreach ($en as $i){
                   $cal++;
                }
               
                          ?>
<div class="bdy3"  >
    <br style="clear:both">        
    <div class="row">
        <div class="col-md-2">
        <div class="box box-red">
            <? 
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="query_date desc";
            $queries=  Query::model()->findAll($crq);
            $n=  sizeof($queries);
            ?>
            <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/queryesc"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Query<br> Module</h4>
            <p><span style="font-size:20px !important"><?=$qesc ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-green">
           
            <a href="<?=Yii::app()->request->baseUrl."/index.php/admission/index"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Admission<br> Module</h4>
            <p><span style="font-size:20px !important"><?=$adm ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-blue">
            <? 
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            $enq= Enquiry::model()->findAll($crq);
            $n= sizeof($enq);
            ?>
            <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>" style="text-decoration:none;color:white">
            <h4 class="text-white">Enquiry <br>Module</h4>
            <p><span style="font-size:20px !important"><?=$eesc ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-pink">
           
            <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/interview"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Interview<br> Module</h4>
            <p><span style="font-size:20px !important"><?=$iesc ?></span> Notifications</p>
            </a>
        </div>
        </div>
        <div class="col-md-2">
        <div class="box box-lav">
           
            <a href="<?=Yii::app()->request->baseUrl."/index.php/call/index"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Call<br> Module</h4>
            <p><span style="font-size:20px !important"><?=$cal ?></span> Notifications</p>
            </a>
        </div>
        </div>
    </div> 
        
  <div class="dashbrd">
      <div class="container">
          <div class="row">
          <div class="col-md-4">
              <div  id="holcal">
                  <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Calender
                      <a href="<?=Yii::app()->request->baseUrl."/index.php/holiday/index"?>" style="float:right;color:black">
                      <i class="glyphicon glyphicon-th"></i>
                      </a>
                  </h5>
                  
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcalenderaca'), // URL to get event
                //////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getcalenderacamodal/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            /////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>   
            <ul class="hpal">
                                    <li><div class="bred"></div>&nbsp;Holidays</li>
                                    <li><div class="bora"></div>&nbsp;Enquiry</li>
                                    <li><div class="byel"></div>&nbsp;Interview</li>
                                    <li><div class="bpur"></div>&nbsp;Calls</li>
                                </ul>
            </div>
          </div>
              
              <div class="col-md-5 ilc-back" style="height:450px;overflow: scroll">
                  <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Current Notifications
                      <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications" style="float:right;color:black">
                      <i class="glyphicon glyphicon-th-list"></i>
                      </a>
                  </h5> <?
                  $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'academic');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n4=  sizeof($res);
        foreach($res as $r){
            ?>
           <span style="font-size:13px !important;margin-bottom:3%"><?=date('d-M-Y',$r->date1) ?></span>
            <p style="font-size:14px !important;">
           <?=$r->message ?></p>
                
            
            <?
        } 
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n6=  sizeof($res);
        foreach($res as $r){
            ?>
           <span style="font-size:13px !important;margin-bottom:3%"><?=date('d-M-Y',$r->date1) ?></span>
            <p style="font-size:14px !important;">
           <?=$r->message ?></p>
                
            
            <?
        }
        ?>
              </div>
              
              <!-- <div class="col-md-3 ">
                  
                  <div class="iframe-back">
                      <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Resources
                      
                  </h5>
                      <div class="box box-white" style="margin:4%;width:90%">
                         <span class="badge badge-success pull-left" style="margin:0;"><?=$ni ?></span>
                          <h4> &nbsp;|&nbsp;ILC</h4>
                         
                      </div>
                       <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/managers" style="text-decoration:none;color:black">
                          <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-warning pull-left" style="margin:0;"><?=$nm ?></span>
                          <h4> &nbsp;&nbsp;|&nbsp;Managers</h4>
                          </div>
                       </a>
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-danger pull-left" style="margin:0;"><?=$nu ?></span>
                          <h4> &nbsp;|&nbsp;Coordinators</h4>
                      </div>
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-primary pull-left" style="margin:0;"><?=$np ?></span>
                          <h4> &nbsp;|&nbsp;Partners</h4>
                      </div>
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-dark pull-left" style="margin:0;"><?=$nt ?></span>
                          <h4> &nbsp;|&nbsp;Teachers</h4>
                      </div>
                  </div>
              </div> -->
          </div>
      </div>
            
        </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
         /////////////////////////
var imgurl='<?=Yii::app()->request->baseUrl ?>/images/face.jpg';
$('img').error(function(){
        $(this).attr('src', imgurl);
});
//////////////////////
    });
</script>