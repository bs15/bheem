<?php

class ProfileController extends Controller
{
    
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        $criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = "role='unit' or role='partner' or role='ilcmanager' or role='teacher' or role='academic' or role='director'";
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
            
            return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','updateprof','setnoty','checknum','nlist'),
				'users'=>$modad,
			),
			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
		);
	}
        
	public function actionIndex()
	{
            $uid=Yii::app()->user->getState('user_id');
            $cr = new CDbCriteria();
            $cr->condition='userid=:u';
            $cr->params=array(':u'=>$uid);
            $user=Users::model()->find($cr);
            $userp=Userphoto::model()->find($cr);
	    $this->render('index',array('user'=>$user,'userp'=>$userp));
           
	}
      
        
        public function actionUpdateprof()
        {
            if(Yii::app()->request->isPostRequest) {
           $uid=trim($_POST['uid']);
           $fullname=trim($_POST['fullname']);
           $password=trim($_POST['password']);
           $phone=trim($_POST['phone']);

        $c=new Users();
        $c->updateAll(array('name'=>$fullname,'password'=>md5($password),'phone'=>$phone), 'userid=:u', array(':u'=>$uid));
      Yii::app()->user->setState("name",$fullname);
      echo 'Profile updated';
      }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
        }
   public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
}