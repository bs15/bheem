<?php

class NotificationsController extends Controller
{
    public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
        public function accessRules()
	{
        $criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = "role='unit' or role='partner' or role='ilcmanager' or role='teacher' or role='academic' or role='director'";
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
            
            return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','updateprof','setnoty','getnoty','checknum','nlist','searchnoty'),
				'users'=>$modad,
			),
			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
		);
	}
	public function actionIndex()
	{
            $userid=Yii::app()->user->getState('user_id');
            $rolid=Yii::app()->user->getState("rolid");
            $ilcid=Yii::app()->user->getState("ilc_id");
            $this->render('index');
	}
        
        
        public function actionGetnoty() {
         if(Yii::app()->request->isPostRequest) {  
            $userid=Yii::app()->user->getState('user_id');
            $rolid=Yii::app()->user->getState("rolid");
            $ilcid=Yii::app()->user->getState("ilc_id");
            $n1=$n2=$n3=$n4=$n5=$n6=1;
            
            
    if($rolid=='unit') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'unit',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n1=  sizeof($res);
        foreach($res as $r){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
    }
    
    if($rolid=='partner') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'partner',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n2=  sizeof($res);
        foreach($res as $r){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
    }
    
    if($rolid=='ilcmanager') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r ';
        $cr->params=array(':r'=>'ilcmanager');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        
        $cr1=new CDbCriteria();
        $cr1->condition='ilcmid=:r ';
        $cr1->params=array(':r'=>$userid);
        $res1= ManagerIlc::model()->findAll($cr1);
        
        $ilcs=array();
        foreach($res1 as $r1){
            array_push($ilcs, $r1->ilcid);
        }
        
        $n3=  sizeof($res);
        foreach($res as $r){
            if(in_array($r->ilc,$ilcs)){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
      }
    }
  
    if($rolid=='academic') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'academic');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n4=  sizeof($res);
        foreach($res as $r){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
    }
    
    if($rolid=='director') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'director');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        foreach($res as $r){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
    }
     if($rolid=='director' || $rolid=='academic' || $rolid=='ilcmanager' || $rolid=='unit' || $rolid=='partner' || $rolid=='teacher'
        ) {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n6=  sizeof($res);
        ?> <h4>Chat Notifications</h4> <?
        foreach($res as $r){
            ?>
          <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
    }
        if(($n1==0  && $n6==0)|| ($n2==0 && $n6==0) || ($n3==0 && $n6==0)|| ($n4==0 && $n6==0) || ($n5==0 && $n6==0)){
            ?>

            <div class="alert alert-light" role="alert">
                <p>No Notifications to show !</p>
            </div>
            <?
        }
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }

        }
        
        public function actionSearchnoty() {
          if(Yii::app()->request->isPostRequest) {  
            $sn=  trim($_POST['sn']);
            $sn=  explode('-', $sn);
            $ll= intval($sn[0]);
            $hl= intval($sn[1]);
            //$s=intval($sn);
            
            
            $userid=Yii::app()->user->getState('user_id');
            $rolid=Yii::app()->user->getState("rolid");
            $ilcid=Yii::app()->user->getState("ilc_id");
            $n1=$n2=$n3=$n4=$n5=$n6=1;
            
            
    if($rolid=='unit') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'unit',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n1=  sizeof($res);
        foreach($res as $r){
            /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-($r->date1); 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                if($d>=$ll && $d<=$hl){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
        
                }
    }
    
    if($rolid=='partner') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'partner',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n2=  sizeof($res);
        foreach($res as $r){
            /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-($r->date1); 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                if($d>=$ll && $d<=$hl){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
            }
        }
    }
    
    if($rolid=='ilcmanager') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r ';
        $cr->params=array(':r'=>'ilcmanager');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        
        $cr1=new CDbCriteria();
        $cr1->condition='ilcmid=:r ';
        $cr1->params=array(':r'=>$userid);
        $res1= ManagerIlc::model()->findAll($cr1);
        
        $ilcs=array();
        foreach($res1 as $r1){
            array_push($ilcs, $r1->ilcid);
        }
        
        $n3=  sizeof($res);
        foreach($res as $r){
            /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-($r->date1); 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
            if(in_array($r->ilc,$ilcs)){
                if($d>=$ll && $d<=$hl){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
            }
        }
      }
    }
  
    if($rolid=='academic') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'academic');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n4=  sizeof($res);
        foreach($res as $r){
            /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-($r->date1); 
		$d=intval(round($datediff/(60 * 60 * 24))*24);
                ///////////////////////////////////////
                //echo "$d , $ll - $hl \n";
                //die();
                if($d>=$ll && $d<=$hl){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
       }
    }
    
    if($rolid=='director') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'director');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        foreach($res as $r){
            /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-($r->date1); 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                if($d>=$ll && $d<=$hl){
            ?>
            <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
        }
      }
    }
    if($rolid=='director' || $rolid=='academic' || $rolid=='ilcmanager' || $rolid=='unit' || $rolid=='partner' || $rolid=='teacher'
        ) {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n6=  sizeof($res);
        ?> <h4>Chat Notifications</h4> <?
        foreach($res as $r){
            /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-($r->date1); 
		$d=round($datediff/(60 * 60 * 24))*24;
                
                ///////////////////////////////////////
                if($d>=$ll && $d<=$hl){
                    
                   // $n6++;
            
            ?>
          <div class="alert alert-dark" role="alert">
                <p><?=$r->message ?></p>
                <span class="pull-right"><?=date('d-M-Y',$r->date1) ?></span>
            </div>
            <?
                }
            }
        
         }
        if(($n1==0 && $n6==0) || ($n2==0 && $n6==0) || ($n3==0 && $n6==0) || ($n4==0 && $n6==0) || ($n5==0 && $n6==0)){
            ?>
            <div class="alert alert-dark" role="alert">
                <p>No Notifications to show !</p>
            </div>
            <?
        }
  }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }

        }

        public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
          $nl->setnoty1();
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
        }
          public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {  
  
              $nl= new Nlist();
            $nl->checknum1();
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
        
   public function actionNlist() {
         if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
       $nl->nlist1();
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}