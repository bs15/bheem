<?php

class IlcmanagerController extends Controller
{
      public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        
          public function accessRules()
                        	{

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid';
    	$criteria1->params = array(':rolid'=>'ilcmanager');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','ilcmanager','createilcm','getilcm','delilcm','searchbyilc'),
    		'users'=>$modad,
    	),
          array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('query','querylist','queryhistory','createquery','getquery','approvequery','showchat','sendchat'),
    		'users'=>$modad,
    	),
          array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('editquery','updatequery','getquery3','getqmy','getchat','setnoty','checknum','nlist','manage','uploadpost'),
    		'users'=>$modad,
    	),
           array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('interview','interview2','intlist1','intlist2','getimy','approveint','rescheduleint','updateint','icomment','updatecom'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('leave','hol','sethol','searchbyilcint'),
    		'users'=>$modad,
    	),
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        public function actionInterview() {
                  $uid=Yii::app()->user->getState('user_id');
           $cr1=new CDbCriteria();
           $cr1->condition='ilcmid=:r ';
            $cr1->params=array(':r'=>$uid);
            $res1= ManagerIlc::model()->findAll($cr1);
            
            $this->render('interview',array('res'=>$res1));
        }
        
        public function actionInterview2() {
        $this->render('interview2');
    }
    
        public function actionLeave() {
        $this->render('leave');
    }
        
        public function actionIntlist1() {
            if(Yii::app()->request->isPostRequest) {
           $userid=Yii::app()->user->getState('user_id');
            
            //////////////reassigned maneger////////////////
                $crq=new CDbCriteria();
               $crq->condition='ilcmid=:i and close=0 and intid<>:c';
               $crq->params=array(':i'=>$userid,':c'=>'-');
               $ri= Assignesc::model()->findAll($crq);
               $ni=sizeof($ri);
            /////////////////////////////////////////////////
            
            $crq=new CDbCriteria();
            $crq->condition='ilcmid=:u';
            $crq->params=array(':u'=>$userid);
            $ilcs= ManagerIlc::model()->findAll($crq);
            
            $ii=array();
            foreach($ilcs as $il){
                array_push($ii,$il->ilcid);
            }
                
                $crq1=new CDbCriteria();
                $crq1->condition='iclosed=0';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                if($n==0) {
                ?> <p style="text-align:center;font-size: 12px !important">No scheduled interviews</p> <?
                }
                else 
                {
                    ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Partner/Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr>
            <?
            
             
            
                foreach($interviews as $i){
                    
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);
                
                $role1="";
                //$role="";
                
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                
                
                $crid=$i->userid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$crid);
                $user2= Users::model()->find($crq1);
                $uname1=$user2->name;
                $rolex=$user2->role;
                
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                } 
                if($rolex=="unit"){
                    $rolex="Coordinator";
                }
                else if($rolex=="partner"){
                    $rolex="Partner";
                }
                    
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                if(in_array($ilc,$ii)){
                    
                
                ?>
                    <tr>
                        <td><span style="text-transform:uppercase"><?=$il->ilcname ?></span></td>
                        <td><?=$uname1 ."[".$rolex."]" ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                          <? if($is->communication=='' || $is->academic_knowledge=='' || $is->attitude=='' ){ ?>
                            <button class="btn-green" id="<?=$intid ?>" onclick="icomment(this.id)">Feedback</button>
                       <?   } ?>  
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if($istatus=='0' && $d<=48){
                            ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Approve" onclick="approveint(this.id)" />
                            <br>  
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Reschedule" onclick="rescheduleint(this.id)" />
                             <? } 
                             elseif($istatus=='1' && $d<=48){
                              ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" value="Active"  />

                            <?   
                             }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            else if($istatus==1 && $i->icomment==""){
                               ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Comment" onclick="icomment(this.id)" />
                            <?
                            }?>
                        </td>
                    
                    </tr>
                   
                <?              
            
                }
            }
            ?>
                </table>
            </div>
        <?
        }
        
            if($ni!=0){
            //////////////////////reassigned interviews///////////////
            ?>
            <div class="table table-striped table-font">  
                <table>
                    
            <?
             foreach($ri as $r){
                $intid=$r->intid;
                //echo $intid;
                
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $i=Interview::model()->find($cri);
                
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);
                
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                } 
                    
                //$ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$r->ilcid);
                $uinfo=Users::model()->find($cru);
                
                 $cru=new CDbCriteria();
                $cru->condition='ilcid=:u ';
                $cru->params=array(":u"=>$r->ilcid);
                $lc=Ilc::model()->find($cru);
                
                
                 ?>
                    <tr>
                        <td><span style="text-transform:uppercase"><?=$lc->ins_name ?></span></td>
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                          <? if($is->communication=='' || $is->academic_knowledge=='' || $is->attitude=='' ){ ?>
                            <button class="btn-green" id="<?=$intid ?>" onclick="icomment(this.id)">Feedback</button>
                       <?   } ?>  
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td><button class="btn-danger">Reassigned</button> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if($istatus=='0' && $d<=48){
                            ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Approve" onclick="approveint(this.id)" />
                            <br>  
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Reschedule" onclick="rescheduleint(this.id)" />
                             <? } 
                             elseif($istatus=='1' && $d<=48){
                              ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" value="Active"  />

                            <?   
                             }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            else if($istatus==1 && $i->icomment==""){
                               ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Comment" onclick="icomment(this.id)" />
                            <?
                            }?>
                        </td>
                    
                    </tr>
                   
                <?              
            
                
            }   
            ///////////////////////////////////////////////////////////
            
            ?> </table></div> <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
        }
    }
    
        public function actionSearchbyilcint() {
            if(Yii::app()->request->isPostRequest) {
            $silc=trim($_POST['silc']);

            $userid=Yii::app()->user->getState('user_id');
            $crq=new CDbCriteria();
            $crq->condition='ilcmid=:u and ilcid=:i';
            $crq->params=array(':u'=>$userid,':i'=>$silc);
            $ilcs= ManagerIlc::model()->findAll($crq);
            
            $ii=array();
            foreach($ilcs as $il){
                array_push($ii,$il->ilcid);
            }
                
                $crq1=new CDbCriteria();
                $crq1->condition='iclosed=0';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                if($n==0) {
                ?> <p style="text-align:center;font-size: 12px !important">No scheduled interviews</p> <?
                }
                else 
                {
                    ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr>
            <?
                foreach($interviews as $i){
                    
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);
                
                $role1="";
                //$role="";
                
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                
                
                $crid=$i->userid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$crid);
                $user2= Users::model()->find($crq1);
                $uname1=$user2->name;
                $rolex=$user2->role;
                
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                } 
                if($rolex=="unit"){
                    $rolex="Coordinator";
                }
                else if($rolex=="partner"){
                    $rolex="Partner";
                }
                    
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                if(in_array($ilc,$ii)){
                    
                
                ?>
                    <tr>
                        <td><span style="text-transform:uppercase"><?=$il->ilcname ?></span></td>
                        <td><?=$uname1 ."[".$rolex."]" ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                          <? if($is->communication=='' || $is->academic_knowledge=='' || $is->attitude=='' ){ ?>
                            <button class="btn-green" id="<?=$intid ?>" onclick="icomment(this.id)">Feedback</button>
                       <?   } ?>  
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if($istatus=='0' && $d<=48){
                            ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Approve" onclick="approveint(this.id)" />
                            <br>  
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-inverse" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Reschedule" onclick="rescheduleint(this.id)" />
                             <? } 
                             elseif($istatus=='1' && $d<=48){
                              ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" value="Active"  />

                            <?   
                             }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            else if($istatus==1 && $i->icomment==""){
                               ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Comment" onclick="icomment(this.id)" />
                            <?
                            }?>
                        </td>
                    
                    </tr>
                   
                <?              
            
                }
            }
            ?>
                </table>
            </div>
        <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
    }
   
        public function actionIntlist2() {
            if(Yii::app()->request->isPostRequest) {
           

             $userid=Yii::app()->user->getState('user_id');
            $crq=new CDbCriteria();
            $crq->condition='ilcmid=:u';
            $crq->params=array(':u'=>$userid);
            $ilcs= ManagerIlc::model()->findAll($crq);
            
            $ii=array();
            foreach($ilcs as $il){
                array_push($ii,$il->ilcid);
            }
                
                $crq1=new CDbCriteria();
                $crq1->condition='iclosed=1';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                if($n==0) {
                ?> <p> No interviews have been closed yet</p> <?
                }
                else 
                { ?>
                
                <div   style="margin-top:0%">  
                <table class="table table-striped table-font">
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
                foreach($interviews as $i){
                 $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);   
                 $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                    
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                if(in_array($ilc,$ii)){
                ?>
         <tr>
                        <td><span style="text-transform:uppercase"><?=$il->ilcname ?></span></td>
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         <? } ?>
                       
                    
                    </tr>
                
                <?              
                }
                ?>
                </table>
            </div>
            <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
        }
 }
 
        public function actionApproveint() {
            if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);

         $userid=Yii::app()->user->getState('user_id');
         $qq=new Interview();
         $qq->updateAll(array("istatus"=>1,"ilcmid"=>$userid,"interviewerid"=>$userid), 'intid=:q', array(":q"=>$intid));
         
         $cru=new CDbCriteria();
         $cru->condition='intid=:u';
         $cru->params=array(":u"=>$intid);
         $inter=Interview::model()->find($cru);
         $uid=$inter->userid;
         
         $mst="Interview scheduled for ".$inter->candidatename." on ".$inter->idate." at ".$inter->ihour.":".$inter->imin." has been approved by ILC Manager ";
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="unit";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="partner";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    
    
    $usr=new Nlist();
    $uu=$usr->getuser($userid);
    $uname=$uu->username;
    $name=$uu->name;
    
    
    ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Interview Schedule Approved";
            $msg="Dear $name,<br>
                Interview scheduled for $inter->candidatename on $inter->idate at $inter->ihour:$inter->imin has been approved by ILC Manager<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////   
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
  }

        public function actionRescheduleint(){
            if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);

        $cru=new CDbCriteria(); 
        $cru->condition='intid=:u';
        $cru->params=array(":u"=>$intid);
        $interview= Interview::model()->find($cru);
        
     ?>
         <div class="row">            
            <div class="col-md-12">      
                <div class="input-group mb-3">
                    <input type="hidden" value="<?=$intid ?>" id="qqid">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Date</span>
                        </div>
                        <input type="text" class="form-control" value="<?=$interview->idate ?>" id="datepicker" placeholder="Select date" aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </div>
             
            <div class="col-md-12">  
                <div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">Time </span>
  </div>
                    <input type="time" class="form-control" id="stime2" value="<?=$interview->ihour ?>:<?=$interview->imin ?>" />
</div>
                        </div>
                       
                </div>
                <div class="input-group">
                    <input type="button" class="btn btn-success" value="Update" onclick="updateint()"/>
                </div>    
         <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
        }
 }
 
        public function actionUpdateint(){
       if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);
           $idate=trim($_POST['idate']);
           $ihour=trim($_POST['ihour']);
           $imin=trim($_POST['imin']);
     
     $cru=new CDbCriteria(); 
     $cru->condition='intid=:u';
     $cru->params=array(":u"=>$intid);
     $inter= Interview::model()->find($cru);
     $user=$inter->userid;
     
     $qq=new Interview();
     $qq->updateAll(array('idate'=>$idate,'ihour'=>$ihour,'imin'=>$imin), "intid=:q", array(':q'=>$intid));
     
     
     
     $mst="Interview scheduled for ".$inter->candidatename." on ".$inter->idate." at ".$inter->ihour.":".$inter->imin." has been rescheduled to ".$idate." at ".$ihour.":".$imin."by ILC Manager ";
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="unit";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="partner";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $usr=new Nlist();
    $uu=$usr->getuser($user);
    $uname=$uu->username;
    $name=$uu->name;
    
    
    ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Interview Rescheduled ";
            $msg="Dear $name,<br>
                Interview scheduled for $inter->candidatename on $inter->idate at $inter->ihour:$inter->imin has been rescheduled to $idate at $ihour:$imin by ILC Manager<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
 } 
 
        public function actionIcomment(){
            if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);


        $cru=new CDbCriteria(); 
        $cru->condition='intid=:u';
        $cru->params=array(":u"=>$intid);
        $interview= Interview::model()->find($cru);
        $inid=$interview->interviewerid;
        
     ?>
         <div class="row">            
            <div class="col-md-12">      
                <div class="input-group mb-3">
                    <input type="hidden" value="<?=$intid ?>" id="qqid">
                    <input type="hidden" value="<?=$inid ?>" id="inid">    
                    <textarea class="form-control" id="icom" rows="4" placeholder="Message for Unit Coordinator" aria-label="Username" aria-describedby="basic-addon1"></textarea>
                </div>
                <p style="color:lightgrey">Slide the following to give points</p>
                <label>Academic Knowledge</label>&nbsp;<span class="badge badge-danger" id="aval"></span>
                <input type="range" onchange="aval(this.value)" class="custom-range" min="1" max="10" id="ack" >
                <br ><br>
                
                <label>Communication</label>&nbsp;<span class="badge badge-danger" id="cval"></span>
                <input type="range" onchange="cval(this.value)" class="custom-range" min="1" max="10" id="com1">
                <br ><br>
                
                <label>Attitude</label>&nbsp;<span class="badge badge-danger" id="tval"></span>
                <input type="range" onchange="tval(this.value)" class="custom-range" min="1" max="10" id="att"><br>
                <br><br>
                
                </div>
        </div>
        <div class="input-group">
            <input type="button" class="btn btn-success" value="Send" onclick="updatecom()"/>
        </div>    
         <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
        }
 }
        
        public function actionUpdatecom(){
         if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);
           $icom=trim($_POST['icom']);
           $inid=trim($_POST['inid']);
           $ack=trim($_POST['ack']);
           $com1=trim($_POST['com1']);
           $acc=trim($_POST['acc']);

            $userid=Yii::app()->user->getState('user_id');
            
         $cru=new CDbCriteria(); 
        $cru->condition='intid=:u';
        $cru->params=array(":u"=>$intid);
        $inter= Interview::model()->find($cru);
        $user=$inter->userid;
        
        $qq=new Interview();
        $qq->updateAll(array('icomment'=>$icom,'ilcmid'=>$userid), "intid=:q", array(':q'=>$intid));
        
        $qi=new InterviewScore();
        $qi->intid=$intid;
        $qi->interviewer_id=$inid;
        $qi->academic_knowledge=$ack;
        $qi->communication=$com1;
        $qi->attitude=$acc;
        
        $fs=intval($ack)+intval($com1)+intval($acc);
        $qi->final_score=$fs;
        $qi->isNewRecord=true;
        $qi->save(FALSE);
        
        
    $mst="ILC Manager has made a comment regarding the interview of ".$inter->candidatename.".You are requested to close the interview.";
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="unit";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="partner";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $usr=new Nlist();
    $uu=$usr->getuser($user);
    $uname=$uu->username;
    $name=$uu->name;
    
    
    ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Interview Manager Feedback ";
            $msg="Dear $name,<br>
                ILC Manager has made a comment regarding the interview of $inter->candidatename. You are requested to close the interview<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
 } 
        
        public function actionSearchbyilc(){
       if(Yii::app()->request->isPostRequest) {
        $silc=trim($_POST['silc']);

        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ilc_id=:s and query_closed=0";
        $crq->params=array(':s'=>$silc);
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?>
            <br style="clear:both">
            <br style="clear:both">
        <p>No queries have been raised from ths ILC</p>    
            <?
        }
        else 
        {
         ?>
        <br style="clear:both">
        <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                
                ?>
<tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
   <td>   
        <?
        if($d<48)
        {
        ?>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</span>
        <?
        }
        else if($d>48 || $d==48) {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</span>
        <? } ?></td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
 <?
                
            }?>
                </table>
        </div>
                <?
          
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        
        public function actionQuery() {
            $model=new QueryChat();
            
            $uid=Yii::app()->user->getState('user_id');
           $cr1=new CDbCriteria();
           $cr1->condition='ilcmid=:r ';
            $cr1->params=array(':r'=>$uid);
            $res1= ManagerIlc::model()->findAll($cr1);
            
             $this->render('query',array('model'=>$model,'res'=>$res1));
            }
        //////////////////////upload post ///////////////////
        
        public function actionUploadpost() {
        $model = new QueryChat();
        $userid=Yii::app()->user->getState('user_id');
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
       // $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
                $model->queryid=trim($_POST['qid']);
                $model -> document = $fileName;
	
                
                $model->chatid=  uniqid();
         $model->senderid=$userid;
         $model->recid='-';
         $model->message='-';
         $model->date1=  strtotime(date('Y-m-d H:i:s'));
         
	if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../chatdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
            }
        ///////////////////////////////////////////
            
	public function actionIndex(){
	$uid = Yii::app()->user->getState("user_id");
        $cr = new CDbCriteria();
        $cr->condition='userid=:u';
        $cr->params=array(':u'=>$uid);
        $user=Users::model()->find($cr);
        $userp=Userphoto::model()->find($cr);
	$this->render('index',array('user'=>$user,'userp'=>$userp));	
	}
        
        public function actionManage(){
            $uid = Yii::app()->user->getState("user_id");
            $cr = new CDbCriteria();
            $cr->condition='ilcmid=:u';
            $cr->params=array(':u'=>$uid);
            $user=ManagerIlc::model()->findAll($cr);
            
            $this->render('manage',array('user'=>$user));	
        }
        
        public function actionGetquery() {
            if(Yii::app()->request->isPostRequest) {
           
        $uid=Yii::app()->user->getState('user_id');
        $ilcs=array();        
        ///////////////////////////////////////
        $crm=new CDbCriteria();
        $crm->condition='ilcmid=:u';
        $crm->params=array(":u"=>$uid);
        $res=  ManagerIlc::model()->findAll($crm);
        foreach ($res as $r) {
            array_push($ilcs, $r->ilcid);
        }
        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="query_closed=0";
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
            ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                if(in_array($ilc, $ilcs))
                {
                ?>
<tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>     
        <?
        if($d<48)
        {
        ?>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</span>
        <?
        }
        else if($d>48 || $d==48) {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</span>
        <? } ?> </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
 <?
                }
                } ?>
                </table>
            </div><?
          
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
 }
 
        public function actionEditquery(){
            if(Yii::app()->request->isPostRequest) {
           $id=trim($_POST['id']);

    $cru=new CDbCriteria();
    $cru->condition='qid=:u';
    $cru->params=array(":u"=>$id);
    $query= Query::model()->find($cru);
    $hh=explode(":",$query->schedule_call_time);
    
     ?>
         <div class="row">            
                    <div class="col-md-12">      
                <div class="input-group mb-3">
                    <input type="hidden" value="<?=$id ?>" id="qqid">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Date</span>
  </div>
                    <input type="text" class="form-control" value="<?=$query->schedule_call_date ?>" id="datepicker" placeholder="Select date" aria-label="Username" aria-describedby="basic-addon1">
</div>
                    </div>
                        <div class="col-md-12">  
<div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">Hour</span>
  </div>
    <select class="form-control" id="hour" >
        <option value="<?=$hh[0] ?>" selected><?=$hh[0] ?></option>
        <?
        for($i=9;$i<=23;$i++)
        {
            if($i<10)
                $d="0".$i;
            else
                $d=$i;
            
        ?>
        <option value="<?=$d ?>"><?=$d ?></option>
            <?
        }
        ?>
    </select>
</div>
                        </div>
                        <div class="col-md-12">  
<div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">Minutes</span>
  </div>
    <select class="form-control" id="min" >
        <option value="<?=$hh[1] ?>" selected><?=$hh[1] ?></option>
        <?
        for($i=0;$i<=59;$i++)
        {
            if($i<10)
                $d="0".$i;
            else
                $d=$i;
            
        ?>
        <option value="<?=$d ?>"><?=$d ?></option>
            <?
        }
        ?>
    </select>
</div>
                </div>
                </div>
                <div class="input-group">
                    <input type="button" class="btn btn-success" value="Update" onclick="updatequery()"/>
                </div>    
         <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
        }
 }
 
        public function actionUpdatequery(){
            if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $date1=trim($_POST['date1']);
           $hour=trim($_POST['hour']);
           $min=trim($_POST['min']);

     $qq=new Query();
     $time=$hour.":".$min;
     $qq->updateAll(array('schedule_call_date'=>$date1,'schedule_call_time'=>$time), "qid=:q", array(':q'=>$qid));
     
     $c=new QueryChat();
     $c->chatid=  uniqid();
     $c->queryid=$qid;
     $c->senderid = Yii::app()->user->getState('user_id');
     $c->recid = "-";
     $c->message="I have changed the date and time or discussion about your query.The new updated time is on $date1 at $time";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
    $cru=new CDbCriteria();
    $cru->condition='qid=:u';
    $cru->params=array(":u"=>$qid);
    $query= Query::model()->find($cru);
     
     $c=new Notifications();
     $c->notid=  uniqid();
     $c->role = 'unit';
     $c->queryid = $qid;
     $c->ilc = $query->ilc_id;
     $c->message="Manager has changed the date and time or discussion about your query.The new updated time is on $date1 at $time";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
         $c=new Notifications();
     $c->notid=  uniqid();
     $c->role = 'partner';
     $c->queryid = $qid;
     $c->ilc = $query->ilc_id;
     $c->message="Manager has changed the date and time or discussion about the query.The new updated time is on $date1 at $time";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
 }
 
        public function actionApprovequery() {
            if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);

            $qq=new Query();
            $qq->updateAll(array("ilc_approved"=>1), 'qid=:q', array(":q"=>$qid));

             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
             }
        }

        public function actionQueryhistory() {
    $this->render('queryhistory');
}

        public function actionGetchat() {
          if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $uid=trim($_POST['uid']);
            
    $cr=new CDbCriteria();
    $cr->condition='queryid=:q';
    $cr->params=array(":q"=>$qid);
    $cr->order='date1 asc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       ?><br>
       <p>Start a conversation now!</p><?
   }
 else {
     $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
    ?>
<br>   
<p style="font-size:16px !important" ><b>Topic: </b><?=$topic ?>  </p>
<div id="chatui">
     <?
       foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
     if($uinf->role=='unit'){
           ?>
                <div class="alert alert-success talk float-left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
          if($uinf->role=='partner'){
           ?>
                <div class="alert alert-warning talk float-left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='ilcmanager'){
             ?>
                <div class="alert alert-danger talk float-right" >
    <b style="font-size:14px !important"><?=$uinf->name ?>[&nbsp;<?=$uinf->role ?>&nbsp;]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
                 <?
         }
        } 
        ?>
</div>
    <?
   }
  }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}  
}

        public function actionSendchat() {
            if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $msg=trim($_POST['msg']);

        $uid=Yii::app()->user->getState("user_id");
  
        $cru=new CDbCriteria();
        $cru->condition='userid=:r';
        $cru->params=array(':r'=>$uid);
        $resu=  Users::model()->find($cru);
        $ilcid=$resu->ilcid;
       
        $crt=new CDbCriteria();
        $crt->condition='qid=:r';
        $crt->params=array(':r'=>$qid);
        $rest=  Query::model()->find($crt);
        $qtopic=$rest->query_topic;
        $quser=$rest->userid;
        
        $crt=new CDbCriteria();
        $crt->condition='userid=:r';
        $crt->params=array(':r'=>$quser);
        $rest= Users::model()->find($crt);
        $uname=$rest->username;
        $name=$rest->name;
        
        
  
    $qc=new QueryChat();
    $qc->queryid=$qid;
    $qc->chatid=  uniqid();
    $qc->senderid=$uid;
    $qc->recid='-';
    $qc->message=$msg;
    $qc->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qc->isNewRecord=true;
    $qc->save(FALSE);
    
    $mst="There is a reply for the query '".substr($qtopic, 0, 100)."..'";
    $qn=new Notifications();
    $qn->queryid=$qid;
    $qn->notid=  uniqid();
    $qn->role="all";
    $qn->ilc=$ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Query Chat Reply";
            $msg="Dear $name,<br>
                ILC Manager has replied to your query <b>".substr($qtopic, 0, 100)."</b><br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
} 

        public function actionGetquery3() {
        if(Yii::app()->request->isPostRequest) {
           
        $uid=Yii::app()->user->getState('user_id');
        $ilcs=array();        
        ///////////////////////////////////////
        $crm=new CDbCriteria();
        $crm->condition='ilcmid=:u';
        $crm->params=array(":u"=>$uid);
        $res=  ManagerIlc::model()->findAll($crm);
        foreach ($res as $r) {
            array_push($ilcs, $r->ilcid);
        }
        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="ilc_approved=1 and query_closed=1";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <? foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                if(in_array($ilc, $ilcs))
                {
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
                    <?
                }
            }
            ?>
                </table>
           </div>
            <?
          }
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
 }
        public function actionGetimy() {
            if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);

      $y=  intval($yea);
      $m= intval($mon);
      $userid=Yii::app()->user->getState('user_id');
      
        $crq=new CDbCriteria();
        if($y!=0 && $m==0)
        {
           $crq->condition='ilcmid=:u and year=:y and iclosed=1';
        $crq->params=array(':u'=>$userid,':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
         $crq->condition='ilcmid=:u and year=:y and month=:m and iclosed=1';
        $crq->params=array(':u'=>$userid,':m'=>$m,':y'=>$y);
        }
      $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p> No Interviews have been conducted yet</p> <?
        }
        else 
        {
            ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Resume</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
//                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                ?>
            <tr>
                 <td><?=$i->candidatename ?>
                            
                        </td>
                        <td>
                            <a style="font-size:14px !important; padding:4px;border-radius:2px;border:1px solid black" class="btn-light" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a></td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         
                    </tr>

<?
            } ?>
                </table>
                    </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
 }
        public function actionGetqmy() {
            if(Yii::app()->request->isPostRequest) {
            $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);
           //echo $yea." ".$mon;

      $y=  intval($yea);
      $m= intval($mon);
      $uid=Yii::app()->user->getState('user_id');
      
        $crq=new CDbCriteria();
        if($y!=0 && $m==0)
        {
           $crq->condition='ilcmid=:u and year=:y and query_closed=1';
           $crq->params=array(':u'=>$uid,':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
         $crq->condition='ilcmid=:u and year=:y and month=:m and query_closed=1';
        $crq->params=array(':u'=>$uid,':m'=>$m,':y'=>$y);
        }
        
        
        $ilcs=array();        
        ///////////////////////////////////////
        $crm=new CDbCriteria();
        $crm->condition="ilcmid=:i";
        $crm->params=array(":i"=>$uid);
        $res= ManagerIlc::model()->findAll($crm);
        foreach ($res as $r) {
            array_push($ilcs, $r->ilcid);
        }
        //////////////////////////////////////////
        
        $crq->select="*";
        $crq->order="query_date desc";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?> <p style="margin-top:8%">No Queries were raised in this month !</p> <?
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <? foreach ($queries as $q) {
               
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                if(in_array($ilc, $ilcs))
                {
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
                    <?
                }
            }
            ?>
                </table>
           </div>
            <?
          }
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
 }
 
        public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }

        public function actionHol(){
            if(Yii::app()->request->isPostRequest) {
           
           $rolid= Yii::app()->user->getState("rolid");
           $userid=Yii::app()->user->getState('user_id');
           $ilcid=Yii::app()->user->getState('ilcid');
           
            $crq=new CDbCriteria();
            $crq->condition='userid=:u';
            $crq->params=array(':u'=>$userid);
            $crq->order='startd asc';
            $en= CalenderDate::model()->findAll($crq);
     
            
            foreach($en as $e){
                       $approve=$e->approved;
            ?>
                <h4>
                <? if($approve==0){ ?>
                    <button id="<?=$e->calid ?>" class="btn-warning but">
                     <i class="glyphicon glyphicon-tick"></i>     
                    </button>&nbsp;
                <? } 
                if($approve==1){
                    ?>
                    <button id="<?=$e->calid ?>" class="btn-success but">
                        <i class="glyphicon glyphicon-tick"></i>   
                    </button>&nbsp;
                        <?
                }
                if($approve==2){
                    ?>
                    <button  id="<?=$e->calid ?>" class="btn-danger but">
                       <i class="glyphicon glyphicon-cancel"></i>   
   
                    </button>&nbsp;
                        <?
                }
                ?>
                <?=date('d-M-Y' ,strtotime($e->startd)) ?> &nbsp;
                |<?=$e->topic ?>&nbsp;</h4>
            <?
            }
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
       
        public function actionSethol(){
            if(Yii::app()->request->isPostRequest) {
           $htopic=trim($_POST['htopic']);
           $hdate=trim($_POST['hdate']);

           $rolid= Yii::app()->user->getState("rolid");
           $userid=Yii::app()->user->getState('user_id');
           $ilcid=Yii::app()->user->getState('ilcid');
           
           $hy=date('Y',  strtotime($hdate));
           $hm=date('m',  strtotime($hdate));
           
            $crq=new CDbCriteria();
            $crq->condition='startd=:u and userid=:q';
            $crq->params=array(':u'=>$hdate ,':q'=>$userid);
            $en=CalenderDate::model()->findAll($crq);
            $nq=  sizeof($en);
            
            if($nq==0){
           
            $calid=uniqid();
            $qq=new CalenderDate();
            $qq->calid=$calid;
            $qq->startd=$hdate;
            $qq->endd=$hdate;
            $qq->topic=$htopic;
            $qq->userid=$userid;
            $qq->queryid='ilc';
            $qq->approved=0;
            $qq->year=$hy;
            $qq->mon=$hm;
            $qq->date1=intval(strtotime(date('Y-m-d H:i:s')));
            $qq->isNewRecord=true;
            $qq->save(FALSE);
            
            $mst="Please approve/disapprove IlC Manager's leave on ".$hdate;
            $qn=new Notifications();
            $qn->queryid=$userid;
            $qn->notid=  uniqid();
            $qn->role="academic";
            $qn->ilc=$calid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            $crq=new CDbCriteria();
            $crq->condition='role=:q';
            $crq->params=array(':q'=>'academic');
            $ud=Users::model()->find($crq);
            $user=$ud->userid;
            
            $usr=new Nlist();
            $uu=$usr->getuser($user);
            $uname=$uu->username;
            $name=$uu->name;
    
    
    ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Personal Leave Approval";
            $msg="Dear $name,<br>
                Please approve/disapprove IlC Manager's leave on $hdate<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            
            } 
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
       }
}