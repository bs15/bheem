<?php /* @var $this Controller */ 
  $logged=Yii::app()->user->getState('logged');
  $userid=Yii::app()->user->getState('user_id');
  $rolid=Yii::app()->user->getState("rolid");
  
  if($rolid=='director'){
      $home=Yii::app()->request->baseUrl.'/index.php/director/';
      $name="Director";
  }
  if($rolid=='academic'){
      $home=Yii::app()->request->baseUrl.'/index.php/academic/';
      $name="Academic Head";
  }
  if($rolid=='ilcmanager'){
      $home=Yii::app()->request->baseUrl.'/index.php/ilcmanager/';
      $name="Ilc Manager";
  }
  if($rolid=='unit'){
      $home=Yii::app()->request->baseUrl.'/index.php/unit/';
      $name="Unit Coordinator";
  }
  if($rolid=='partner'){
      $home=Yii::app()->request->baseUrl.'/index.php/partner/';
      $name="Partner";
  }
  if($rolid=='teacher'){
      $home=Yii::app()->request->baseUrl.'/index.php/teacher/';
      $name="Teacher";
  }
  $uu=Yii::app()->urlManager->parseUrl(Yii::app()->request);
?>
 <? if($logged=="true") { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
  
    
    
	<!-- blueprint CSS framework -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
    
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php //echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/css/bootstrap.min.css" />

<? if($uu=='unit/interview' || $uu=='ilcmanager/interview' || $uu=='ilcmanager/leave' || $uu=='partner/interview' || $uu=='academic/interview' || $uu=='director/interview' || $uu=='enquiry/index' || $uu=='admission/index' || $uu=='holiday/index' || $uu=='call/index'){ ?>	
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/vader/jquery-ui.css" integrity="sha256-MnGrvs1IqDGIBDp+bZjQfzWzGhg8jPQ+ZWFWaUPjBsA=" crossorigin="anonymous" />-->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
<? } ?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet" />
 <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    
  <style type="text/css">
      html {
          font-size: 20px !important;
      }
      .errorMessage{
          color:red !important;
      }
      .bread{
          margin-top:-2% !important;
          font-size:15px !important;
          color:grey;
      }
      .bread a {
          text-decoration:none;
          color:grey;
      }
      .bread{
          font-size:15px !important;
          color:grey;
      }
      .box{
          width:99%;
          border-radius:6px;
          color:white;
          letter-spacing:1px;
          font-weight:500;
          padding:10px;
         
      }
      .box-lav{
          background-color:#ccccff; 
      }
      .box-red{
         background-color:lightcoral; 
      }
      .box-green{
         background-color:lightgreen; 
      }
      .box-blue{
         background-color:lightskyblue; 
      }
      .box-pink{
         background-color:lightpink; 
      }
      .box-yellow{
          background-color: lightsalmon;
      }
      .box-green1{
          background-color: lightgreen;
      }
      .box-white{
          background-color:white;
      }
      .box h1{
          font-size:20px !important;
        }
      .box h4{
          font-size:18px !important;
          color:black;
          margin-top:3%;
      }
      .box p{
          font-size:14px !important;
      }
      .nav-font{
          font-size:16px !important;
          letter-spacing: 0px;
          word-spacing:1px;
      }
      .items{
          font-size:14px !important;
      }
      .table-img{
          border:1px solid grey;
          padding:4px;
          width:40px;
          height:40px;
          border-radius:30px;
      }
      .table-font{
          font-size:14px !important;
      }
      .ilc-back{
          background-color:#f4f4f4;
          border-radius:4px;
          float:right;
          
       }
      .iframe-back{
          background-color: #f4f4f4;
          border-radius:4px;
          height:450px;
          
      }
     .wrapper {
    position: relative;
    padding-bottom: 56.25%; /* 16:9 */
    padding-top: 25px;
    height: 0;
}
.wrapper iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 450px;
}
      .bold{
          font-weight: 400;
      }
      .accordian{
          margin:0 !important;
          padding:0 !important;
         
      }
      .submenu{
          background-color: #122b4a;
          padding-top:10px !important;
          padding-bottom: 10px !important;
          margin:0 !important;
          list-style-type: disc !important;
          list-style: disc !important;
          display:block !important;
          font-size:16px !important;
      }
      .btn-sidemenu i{
          margin-top:10px !important;
          font-size:10px !important;
      }
      
      .submenu li{
          display:block !important;
          color:white !important;
          font-size:14px !important ;
      }
      .submenu li i{
          font-size:8px !important;
      }
      .submenu li a{
          color:white !important;
          text-decoration: none !important;
          
      }
      .submenu li a:hover, .submenu li a:active{
          text-decoration: underline !important;
          color:yellow !important;
      }
      .btn-sidemenu{
          width:100%;
          text-align: left;
          
          background-color:transparent;
          color:#FFD324;
          border:0;
          font-size: 16px !important;
          padding-bottom: 7px!important;
          
          }
      .bb{
          width:100%;
          height:8px;
          border-bottom:1px solid #255899;
      }
      .bg-sidemenu{
/*          background-color:#3173c8;*/
          margin-top:-0%;
          background-color:#0e213a;
          min-height: 90vh !important;
          margin-bottom:-1%;
      }
      #silc{
          margin-left:-8%;
          margin-bottom: 2%;
      }
      .but{
          font-size:14px !important;
          padding:2px;
          border-radius:20px;
          width:20px;
          height:20px;
          padding-top:-6% !important;
      }
      .admpic{
          width:50px;
          height:50px;
          border-radius:50%;
          padding:3px;
          border:1px solid black;
      }
      
      .gap{
          width:100%;
          height:70px;
          clear:both;
      }
      .enin{
          border:1px solid lightgrey;
          padding-left:4px;
          height:30px;
          width:100%;
          font-size:16px !important;
          margin-bottom:10px;
          border-radius:4px;
          box-shadow: 3px 2px 0 0 lightgrey;
      }
      .enin:active{
         box-shadow: 0 4px 4px 0 lightgreen; 
      }
      .subhead{
          margin-top:3%;
          margin-bottom:15px;
          margin-left:4%;
          
      }
      h5:after{
          background-color:#16a085;
          height:2px;
          width:30%;
      }
      .back-grey{
          margin-left:5%;
          background-color:#f4f4f4!important;
          border:1px solid lightgrey;
          border-radius:5px;
          margin-right:-1%;
          height:38px;
      }
      .back-grey1{
          margin-left:1%;
          background-color:#f4f4f4!important;
          border:1px solid lightgrey;
          border-radius:5px;
          margin-right:-1%;
          height:38px;
      }
    #customBtn {
      display: inline-block;
      background: white;
      color: #444;
      /*width: 190px;*/
      width: 90%;
      border-radius: 5px;
      border: thin solid #888;
      box-shadow: 1px 1px 1px grey;
      white-space: nowrap;
    }
    #customBtn:hover {
      cursor: pointer;
    }
    span.label {
      font-family: serif;
      font-weight: normal;
    }
    span.icon {
      background: url('/identity/sign-in/g-normal.png') transparent 5px 50% no-repeat;
      display: inline-block;
      vertical-align: middle;
      width: 42px;
      height: 42px;
    }
    span.buttonText {
      display: inline-block;
      vertical-align: middle;
      padding-left: 42px;
      padding-right: 42px;
      font-size: 14px;
      font-weight: bold;
      /* Use the Roboto font that is loaded in the <head> */
      font-family: 'Roboto', sans-serif;
    }
    #alert{
        width: 100% !important;
        padding: 10px 0px;
        
    }
    .profphoto{
        width:150px;
        height:150px;
        padding:10px;
        border-radius: 50%;
    }
    .heading2{
        word-spacing: 2px;
        letter-spacing:1px;
        font-size: 40px;
        text-rendering: optimizeLegibility;
        font-weight:300;
        margin-top: 5%;
        
    }
   
      @media (min-width:320px) and (max-width:480px) { 
          .heading2{
        word-spacing: 2px;
        letter-spacing:1px;
        font-size: 40px;
        text-rendering: optimizeLegibility;
        font-weight:300;
        margin-top: 15%;
        
      }
      .bg-sidemenu{
          display:none;
      }
      }
      @media (min-width:481px) and (max-width:768px) { /* portrait e-readers (Nook/Kindle), smaller tablets @ 600 or @ 640 wide. */ 
       .bg-sidemenu{
          display:none;
      }
          .heading2{
        word-spacing: 2px;
        letter-spacing:1px;
        font-size: 40px;
        text-rendering: optimizeLegibility;
        font-weight:300;
        margin-top: 15%;
    }
   }
    .color-white {
        color: white !important;
        font-size: 17px;
        text-decoration: underline;
        margin: 2px;
    }
  
    .padding{
        padding:15px;
    }
    .mar-l{
        margin-left:4% !important;
    }
    .iheight{
        margin-top: -0% !important;
        border-radius: 5px;
    }
   #left .card-body p {
        font-size: 70% !important;
        line-height: 90% !important;
    }
    #min-gal img {
        width: 31%;
        height: 60px;
        padding: 1px;;
        margin: 1px;
        background-color: grey;
    }
    #min-gal , #min-gal a {
        width: 100%;
        height: auto;
    }
    #network a {
        width: 31%;
        height: auto;
        float: left;
    }
    #network a img {
        width: 98%;
        height: 60px;
        background-color: grey;
        padding: 1px;
        margin: 1px;
        border: 1px solid black;
    }
    #network a p {
        width: 98%;
        
        /*padding:3px 26%;*/
        color: white;
        background-color: black;
        text-align: center;
        font-size: 80%;
    }
    #social a {
        display: inline-block;
        float: left;
        margin: 5px;
    }
    .img {
        width: 100%;
        height: 200px;
    }
    #ffeed div {
        font-size: 80%
    }
    #ffeed div span {
        font-size: 50%;
        color: #000;
    }
    #ffeed div img {
        width: 90%;
        height: auto;
    }
     #nwfeed div {
        font-size: 80%
    }
    #nwfeed div span {
        font-size: 50%;
        color: #000;
    }
    #nwfeed div img {
        width: 90%;
        height: auto;
    }
    #mynet a {
        text-decoration: none;
        width: 100%;
    }
    #mynet a img {
        width: 100%;
        height: 180px;
    }
    #mynet a p {
        width: 100%;
        background-color: black;
        color: white;
        padding: 5px;
        text-align: center;
    }
    #mynet a p span {
        font-size: 65%;
        font-style: italic;
        margin-left: 20%;
    }
      .back{
          background: rgba(180,198,240,1);
background: -moz-linear-gradient(left, rgba(180,198,240,1) 0%, rgba(246,246,246,0.7) 53%, rgba(237,237,237,0.43) 100%);
background: -webkit-gradient(left top, right top, color-stop(0%, rgba(180,198,240,1)), color-stop(53%, rgba(246,246,246,0.7)), color-stop(100%, rgba(237,237,237,0.43)));
background: -webkit-linear-gradient(left, rgba(180,198,240,1) 0%, rgba(246,246,246,0.7) 53%, rgba(237,237,237,0.43) 100%);
background: -o-linear-gradient(left, rgba(180,198,240,1) 0%, rgba(246,246,246,0.7) 53%, rgba(237,237,237,0.43) 100%);
background: -ms-linear-gradient(left, rgba(180,198,240,1) 0%, rgba(246,246,246,0.7) 53%, rgba(237,237,237,0.43) 100%);
background: linear-gradient(to right, rgba(180,198,240,1) 0%, rgba(246,246,246,0.7) 53%, rgba(237,237,237,0.43) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4c6f0', endColorstr='#ededed', GradientType=1 );
      }
      .login-img{
          width: 100%;
          height: auto;
          margin-top: 0%;
          margin-left: 0%;
      }
      .login-font{
          font-size:17px;
      }
      .login-btn{
          width: 75%;
          
      }
      .social-i{
          color:lightgrey;
          margin-right: 20px;
          margin-top: 5%;
      }
      .social-i-footer{
          color:white;
          margin-left: 20px;
          margin-top: 10px;
      }
      .social-i:hover{
          color:white;
      }
      .page-footer{
          background-color: #e1c808;
          padding-top: 2% !important;
      }
      .footer-back{
          background-color: #0e213a;
          border-top:2px solid yellow;
          
      }
      .footer-back p{
          margin-top:1%;
          color:lightgrey;
         font-size:14px !important; 
      }
      .white{
          color: white;
          padding: 3px;
      }
      .text-decor{
          text-decoration: none;
      }
      .btn-teal{
          background-color:#16a085;
          border:1px solid #16a085 !important;
      }
      .card-img-top{
          height: 200px;
      }
      .testi-img{
          width:100px;
          height:100px;
          border-radius:200px;
      }
      .btn-light{
          background-color: transparent;
          border:0;
      }
      .err { clear: both;
      color: red;
      display: block;
      width: 100%;
      padding: 10px;
      margin: 5px;
      }
      .cbody {
          min-height: 60vh;
      } 
      .fixed-top{
          z-index: 100 !important;
      }
      #datepicker{
          z-index: 9997 !important;
      }
      #datepicker1{
          z-index: 9997 !important;
      }
      #ilcmlist {
          font-size: 95% !important;
      }
      #qchat {
          min-height: 400px;
          height: 350px;
          /*overflow: scroll;*/
          
      }
     
      
      #chatui{
/*          display:flex;
          flex-direction:column-reverse;*/
          overflow: scroll;
          height: 350px;
      }
      .float-left{
          left:0 !important;
      }
      .float-right{
         right:0 !important;
      }
      .talk {
          width: 70% !important;
          padding: 5px !important;
          margin: 10px !important;
      }
      .talk i {
          font-size: 12px;
      }
       *{
   font-size: 100% !important;
    /*line-height: 12px !important;*/
   }
   input, .form-control, .input-group, .form-group, .card-header, .card-body, .card-footer{
       line-height: 18px !important;
   }
   #page {
       min-height: 70vh !important;
   }
   #nlist span{
       float:right !important;
       margin-top: -1% !important;
   }
   #cnum{
       font-size: 12px !important;
       width: 15px !important;
       height:15px !important;
   }
   
   .list .card-header, .list .card-body, .list .card-footer {
          line-height: 18px !important;
   }
   .fchat {
       width: 99%;
       display: block;
       height: auto;
   }
   section {
       display: block;
       height: auto;
   }
   .dashmenu {
       background-color: #81dd2c;
       padding: 20px;
       height: auto;
       min-height:200px;
       width: 100%;
   }
   .tit {
       text-align: center;
       margin-top: 25%;
       font-size: 300% !important;
   }
   .lg {
       background-color: greenyellow !important;
       border:2px solid darkgreen;
       color: darkgreen;
       width:98%;
   }
   .dg {
       background-color: darkgreen !important;
       border:2px solid greenyellow;
       color: greenyellow;
       width:98%;
   }
   .pdl {padding-left: 0%;
   }
   .rt h1 , .rt p , .lt h1 , .lt p {
       padding: 5px;
       color: dargrey;
   }
   .rt h1 , .lt h1 {
       text-align: center;
   }
   .lt h1 {
       margin-top: 15%;
   }
   .rt p , .lt p {
       text-align: left;
       font-size: 17px !important;
       font-style: italic;
   }
   .rt , .lt {
       border: 1px solid lightgray;
   }
   .dashbrd {
       
   }
   #fn {
       display: block;
       clear: both;
   }
    #fn {
       display: block;
       clear: both;
   }
   .post-form {
       margin-top:1%;
       background-color:#f4f4f4;
       padding: 5px;
       border: 1px solid lightgrey;
   }
   .srcfrm {
       margin-top:4%;
       background-color: #f4f4f4;
       padding: 5px;
       border: 1px solid lightgrey;
   }
   .pfoto {
       width: 45px;
       height: 45px;
       border-radius: 50%;
-moz-border-radius: 50%;
-webkit-border-radius: 50%;
border: 0px solid #000000;
   }
    .pfoto2 {
       width: 22px;
       height: 22px;
       border-radius: 50%;
-moz-border-radius: 50%;
-webkit-border-radius: 50%;
border: 0px solid #000000;
   }
   .hhh {font-size:  14px !important;}
   .pdate {
       font-size: 12px !important;
       font-weight: bold !important;
   }
   .tt {
       color: black;
       text-decoration: underline;
       font-weight: 700;
   }
   .cimg {
       display: block;
       width: 65%;
       height: auto;
       clear: both;
       text-align: center;
   }
   .ct {
       font-size: 16px !important;
       letter-spacing: 1px !important;
   }
   .ctt {
       display: block !important;
       width: 99% !important;
       clear: both;
   }
   .pull-left {float: left !important;}
   .pull-right {float: right !important;}
   .bt{
       padding:6px !important;
       border-radius:5px !important;
   }
   .slist{
       list-style: none;
       width:300px;
       border:1px solid lightgrey;
   }
   .slist li{
       border-bottom:1px solid lightgrey;
       font-size:14px;
       text-transform: uppercase;
   }
   #silc1{
       margin-left: -4%;
   }
   .loader {
       position: absolute !important;
/*       margin: 0 auto !important;*/
       width: 30% !important;
       top: 35% !important;
       left: 35% !important;
       z-index: 998 !important;
       padding: 5px;
        -webkit-box-shadow: 3px 3px 5px 6px #ccc;  /* Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ */
  -moz-box-shadow:    3px 3px 5px 6px #ccc;  /* Firefox 3.5 - 3.6 */
  box-shadow:         3px 3px 5px 6px #ccc;  /* Opera 10.5, IE 9, Firefox 4+, Chrome 6+, iOS 5 */
   }
   .fc-day-content {
       font-size: 15px !important;
   }
   #holcal{
       background-color:#f4f4f4;
       padding:4px;
       border-radius:4px;
       font-size:14px !important;
   }
   #holcal h2{
       border-bottom:none;
       margin-bottom:0 !important;
   }
   #holcal h2:after{
       background-color:transparent !important;
       height:0 !important;
       margin:0 !important;
   }
   .hpal {
       clear: both;
       margin-top: 1%;
       list-style: none;
       margin-left:-3%;
       padding-bottom:2%!important;
       font-size:14px !important;
   }
   .hpal li { float: left}
   .bred,.reddd {
       
       width: 10px !important;
       height: 10px !important;
       margin:5px;
       background-color: red;
        border-radius: 10px;
       float: left
   }
   
     .byel {
       width: 10px;
       height: 10px;
       background-color: green;
       margin: 5px;
        border-radius: 10px;
       float: left
   }
     .bora {
       width: 10px;
       height: 10px;
       background-color: orange;
        border-radius: 10px;
       margin: 5px;
       float: left
   }
   .bblu 
    {
       width: 10px;
       height: 10px;
       background-color: blue;
       margin: 5px;
        border-radius: 10px;
       float: left
   }
   .bbla 
    {
       width: 10px;
       height: 10px;
       background-color: black;
       margin: 5px;
        border-radius: 10px;
       float: left
   }
   .bgre 
    {
       width: 10px;
       height: 10px;
       background-color: green;
       margin: 5px;
        border-radius: 10px;
       float: left
   }
   .by 
    {
       width: 10px;
       height: 10px;
       background-color: yellow;
       margin: 5px;
        border-radius: 10px;
       float: left
   }
   .bpur 
    {
       width: 10px;
       height: 10px;
       background-color: purple;
       margin: 5px;
        border-radius: 10px;
       float: left
   }
   .home-l{
/*       margin-left:-1% !important;
       background-color: #FFD324;
       width:16%;
       padding:14px;
       margin:0;
       color:black !important;*/
   }
      .fc-day-content {
       width: 100% !important;
   }
   .fc-day-content div {
       float: right !important;
       width: 20px !important;
       overflow:hidden !important;
       clear: none !important;
/*       height: 15px !important;
       display: inline !important;
       position: absolute !important;
       border-radius: 50% !important;
       overflow: hidden !important;*/
   }
   .fc-event-time {
       display: none !important;
   }
   .fc-event , .fc-event-inner {
       clear: none !important;
       float: left !important;
   }
   .fxform {
       width: 65% !important;
/* -webkit-box-shadow: 3px 3px 5px 6px #ccc;   Safari 3-4, iOS 4.0.2 - 4.2, Android 2.3+ 
  -moz-box-shadow:    3px 3px 5px 6px #ccc;   Firefox 3.5 - 3.6 
  box-shadow:         3px 3px 5px 6px #ccc;   Opera 10.5, IE 9, Firefox 4+, Chrome 6+, iOS 5 */
padding: 10px;
margin: 5px;
background-color: #fffff;
border: 1px solid lightgray;
   }
   .fxform label {
       font-size: 11px !important;
   }
   .fxform input {border: 1px solid lightgray;}
   .ft {
       width: 80% !important;
   }
   .btn-sm {
       /*font-size: 11px;*/
   }
   .er {
       display: block;
       color: red;
       font-size: 10px !important;
       margin-top: 5px;
       width: 99% !important;
   }
   .btn-ss {
       background-color: #3173c8;
       color: white;
       font-size: 11px !important;
   }
   .ts {
       line-height: 20% !important;
       font-size: 80% !important;
   }
   .detail-view {
    line-height: 40% !important;
       font-size: 55% !important;
   }
   .navbar {
/*       padding-top:-15px !important;*/
       height: 40px !important
   }
   .nav-item img {
/*       margin-top: 12px !important;*/
   }
   .uil, .uil span {
       font-size: 80% !important;
       margin-top: 1px !important;
   }
   .usrnm {
       margin-top:-2% !important;
       margin-left:23% !important;
       font-size: 60% !important; 
   }
   .gapx {
       height: 36px !important;
       display: block !important;
   }
   ::-webkit-input-placeholder { /* Edge */
  color: gray !important;
  font-size: 14px !important;
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: gray !important;
  font-size: 14px !important;
}

::placeholder {
  color: gray !important;
  font-size: 14px !important;
}
.btn-green{
    background-color:limegreen;
    padding:5px;
    color:white;
    font-size:15px !important;
    border-radius:5px;
    border:1px solid limegreen
}
.btn-danger{
    background-color:red;
    padding:5px;
    color:white;
    font-size:15px !important;
    border-radius:5px;
    border:1px solid red
}
.br{
    border:1px solid lightgrey;
    border-radius:2px;
    padding:5px 0 0 10px;
    width:32%;
    margin-right:1%;
    background-color:#f4f4f4;
    margin-top:-1%;
}
.font14{
    font-size:14px!important;
}
.font18{
    font-size:18px!important;
}
.font16{
    font-size:16px!important;
}
.prof{
    padding:5px;
    font-size:16px !important;
    background-color:black;
    border-radius:5px;
    color:white;
    font-weight:500;
}
.uph {
    margin-top:5%;
    width: 25px;
    height: 25px; 
    border-radius: 50%;
}
.up {
    width: 140px;
    height: 140px; 
    border-radius:3%;
}
.imm {
    padding-right: 60px;
}
.cn {
       font-size: 14px !important;
       width: 200px !important;
       padding-left:10px !important;
       color:black;
       display: block;
   }
   
#nlist2 .dropdown-item {
   width:285px !important;
   
}
.noti{
    max-height:80vh !important;
   overflow: scroll !important;
}

#notext a {
    width:285px !important;
   
}
.accordian button , .accordian ul li a , .btn-sidemenu , .submenu ul li a {
    text-transform: capitalize !important;
}
.navbar-dark{
   background-color:#0e213a !important;
}
   .ferr {
    display: block;
    color: red;
    padding: 5px;
}
.forgot {
    font-size: 11px !important;
    font-weight: 600;
    text-decoration: none ;
}
.forgot:hover {
    text-decoration: none;
}
  </style>
  <link href="https://unpkg.com/ionicons@4.4.2/dist/css/ionicons.min.css" rel="stylesheet" />
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
   <header>
        <!--<div class="container">-->
           
        <!--</div>-->    
        
        <nav class="navbar navbar-default fixed-top navbar-expand-sm  navbar-dark bg-dark ">
                <a class="home-l" href="<?=$home ?>">
                    <img src="<?=Yii::app()->request->baseUrl ?>/images/logo2.png" class="imm" />
                </a>
            <? if($logged=="true") {
               
            $us=new CDbCriteria();
            $us->condition="userid=:r";
            $us->params=array(":r"=>$userid);
            $usp= Userphoto::model()->find($us);
       
                ?>
            <a class="navbar-brand nav-link color-white uil" style="font-size:14px;text-decoration:none;" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/profile/">
            <img src="<?= Yii::app()->request->baseUrl.'/userphoto/'.$usp->uphoto ?>" class="uph">
            <?=Yii::app()->user->getState('name') ?><br>
                <span class="usrnm"><?=$name ?></span>
           </a>
            <? } ?>
        
            
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon">
          <i class="glyphicon glyphicon-align-justify"></i>
      </span>
  </button>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="width:99% !important">
      
    <ul class="nav navbar-nav pull-right">
        
        
        <?php
          switch($rolid)
          {
                case "academic":
          {
      $mh=new Nlist();
      $mh->headergap();
       if($logged=="true") { 
        ?>
          <li  class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="glyphicon glyphicon-bell"></i><span id="cnum"></span>
        </a>
        <div id="notext" class="noti dropdown-menu" aria-labelledby="navbarDropdown">
            <div id="nlist2">
                
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
            </div>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications">View All</a>
        </div>
      </li>
        
        <?
}
?>  
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout">
            <i class="glyphicon glyphicon-off">
            </i>
        </a>
      </li> 
        <?php
                  break;
          }
               case "ilcmanager":
          {
      $mh=new Nlist();
      $mh->headergap();
       if($logged=="true") { 
        ?>
          <li  class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="glyphicon glyphicon-bell"></i><span id="cnum"></span>
        </a>
        <div id="notext" class="noti dropdown-menu" aria-labelledby="navbarDropdown">
            <div id="nlist2">
                
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
            </div>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications">View All</a>
        </div>
      </li>
        
        <?
}
?>  
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout">
            <i class="glyphicon glyphicon-off">
            </i>
        </a>
      </li> 
        <?php
                  break;
          }
               case "partner":
          {
      $mh=new Nlist();
      $mh->headergap();
       if($logged=="true") { 
        ?>
          <li  class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="glyphicon glyphicon-bell"></i><span id="cnum"></span>
        </a>
        <div id="notext" class="noti dropdown-menu" aria-labelledby="navbarDropdown">
            <div id="nlist2">
                
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
            </div>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications">View All</a>
        </div>
      </li>
        
        <?
}
?>  
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout">
            <i class="glyphicon glyphicon-off">
            </i>
        </a>
      </li> 
        <?php
                  break;
          }
             case "unit":
          {
      $mh=new Nlist();
      $mh->headergap();
       if($logged=="true") { 
        ?>
          <li  class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="glyphicon glyphicon-bell"></i><span id="cnum"></span>
        </a>
        <div id="notext" class="noti dropdown-menu" aria-labelledby="navbarDropdown">
            <div id="nlist2">
                
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
            </div>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications">View All</a>
        </div>
      </li>
        
        <?
}
?>  
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout">
            <i class="glyphicon glyphicon-off">
            </i>
        </a>
      </li> 
        <?php
                  break;
          }
             case "teacher":
          {
      $mh=new Nlist();
      $mh->headergap();
       if($logged=="true") { 
        ?>
          <li  class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="glyphicon glyphicon-bell"></i><span id="cnum"></span>
        </a>
        <div id="notext" class="noti dropdown-menu" aria-labelledby="navbarDropdown">
            <div id="nlist2">
                
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
            </div>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications">View All</a>
        </div>
      </li>
        
        <?
}
?>  
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout">
            <i class="glyphicon glyphicon-off">
            </i>
        </a>
      </li> 
        <?php
                  break;
          }
            case "director":
          {
      $mh=new Nlist();
      $mh->headergap();
       if($logged=="true") { 
        ?>
          <li  class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="glyphicon glyphicon-bell"></i><span id="cnum"></span>
        </a>
        <div id="notext" class="noti dropdown-menu" aria-labelledby="navbarDropdown">
            <div id="nlist2">
                
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
            </div>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications">View All</a>
        </div>
      </li>
        
        <?
}
?>  
      <li class="nav-item">
        <a class="nav-link active" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/logout">
            <i class="glyphicon glyphicon-off">
            </i>
        </a>
      </li> 
        <?php
                  break;
          }
         default :{
    ?>
        <li class="nav-item ">
        <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/">Home</a>
      </li>
        <li class="nav-item ">
        <a class="nav-link" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/login">Login</a>
      </li>
        <?php 
          } //default
          ?>
       
        
          
   <?   } // switch
         ?>
        
    </ul>
      
   
<!--    <form class="form-inline my-2 my-lg-0">
      <i class="fab fa-facebook-f social-i"></i>
        <i class="fab fa-twitter social-i"></i>
        <i class="fab fa-instagram social-i"></i>
        <i class="fab fa-google-plus-g social-i"></i>
        <i class="fab fa-linkedin social-i"></i>
        
        
    </form>-->
  </div>
</nav>
    </header>
    
    
<div  id="back ">
<div class="gapx"></div>

<div  id="page" >
            <div class="row" style="margin:0">
                <?//$rolid."j" ?>
                
<!------------------------Sidemenu--------------------------->
                
                
                <? if($logged==true && $rolid!='not_valid'){ ?>
                <div class="col-md-2 bg-sidemenu hidden-sm" >
                    <!--Accordian SidemenuAccordian-->
                 <? if($rolid=='unit'){ ?>   
                    <div class="accordion" id="accordionExample">
                        <button class="btn btn-sidemenu">
                            <strong><a href="/training" style="text-decoration: none; color: yellow" target="_blank">Training Program</a></strong>
                        </button> 
                        <div class="bb"></div>
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            ADMINISTRATION
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                         &nbsp;
                        </button>  
                        <div id="collapseFour" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>">New Enquiry Form</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>">Enquiry Management</a>
                            </li>
                             <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/admission/index"?>">Admission Records</a>
                            </li>
                             </ul>
                        </div>
                        <div class="bb"></div>
                            
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        TASK SCHEDULER
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                         &nbsp;
                        </button>  
                        <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/unit/query"?>">Post Query</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/unit/interview"?>">Schedule Interview</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/call/index"?>">Call Scheduler</a>
                            </li>
                            
                                          
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        
                      
                      <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                        Archives
                        <span style="float:right"><i class="glyphicon glyphicon-play" ></i></span>
                      </button>  
                        <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/unit/queryhistory"?>">Query Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/unit/interview2"?>">Interview Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/archive"?>">Enquiry Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/admission/archive"?>">Admission Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/call/archive"?>">Call Archive</a>
                            </li>  
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseten" aria-expanded="true" aria-controls="collapseten">
                        Query Search
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                         &nbsp;
                        </button>  
                        <div id="collapseten" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/querysearch/"?>">Search</a>
                            </li>
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                          Reports
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseSeven" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/ilc"?>">Ilc Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/rating"?>">Rating Report</a>
                           </li>
                            
                         </ul>
                        </div>
                        <div class="bb"></div>
                    </div>
                 <? } 
                    if($rolid=='teacher'){
                        ?>
                    <div class="accordion" id="accordionExample">
                        <button class="btn btn-sidemenu">
                            <strong><a href="/training" style="text-decoration: none; color: yellow" target="_blank">Training Program</a></strong>
                        </button> 
                        <div class="bb"></div>
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Query
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                         &nbsp;
                        </button>  
                        <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/teacher/querylist"?>">Manage</a>
    </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/teacher/queryhistory"?>">Archive</a>
    </li>
                         </ul>
                        </div>
                        <div class="bb"></div>
                        
</div>
                        <?
                    }
                    if($rolid=='partner'){
                        ?>
                        <div class="accordion" id="accordionExample">
                        <button class="btn btn-sidemenu">
                            <strong><a href="/training" style="text-decoration: none; color: yellow" target="_blank">Training Program</a></strong>
                        </button> 
                        <div class="bb"></div>  
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <strong>Master Data<br> Management</strong>
                            <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                            &nbsp;
                        </button>  
                        <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/partner/unit"?>">Create Coordinator</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/partner/teacher"?>">Create Teacher</a>
                            </li>
                           </ul>
                        </div>
                        <div class="bb"></div>  
                        
                         <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            ADMINISTRATION
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                         &nbsp;
                        </button>  
                        <div id="collapseFour" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>">New Enquiry Form</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>">Enquiry Management</a>
                            </li>
                             <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/admission/index"?>">Admission Records</a>
                            </li>
                             </ul>
                        </div>
                        <div class="bb"></div>
                            
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        TASK SCHEDULER
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                         &nbsp;
                        </button>  
                        <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/partner/querylist"?>">Post Query</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/partner/interview"?>">Schedule Interview</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/call/index"?>">Call Scheduler</a>
                            </li>
                            
                                          
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        
                      
                      <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                        Archives
                        <span style="float:right"><i class="glyphicon glyphicon-play" ></i></span>
                      </button>  
                        <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/partner/queryhistory"?>">Query Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/partner/interview2"?>">Interview Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/archive"?>">Enquiry Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/admission/archive"?>">Admission Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/call/archive"?>">Call Archive</a>
                            </li>  
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                       
                        
                        
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseten" aria-expanded="true" aria-controls="collapseten">
                        Query Search
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                         &nbsp;
                        </button>  
                        <div id="collapseten" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                                &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/querysearch/"?>">Search</a>
                            </li>
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                          Reports
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseSeven" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/ilc"?>">Ilc Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/rating"?>">Rating Report</a>
                           </li>
                            
                         </ul>
                        </div>
                        <div class="bb"></div>
  
</div>
                            <?
                    }
                    if($rolid=='ilcmanager'){
                        ?>
                        <div class="accordion" id="accordionExample">
                         
                            
                         <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseq" aria-expanded="true" aria-controls="collapseq">
                             <strong>Internal <br>Management</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseq" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;<a  href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/leave"?>">Leave Management</a>
                         </li>
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/manage"?>">View Ilc</a>
                         </li>
                         
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/communicator/"?>">Communicator</a>
                        </li>
                        </ul>
                        </div>
                        <div class="bb"></div>  
                            
                        
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                       Management
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/query"?>">Query</a>
                         </li>
                          <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;<a href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/interview/"?>">Interview</a>
                         </li>   
                        <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>">Enquiry </a>
                        </li>
                          <li><i class="glyphicon glyphicon-play"></i>
                            &nbsp;
                                <a href="<?=Yii::app()->request->baseUrl."/index.php/admission/index"?>">Admission</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                            &nbsp;
                            <a  href="<?=Yii::app()->request->baseUrl."/index.php/call/index"?>">Call</a>
                            </li>  
                         
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        Archives
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/queryhistory"?>">Query Archive</a>
                         </li>
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/ilcmanager/interview2/"?>">Interview Archive</a>
                         </li>
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/archive"?>">Enquiry Archive</a>
                         </li>
                            <li>
                             <i class="glyphicon glyphicon-play"></i>
                            &nbsp;  
                            <a href="<?=Yii::app()->request->baseUrl."/index.php/admission/archive"?>">Admission Archive</a>
                            </li>
                            <li>
                             <i class="glyphicon glyphicon-play"></i>
                            &nbsp;  
                            <a href="<?=Yii::app()->request->baseUrl."/index.php/call/archive"?>">Call Archive</a>
                            </li>
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
                        Query Search
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseNine" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                          <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/querysearch/"?>">Search</a>
                         </li>  
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                          Reports
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseSeven" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/ilc"?>">Ilc Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/escalation"?>">Escalation Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/rating"?>">Rating Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/weekly"?>">Weekly Report</a>
                           </li>
                         </ul>
                        </div>
                        <div class="bb"></div>
                        
                         
                        
                        </div>
                        <?
                    }
                    if($rolid=='academic'){
                        ?>
                        <div class="accordion " id="accordionExample" >
                            
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <strong>Master Data <br>Management</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;Create
                           <ul>
                               <li>-
                                  &nbsp;
                                  <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/ilcmanager"?>">Ilc Manager</a>
                                </li>
                               <li>-
                             &nbsp;
                         <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/partner"?>">Partner</a>
                         </li>
                            
                          <li>-
                             &nbsp;
                          <a href="<?=Yii::app()->request->baseUrl."/index.php/ilc/create"?>">Ilc</a>
                          </li>
                           </ul>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;Manage
                           <ul>
                               <li>-
                           &nbsp;
                            <a href="<?=Yii::app()->request->baseUrl."/index.php/ilc/ilclist"?>">Ilc</a>
                           </li>
                             <li>-
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/assignilc"?>">Assign</a>
                           </li>
                           </ul>  
                            </li>
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <strong>Internal <br>Management</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                             <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/holiday/index"?>">Holiday Management</a>
                           </li>
                             <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/mleave"?>">Leave Management</a>
                           </li>
                             <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/mcal"?>">View Schedule</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/communicator/"?>">Communicator</a>
                           </li>
                         </ul>
                        </div>
                        <div class="bb"></div>
                       
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            Escalations
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                            <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/queryesc"?>">Query Escalation</a>
                         </li>
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a  href="<?=Yii::app()->request->baseUrl."/index.php/academic/interview/"?>">Interview Escalation</a>
                         </li>
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>">Enquiry Escalation</a>
                         </li>
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a  href="<?=Yii::app()->request->baseUrl."/index.php/admission/index"?>">Admission Escalation</a>
                         </li> 
                         <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a  href="<?=Yii::app()->request->baseUrl."/index.php/call/index"?>">Call Escalation</a>
                           </li>
                         
                         </ul>
                         </div>
                        <div class="bb"></div>    
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            Archives
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseFour" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/queryhistory"?>">Query Archive</a>
                         </li>
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/academic/interview2/"?>">Interview Archives</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/archive"?>">Enquiry Archive</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/admission/archive"?>">Admission Archive</a>
                           </li>
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/call/archive"?>">Call Archive</a>
                           </li>
                          </ul>
                         </div>
                        <div class="bb"></div> 
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                            Query Search
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseFive" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           
                        <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/querysearch/"?>">Search</a>
                         </li>
                        </ul>
                        </div>
                        <div class="bb"></div> 
                        
                        
                        
                         <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                          Reports
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseSeven" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/ilc"?>">Ilc Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/escalation"?>">Escalation Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/rating"?>">Rating Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/weekly"?>">Weekly Report</a>
                           </li>
                         </ul>
                        </div>
                        <div class="bb"></div>
                        </div>
                            <?
                    }
                    if($rolid=='director'){
                        ?>
                        <div class="accordion" id="accordionExample">
                            
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseq" aria-expanded="true" aria-controls="collapseq">
                            <strong>Internal</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseq" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/holiday/index"?>">Holiday Management</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/director/mcal"?>">View Schedule</a>
                           </li>
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/communicator/"?>">Communicator</a>
                            </li> 
                               <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/director/userpass/"?>">User Password</a>
                            </li> 
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseSeven">
                            <strong>Escalations</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a  href="<?=Yii::app()->request->baseUrl."/index.php/director/queryesc"?>">Query Escalation</a>
                           </li>
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/director/interview"?>">Interview Escalation</a>
                           </li>
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/index"?>">Enquiry Escalation</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/admission/index"?>">Admission Escalation</a>
                           </li>
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/call/index"?>">Call Escalation</a>
                           </li> 
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <strong>Archives</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/director/queryhistory"?>">Query Archives</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/director/interview2"?>">Interview Archives</a>
                            </li>
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/enquiry/archive"?>">Enquiry Archives</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/admission/archive"?>">Admission Archives</a>
                            </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/call/archive"?>">Call Archives</a></li>
                             </li>
                        </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseSeven">
                            <strong> Query Search</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/querysearch/"?>">Search</a>
                           </li>
                         </ul>
                        </div>
                        <div class="bb"></div>
                        
                        <button class="btn btn-sidemenu" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                            <strong>Reports</strong>
                        <span style="float:right"><i class="glyphicon glyphicon-play"></i></span>
                        &nbsp;
                        </button>  
                        <div id="collapseSeven" class="collapse" data-parent="#accordionExample">
                        <ul class="submenu">
                           <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/ilc"?>">Ilc Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/escalation"?>">Escalation Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/rating"?>">Rating Report</a>
                           </li>
                            <li><i class="glyphicon glyphicon-play"></i>
                           &nbsp;
                           <a href="<?=Yii::app()->request->baseUrl."/index.php/report/weekly"?>">Weekly Report</a>
                           </li>
                         </ul>
                        </div>
                        <div class="bb"></div>
                        
                        
                        
                        
                        </div>
                    
                        <?
                    }
                 ?>
                    
                    <!--end accordian-->
                    
                </div>
                <? } ?>
                <div class="col-md-10">
                    <div class="clear"></div>
	<?php echo $content; ?>

	<div class="clear"></div></div>
            </div>


	

</div>
        
        </div>
   
   <footer >
          
       
          <div class="footer-copyright footer-back">
            <div class="container">
                <p>
             Copyright © 2019-<?=date("Y") ?> BeanStalk All Rights Reserved.
             </p>   
            
<!--            <i class="fab fa-linkedin social-i-footer right "></i>
                <i class="fab fa-google-plus-g social-i-footer right "></i>
                <i class="fab fa-instagram social-i-footer right"></i>
                <i class="fab fa-twitter social-i-footer right"></i>
                <i class="fab fa-facebook-f social-i-footer right"></i>-->
            </div>
          </div>
        </footer>
    
    <div class="loader">
        <div class="alert-success" style="text-align: center">
            LOADING...
        </div>
    </div>

  <?php }else{
    echo $content;
  } ?>
<?   Yii::app()->clientScript->registerCoreScript("jquery"); ?>
   
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/ionicons@4.4.2/dist/ionicons.js"></script>
<!--<script src="<?php //echo Yii::app()->request->baseUrl; ?>/lightbox/js/jquery.lightbox.min.js"></script>-->

<? if($uu=='unit/interview' || $uu=='ilcmanager/interview' || $uu=='ilcmanager/leave' || $uu=='partner/interview' || $uu=='academic/interview' || $uu=='director/interview' || $uu=='enquiry/index' || $uu=='admission/index' || $uu=='holiday/index' || $uu=='call/index'){ ?>
<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<? } ?>
<script>
 
   
$(document).ready(function(){
    
 //   var sn = setInterval(function(){
      //checknoti();
//},3000); 
$(".loader").hide();
 //checknoti();
 <?php
        if($logged=="true"){
        ?>
   $.when(checknotinum()).then(function(){
  $.when(nlist()).then(function(){
     //$.when(checknoti()).then(function(){ 
    
//});  
});
    });
        <? } ?>
});

function validateEmail(sEmail) {
var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
if (filter.test(sEmail)) {
return true;
}
else {
return false;
}
}
function checknoti() {
  $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
   $(".loader").show();
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
 //    $("#qer").empty().append(html);
 $(".loader").hide();
   console.log("setnoty called");
   //$("#dtest").empty().append(html);
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("setnoty"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    //  async: false,
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

     
function checknotinum() {
 //   alert("testing...");
 //sleep(3000);
    ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $(".loader").show();
        },
                
        success:function(data){
            $(".loader").hide();
            var n=data['num'];
            if(n>0){
            $("#cnum").html('<span class="badge badge-danger badge-small">'+n+'</span>');
        }
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("checknum"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"json",
 //   async: false,
    cache:false
})//ajax
                 
                  ///////////////////////////////////////////////////
}
function nlist() {
//sleep(3000);
 //   alert("testing...");
    ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $(".loader").show();
        },
                
        success:function(html){
            $(".loader").hide();
          $("#nlist2").empty().append(html);
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("nlist"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
  //  async: false,
    cache:false
})//ajax
                 
                  ///////////////////////////////////////////////////
}

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

  </script>
</body>
</html>
