<?php
/* @var $this CommunicatorController */

$this->breadcrumbs=array(
	'Communicator',
);
$rolid=Yii::app()->user->getState("rolid");

    
?>

<div class="row">
    <br style="clear:both">
    <div class="col-md-4 col-lg-4 col-sm-12">
        <br>
       <? 
        if($rolid=='director'){ ?>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
                Internal > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/communicator/index">
                    <span style="color:blue">Communicator</span>
                </a>
            </span> 
        <? }
        if($rolid=='academic'){ ?>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Internal > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/communicator/index">
                    <span style="color:blue">Communicator</span>
                </a>
            </span> 
        <? }
        if($rolid=='ilcmanager'){ ?>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
                Internal Management > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/communicator/index">
                    <span style="color:blue">Communicator</span>
                </a>
            </span> 
        <? }
        ?>
        <div class="srcfrm">
            <form id="sf">
                <div class="row">               
                   <div class="col-md-8">
                       <input type="text" id="stext" class="enin" placeholder="Search by posts,tags" >
                   </div>
                   <div class="col-md-4">
                       <button class="btn-green" type="button" onclick="commsearch()" id="button-addon2">SEARCH</button>
                   </div>
                </div>
            </form>  
            <div class="alert alert-danger" style="display: block; clear: both" id="err2"></div>
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        <br><br>
        <div class="post-form">
            <form id="postform">
             
            <div id="err" class="alert alert-danger"></div>
            <div class="row">
                <div class="col-md-12">
                    <textarea class="enin" id="posttext" placeholder="Type post content.." rows="7"></textarea>
                </div> 
            </div>
            <div class="row">
                <div class="col-md-2">
                    <button class="enin" >Tags</button>
                </div>
                <div class="col-md-5">
                    <input type="text" id="tags" class="enin" placeholder="Comma saperated words" >
                    <br>
                    <div id="fn" style="width: 99%;float: left;margin-top: 2%;font-size: 10px; color: green"></div>
                </div>
                <div class="col-md-1" >
                    <button onclick="upfile()" type="button" style="width:100%;font-size:14px !important;padding: 4px"> 
                        <i class="glyphicon glyphicon-paperclip"></i></button>
                </div>
                <div class="col-md-4">
                    
                    <input type="file" id="file" onchange="cfile()" style="width:100%;font-size:14px !important" />
                </div>
                <div class="col-md-3">
                    <button type="button" onclick="dopost()" class="btn-green">CREATE POST</button>
                    <br style="clear:both">
                </div>
                
            </div>
           </form>
            <div id="succ" class="alert alert-success"></div>
        
        <div id="commlist">
            
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#err").hide();
        $("#succ").hide();
        $("#err2").hide();
        getcomm();
    });
function upfile() {
    $('#file').click();
    
}
function ucpfile(fid) {
    $('#'+fid).click();
    
}
function cfile() {
    var f= $('#file').val();
    $("#fn").html(f);
}
function dopost2(comid,ctextid,commnum)
{
   // alert(commnum);
    var ctext=$("#cmt"+ctextid).val();
    if(ctext=="" || ctext==null)
    {
        alert("Please enter comment content!");
    }
    else
    {
        var form_data = new FormData();                  
   
    form_data.append('comid', comid);
     form_data.append('ctext', ctext);  
            $.ajax({
        url: '<? echo $this->createUrl("uploadcomment"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
//         $("#postform").trigger("reset");
//         $("#succ").show().html('Post uploaded successfully!');
//         $("#fn").html('');
     getcomm();
//         $('html, body').animate({
//        scrollTop: $("#comm"+commnum).offset().top
//    }, 2000);
        }
     }); 
    }
}
function dopost() {
       var file_data = $('#file').prop('files')[0];   
      var ptext = $('#posttext').val();
      var tag = $("#tags").val();
      var fn=$("#file").val();
      if(ptext=="" || ptext==null) {
      $("#err").show().html("Please enter post content..")
        }
       else if(tag=="" || tag==null) {
      $("#err").show().html("Please enter tags..")
        }
      else
      {
      if(fn=="" || fn==null){
           var form_data = new FormData();                  
   
    form_data.append('ptext', ptext);
     form_data.append('tag', tag);
       $.ajax({
        url: '<? echo $this->createUrl("uploadpost2"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
         $("#postform").trigger("reset");
         $("#succ").show().html('Post uploaded successfully!');
         $("#fn").html('');
         getcomm();
        }
     }); 
      }   
      else 
      {
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('ptext', ptext);
     form_data.append('tag', tag);
       $.ajax({
        url: '<? echo $this->createUrl("uploadpost"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
         $("#postform").trigger("reset");
         $("#succ").show().html('Post uploaded successfully!');
         $("#fn").html('');
              getcomm();
        }
     });
     }
    } 
}
function getcomm() {
      $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#commlist").show().html('loading...........');
        },
                
        success:function(html){
    $("#commlist").empty().append(html);
//      $('html, body').animate({
//        scrollTop: $("#commlist").offset().top
//    }, 2000);
         /////////////////////////
var imgurl='<?=Yii::app()->request->baseUrl ?>/images/face.jpg';
$('img').error(function(){
        $(this).attr('src', imgurl);
});
//////////////////////
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getcomm"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
}
function commsearch() {
var stext=$("#stext").val();
if(stext=="" || stext==null)
{
    $("#err2").show().html("Enter search keywords!");
}
else {
      $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#commlist").show().html('loading...........');
        },
                
        success:function(html){
    $("#commlist").empty().append(html);
      $('html, body').animate({
        scrollTop: $("#commlist").offset().top
    }, 2000);
    $("#sf").trigger("reset");
   
         /////////////////////////
var imgurl='<?=Yii::app()->request->baseUrl ?>/images/face.jpg';
$('img').error(function(){
        $(this).attr('src', imgurl);
});
//////////////////////
 $("#err2").hide();
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("commsearch"); ?>',
        data:{
          stext:stext
        },
    dataType:"html",
    cache:false
})//ajax
}
}
function comfile(fid,comid,pn) {
    var file_data = $('#'+fid).prop('files')[0];   
      var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('comid', comid);
     form_data.append('pn', pn);
       $.ajax({
        url: '<? echo $this->createUrl("uploadcom"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
     
              getcomm();
        }
     });
}
</script>