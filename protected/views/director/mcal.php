<div class="row">
   <div class="col-md-12" >
       <br>
       <span class="bread">
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
           Internal > 
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/mcal">
               <span style="color:blue">View Schedule</span>
           </a>
       </span>
    </div> 
</div>
<? $rolid=Yii::app()->user->getState("rolid"); 
$userid=Yii::app()->user->getState('user_id');?>
<div class="row" >
    <div class="col-md-12">
        <h1>Schedules of ILC MANAGERS</h1>
    <br style="clear:both">
    </div>
    
</div>
<div class="row" style="margin-top:-3%">
    <div class="col-md-12">
        <?
               if($rolid=='academic' || $rolid=='director'){
               
                  
               $crqm=new CDbCriteria();
               $crqm->condition='role=:u';
               $crqm->params=array(':u'=>'ilcmanager');
               $ilcs= Users::model()->findAll($crqm);
                   
               ?>
                <ul class="nav nav-tabs">
                <?
               foreach($ilcs as $ilc){
               
                if($rolid=='academic' || $rolid=='director'){
                    $i=$ilc->name;
                    $uid=$ilc->userid;
                }
               ?>
                <li class="nav-item">
                    
                    <a href="<?=Yii::app()->request->baseUrl.'/index.php/director/mcal/?q='.$uid ?>" class="nav-link active"  style="cursor:pointer" ><?=$i ?></a>
                </li>
                 <? } ?>
                </ul>
    </div>
    <div class="col-md-5">
            
            
              
            <br>
            
            <div class="col-md-12">
            <div  id="holcal">
                <ul class="hpal">
                      
                    <li><div class="bred"></div>&nbsp;Holidays</li>
                    <li><div class="bbla"></div>&nbsp;Leaves</li>
                    <li><div class="bora"></div>&nbsp;Enquiry</li>
                    <li><div class="byel"></div>&nbsp;Interview</li>
                    
                </ul>
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcilcm/?q='.$man), // URL to get event
                //////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getilcmm/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            /////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
                <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>                 
            </div>
            </div>
               <? } ?>
        </div>
    
    
</div>

<script>
$(document).ready(function(){

  
});


function getq() {
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#adm").show().html('Loading...........');
        },
                
        success:function(html){
        $("#adm").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("adm"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function searchilc(ilcid1) {
    //alert(ilcid);
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#adm").show().html('Loading...........');
        },
                
        success:function(html){
        $("#adm").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("adm2"); ?>',
        data:{
          ilcid1:ilcid1,
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function setai(adid){
    $("#myModalai").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyai").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyai").empty().append(html);
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        $("#datepicker1").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        gethow();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("setai"); ?>',
        data:{
          adid:adid,
        },
    dataType:"html",
    cache:false
})//ajax
}

function viewai(adid){
    $("#myModalai").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyai").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyai").empty().append(html);
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        $("#datepicker1").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        gethow();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("viewai"); ?>',
        data:{
          adid:adid,
        },
    dataType:"html",
    cache:false
})//ajax
}

function updateai(){
    var hhear="";
    var adid=$("#adid").val();
    var parid=$("#parid").val();
    var adtype1=$("#adtype option:selected").val();
    var recno=$("#recno").val();
    var enrno=$("#enrno").val();
    var recdate=$("#datepicker").val();
    var enrdate=$("#datepicker1").val();
    var mto=$("#mto").val();
    var mname=$("#mname").val();
    var focc=$("#focc").val();
    var mocc=$("#mocc").val();
    var foa=$("#foa").val();
    var moa=$("#moa").val();
    
    $('.form-check-input').each(function(e){
        if($(this).is(':checked')){
            hhear+=$(this).val()+',';
        }
    });
    var mphone=$("#mphone").val();
    var memail=$("#memail").val();
    var allergy=$("#allergy").val();
    var blood=$("#blood").val();
    var omed=$("#omed").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyd").show().html('loading...........');
        },
                
        success:function(html){
        $("#bodyd").empty().append(html);
        $("#btn-closed").click();
        getq();
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updateai"); ?>',
        data:{
            adid:adid,parid:parid,recno:recno,enrno:enrno,recdate:recdate,enrdate:enrdate,mto:mto,mname:mname,focc:focc,mocc:mocc,foa:foa,moa:moa,hhear:hhear,mphone:mphone,memail:memail,allergy:allergy,blood:blood,omed:omed,adtype1:adtype1
           },
    dataType:"html",
    cache:false
})//ajax
}

function updatefeed(){
    var adid=$("#adid").val();
    var feed=$("#feed").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyfeed").show().html('loading...........');
        },
                
        success:function(html){
        $("#bodyfeed").empty().append(html);
        $("#btn-closed1").click();
        getq();
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
            adid:adid,feed:feed,
    },
    dataType:"html",
    cache:false
})//ajax
}

function gethow(){
    var how=$('#how').val();
    $('.form-check-input').each(function(e){
       var howval=$(this).val(); 
       if(how.indexOf(howval) != -1){
           $(this).attr('checked','true');
       }
    });

}

function browsefile2() {
$("#file2").click();
}

function browsefile() {
$("#file").click();
}

function upfile() {
    var file_data = $('#file').prop('files')[0]; 
    var adid= $("#adid").val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('adid', adid);
 
       $.ajax({
        url: '<? echo $this->createUrl("uploadphoto"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
      
            alert("Picture selected..");
            getq();
            
        }
     });
}

function upfile2() {
    var file_data = $('#file2').prop('files')[0]; 
    var adid= $("#adid").val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('adid', adid);
 
    $.ajax({
        url: '<? echo $this->createUrl("uploadphoto2"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
      
            alert("File Attached");
            getq();
            
        }
     });
}

function setfeed(adid){
    $("#myModalfeed").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyfeed").show().html('Loading...');
        },
                
        success:function(html){
         $("#bodyfeed").empty().append(html);
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("setfeed"); ?>',
        data:{
          adid:adid,
        },
    dataType:"html",
    cache:false
})//ajax
}
function updatefeed() {
var adid=$("#adid2").val();
var feed=$("#feed").val();
if(feed=="") {
    alert("Give a feedback first!");
}
else
{
     $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyfeed").show().html('Updating...');
        },
                
        success:function(html){
         $("#bodyfeed").empty().append(html);
           $("#myModalfeed").modal('hide');
           getq();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
          adid:adid,feed:feed
        },
    dataType:"html",
    cache:false
})//ajax
}
}

function closeadm(adid) {
var y=confirm("Are you sure you want to close?");
if(y) {
///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                 
        success:function(html){
        alert("This Admission has been closed"); 
           var url1='<? echo Yii::app()->request->baseUrl?>/index.php/admission/archive';
           window.location.href=url1;
         },
             error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("closeadm"); ?>',
        data:{
          adid:adid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
</script>
