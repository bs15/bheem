<? $rolid=Yii::app()->user->getState("rolid"); ?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
        <? if($rtype=='ilc'){
            $t="ilctab";
            $n="-ilc report-";
            
        }
        if($rtype=='weekly'){  
            $t="weektab";
            $n="-weekly report-";
             }
        if($rtype=='rating'){  
            $t="ilctab";
            $n="-ilc report-";
             }
        if($rtype=='escalation'){  
            $t="esctab";
            $n="-escalation report-";
            }
        ?>
        </div>
     
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="divTableDataHolder">
            <? 
            $r=new Report();
            if($rtype=='ilc'){
            $r->ilcreport($il,$yr,$mon,$wee,$ci,$st);
            }
            if($rtype=='weekly'){
            $r->weeklyreport($il,$yr,$mon,$wee,$ci,$st,$ilcm);
            }
            if($rtype=='rating'){
            $r->ratingreport($il,$yr,$mon,$wee,$ci,$st,$ilcm);
            }
            if($rtype=='escalation'){
            $r->escalationreport($il,$yr,$mon,$wee,$ci,$st,$ilcm);
            }
           ?>
            </div>
        </div>
    </div>
<!--    <div class="row">
        <div class="col-md-12" aligsn="center">
            <button class="btn-green" onclick="save()">Save as/ Print</button>
        </div>
    </div>-->
        <div class="row">
        <div class="col-md-12" aligsn="center">
            <!--<button class="btn-green" onclick="fnExcelReport()">Export</button>&nbsp;-->
             <button class="btn-green" onclick="save()">Export to pdf</button>&nbsp;
                <button class="btn-green" onclick="export3()">Export to excel</button>&nbsp;
                  <!--<a href="javascript:exportToExcel();">Export to Excel</a><br/>-->
        </div>
    </div>
</div>
<!--<script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>-->
<script>
    $(document).ready(function() {
//TableToExcel();
} );
 
    function fnExcelReport()
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange; var j=0;
    tab = document.getElementById('<?=$t ?>'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"exported<?=$n.date('Y-m-d H:i:s') ?>.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}
    function save(){
        window.print();
    }
    function expor() {
        var htmls = "";
            var uri = 'data:application/vnd.ms-excel;base64,';
            var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'; 
            var base64 = function(s) {
                return window.btoa(unescape(encodeURIComponent(s)))
            };

            var format = function(s, c) {
                return s.replace(/{(\w+)}/g, function(m, p) {
                    return c[p];
                })
            };

            htmls = document.getElementById('<?=$t ?>');

            var ctx = {
                worksheet : 'Worksheet',
                table : htmls
            }


            var link = document.createElement("a");
            link.download = "export<?=$n.date('Y-m-d H:i:s') ?>.xls";
            link.href = uri + base64(format(template, ctx));
            link.click();
    }
    function export2() {
          window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=divTableDataHolder]').html()));
    e.preventDefault();
    }
     function TableToExcel() {
            var strCopy = document.getElementById('<?=$t ?>').innerHTML;
            window.clipboardData.setData("Text", strCopy);
            var objExcel = new ActiveXObject("Excel.Application");
            objExcel.visible = true;

            var objWorkbook = objExcel.Workbooks.Add;
            var objWorksheet = objWorkbook.Worksheets(1);
            objWorksheet.Paste;
            /////////////call export///////////////
         //   exportToExcel();
        }

        function exportToExcel() {
            var oExcel = new ActiveXObject("Excel.Application");
            var oBook = oExcel.Workbooks.Add;
            var oSheet = oBook.Worksheets(1);
            for (var y = 0; y < detailsTable.rows.length; y++)
            // detailsTable is the table where the content to be exported is
            {
                for (var x = 0; x < detailsTable.rows(y).cells.length; x++) {
                    oSheet.Cells(y + 1, x + 1) = detailsTable.rows(y).cells(x).innerText;
                }
            }
            oExcel.Visible = true;
            oExcel.UserControl = true;
        }
    function export3() {
         $("#<?=$t ?>").table2excel({

    exclude:".noExl",
  name:"Worksheet 1",
  filename:"<?=date('Y-m-d H:i:s').$n.'exported' ?>",//do not include extension

    fileext:".xlsx" // file extension

  });

    }
</script>