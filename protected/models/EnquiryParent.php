<?php

/**
 * This is the model class for table "enquiry_parent".
 *
 * The followings are the available columns in table 'enquiry_parent':
 * @property string $parentid
 * @property string $parent_name
 * @property string $child_name
 * @property string $child_dob
 * @property string $gender
 * @property string $f_occupation
 * @property string $parent_f_email
 * @property string $parent_f_phone
 * @property string $parent_f_calltime
 * @property string $parent_m_email
 * @property string $parent_m_phone
 * @property string $parent_m_calltime
 * @property string $parent_add
 * @property string $how_hear
 * @property string $hoarding
 * @property string $parent_feedback
 * @property string $city
 * @property string $ilc_id
 */
class EnquiryParent extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EnquiryParent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'enquiry_parent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parentid,parent_f_name,parent_m_name, child_name, child_dob, gender, f_occupation, parent_f_email, parent_f_phone, parent_f_calltime, parent_m_email, parent_m_phone, parent_m_calltime, parent_add, how_hear, hoarding, parent_feedback, city, ilc_id', 'required'),
			array('parentid, ilc_id', 'length', 'max'=>100),
                        array('id', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parentid, parent_f_name, parent_m_name, child_name, child_dob, gender, f_occupation, parent_f_email, parent_f_phone, parent_f_calltime, parent_m_email, parent_m_phone, parent_m_calltime, parent_add, how_hear, hoarding, parent_feedback, city, ilc_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'parentid' => 'Parentid',
			'parent_f_name' => 'Father Name',
                        'parent_m_name' => 'Mother Name',
			'child_name' => 'Child Name',
			'child_dob' => 'Child Dob',
			'gender' => 'Gender',
			'f_occupation' => 'F Occupation',
			'parent_f_email' => 'Parent F Email',
			'parent_f_phone' => 'Parent F Phone',
			'parent_f_calltime' => 'Parent F Calltime',
			'parent_m_email' => 'Parent M Email',
			'parent_m_phone' => 'Parent M Phone',
			'parent_m_calltime' => 'Parent M Calltime',
			'parent_add' => 'Parent Add',
			'how_hear' => 'How Hear',
			'hoarding' => 'Hoarding',
			'parent_feedback' => 'Parent Feedback',
			'city' => 'City',
			'ilc_id' => 'Ilc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('parentid',$this->parentid,true);
		$criteria->compare('parent_f_name',$this->parent_f_name,true);
                $criteria->compare('parent_m_name',$this->parent_m_name,true);
		$criteria->compare('child_name',$this->child_name,true);
		$criteria->compare('child_dob',$this->child_dob,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('f_occupation',$this->f_occupation,true);
		$criteria->compare('parent_f_email',$this->parent_f_email,true);
		$criteria->compare('parent_f_phone',$this->parent_f_phone,true);
		$criteria->compare('parent_f_calltime',$this->parent_f_calltime,true);
		$criteria->compare('parent_m_email',$this->parent_m_email,true);
		$criteria->compare('parent_m_phone',$this->parent_m_phone,true);
		$criteria->compare('parent_m_calltime',$this->parent_m_calltime,true);
		$criteria->compare('parent_add',$this->parent_add,true);
		$criteria->compare('how_hear',$this->how_hear,true);
		$criteria->compare('hoarding',$this->hoarding,true);
		$criteria->compare('parent_feedback',$this->parent_feedback,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('ilc_id',$this->ilc_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}