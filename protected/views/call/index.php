<? $rolid=Yii::app()->user->getState("rolid"); ?>
<?
$userid=Yii::app()->user->getState("user_id");
$criteria1 = new CDbCriteria();
$criteria1->condition = 'ilcmid=:ilm';
$criteria1->params=array("ilm"=>$userid);
$ilcs = ManagerIlc::model()->findAll($criteria1);

$criteria1 = new CDbCriteria();
$criteria1->select = 'state';
$criteria1->group="state";
$states = Ilc::model()->findAll($criteria1);
?>
<? if($rolid=='director'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
                Escalations > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/call/index">
                    <span style="color:blue">Call Escalations</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='academic'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Escalations > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/call/index">
                    <span style="color:blue">Call Escalations</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='ilcmanager'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
                Management > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/call/index">
                    <span style="color:blue">Call </span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='unit'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Task Scheduler > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/call/index">
                    <span style="color:blue">Call Scheduler </span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='partner'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/index">Home</a> >
                Task Scheduler > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/call/index">
                    <span style="color:blue">Call Scheduler </span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    ?>
<div class="row">
    <div class="col-md-8">
 
            <? if($rolid=='partner' || $rolid=='unit') { ?>
            <form id="qform">
                <br>
                <a style="margin-left:1%;color:white !important;" id="inttogg" class="btn-dark btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
    <i class="glyphicon glyphicon-plus"></i>&nbsp;Schedule Call
  </a>
                
                
                <!--<br style="clear:both"><br>-->       
        <div  class="card collapse table-font" id="collapseExample" >
            
            <?
                $user=Yii::app()->user->getState('user_id');
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc= $uinfo->ilcid;
                
                $cru1=new CDbCriteria();
                $cru1->condition='ilcid=:u';
                $cru1->params=array(":u"=>$ilc);
                $ilcn=  Ilc::model()->find($cru1);
                $iname=$ilcn->ins_name;
                
                $cru2=new CDbCriteria();
                $cru2->condition='ilcid=:u';
                $cru2->params=array(":u"=>$ilc);
                $ilcm= ManagerIlc::model()->find($cru2);
                $mname=$ilcm->managername;
                
                ?>
            <div class="card-header"><h4>Fill up the fields in order to schedule a call 
                <span style="float:right">Manager :  
                <?=$mname ?></span></h4>
            </div>
            <div class="card-body">
                <div id="qer" class="alert alert-danger"></div> 
                
                <p>Call Details </p>
                <div class="row"> 
                    <div class="col-md-12">
                        <input class="enin" id="topic" placeholder="Call Topic" >
                    </div>
                    <div class="col-md-4">      
                        <input type="text" class="enin" id="datepicker" placeholder="Select date" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    
                    <div class="col-md-3"> 
                        <div class="enin" style="padding-top:4px;">Select Time</div>
                    </div>
                    
                    <div class="col-md-3">
                        <input type="time" id="stime" class="form-control" />
                    </div>

                  
                    </div>
                
<div class="input-group">
    <input type="button" class="btn btn-success" value="Create" onclick="setcall()" />
</div>         
<!--<div id="qer" class="alert alert-success"></div>           -->
</div>
<div class="card-footer">
    <span style="text-transform: uppercase;font-weight: bold"><?=$iname ?></span><br>
    <span><?=$ilcn->address.", ".$ilcn->city.", ".$ilcn->state.", ".$ilcn->country."." ?></span>
</div>
</div>    
</form>
<? } ?>

       
        <!--<div class="col-md-12">-->
            <h1>Call SCHEDULES</h1>
            <? if($rolid=='academic' || $rolid=='director'){?>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <input type="hidden" id="silc2" value="-">
                        <select id="st" class="enin" onchange="getcity()" style="margin-left:1%">
                            <option value="0">Select State</option>
                            <? foreach($states as $st){
                                ?>
                                <option value="<?=$st->state ?>"><?=$st->state ?></option>
                                <?
                            } ?>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-12" id="loadcity"></div>

                    <div class="col-md-3 col-sm-12" id="loadilc"></div>
                </div>
            <? } ?>
            <? if($rolid=='ilcmanager'){?>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <input type="hidden" id="silc2" value="-">
                        <select id="il" class="enin" onchange="getres()" style="margin-left:1%">
                            <option value="0">Select ILC</option>
                            <? foreach($ilcs as $i){
                                ?>
                                <option value="<?=$i->ilcid ?>"><?=$i->ilcname ?></option>
                                <?
                            } ?>
                        </select>
                    </div>
                </div>
            <? } ?>
            <div id="enq" style="margin-top:-0%">
                
            </div>
<!--        </div>-->
    </div>
    <div class="col-md-4 col-sm-12">
        <? if($rolid=='unit' || $rolid=='partner' ){ ?>
        <div class="col-md-12 col-sm-12" align="center">
            <br>
        <h3>ILC Manager's Calendar</h3>
        <div  id="holcal">
                <ul class="hpal">
                      
                    <li><div class="bred"></div>Holidays</li>
                    <li><div class="bbla"></div>Leaves</li>
                    <li><div class="bora"></div>Enquiry</li>
                    <li><div class="byel"></div>Interview</li>
                    <li><div class="bpur"></div>Calls</li>
              
                </ul>
            <div style="padding-top: 35px"> 
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcilcm/?q='.$man), // URL to get event
                /////////////////////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
                $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody1").load("'.$this->createUrl("calender/getilcmm/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal1").modal();
	    }',
            //////////////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
            </div>
              <div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody1">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>                  
            </div>

    </div>
        <? } 
        if($rolid=='ilcmanager'){ ?>
        <div class="col-md-12 col-sm-12" align="center">
            <br>
        <div  id="holcal">
                <ul class="hpal">
                    <li><div class="bred"></div>&nbsp;Holidays</li>
                    <li><div class="bbla"></div>&nbsp;Leaves</li>
                    <li><div class="bora"></div>&nbsp;Enquiry</li>
                    <li><div class="byel"></div>&nbsp;Interview</li>
                     <li><div class="bpur"></div>&nbsp;Call</li>
                </ul>
                <div style="padding-top: 35px"> 
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcalenderilcm'), // URL to get event
                /////////////////////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getcalenderilcmmodal/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            //////////////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
                </div>
            <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>                     
            </div>

    </div>
        <? }
        ?>
    </div>
</div>

<div id="myModali" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Remarks for Call</h4>
        <button type="button" class="close" id="btn-closei" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="bodyi">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="btn-close">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left">Appoint NEW Manager</h4>
        <button type="button" class="close" id="btn-close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="body1">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="btn-close1">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="myModald" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left">Add more details</h4>
        <button type="button" class="close" id="btn-closed" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="bodyd">
        <p>Some text in the modal.</p>
      </div>
      
    </div>

  </div>
</div>

<div id="myModalai" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left">Additional Information</h4>
        <button type="button" class="close" id="btn-closed" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="bodyai">
        <p>Some text in the modal.</p>
      </div>
      
    </div>

  </div>
</div>


<script type="text/javascript">
$(document).ready(function(){
  $("#qer").hide(); 
    $("#datepicker").datepicker({
  dateFormat: "yy-mm-dd" ,minDate: 0
});
$("#datepicker1").datepicker({
  dateFormat: "yy-mm-dd" ,maxDate: 0
});

getq();
});
function getres(){
    var ilc1=$("#il option:selected").val();
    var st=$("#st option:selected").val();
    var ci=$("#ci option:selected").val();
    
    //alert("ilc"+ilc1+" city"+ci+" state"+st);
   if(ilc1=='0' && st=='0' && ci=='0'){
        alert("Select ILC");
    }
    if(ilc1==undefined){
        ilc1='0';
    }
    if(ci==undefined){
        ci='0';
    } 
    if(st==undefined){
        st='0';
    } 
   
        //////////////////////ajax////////////////////////
        //alert("ilc="+il+" year="+yr+" month="+mon+" week="+wee);
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#enq").show().html('loading...........');
        },
        success:function(html){
        $("#enq").empty().append(html);
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getcalilc"); ?>',
        data:{
          st:st,ci:ci,ilc1:ilc1,
        },
    dataType:"html",
    cache:false
})//ajax
    
 }
function getcity(){
     var st1=$('#st option:selected').val();
     if(st1=='0')
     {
         alert('Select a state ');
     }
     else
     {
         //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#loadcity").show().html('loading...........');
        },
        success:function(html){
            getres();
        $("#loadcity").empty().append(html);
        
     
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getcity"); ?>',
        data:{
          st1:st1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
function getilc(){
     var ci1=$('#ci option:selected').val();
     if(ci1=='0')
     {
         alert('Select a city ');
     }
     else
     {
        //////////////////////ajax////////////////////////
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#loadilc").show().html('loading...........');
        },
        success:function(html){
        getres();     
        $("#loadilc").empty().append(html);
        
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getilc"); ?>',
        data:{
          ci1:ci1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 }
function editfeed(id){
    
    //alert(id);
    $("#myModal").modal();
    revshowf();
    $("#revshow").hide();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
        success:function(html){
        $("#body1").empty().append(html);
     revshowf();
    $("#revshow").hide();
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("editfeed"); ?>',
        data:{
          id:id
        },
    dataType:"html",
    cache:false
})//ajax
}
function revshowf(){
 var rat=$("#rating option:selected").val();
 if(rat=="1" || rat=="2" || rat=="3"){
    $("#revshow").show(); 
    }
   else{
     $("#revshow").hide();   
   } 
}
function updatefeed(){
    var qid=$("#qqid").val();
    var ilcmid=$("#ilcmid").val();
    var rating=$("#rating").val();
    var review=$("#review").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     //$("#body1").empty().append(html);
     
   $("#btn-close").click();
   
     getq();
     alert("Your feedback has been submitted");
     closecall(qid);
    
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
          qid:qid,ilcmid:ilcmid,rating:rating,review:review
        },
    dataType:"html",
    cache:false
})//ajax
}

function setcall() {
    var topic=$("#topic").val();
    var datep= $("#datepicker").val();
//    var h=$("#hour option:selected").val();
//    var m=$("#min option:selected").val();
    
      var tt=$("#stime").val();
    
     if(topic=='')
     {
         $("#qer").show().html("Enter Topic");
     }
     if(datep=="" || datep==null)
     {
         $("#qer").show().html("Enter Enquiry Date");
     }
   
     else if(tt=="" || tt==null)
     {
         $("#qer").show().html("Enter time..");
     }
    
     
      else 
     {
             var rest=tt.split(":");
             var h=rest[0];
             var m=rest[1];
        $.ajax({
         beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Scheduling call');
        },
                
        success:function(html){
        $("#qer").empty().append(html);
        $("#qer").hide();
        $("#inttogg").click();
        getq();
   },
              error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("setcall"); ?>',
        data:{
          topic:topic,datep:datep,h:h,m:m,
        },
    dataType:"html",
    cache:false
})//ajax
            
            
                 
                 
                 ///////////////////////////////////////////////////
     }
}

function getq() {
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#enq").show().html('Loading...........');
        },
                
        success:function(html){
        $("#enq").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'get',
        url:'<? echo $this->createUrl("cal"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function setst(enqid){
    $("#myModali").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyi").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyi").empty().append(html);
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("setst"); ?>',
        data:{
          enqid:enqid
        },
    dataType:"html",
    cache:false
})//ajax
}

function updatest(){
    var enqid=$("#enqid").val();
    var st1=$("#st1").val();
    
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyi").show().html('loading...........');
        },
                
        success:function(html){
        $("#bodyi").empty().append(html);
        $("#btn-close").click();
        getq();
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatest"); ?>',
        data:{
          enqid:enqid,st1:st1
        },
    dataType:"html",
    cache:false
})//ajax
}

function newm(callid){
    $("#myModal").modal();
    //////////////////////ajax////////////////////////
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
        $("#body1").empty().append(html);
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("newm"); ?>',
        data:{
          callid:callid,
        },
    dataType:"html",
    cache:false
    })
}

function updatem(){
    
    var callid=$("#callid").val();
    var ilcmid=$("#ilcmid option:selected").val();
    var ilcmname=$("#ilcmid option:selected").text();
    
    if(ilcmid=='0')
    { 
        alert("Please select New Manager");
    }
    else
    {
       // alert(callid+" "+ilcmid+" "+ilcmname);
    /////////ajax/////////////
        $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#body1").show().html('Loading..');
        },
        success:function(html){
        $("#body1").empty().append(html);
        $("#btn-close1").click();
        getq();
        alert("New Manager has been assigned");
        },
        error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("updatem"); ?>',
        data:{
          callid:callid,ilcmid:ilcmid,ilcmname:ilcmname
        },
    dataType:"html",
    cache:false
    })//ajax
    }
}

function closecall(callid) {

///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                 
        success:function(html){
        
        alert("This Scheduled call has been closed"); 
        var url1='<? echo Yii::app()->request->baseUrl?>/index.php/call/index';
        window.location.href=url1;
        
    },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("closecall"); ?>',
        data:{
          callid:callid,
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 
}

function setdetail(enqid){
    $("#myModald").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyd").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyd").empty().append(html);
        gethow();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("setdetail"); ?>',
        data:{
          enqid:enqid
        },
    dataType:"html",
    cache:false
})//ajax
}

function updatedetail(){
    var hhear="";
    var parid=$("#parid1").val();
    var femail=$("#femail").val();
    var focc=$("#focc").val();
    var fphn=$("#fphn").val();
    var fcall=$("#fcall option:selected").val();
    var mname=$("#mname").val();
    var memail=$("#memail").val();
    var mphn=$("#mphn").val();
    var mcall=$("#mcall option:selected").val();
    var add=$("#add").val();
    $('.form-check-input').each(function(e){
        if($(this).is(':checked')){
            hhear+=$(this).val()+',';
        }
    });
    var hoarding=$("#hoarding").val();
    var feed=$("#feed").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyd").show().html('loading...........');
        },
                
        success:function(html){
        $("#bodyd").empty().append(html);
        $("#btn-closed").click();
        getq();
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatedetail"); ?>',
        data:{
          parid:parid,femail:femail,focc:focc,fcall:fcall,fphn:fphn,mname:mname,memail:memail,mphn:mphn,mcall:mcall,add:add,hhear:hhear,hoarding:hoarding,feed:feed,
        },
    dataType:"html",
    cache:false
})//ajax
}

function gethow(){
    var how=$('#how').val();
    $('.form-check-input').each(function(e){
       var howval=$(this).val(); 
       if(how.indexOf(howval) != -1){
           $(this).attr('checked','true');
       }
    });

}

function setai(enqid){
    $("#myModalai").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyai").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyai").empty().append(html);
        gethow();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("setai"); ?>',
        data:{
          enqid:enqid
        },
    dataType:"html",
    cache:false
})//ajax
}
</script>