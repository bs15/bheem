<div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/interview2">
                    <span style="color:blue">Interview Archive</span>
                </a>
            </span>
        </div> 
    </div>

<div class="row">
    <div class="col-md-12">
               
         <h1>INTERVIEW Archive</h1>
              <div class="row">
                  <div class="col-md-3">
        <select class="enin" id="yea" onchange="searchimy()">
             <option value="0">Choose Year</option>
             <?
             for($i=2019;$i<=2040;$i++) {
                 ?>
             <option value="<?=$i ?>"><?=$i ?></option>
                     <?
             }
             ?>
        </select>
    <!--<input type="number" min="2019" max="<? //date('Y') ?>" placeholder="Enter year"  class="enin" id="yea" >-->
    </div>
    <div class="col-md-3">
        <select id="mon" class="enin" onchange="searchimy()">
        <option value="0">Choose Month</option>
        <option value="1">Jan</option> 
        <option value="2">Feb</option> 
        <option value="3">Mar</option> 
        <option value="4">Apr</option> 
        <option value="5">May</option> 
        <option value="6">Jun</option> 
        <option value="7">Jul</option> 
        <option value="8">Aug</option> 
        <option value="9">Sep</option> 
        <option value="10">Oct</option> 
        <option value="11">Nov</option>
        <option value="12">Dec</option> 
    </select>
    </div>
    
  
    
    <div class="col-md-2">
    <button class="btn btn-sm btn-small btn-danger" onclick="reset()">Reset</button>
    </div>
    <div class="col-md-3" >
        <div id="err" style="color:red;font-size:14px;font-weight:500" ></div>
    </div>
</div>
            <div id="intlist2">
                
            </div>
        
    </div>
    
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Share your experience</h4>
      </div>
      <div class="modal-body" id="body1">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default " data-dismiss="modal" id="btn-close">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
    $("#qer").hide();
    //$("#intlist").show();
     // $("#chttext").hide();
   // $("#chbtn").hide();
    //$("#fchat").hide();
    //$("#createq").hide();
   // $("#chbtn").prop('disabled','true');
    
    getq();
    



/////////////////////////////////////////////
//setInterval(function(){
//    var qid=$("#qid").val();
//     var uid=$("#uuid").val();
//    if(qid!="-") {
//   getchat(qid,uid)  
//    }
//},10000);
///////////////////////////////////////////////
});
function searchimy() {
    var mon=$("#mon option:selected").val();
    var yea=$("#yea option:selected").val();
    // alert(yea+" "+mon);     
    if(mon=="0" && yea=="0")
     {
         $("#err").show().html("Choose Month and year");
     }
    else if(mon!="0" && yea=="0")
     {
         $("#err").show().html("Choose Year and then month");
     }
     else 
     {
      ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#intlist2").show().html('Loading..');
        },
                
        success:function(html){
     $("#intlist2").empty().append(html);
  //  document.getElementById('ilcfrm').reset();
    
        //window.location.href=window.location.href;
        //}
        },
                 error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },      
        type:'post',
        url:'<? echo $this->createUrl("getimy"); ?>',
        data:{
          yea:yea,mon:mon,
        },
    dataType:"html",
    cache:false
})//ajax
///////////////////////////////////////////////////
}
}
function showq(){
    if( $("#createq"))
    {
        $("#createq").show(); 
    }
    else{
    $("#createq").show(); 
    }
}
//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var f=$("#cfile").val();
      $("#fn").html(f);
}
////////////////////////////////////////////////////////////

function revshowf(){
 var rat=$("#rating option:selected").val();
 if(rat=="1" || rat=="2" || rat=="3"){
    $("#revshow").show(); 
    }
   else{
     $("#revshow").hide();   
   } 
}


function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#intlist2").show().html('Loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#intlist2").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("intlist2"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}


function reset() {
window.location.href=window.location.href;
}
</script>