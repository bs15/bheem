<?php
/* @var $this UserphotoController */
/* @var $model Userphoto */

$this->breadcrumbs=array(
	'Userphotos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Userphoto', 'url'=>array('index')),
	array('label'=>'Create Userphoto', 'url'=>array('create')),
	array('label'=>'Update Userphoto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Userphoto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Userphoto', 'url'=>array('admin')),
);
?>

<h1>View Userphoto #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'userid',
		'uphoto',
	),
)); ?>
