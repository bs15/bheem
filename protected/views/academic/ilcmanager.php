<div class="row">
    <div class="col-md-12">
        <br>
        <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Master Data Management > Create > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/ilcmanager">
                    <span style="color:blue">ILC Manager</span>
                </a>
            </span> 
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h1>Create ILC MANAGERS</h1>
        <form id="ilcfrm" class="table-font">
  
        <input type="email" class="enin" id="uname" style="width:24%;margin-bottom:20px;" aria-describedby="emailHelp" placeholder="Enter email">
        
        <input type="text" class="enin" id="name" style="width:24%;margin-bottom:20px;" placeholder="Fullname">

        <input type="text" class="enin" id="org" style="width:24%;margin-bottom:20px;" placeholder="Company">

        <input type="text" class="enin" id="nation" style="width:24%;margin-bottom:20px;" placeholder="Nationality">

        <textarea class="enin" id="address" style="width:24%;margin-bottom:20px;" placeholder="Address"></textarea>

        <input type="text" class="enin" id="country" style="width:24%;margin-bottom:20px;" placeholder="Country">

        <input type="text" class="enin" id="phone" style="width:24%;margin-bottom:20px;" placeholder="Phone">

        <button type="button" class="btn-primary" style="padding:5px;border-radius:5px" onclick="createilcm()">Create</button>
        <div class="alert alert-danger card" id="err"></div>
        </form>
    </div>
     <div class="col-md-12">
         <br>
        <h3>All Managers</h3>
        <div id="ilcmlist" class="list">
            
        </div>
    </div>
</div>
    <script type="text/javascript">
    $(document).ready(function(){
       $("#err").hide();
       getilcm();
    
    });
      // Function that validates email address through a regular expression.
function validateEmail(sEmail) {
var filter = /^[w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
if (filter.test(sEmail)) {
return true;
}
else {
return false;
}
}
function validatePhone(value) {
      var regex = new RegExp(/^\+?[0-9(),.-]+$/);
    if(value.match(regex)) {return true;}
    return false;
}
    function createilcm() {
        var uname = $("#uname").val();
        var name = $("#name").val();
         var org = $("#org").val();
          var nation = $("#nation").val();
           var address = $("#address").val();
            var country = $("#country").val();
             var phone = $("#phone").val();
             
             if (uname=="" || uname==null)
             {
                 $("#err").show().html("Enter email..");
             }
           else if (name=="" || name==null)
             {
                 $("#err").show().html("Enter name..");
             }
               else if (org=="" || org==null)
             {
                 $("#err").show().html("Enter company name..");
             }
               else if (nation=="" || nation==null)
             {
                 $("#err").show().html("Enter nation..");
             }
               else if (address=="" || address==null)
             {
                 $("#err").show().html("Enter address..");
             }
               else if (country=="" || country==null)
             {
                 $("#err").show().html("Enter country..");
             }
               else if (phone=="" || phone==null)
             {
                 $("#err").show().html("Enter phone..");
             } 
              else if(uname!="" && !validateEmail(uname)) {
          $("#err").show().html("Enter valid email");
     }
     else if(!validatePhone(phone) || (phone.length < 10)) {
           $("#err").show().html("Enter valid phone number"); 
     }
             else  {
                 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#err").show().html('creating...........');
        },
                
        success:function(html){
     $("#ilcfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#err").show().html(html);
     getilcm();
     
        //window.location.href=window.location.href;
        //}
        },
                
        type:'post',
        url:'<? echo $this->createUrl("createilcm"); ?>',
        data:{
            uname:uname,name:name,org:org,nation:nation,address:address,country:country,phone:phone
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
             }
    }
function getilcm()   {
   ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#ilcmlist").show().html('loading...........');
        },
                
        success:function(html){
 $("#ilcmlist").empty().append(html);
     
     /////////////////////////
var imgurl='<?=Yii::app()->request->baseUrl ?>/images/face.jpg';
$('img').error(function(){
        $(this).attr('src', imgurl);
});
////////////////////// 
  
        },
                
        type:'post',
        url:'<? echo $this->createUrl("getilcm"); ?>',
        data:{
           hello:'hello'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function delilcm(id){
y=confirm("Are you sure ?");
if(y) {
    
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#ilcmlist").show().html('loading...........');
        },
                
        success:function(html){
getilcm();
     
  
        },
                
        type:'post',
        url:'<? echo $this->createUrl("delilcm"); ?>',
        data:{
           id:id
           },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////  
}

}
</script>