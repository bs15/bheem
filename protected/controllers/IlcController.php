<?php

class IlcController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
            $criteria1 = new CDbCriteria();
            $criteria1->select = '*';
            $criteria1->condition = 'role=:rolid';
            $criteria1->params = array(':rolid'=>'academic');
            $model1 = Users::model()->findAll($criteria1);
            $modad=array();
            $i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
            
            return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getcity','getilccity'),
				'users'=>$modad,
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','nlist'),
				'users'=>$modad,
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','setnoty','checknum','ilclist'),
				'users'=>$modad,
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
         public function actionGetcity()
        {
            if(Yii::app()->request->isPostRequest) {
            $st1=trim($_POST['st1']);
            $nl= new Nlist();
            $nl->getcity2($st1);
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
} 
        }
        public function actionIlclist()
	{
            $cr1=new CDbCriteria();
            $cr1->select="*";
            $ilcs= Ilc::model()->findAll($cr1);
            $this->render('ilclist',array('ilcs'=>$ilcs));
	}
        public function actionGetilccity()
	{
            if(Yii::app()->request->isPostRequest) {
            $ci=trim($_POST['ci']);


            $cr1=new CDbCriteria();
            $cr1->condition="city=:ci";
            $cr1->params=array(':ci'=>$ci);
            $cr1->select="*";
            $ilcs= Ilc::model()->findAll($cr1);
            
            ?>
             <table class="table table-striped table-font">
            <tr>
            <th>Type of ILC</th>
            <th>Institute Details</th>
            <th>Contact Details</th>
            <th>Partner</th>
            <th>Agreement</th>
            <th>Junior Preschool</th>
            <th>Senior Preschool</th>
            <th>Teacher Training</th>
            
            </tr>
            <?
            foreach($ilcs as $il){
            ?>
            <tr>
                <td>
                    
                  <? if($il->preschool==1) { ?> 
                    Preschool<br>
                  <? }
                  if($il->afterschool_activity==1) { ?>
                    Afterschool<br>
                  <? } 
                  if($il->teacher_training==1) { ?>
                    Teachers' Training
                  <? } ?> 
                    <br>
                    <a class="btn-green" href="<?=Yii::app()->request->baseUrl."/index.php/ilc/update/".$il->id ?>">Manage</a>
                </td>
                <td><b><?=$il->ins_name ?></b>
                    <br>
                    <p><?=$il->address ?>, <?=$il->city ?>, <?=$il->state ?>, <?=$il->country ?></p>
                </td>
                <td><?=$il->emails ?>
                    <br>
                <?=$il->phones ?></td>
                <td>
                   <?=$il->partner_name ?> 
                </td>
                <td>
                    <?=$il->agreement_date ?><br>to<br>
                    <?=$il->agreement_valid ?><br> <br>
                    <? if($il->agreement!=''){ ?>
                    <a target="_blank" href="<?=  Yii::app()->request->baseUrl.'/agreement/'.$il->agreement ?>" class="btn-green">Download</a>
                    <? } ?>
                </td>
                <td>
                    <? if($il->junior_pres_prog!='' ){ ?>
                     <?=$il->junior_pres_prog ?><br>
                     valid from<br>
                     <?=$il->preschool_valid_from ?>
                    <? } else{ ?>
                     NIL
                    <? } ?>
                </td>
                <td>
                    <? if($il->senior_pres_prog!='' ){ ?>
                     <?=$il->senior_pres_prog ?><br>
                     valid from<br>
                     <?=$il->senior_pres_valid_from ?>
                    <? } else{ ?>
                     NIL
                    <? } ?>
                </td>
                <td>
                    <? if($il->teacher_training_prog!='' ){ ?>
                     <?=$il->teacher_training_prog ?><br>
                     valid from<br>
                     <?=$il->teacher_training_valid_from ?>
                    <? } else{ ?>
                     NIL
                    <? } ?>
                </td>
                
            </tr>
            <?
            }
            ?>
        </table>
            <?
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
	}
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Ilc;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ilc']))
		{
			$model->attributes=$_POST['Ilc'];
                        
                        /////////////////////////////////////////////////
                            $rid=$_POST['Ilc']['ilcid'];
                          $Criteria1=new CDbCriteria();
                           $Criteria1->condition="ilcid=:uid";
                           $Criteria1->params=array(':uid'=>$rid);
                           $row=  Ilc::model()->find($Criteria1);
                           
                            if(sizeof($row)==0)
                        {
                         //image upload
           $rnd = rand(0,9999);  // generate random number between 0-9999
              $uploadedFile=CUploadedFile::getInstance($model,'agreement');
              
              if( is_object($uploadedFile) )
              {
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
            $model->agreement = $fileName;
            $uploadedFile->saveAs(Yii::app()->basePath.'/../agreement/'.$fileName);  // image will uplode to rootDirectory/banner/
            //end of image upload  
            
             //resize
            $name=Yii::app()->basePath.'/../agreement/'.$fileName;
//            	    $image = Yii::app()->image->load($name);
//	    $image->resize(300,500);
//	    $image->save();
                        }
                        }
                         else
                        {
                            $rid=$_POST['Ilc']['ilcid'];
                            //$comp_id=$_POST['ProfPic']['comp_id'];
                           // $model->attributes=$_POST['ProfPic'];
                           $Criteria=new CDbCriteria();
                           $Criteria->condition="ilcid=:uid";
                           $Criteria->params=array(':uid'=>$rid);
                           $row= Ilc::model()->find($Criteria);
                         //unlink image
                        unlink(getcwd().'/agreement/'.$row->agreement);
                         Ilc::model()->deleteAll('ilcid=:uid' , array(':uid'=>$rid));
                             //image upload
           $rnd = rand(0,9999);  // generate random number between 0-9999
              $uploadedFile=CUploadedFile::getInstance($model,'agreement');
              
              if( is_object($uploadedFile) )
              {
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
            $model->agreement = $fileName;
            $uploadedFile->saveAs(Yii::app()->basePath.'/../agreement/'.$fileName);  // image will uplode to rootDirectory/banner/
            //end of image upload  
            
             //resize
            $name=Yii::app()->basePath.'/../agreement/'.$fileName;
//            	    $image = Yii::app()->image->load($name);
//	    $image->resize(300, 500);
//	    $image->save(); 
              }
                        }
                        //////////////////////////////////////////////////
                        
			if($model->save())
                        {	
                            $ilcid=$model->ilcid;
                            $partner=$model->partner_name;
                            $uu=new Users();
                            $uu->updateAll(array('ilcid'=>$ilcid,'pa'=>1), "userid=:n", array(':n'=>$partner));
                            
                            $this->redirect(array('ilclist','id'=>$model->id));
                            
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Ilc']))
		{
			$model->attributes=$_POST['Ilc'];
                        
                        /////////////////////////////////////////////////
                            $rid=$_POST['Ilc']['ilcid'];
                          $Criteria1=new CDbCriteria();
                           $Criteria1->condition="ilcid=:uid";
                           $Criteria1->params=array(':uid'=>$rid);
                           $row=  Ilc::model()->find($Criteria1);
                           
                            if(sizeof($row)==0)
                        {
                         //image upload
           $rnd = rand(0,9999);  // generate random number between 0-9999
              $uploadedFile=CUploadedFile::getInstance($model,'agreement');
              
              if( is_object($uploadedFile) )
              {
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
            $model->agreement = $fileName;
            $uploadedFile->saveAs(Yii::app()->basePath.'/../agreement/'.$fileName);  // image will uplode to rootDirectory/banner/
            //end of image upload  
            
             //resize
            $name=Yii::app()->basePath.'/../agreement/'.$fileName;
//            	    $image = Yii::app()->image->load($name);
//	    $image->resize(300,500);
//	    $image->save();
                        }
                        }
                         else
                        {
                            $rid=$_POST['Ilc']['ilcid'];
                            //$comp_id=$_POST['ProfPic']['comp_id'];
                           // $model->attributes=$_POST['ProfPic'];
                           $Criteria=new CDbCriteria();
                           $Criteria->condition="ilcid=:uid";
                           $Criteria->params=array(':uid'=>$rid);
                           $row= Ilc::model()->find($Criteria);
                         //unlink image
                        unlink(getcwd().'/agreement/'.$row->agreement);
                         //Ilc::model()->deleteAll('ilcid=:uid' , array(':uid'=>$rid));
                             //image upload
           $rnd = rand(0,9999);  // generate random number between 0-9999
              $uploadedFile=CUploadedFile::getInstance($model,'agreement');
              
              if( is_object($uploadedFile) )
              {
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
            $model->agreement = $fileName;
            $uploadedFile->saveAs(Yii::app()->basePath.'/../agreement/'.$fileName);  // image will uplode to rootDirectory/banner/
            //end of image upload  
            
             //resize
            $name=Yii::app()->basePath.'/../agreement/'.$fileName;
//            	    $image = Yii::app()->image->load($name);
//	    $image->resize(300, 500);
//	    $image->save(); 
              }
                        }
                        //////////////////////////////////////////////////
                        
			if($model->save()){
//                             $ilcid=$model->ilcid;
//                            $othf=$model->othf;
//                            echo "$ilcid-$othf";
//                            die();
//                            $uu=new Ilc();
//                            $uu->updateAll(array('othf'=>$othf), "ilcid=:n", array(':n'=>$ilcid));
//                            
                            $this->redirect(array('ilclist','id'=>$model->id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            $m=$this->loadModel($id);
            $ilcid=$m->ilcid;
            
            $milc=new ManagerIlc();
            $milc->deleteAll('ilcid=:u', array(':u'=>$ilcid));
            
            $miln=new Notifications();
            $miln->deleteAll('ilc=:u', array(':u'=>$ilcid));
                    
            $this->loadModel($id)->delete();
                
                
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Ilc');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
           // $this->redirect('admin');
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Ilc('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Ilc']))
			$model->attributes=$_GET['Ilc'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Ilc the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Ilc::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Ilc $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ilc-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
    public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
}
