<?php

class HolidayController extends Controller
{
    public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        public function accessRules()
        {

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolida or role=:rolidd';
    	$criteria1->params = array(':rolida'=>'academic',':rolidd'=>'director');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
        
        return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','getnoty','checknum','nlist','getweekend'),
    		'users'=>$modad,
    	),
        
        array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('sethol','hol','delhol','setwcal','setwhol','getwhol','gethmon'),
    		'users'=>$modad,
    	),
            
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
	public function actionIndex()
	{
		$this->render('index');
	}
         public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
       public function actionSethol(){
            if(Yii::app()->request->isPostRequest) {
           $htopic=trim($_POST['htopic']);
           $hdate=trim($_POST['hdate']);
           $rolid = Yii::app()->user->getState("rolid");
           $userid=Yii::app()->user->getState('user_id');
           $hy=date('Y',  strtotime($hdate));
           $hm=date('m',  strtotime($hdate));
            $qq=new CalenderDate();
            $qq->calid=uniqid();
            $qq->startd=$hdate;
            $qq->endd=$hdate;
            $qq->topic=$htopic;
            $qq->userid=$userid;
            $qq->queryid='hol';
            $qq->approved=1;
            $qq->year=$hy;
            $qq->mon=$hm;
            $qq->date1=intval(strtotime(date('Y-m-d H:i:s')));
            $qq->isNewRecord=true;
            $qq->save(FALSE);
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       public function actionHol(){
           if(Yii::app()->request->isPostRequest) {
           $crq=new CDbCriteria();
            $crq->condition='queryid=:u';
            $crq->params=array(':u'=>'hol');
            $crq->order='startd asc';
            $en= CalenderDate::model()->findAll($crq);
            ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Date</b></td>
                        <td><b>Holiday Topic</b></td>
                        <td><b>Remove</b></td>
                    </tr>
                    <?
            foreach($en as $e){
            ?>
                    <tr>
                        <td><?=date('d-M-Y' ,strtotime($e->startd)) ?></td>
                        <td><?=$e->topic ?></td>
                        <td>
                            <button type="button" id="<?=$e->calid ?>" onclick="delhol(this.id)" class="btn" style="background-color: transparent;border:0">
                                <ion-icon name="close" style="font-size:20px;color:black;important"></ion-icon>
                            </button>
                        </td>
                    </tr>
            <?
            } ?>
            </table>
            </div> <?
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       public function actionDelhol() {
           if(Yii::app()->request->isPostRequest) {
           $hid=trim($_POST['hid']);

           
           $cd=new CalenderDate();
           $cd->deleteAll('calid=:c', array(':c'=>$hid));
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       
       public function actionSetwhol() {
           if(Yii::app()->request->isPostRequest){
           $mons=trim($_POST['mons']);
           $year=trim($_POST['year']);
           $rolid = Yii::app()->user->getState("rolid");
           $userid=Yii::app()->user->getState('user_id');
           $qd=new CalenderDate();
           $qd->deleteAll('year=:y and queryid=:q', array(':y'=>$year,':q'=>'weekend'));
           $months=  explode(',', $mons);
           $pd=  trim($year)."-";
           foreach ($months as $m) { // month loop
               if(!empty($m)) {
                   ///////////////////////////anko/////////////
                   $mm=  intval($m);
                   $d=intval(cal_days_in_month(CAL_GREGORIAN, $mm, $year)); 
                   $str = $pd.$m.'-';
                    for($i2=1; $i2<$d; $i2++) // day loop
                    {

                      // echo '<br>',
                        $ddd = $str.$i2;
                      // echo '',
                        $date = date('Y M D d', $time = strtotime($ddd) );

                      if( strpos($date, 'Sat') || strpos($date, 'Sun') )
                      {
                      $sdate=date('Y-m-d', strtotime($ddd));
                        $qq=new CalenderDate();
                        $nq=  intval($qq->count('startd=:s or endd=:s', array(':s'=>$sdate)));
                        if($nq==0) {
                        $qq->calid=uniqid();
                        $qq->startd=$sdate;
                        $qq->endd=$sdate;
                        $qq->months=$mons;
                        $qq->topic='Weekend';
                        $qq->userid=$userid;
                        $qq->queryid='weekend';
                        $qq->approved=1;
                        $qq->year=$year;
                        $qq->mon=$m;
                        $qq->date1=intval(strtotime(date('Y-m-d H:i:s')));
                        $qq->isNewRecord=true;
                        $qq->save(FALSE);
                      }
                      
                        }
                    }
                   //////////////////////////////////////////
               }
           }
           
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }  
        
       }
       public function actionGetwhol() {
           if(Yii::app()->request->isPostRequest) {
           
           //////////////////////ca;lender///////////////////////
           $items=array();
           $calq=new CDbCriteria();
           $calq->condition='queryid=:q';
           $calq->params=array(":q"=>'weekend');
           $hd=  CalenderDate::model()->findAll($calq);
           foreach ($hd as $h) {
               $items[]=array(
				'title'=>$h->topic,
				'start'=>$h->startd,
				'end'=>$h->endd,
				//'color'=>'#CC0000',
	        	//'allDay'=>true,
	        	//'url'=>'http://anyurl.com'
			);
           }
           
           $ddays=CJSON::encode($items);
           $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
	//'themeCssFile'=>'cupertino/jquery-ui.min.css',
	'options'=>array(
		'header'=>array(
			'left'=>'prev,next,today',
			'center'=>'title',
			'right'=>'month,agendaWeek,agendaDay',
		),
		//'events'=>$this->createUrl('latihan/training/calendarEvents'), // URL to get event
            'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       public function actionGetweekend() {
                  $items=array();
           $calq=new CDbCriteria();
           $calq->condition='queryid=:q or queryid=:r';
           $calq->params=array(":q"=>'weekend' ,":r"=>'hol');
           $hd=  CalenderDate::model()->findAll($calq);
           foreach ($hd as $h) {
               $items[]=array(
				'title'=>$h->topic,
				'start'=>$h->startd,
				'end'=>$h->endd,
				//'color'=>'#CC0000',
	        	//'allDay'=>true,
	        	//'url'=>'http://anyurl.com'
			);
           }
           
           echo CJSON::encode($items);
       }
       public function actionGethmon()
       {
           if(Yii::app()->request->isPostRequest) {
           $y=trim($_POST['y']);
           $cry=new CDbCriteria();
           $cry->condition='year=:y';
           $cry->params=array(':y'=>$y);
           $ry=  CalenderDate::model()->find($cry);
           ?>
          <input type="hidden" id="smon" value="<?=$ry->months ?>" />     
               <?
               }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
}