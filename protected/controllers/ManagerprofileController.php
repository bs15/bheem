<?php

class ManagerprofileController extends Controller
{
	public function actionIndex()
	{
            $uid=$_REQUEST['q'];
            //////////////////query////////////////
            $nq=0;
            $crm=new CDbCriteria();
            $crm->condition='ilcmid=:q';
            $crm->params=array(":q"=>$uid);
            $manilc= ManagerIlc::model()->findAll($crm);
            
            foreach($manilc as $mi)
            {
            $ilcid=$mi->ilcid;
            $crq=new CDbCriteria();
            $crq->condition='query_closed=:q and ilc_id=:i';
            $crq->params=array(':q'=>1,':i'=>$ilcid);
            $queries=  Query::model()->findAll($crq);
            
            foreach($queries as $q)
            {
               $nq++; 
            }
            }
///////////////////////////////////////
            
//////////////call/////////////////////
            $nc=0;
            $crc=new CDbCriteria();
            $crc->condition='closed=:q and ilcmid=:i';
            $crc->params=array(':q'=>1,':i'=>$uid);
            $call=  Call::model()->findAll($crc);
            
            foreach($call as $c)
            {
               $nc++; 
            }
            
///////////////////////////////////////
            
/////////////enquiry///////////////////
            $ne=0;
            $cre=new CDbCriteria();
            $cre->condition='eclosed=:q and ilcmid=:i';
            $cre->params=array(':q'=>1,':i'=>$uid);
            $enqr=  Enquiry::model()->findAll($cre);
            
            foreach($enqr as $e)
            {
               $ne++; 
            }
///////////////////////////////////////
            
//////////////interview/////////////////
            $ni=0;
            $cri=new CDbCriteria();
            $cri->condition='iclosed=:q and ilcmid=:i';
            $cri->params=array(':q'=>1,':i'=>$uid);
            $intr=  Interview::model()->findAll($cri);
            
            foreach($intr as $i)
            {
               $ni++; 
            }
///////////////////////////////////////

//////////////admission/////////////////
            $na=0;
            $crmm=new CDbCriteria();
            $crmm->condition='ilcmid=:q';
            $crmm->params=array(":q"=>$uid);
            $manilcs= ManagerIlc::model()->findAll($crm);
            
            foreach($manilcs as $mm)
            {
            $ilcid=$mm->ilcid;
            $cra=new CDbCriteria();
            $cra->condition='aclosed=:q and ilcid=:i';
            $cra->params=array(':q'=>1,':i'=>$ilcid);
            $adm=  Admission::model()->findAll($cra);
            
            foreach($adm as $a)
            {
               $na++; 
            }
            }
///////////////////////////////////////
            
            $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$uid);
                $uinfo=  Users::model()->find($cru);
                $uinfop=  Userphoto::model()->find($cru);
                
            $this->render('index',array("uid"=>$uid,"ni"=>$ni,"na"=>$na,"nq"=>$nq,"nc"=>$nc,"ne"=>$ne,"uinfo"=>$uinfo,"uinfop"=>$uinfop));
	}

	 public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
       
}