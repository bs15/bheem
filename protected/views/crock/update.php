<?php
/* @var $this CrockController */
/* @var $model Crock */

$this->breadcrumbs=array(
	'Crocks'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Crock', 'url'=>array('index')),
	array('label'=>'Create Crock', 'url'=>array('create')),
	array('label'=>'View Crock', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Crock', 'url'=>array('admin')),
);
?>

<h1>Update Crock <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>