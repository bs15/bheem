<? $rolid=Yii::app()->user->getState("rolid"); 
$userid=Yii::app()->user->getState('user_id');?>
<?
$criteria1 = new CDbCriteria();
$criteria1->condition = 'ilcmid=:ilm';
$criteria1->params=array("ilm"=>$userid);
$ilcs = ManagerIlc::model()->findAll($criteria1);

$criteria1 = new CDbCriteria();
$criteria1->select = 'state';
$criteria1->group="state";
$states = Ilc::model()->findAll($criteria1);
?>
<? if($rolid=='director'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
                Escalations > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/index">
                    <span style="color:blue">Admission Escalations</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='academic'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Escalations > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/index">
                    <span style="color:blue">Admission Escalations</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='ilcmanager'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
                Management > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/index">
                    <span style="color:blue">Admission </span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='unit'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Administration > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/index">
                    <span style="color:blue">Admission Records</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='partner'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/index">Home</a> >
                Administration > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/index">
                    <span style="color:blue">Admission Records</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    ?>
<div class="row" style="margin-top:-2%">
    
        <div class="col-md-12">
            <br>
            <h1>Current ADMISSIONS</h1>
               
        </div>
</div>
        <div class="row">
            <div class="col-md-12">
                
                <? if($rolid=='academic' || $rolid=='director'){?>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <input type="hidden" id="silc2" value="-">
                        <select id="st" class="enin" onchange="getcity()" style="margin-left:1%">
                            <option value="0">Select State</option>
                            <? foreach($states as $st){
                                ?>
                                <option value="<?=$st->state ?>"><?=$st->state ?></option>
                                <?
                            } ?>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-12" id="loadcity"></div>

                    <div class="col-md-3 col-sm-12" id="loadilc"></div>
                </div>
            <? } ?>
            <? if($rolid=='ilcmanager'){?>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <select id="il" class="enin" onchange="getres()" style="margin-left:1%">
                            <option value="0">Select ILC</option>
                            <? foreach($ilcs as $i){
                                ?>
                                <option value="<?=$i->ilcid ?>"><?=$i->ilcname ?></option>
                                <?
                            } ?>
                        </select>
                    </div>
                </div>
            <? } ?>
                
            <div id="adm">
                
            </div>
            </div>
        </div>

<div id="myModalai" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left">Additional Information</h4>
        <button type="button" class="close" id="btn-closed" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="bodyai">
        <p>Some text in the modal.</p>
      </div>
      
    </div>

  </div>
</div>

<div id="myModalfeed" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left">Give call feedback</h4>
        <button type="button" class="close" id="btn-closed1" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="bodyfeed">
        <p>Some text in the modal.</p>
      </div>
      
    </div>

  </div>
</div>


<script>
$(document).ready(function(){

    $("#datepicker").datepicker({
  dateFormat: "yy-mm-dd" ,minDate: 0
});
$("#datepicker1").datepicker({
  dateFormat: "yy-mm-dd" ,minDate: 0
});
    getq();
});

function getres(){
    var ilc1=$("#il option:selected").val();
    var st=$("#st option:selected").val();
    var ci=$("#ci option:selected").val();
    
    if(ilc1=='0' && st=='0' && ci=='0'){
        alert("Select ILC");
    }
    if(ilc1==undefined){
        ilc1='0';
    }
    if(ci==undefined){
        ci='0';
    } 
    if(st==undefined){
        st='0';
    } 
   
        //////////////////////ajax////////////////////////
        //alert("ilc="+ilc1+" city="+ci+" state="+st);
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#adm").show().html('loading...........');
        },
        success:function(html){
        $("#adm").empty().append(html);
        
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getadmilc"); ?>',
        data:{
          st:st,ci:ci,ilc1:ilc1,
        },
    dataType:"html",
    cache:false
})//ajax
    
 }
function getcity(){
     var st1=$('#st option:selected').val();
     if(st1=='0')
     {
         alert('Select a state ');
     }
     else
     {
         //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#loadcity").show().html('loading...........');
        },
        success:function(html){
           
        $("#loadcity").empty().append(html);
         getres();
     
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getcity"); ?>',
        data:{
          st1:st1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
function getilc(){
     var ci1=$('#ci option:selected').val();
     if(ci1=='0')
     {
         alert('Select a city ');
     }
     else
     {
        //////////////////////ajax////////////////////////
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#loadilc").show().html('loading...........');
        },
        success:function(html){
            getres();
        $("#loadilc").empty().append(html);
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getilc"); ?>',
        data:{
          ci1:ci1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 }
function getq() {
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#adm").show().html('Loading...........');
        },
                
        success:function(html){
        $("#adm").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("adm"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function searchilc(ilcid1) {
    //alert(ilcid);
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#adm").show().html('Loading...........');
        },
                
        success:function(html){
        $("#adm").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("adm2"); ?>',
        data:{
          ilcid1:ilcid1,
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function setai(adid){
    $("#myModalai").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyai").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyai").empty().append(html);
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        $("#datepicker1").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        gethow();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("setai"); ?>',
        data:{
          adid:adid,
        },
    dataType:"html",
    cache:false
})//ajax
}

function viewai(adid){
    $("#myModalai").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyai").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyai").empty().append(html);
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        $("#datepicker1").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        gethow();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("viewai"); ?>',
        data:{
          adid:adid,
        },
    dataType:"html",
    cache:false
})//ajax
}

function updateai(){
    var hhear="";
    var adid=$("#adid").val();
    var parid=$("#parid").val();
    var adtype1=$("#adtype option:selected").val();
    var recno=$("#recno").val();
    var enrno=$("#enrno").val();
    var recdate=$("#datepicker").val();
    var enrdate=$("#datepicker1").val();
    var mto=$("#mto").val();
    var mname=$("#mname").val();
    var focc=$("#focc").val();
    var mocc=$("#mocc").val();
    var foa=$("#foa").val();
    var moa=$("#moa").val();
    
    $('.form-check-input').each(function(e){
        if($(this).is(':checked')){
            hhear+=$(this).val()+',';
        }
    });
    var mphone=$("#mphone").val();
    var memail=$("#memail").val();
    var allergy=$("#allergy").val();
    var blood=$("#blood").val();
    var omed=$("#omed").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyd").show().html('loading...........');
        },
                
        success:function(html){
        $("#bodyd").empty().append(html);
        $("#btn-closed").click();
        getq();
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updateai"); ?>',
        data:{
            adid:adid,parid:parid,recno:recno,enrno:enrno,recdate:recdate,enrdate:enrdate,mto:mto,mname:mname,focc:focc,mocc:mocc,foa:foa,moa:moa,hhear:hhear,mphone:mphone,memail:memail,allergy:allergy,blood:blood,omed:omed,adtype1:adtype1
           },
    dataType:"html",
    cache:false
})//ajax
}

function updatefeed(){
    var adid=$("#adid").val();
    var feed=$("#feed").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyfeed").show().html('loading...........');
        },
                
        success:function(html){
        $("#bodyfeed").empty().append(html);
        $("#btn-closed1").click();
        getq();
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
            adid:adid,feed:feed,
    },
    dataType:"html",
    cache:false
})//ajax
}

function gethow(){
    var how=$('#how').val();
    $('.form-check-input').each(function(e){
       var howval=$(this).val(); 
       if(how.indexOf(howval) != -1){
           $(this).attr('checked','true');
       }
    });

}

function browsefile2() {
$("#file2").click();
}

function browsefile() {
$("#file").click();
}

function upfile() {
    var file_data = $('#file').prop('files')[0]; 
    var adid= $("#adid").val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('adid', adid);
 
       $.ajax({
        url: '<? echo $this->createUrl("uploadphoto"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
      
            alert("Picture selected..");
            getq();
            
        }
     });
}

function upfile2() {
    var file_data = $('#file2').prop('files')[0]; 
    var adid= $("#adid").val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('adid', adid);
 
    $.ajax({
        url: '<? echo $this->createUrl("uploadphoto2"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
           error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },     
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
      
            alert("File Attached");
            getq();
            
        }
     });
}

function setfeed(adid){
    $("#myModalfeed").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyfeed").show().html('Loading...');
        },
                
        success:function(html){
         $("#bodyfeed").empty().append(html);
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("setfeed"); ?>',
        data:{
          adid:adid,
        },
    dataType:"html",
    cache:false
})//ajax
}
function updatefeed() {
var adid=$("#adid2").val();
var feed=$("#feed").val();
if(feed=="") {
    alert("Give a feedback first!");
}
else
{
     $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyfeed").show().html('Updating...');
        },
                
        success:function(html){
         $("#bodyfeed").empty().append(html);
           $("#myModalfeed").modal('hide');
           getq();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
          adid:adid,feed:feed
        },
    dataType:"html",
    cache:false
})//ajax
}
}

function closeadm(adid) {
var y=confirm("Are you sure you want to close?");
if(y) {
///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                 
        success:function(html){
        alert("This Admission has been closed"); 
           var url1='<? echo Yii::app()->request->baseUrl?>/index.php/admission/archive';
           window.location.href=url1;
         },
             error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("closeadm"); ?>',
        data:{
          adid:adid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
</script>
