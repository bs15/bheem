<?php
/* @var $this UserphotoController */
/* @var $model Userphoto */
/* @var $form CActiveForm */
$userid=Yii::app()->user->getState('user_id');
?>

<div class="form">
    <div class="container">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'userphoto-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
        'enctype' => 'multipart/form-data',)
)); ?>

	<span style="margin-left:3%;font-size:12px !important;margin-top:-2%">Fields with <span class="required">*</span> are required.</span>
        <br style="clear:both">
	<?php echo $form->errorSummary($model); ?>

        
	
		<?php //echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->hiddenField($model,'userid',array('size'=>60,'maxlength'=>100,'value'=>$userid)); ?>
		
	

	<div class="row" style="margin-bottom:15px">
		<div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'uphoto'); ?></div>
                <div class="col-md-7">
                <div class="input-group input-group-sm">
		<?php echo CHtml::activeFileField($model,'uphoto',array("class"=>"form-control")); ?>
		<?php echo $form->error($model,'uphoto'); ?>
                </div>
                </div>
	</div>

	<div class="row buttons" style="margin:4%">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array("class"=>"btn btn-dark")); ?>
	</div>

<?php $this->endWidget(); ?>
    </div>
</div><!-- form -->