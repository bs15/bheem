<?php

class PartnerController extends Controller
{
      public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        
          public function accessRules()
                        	{

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid';
    	$criteria1->params = array(':rolid'=>'partner');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','ilcmanager','createilcm','getilcm','delilcm'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('unit','teacher','createilcm2','getilcm2'),
    		'users'=>$modad,
    	),
              array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('query','querylist','queryhistory','createquery','getquery','closequery','getchat','sendchat','getqmy','uploadpost'),
    		'users'=>$modad,
    	),
         array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('getquery2','getquery3','setnoty','checknum','nlist'),
    		'users'=>$modad,
             ),   
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('interview','interview2','closeint','schedulei','intlist1','intlist2','getimy'),
    		'users'=>$modad,
    	),
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        
        public function actionInterview() {
            
        $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
     $crt=new CDbCriteria();
        $crt->condition='ilcid=:r';
        $crt->params=array(':r'=>$ilcid);
        $rest= ManagerIlc::model()->find($crt);
        $ilcmid=$rest->ilcmid;   
        if($rolid=='unit' || $rolid=='partner'){
            Yii::app()->user->setState('man',$ilcmid);
            }
           
    $this->render('interview',array('man'=>$ilcmid));
    }
    
        //////////////////////upload post ///////////////////
    public function actionUploadpost() {
        $model = new QueryChat();
        $userid=Yii::app()->user->getState('user_id');
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
     //   $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
                $model->queryid=trim($_POST['qid']);
                $model -> document = $fileName;
	
                
                $model->chatid=  uniqid();
         $model->senderid=$userid;
         $model->recid='-';
         $model->message='-';
         $model->date1=  strtotime(date('Y-m-d H:i:s'));
         
	if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../chatdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
            }

        public function actionInterview2() {
        $this->render('interview2');
        }
 public function actionGetimy() {
     if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);


      $y=  intval($yea);
      $m= intval($mon);
        $userid=Yii::app()->user->getState('user_id');
      $ilcid=Yii::app()->user->getState('ilc_id');
        $crq=new CDbCriteria();
        if($y!=0 && $m==0)
        {
           $crq->condition='userid=:u and year=:y and iclosed=1';
        $crq->params=array(':u'=>$userid,':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
              $crq->condition='userid=:u and year=:y and month=:m and iclosed=1';
        $crq->params=array(':u'=>$userid,':m'=>$m,':y'=>$y);
        }
      
        
        $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p> No Interviews have been conducted yet</p> <?
        }
        else 
        {
            ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Resume</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
//                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                ?>
            <tr>
                 <td><?=$i->candidatename ?>
                            
                        </td>
                        <td>
                            <a style="font-size:14px !important; padding:4px;border-radius:2px;border:1px solid black" class="btn-light" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a></td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         
                    </tr>

<?
            } ?>
                </table>
                    </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
 }
 
        public function actionIntlist1() {
        if(Yii::app()->request->isPostRequest) {
          
        $ilcid=Yii::app()->user->getState('ilc_id');
        $userid=Yii::app()->user->getState('user_id');
        $crq=new CDbCriteria();
        $crq->condition='ilcid=:u and iclosed=0 and userid=:us';
        $crq->params=array(':u'=>$ilcid,':us'=>$userid);
        $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        
         if($n==0) {
            ?> <p>Schedule an interview by clicking on the above button</p> 
            <?
        }
        else 
        { ?>
            <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>
            <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                //echo $uname.' role='.$role;
//                $user=$i->ilcid;
//                $cru=new CDbCriteria();
//                $cru->condition='ilc=:u';
//                $cru->params=array(":u"=>$user);
//                $uinfo=Users::model()->find($cru);
                ?>
            <tr>
                    <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                       
                        
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            
                            if($istatus=='0' && $d<=48){
                              ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" value="Active"  />

                            <?   
                             }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                           ?>
                        </td>
                        <td>
                           <? if($i->iclosed==0) { ?>
                    <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Close" onclick="closeint(this.id)" />
                        <? } ?> </td>
                    </tr> 
<?
            } ?>
                </table>
            </div>
                    <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
        }
 }
 
        public function actionIntlist2() {
        if(Yii::app()->request->isPostRequest) {
         
        $ilcid=Yii::app()->user->getState('ilc_id');
        $userid=Yii::app()->user->getState('user_id');
        $crq=new CDbCriteria();
        $crq->condition='ilcid=:u and iclosed=1 and userid=:us';
        $crq->params=array(':u'=>$ilcid,':us'=>$userid);
        $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p> No interviews have been conducted yet</p> <?
        }
        else 
        { ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Resume</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
            <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
                 $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
//                $user=$i->ilcid;
//                $cru=new CDbCriteria();
//                $cru->condition='ilc=:u';
//                $cru->params=array(":u"=>$user);
//                $uinfo=Users::model()->find($cru);
                ?>
            <tr>
                 <td><?=$i->candidatename ?>
                            
                        </td>
                        <td>
                            <a style="font-size:14px !important; padding:4px;border-radius:2px;border:1px solid black" class="btn-light" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a></td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         
                    </tr>
<?
            } ?>
                </table>
            </div>
<?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
 }
 
        public function actionCloseint() {
            if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);
 
         $cru=new CDbCriteria(); 
        $cru->condition='intid=:u';
        $cru->params=array(":u"=>$intid);
        $inter= Interview::model()->find($cru);
        
        $ilcid=$inter->ilcid;
        
    $qq=new Interview();
    $qq->updateAll(array("iclosed"=>1,"clodate"=>date('Y-m-d')), 'intid=:q', array(":q"=>$intid));
    
    $qq=new Assignesc();
    $qq->updateAll(array("close"=>1), 'intid=:q', array(":q"=>$intid));
    
    $qq=new Notifications();
    $qq->deleteAll("queryid=:q ", array(":q"=>$intid));
    
    $mst="The interview of ".$inter->candidatename." has been closed";
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="ilcmanager";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $im=new Nlist();
    $us=$im->getilcmid($ilcid);
    $ilcmid=$us->ilcmid;
    
    $usr=new Nlist();
    $uu=$usr->getuser($ilcmid);
    $uname=$uu->username;
    $name=$uu->name;
    
    
////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Interview Closed";
            $msg="Dear $name,<br>
                The interview of $inter->candidatename has been closed<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
    
}

        public function actionSchedulei() {
    
        $model = new Interview();
        $userid=Yii::app()->user->getState('user_id');
        $ilcid=Yii::app()->user->getState('ilc_id');
        
        $crq1=new CDbCriteria();
        $crq1->condition='ilcid=:u';
        $crq1->params=array(':u'=>$ilcid);
        $ill= Ilc::model()->find($crq1);
        $state=$ill->state;
        $city=$ill->city;
        
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
        $fname=$_POST['fname'];
        $datep=$_POST['datep'];
        $h=$_POST['hour'];
        $m=$_POST['min'];
        $da=  explode("-", $datep);
        
      //  $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
	$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
        
        $rw=new Report();
        $week=$rw->getweekno($datep);
         $model->intid=  uniqid();
         $model->candidateid=uniqid();
         $model->candidatecv=$fileName;
         $model->candidatename=$fname;
         $model->userid=$userid;
         $model->idate= $datep;
         $model->ihour=$h;
         $model->imin=$m;
         $model->week=$week;
         $model->month=  intval($da[1]);
         $model->year=  intval($da[0]);
         $model->istatus='0';
         $model->ilcid=$ilcid;
         $model->state=$state;
         $model->city=$city;
         $model->ilcmid='-';
         $model->idate1=strtotime(date('Y-m-d H:i:s'));
         
         
         
         if ($model -> save(FALSE)) {
	 move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../cv/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      $intid=$model->intid;
         $cru=new CDbCriteria(); 
        $cru->condition='intid=:u';
        $cru->params=array(":u"=>$intid);
        $inter= Interview::model()->find($cru);
        
        $mst="An interview for".$inter->candidatename." has been scheduled on ".$inter->idate." at ".$inter->ihour.":".$inter->imin;
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="ilcmanager";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $im=new Nlist();
    $us=$im->getilcmid($ilcid);
    $ilcmid=$us->ilcmid;
    
    $usr=new Nlist();
    $uu=$usr->getuser($ilcmid);
    $uname=$uu->username;
    $name=$uu->name;
    
    
////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk New Interview Scheduled";
            $msg="Dear $name,<br>
                An interview for $inter->candidatename has been scheduled on $inter->idate at $inter->ihour:$inter->imin<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
    
    
}

            }
	public function actionIndex()
	{
        $uid = Yii::app()->user->getState("user_id");
        $cr = new CDbCriteria();
        $cr->condition='userid=:u';
        $cr->params=array(':u'=>$uid);
        $user=Users::model()->find($cr);
        $userp=Userphoto::model()->find($cr);
	$this->render('index',array('user'=>$user,'userp'=>$userp));
        
	}
        public function actionUnit() {
            $this->render('unit');
        }
        public function actionTeacher() {
        
            $this->render('teacher');
        }
        public function actionQuerylist() {
    $this->render('querylist');
}

         public function actionGetqmy() {
        if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);


        $y=intval($yea);
        $m=intval($mon);
        $ilcid=Yii::app()->user->getState('ilc_id');
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
       if($m!=0 && $y!=0)
        {
        $crq->condition="month=:m and year=:y and ilc_id=:i and query_closed=1";
        $crq->params=array(":m"=>$m,":y"=>$y,":i"=>$ilcid);
        }
        else if($m==0 && $y!=0)
           {
        $crq->condition="year=:y and ilc_id=:i and query_closed=1";
        $crq->params=array(":y"=>$y,":i"=>$ilcid);
        } 
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?> <p style="margin-top:8%">No Queries were raised in this month !</p> <?
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <? foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ?>
                <tr>
                <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
                <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
                <td><?=$q->query_topic ?></td>
                <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                <td>
                    <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                </td>
                </tr>
            <?
                
            }
            ?>
                </table>
           </div>
            <?
          }
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
 }
        public function actionQueryhistory() {
            $this->render('queryhistory');
        }
  public function actionCreateilcm() {
     if(Yii::app()->request->isPostRequest) {
           $uname=trim($_POST['uname']);
           $name=trim($_POST['name']);
           $org=trim($_POST['org']);
           $nation=trim($_POST['nation']);
           $address=trim($_POST['address']);
           $country=trim($_POST['country']);
           $phone=trim($_POST['phone']);

      $ilcid=Yii::app()->user->getState('ilc_id'); 
     $naame=Yii::app()->user->getState('name');
     $cr=new CDbCriteria();
     $cr->condition='partner_name=:p';
     $cr->params=array(":p"=>$naame);
     $nam=Ilc::model()->find($cr);
      $uid = uniqid();
        $uu = new Users();
        $n = intval($uu->count('username=:u or phone=:p', array(":u" => $uname, ":p" => $phone)));
        if ($n > 0) {
            echo "User already exists......";
        } else {
            
            ////////////////send mail//////////////////
            $to=$uname;
            $url="http://tmp-mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Coordinator Registration";
            $msg="Dear $name,<br>
                You have been registered as a beanstalk coordinator.<br>
                Please click on this <a target=\"_blank\" href=\"$url\">LINK</a> to reset password and complete registration process.<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            $uu->isNewRecord = TRUE;
            $uu->userid = $uid;
            $uu->name = $name;
            $uu->username = $uname;
            $uu->password = md5('-');
            $uu->role = 'unit';
            $uu->block = 0;
            $uu->ilcid=$ilcid;
            $uu->org = $org;
            $uu->address = $address;
            $uu->country = $country;
            $uu->nationality = $nation;
            $uu->phone = $phone;
            $uu->save(FALSE);
            echo "Profile created.";
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
    }
       public function actionGetilcm() {
           if(Yii::app()->request->isPostRequest) {
          
           $ilcid=Yii::app()->user->getState('ilc_id');
        $cr=new CDbCriteria();
            $cr->condition="role=:r and ilcid=:i";
            $cr->params=array(":r"=>'unit',":i"=>$ilcid);
            $ilcms=  Users::model()->findAll($cr);
            if(sizeof($ilcms)==0) {
                echo "No entries..";
            }
 else {
     ?>
            <div class="table-font">
                <table class="table table-striped">
                    <tr>
                        <td><b>Photo</b></td>
                        <td><b>Name</b></td>
                        <td><b>Username</b></td>
                        <td><b>Password</b></td>
                        <td><b>Country</b></td>
                        <td><b>Organization</b></td>
                        <td><b>Phone</b></td>
                        <td><b>Nationality</b></td>
                        <td><b>Reg URL</b></td>
                        <td></td>
                    </tr>
         <?
                foreach ($ilcms as $mm) {
                    ?>
                    <tr>
                        <td>
                            <img src="<?=Yii::app()->request->baseUrl ?>/ilcmimg/<?=$mm->photo ?>" class="table-img" alt="no photo">
                        </td>
                        <td>
                           <?=$mm->name ?> 
                        </td>
                        <td>
                          <?=$mm->username ?>  
                        </td>
                        <td>
                           <?=$mm->password ?> 
                        </td>
                        <td>
                           <?=$mm->country ?> 
                        </td>
                        <td>
                           <?=$mm->org ?> 
                        </td>
                        <td>
                            <?=$mm->phone ?>
                        </td>
                        <td>
                            <?=$mm->nationality ?>
                        </td>
                        <td>
                            <?=Yii::app()->request->baseUrl ?>/index.php/newregister/?p=<?=$mm->username ?>&q=<?=$mm->userid ?>
                        </td>
                        <td>
                            <a id="<?=$mm->userid ?>" onclick="delilcm(this.id)" class="btn-light">
                               <ion-icon name="close" style="font-size:20px;color:black;margin-top:7%!important"></ion-icon></button>
                             </a>
                        </td>
                    </tr>
                    
                    <?
                } ?> </table>
                    </div>
                        <?
        }
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
              }
        }
          public function actionDelilcm() {
              if(Yii::app()->request->isPostRequest) {
               $id=trim($_POST['id']);
              $uu=new Users();
            $uu->deleteAll('userid=:u', array(":u"=>$id));
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
                }
        }
         public function actionCreateilcm2() {
             if(Yii::app()->request->isPostRequest) {
                $uname=trim($_POST['uname']);
                $name=trim($_POST['name']);
                $org=trim($_POST['org']);
                $nation=trim($_POST['nation']);
                $address=trim($_POST['address']);
                $country=trim($_POST['country']);
                $phone=trim($_POST['phone']);

             $ilcid=Yii::app()->user->getState('ilc_id');
          $naame=Yii::app()->user->getState('name');
     $cr=new CDbCriteria();
     $cr->condition='partner_name=:p';
     $cr->params=array(":p"=>$naame);
     $nam=Ilc::model()->find($cr);
             $uid = uniqid();
        $uu = new Users();
        $n = intval($uu->count('username=:u or phone=:p', array(":u" => $uname, ":p" => $phone)));
        if ($n > 0) {
            echo "User already exists......";
        } else {
            
            ////////////////send mail//////////////////
            $to=$uname;
            $url="http://tmp-mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Teacher Registration";
            $msg="Dear $name,<br>
                You have been registered as a beanstalk teacher.<br>
                Please click on this <a target=\"_blank\" href=\"$url\">LINK</a> to reset password and complete registration process.<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            $uu->isNewRecord = TRUE;
            $uu->userid = $uid;
            $uu->name = $name;
            $uu->username = $uname;
            $uu->password = md5('-');
            $uu->ilcid=$ilcid;
            $uu->role = 'teacher';
            $uu->block = 0;
            $uu->org = $org;
            $uu->address = $address;
            $uu->country = $country;
            $uu->nationality = $nation;
            $uu->phone = $phone;
            $uu->save(FALSE);
            echo "Profile created.";
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
    }
       public function actionGetilcm2() {
           if(Yii::app()->request->isPostRequest) {
         
        $ilcid=Yii::app()->user->getState('ilc_id');   
        $cr=new CDbCriteria();
            $cr->condition="role=:r and ilcid=:i";
            $cr->params=array(":r"=>'teacher',':i'=>$ilcid);
            $ilcms=  Users::model()->findAll($cr);
            if(sizeof($ilcms)==0) {
                echo "No entries..";
            }
 else { ?>
            <div class="table-responsive">
                <table class="table table-striped table-font">
                    <tr>
                        <td><b>Photo</b></td>
                        <td><b>Name</b></td>
                        <td><b>Username</b></td>
                        <td><b>Password</b></td>
                        <td><b>Organization</b></td>
                        <td><b>Phone</b></td>
                        <td><b>Nationality</b></td>
                        <td><b>Reg URL</b></td>
                        <td></td>
                    </tr>
     <?
                foreach ($ilcms as $mm) {
                    
                    ?>
                    <tr>
                        <td>
                            <img src="<?=Yii::app()->request->baseUrl ?>/ilcmimg/<?=$mm->photo ?>" class="table-img" alt="no photo">
                        </td>
                        <td><?=$mm->name ?></td>
                        <td><?=$mm->username ?></td>
                        <td><?=$mm->password ?></td>
                        <td><?=$mm->org ?></td>
                        <td><?=$mm->phone ?></td>
                        <td><?=$mm->nationality ?></td>
                        <td><?=Yii::app()->request->baseUrl ?>/index.php/newregister/?p=<?=$mm->username ?>&q=<?=$mm->userid ?></td>
                        <td>
                            <a id="<?=$mm->userid ?>" onclick="delilcm(this.id)" class="btn-light">
                               <ion-icon name="close" style="font-size:20px;color:black;margin-top:7%!important"></ion-icon></button>
                             </a>
                        </td>
                    </tr>
                    
                    <?
                    } ?> </table>
            </div> <?
        }
 
	}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
  public function actionGetquery2() {
      if(Yii::app()->request->isPostRequest) {
         
        $uid=Yii::app()->user->getState('ilc_id');
        $crq=new CDbCriteria();
        $crq->condition='ilc_id=:u and ilc_approved=1 and query_closed=0';
        $crq->order='query_date desc';
        $crq->params=array(':u'=>$uid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?>
            <p>No Queries have been raised by coordinator.</p>
                <?
        }
        else 
        {?>
            <div class="table table-striped table-font">  
                <table>
                    <tr >
                        <td><b>Topic</b></td>
                        <td><b>Created on</b></td>
                        <td><b>Status</b></td>
                        <td><b>Chat</b></td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                ?>
                    <tr>
                        <td><?=$q->query_topic ?></td>
                        <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                        <td>
                            <span class="btn-warning" style="font-size:14px !important; padding:4px;"><?=($q->query_closed==0)?"Active":"Closed" ?></span>
                        </td>
                        <td>
                            <input type="button" style="font-size:14px !important; padding:3px;border-radius:5px;" class="btn-dark" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                        </td>
                        
                    </tr>

                    <?
            } ?>
                </table>
            </div>
                <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
        }
 }
  public function actionGetquery3() {
      if(Yii::app()->request->isPostRequest) {
        
        $uid=Yii::app()->user->getState('ilc_id');
        $userid=Yii::app()->user->getState('user_id');
        $crq=new CDbCriteria();
        $crq->condition='ilc_id=:u and ilc_approved=1 and query_closed=1';
        $crq->order='query_date desc';
        $crq->params=array(':u'=>$uid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        {?>
            <div class="table table-striped table-font">  
                <table>
                    <tr >
                        <td><b>Topic</b></td>
                        <td><b>Created on</b></td>
                        <td><b>Feedback(out of 5)</b></td>
                        <td><b>Chat</b></td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                ?>
<tr>
                        <td><?=$q->query_topic ?></td>
                        <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                       
                        <td>
                        <?
                        $qid1=$q->qid;
                        $crq=new CDbCriteria();
                $crq->condition='queryid=:u';
                $crq->params=array(":u"=>$qid1);
                $qinfo= QueryFeed::model()->find($crq);
                         ?>
                            <p><?=$qinfo->rating ?></p>   
                        </td>
                        <td>
                          <input type="button" style="font-size:14px !important; padding:3px;border-radius:5px;" class="btn-dark" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                        </td>
                    </tr>      
                    <?
            } ?>
                </table>
            </div>
                    <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
             }
 }
 public function actionGetchat() {
   if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $uid=trim($_POST['uid']);


    $cr=new CDbCriteria();
    $cr->condition='queryid=:q and priv=0';
    $cr->params=array(":q"=>$qid);
    $cr->order='date1 asc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       echo "no chats";
   }
 else {
     $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
    ?>
            <br>
            <p style="font-size:16px !important" ><b>Topic:</b> <?=$topic ?>  </p>
<div id="chatui">
     <?
       foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
         if($uinf->role=='unit'){
           ?>
           <div class="alert alert-success talk" style="float:left">
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='partner')
         {
           ?>
            <div class="alert alert-warning talk" style="float:right">
    <b style="font-size:14px !important"><?=$uinf->name ?>[Unit Partner]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
           <?  
         }
         else{
             ?>
                <div class="alert alert-danger talk" style="float:left">
    <b style="font-size:14px !important"><?=$uinf->name ?>[&nbsp;<?=$uinf->role ?>&nbsp;]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
                 <?
         }
         
       }?>
</div> 
    <?
   }
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
    
}
public function actionSendchat() {
    if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $msg=trim($_POST['msg']);


        $uid=Yii::app()->user->getState("user_id");
  
        $cru=new CDbCriteria();
        $cru->condition='userid=:r';
        $cru->params=array(':r'=>$uid);
        $resu=  Users::model()->find($cru);
        $ilcid=$resu->ilcid;
        
        $crt=new CDbCriteria();
        $crt->condition='qid=:r';
        $crt->params=array(':r'=>$qid);
        $rest=  Query::model()->find($crt);
        $qtopic=$rest->query_topic;
  
    $qc=new QueryChat();
    $qc->queryid=$qid;
    $qc->chatid=  uniqid();
    $qc->senderid=$uid;
    $qc->recid='-';
    $qc->message=$msg;
    $qc->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qc->isNewRecord=true;
    $qc->save(FALSE);
    
    $mst="There is a reply for the query '".substr($qtopic, 0, 100)."..'";
    $qn=new Notifications();
    $qn->queryid=$qid;
    $qn->notid=  uniqid();
    $qn->role="all";
    $qn->ilc=$ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
}
        
public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}