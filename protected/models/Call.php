<?php

/**
 * This is the model class for table "call".
 *
 * The followings are the available columns in table 'call':
 * @property string $callid
 * @property string $userid
 * @property string $topic
 * @property string $cdate
 * @property string $chour
 * @property string $cmin
 * @property string $date1
 * @property string $ilcid
 * @property string $ilcmid
 * @property string $academic
 * @property string $cfeed
 */
class Call extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Call the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'calls';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('callid, userid, topic, cdate, chour, cmin, date1, ilcid, ilcmid, academic, cstatus, cfeed, month, year, week, clodate,state,city', 'required'),
                        array('id, closed,weekmo, month, year, week, esc', 'numerical', 'integerOnly'=>true),
                        array('callid, userid, ilcid, ilcmid, academic', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, callid, userid, topic, cdate, chour, cmin, closed, date1, ilcid, ilcmid, academic, cfeed,weekmo, month, year, week, esc, clodate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'callid' => 'Callid',
			'userid' => 'Userid',
			'topic' => 'Topic',
			'cdate' => 'Cdate',
			'chour' => 'Chour',
			'cmin' => 'Cmin',
			'date1' => 'Date1',
			'ilcid' => 'Ilcid',
			'ilcmid' => 'Ilcmid',
			'academic' => 'Academic',
			'cfeed' => 'Cfeed',
                        'cstatus' => 'Cstatus',
                        'closed'=>'Closed',
                        'weekmo'=>'weekMonth',
                        'month'=>'Month',
                        'year'=>'Year',
                        'week'=>'Week',
                        'esc'=>'Esc',
                        'clodate' => 'Closed Date',
                        'state' => 'State',
                        'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('callid',$this->callid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('topic',$this->topic,true);
		$criteria->compare('cdate',$this->cdate,true);
		$criteria->compare('chour',$this->chour,true);
		$criteria->compare('cmin',$this->cmin,true);
		$criteria->compare('date1',$this->date1,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('ilcmid',$this->ilcmid,true);
		$criteria->compare('academic',$this->academic,true);
		$criteria->compare('cfeed',$this->cfeed,true);
                $criteria->compare('cstatus',$this->cstatus,true);
                $criteria->compare('closed',$this->closed,true);
                $criteria->compare('weekmo',$this->weekmo,true);
                $criteria->compare('month',$this->month,true);
                $criteria->compare('year',$this->year,true);
                $criteria->compare('week',$this->week,true);
                $criteria->compare('esc',$this->esc,true);
                $criteria->compare('clodate',$this->clodate);
                $criteria->compare('state',$this->state);
                $criteria->compare('city',$this->city);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}