<?php
/* @var $this AcademicController */
$logged=Yii::app()->user->getState('logged');
 $userid=Yii::app()->user->getState('user_id');
  $rolid=Yii::app()->user->getState("rolid");
  $ilcid=Yii::app()->user->getState("ilc_id");
          $cru=new CDbCriteria();
                $cru->condition='ilcid=:u';
                $cru->params=array(":u"=>$ilcid);
                $uinfo= ManagerIlc::model()->find($cru);
                $ilcmid= $uinfo->ilcmid;
  
  
  $ii=array();
$this->breadcrumbs=array(
	'Academic',
);

$u=new Users();

$ni=0;

        
$nu=0;
$np=0;
$nt=0;

$unit=new CDbCriteria();
$unit->condition='role=:r';
$unit->params=array(':r'=>'academic');
$ures=Users::model()->find($unit);
$aname=$ures->name;

$unit=new CDbCriteria();
$unit->condition='role=:r';
$unit->params=array(':r'=>'director');
$ures=Users::model()->find($unit);
$dname=$ures->name;



$unit=new CDbCriteria();
$unit->condition='ilcid=:i';
$unit->params=array(':i'=>$ilcid);
$ures=  ManagerIlc::model()->find($unit);
$ilcm=$ures->managername;

$unit=new CDbCriteria();
$unit->condition='role=:r and ilcid=:i';
$unit->params=array(':r'=>'unit',':i'=>$ilcid);
$ures=Users::model()->findAll($unit);
foreach($ures as $uu){
    
        $nu++;
    
}

$unit=new CDbCriteria();
$unit->condition='role=:r and ilcid=:i';
$unit->params=array(':r'=>'partner',':i'=>$ilcid);
$ures=Users::model()->findAll($unit);
foreach($ures as $uu){
    
        $np++;
    
}

$unit=new CDbCriteria();
$unit->condition='role=:r and ilcid=:i';
$unit->params=array(':r'=>'teacher',':i'=>$ilcid);
$ures=Users::model()->findAll($unit);
foreach($ures as $uu){
    
        $nt++;
    
}
        

/////////////////////////query///////////////////
$qesc=0;
$crq=new CDbCriteria();
        $crq->condition='ilc_id=:u and query_closed=0 and ilc_approved=1';
        $crq->order='query_date desc';
        $crq->params=array(':u'=>$ilcid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        foreach ($queries as $q) {
                $qesc++;
                }
            
            
///////////////////////interview///////////////////   
$iesc=0;
$crq=new CDbCriteria();
        $crq->condition='userid=:u and iclosed=0';
        $crq->params=array(':u'=>$userid);
        $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        foreach ($interviews as $i){
                 $iesc++;
                 }
                 
                
                
/////////////////////enquiry///////////////////////
//$eesc=0;
//$crq=new CDbCriteria();
//               $crq->condition='ilcid=:u and eclosed=0 and enquired_by=:us';
//               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
//               $crq->order='date1 desc';
//               $en= Enquiry::model()->findAll($crq);
//               $n=sizeof($en);
//               foreach ($en as $i){
//                
//                    $eesc++;
//                }
//                
                
/////////////////////////////admission//////////////////////////
//$adm=0;
//$crq=new CDbCriteria();
//               $crq->condition='ilcid=:u and userid=:uu and aclosed=0';
//               $crq->params=array(':u'=>$ilcid,':uu'=>$userid);
//               $crq->order='date1 desc';
//               $en= Admission::model()->findAll($crq);
//               $n=sizeof($en);
//               foreach ($en as $i){
//                   $adm++;
//               }
//               
                          ?>
<div class="bdy3"  >
    <br style="clear:both">        
    <div class="row">
        <div class="col-md-3">
        <div class="box box-red">
            <? 
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="query_date desc";
            $queries=  Query::model()->findAll($crq);
            $n=  sizeof($queries);
            ?>
            <a href="<?=Yii::app()->request->baseUrl."/index.php/teacher/querylist"?>" style="text-decoration:none;color:white">
                <h4 class="text-white">Query Module</h4>
            <p><span style="font-size:20px !important"><?=$qesc ?></span> Notifications</p>
            </a>
        </div>
        </div>
        
        
    </div> 
        
  <div class="dashbrd">
      <div class="container">
          <div class="row">
          <div class="col-md-4">
              <div  id="holcal">
                  <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Calender
                      <a href="<?=Yii::app()->request->baseUrl."/index.php/holiday/index"?>" style="float:right;color:black">
                      <i class="glyphicon glyphicon-th"></i>
                      </a>
                  </h5>
                  
           <?php 
                
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcalenderteacher'), // URL to get event
          //  'events'=>$ddays,
               /////////////////////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getcalenderteacherm/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            //////////////////////////////////////////////////////////////////////////////
	)));
           
           
           
           ///////////////////////////////////////////////////////
               ?> 
            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>   
            <ul class="hpal">
                                    <li><div class="bred"></div>&nbsp;Holidays</li>
                                    <li><div class="bora"></div>&nbsp;Enquiry</li>
                                    <li><div class="byel"></div>&nbsp;Interview</li>
                                </ul>
            </div>
          </div>
              
              <div class="col-md-5 ilc-back" style="height:450px;overflow: scroll">
                  <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Current Notifications
                      <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications" style="float:right;color:black">
                      <i class="glyphicon glyphicon-th-list"></i>
                      </a>
                  </h5> <?
                 $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'teacher',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n1=  sizeof($res);
        foreach($res as $r){
            ?>
           <span style="font-size:13px !important;margin-bottom:3%"><?=date('d-M-Y',$r->date1) ?></span>
            <p style="font-size:14px !important;">
           <?=$r->message ?></p>
                
            
            <?
        }  
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n6=  sizeof($res);
        foreach($res as $r){
            ?>
           <span style="font-size:13px !important;margin-bottom:3%"><?=date('d-M-Y',$r->date1) ?></span>
            <p style="font-size:14px !important;">
           <?=$r->message ?></p>
                
            
            <?
        }
        ?>
              </div>
              
              <!-- <div class="col-md-3 ">
                  
                  <div class="iframe-back">
                      <h5 style="padding:10px;border-bottom:1px solid white;font-size:16px!important;font-weight:600">
                      Resources
                      
                  </h5>
                      <div class="box box-white" style="margin:2%;width:90%">
                          <span style="font-size:14px !important;margin:0;color:black"><?=$dname ?></span><br>
                          <strong style="font-size:12px !important;color:black"> Director</strong>
                      </div>
                      <div class="box box-white" style="margin:2%;width:90%">
                          <span style="color:black;font-size:14px !important;margin:0;"><?=$aname ?></span><br>
                          <strong style="font-size:12px !important;color:black"> Academic Head</strong>
                      </div>
                      <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/managerprofile?q=<?=$ilcmid?>" style="text-decoration:none;color:black">
                          <div class="box box-white" style="margin:2%;width:90%">
                          <span style="color:black;font-size:14px !important;margin:0;"><?=$ilcm ?></span><br>
                          <strong style="font-size:12px !important;color:black"> ILC Manager</strong>
                          </div>
                      </a>
                       
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-danger pull-left" style="margin:0;"><?=$nu ?></span>
                          <h4> &nbsp;|&nbsp;Coordinators</h4>
                      </div>
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-primary pull-left" style="margin:0;"><?=$np ?></span>
                          <h4> &nbsp;|&nbsp;Partners</h4>
                      </div>
                       <div class="box box-white" style="margin:4%;width:90%">
                          <span class="badge badge-dark pull-left" style="margin:0;"><?=$nt ?></span>
                          <h4> &nbsp;|&nbsp;Teachers</h4>
                      </div>
                  </div>
              </div> -->
          </div>
      </div>
            
        </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
         /////////////////////////
var imgurl='<?=Yii::app()->request->baseUrl ?>/images/face.jpg';
$('img').error(function(){
        $(this).attr('src', imgurl);
});
//////////////////////
    });
</script>