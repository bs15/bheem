<?php

/**
 * This is the model class for table "communicator".
 *
 * The followings are the available columns in table 'communicator':
 * @property string $comid
 * @property string $sender
 * @property string $rec
 * @property string $topic
 * @property string $date
 * @property string $tags
 * @property string $day
 * @property string $mon
 * @property string $year
 * @property string $document
 */
class Communicator extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Communicator the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'communicator';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comid, sender, rec, topic, date, tags, day, mon, year, document', 'required'),
                        array('id', 'integerOnly'=>true),
			array('comid, sender, rec', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, comid, sender, rec, topic, date, tags, day, mon, year, document', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comid' => 'Comid',
			'sender' => 'Sender',
			'rec' => 'Rec',
			'topic' => 'Topic',
			'date' => 'Date',
			'tags' => 'Tags',
			'day' => 'Day',
			'mon' => 'Mon',
			'year' => 'Year',
			'document' => 'Document',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('comid',$this->comid,true);
		$criteria->compare('sender',$this->sender,true);
		$criteria->compare('rec',$this->rec,true);
		$criteria->compare('topic',$this->topic,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('mon',$this->mon,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('document',$this->document,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}