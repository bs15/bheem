<?php
/* @var $this IlcController */
/* @var $data Ilc */
?>
<table class="table table-striped ts">
    <tr>
        <th class="alert-success "><h2 class="text-center" style="text-transform: uppercase; text-align: center"><?php echo CHtml::encode($data->ins_name); ?></h2></th>
     
    </tr>
    <tr>
        <th><?php echo CHtml::encode($data->getAttributeLabel('ilcid')); ?></th>
        <td><?php echo CHtml::encode($data->ilcid); ?></td>
    </tr>
      <tr>
        <th><?php echo CHtml::encode($data->getAttributeLabel('ins_name')); ?></th>
        <td><?php echo CHtml::encode($data->ins_name); ?></td>
    </tr>
      <tr>
        <th><?php echo CHtml::encode($data->getAttributeLabel('address')); ?></th>
        <td><?php echo CHtml::encode($data->address); ?></td>
    </tr>
      <tr>
        <th><?php echo CHtml::encode($data->getAttributeLabel('partner_name')); ?></th>
        <td><?php echo CHtml::encode($data->partner_name); ?></td>
    </tr>
      <tr>
        <th><?php echo CHtml::encode($data->getAttributeLabel('emails')); ?></th>
        <td><?php echo CHtml::encode($data->emails); ?></td>
    </tr>
      <tr>
        <th><?php echo CHtml::encode($data->getAttributeLabel('phones')); ?></th>
        <td><?php echo CHtml::encode($data->phones); ?></td>
    </tr>
      <tr>
          <td>   <a href="<?=  Yii::app()->request->baseUrl.'/agreement/'.CHtml::encode($data->agreement) ?>" class="btn btn-small btn-primary">Download Agreement</a>
	<?php echo CHtml::link(CHtml::encode('View ILC Detail'), array('view', 'id'=>$data->id),array('class'=>'btn btn-danger')); ?>
          </td>
    </tr>
</table>
