<?php

/**
 * This is the model class for table "query_feed".
 *
 * The followings are the available columns in table 'query_feed':
 * @property string $queryid
 * @property string $feedbackid
 * @property string $ilc_managerid
 * @property string $userid
 * @property integer $rating
 * @property string $feedback
 */
class QueryFeed extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return QueryFeed the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'query_feed';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('queryid, feedbackid, ilc_managerid, userid, rating, feedback', 'required'),
			array('id, rating', 'numerical', 'integerOnly'=>true),
			array('queryid, feedbackid, ilc_managerid, userid', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, queryid, feedbackid, ilc_managerid, userid, rating, feedback', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'queryid' => 'Queryid',
			'feedbackid' => 'Feedbackid',
			'ilc_managerid' => 'Ilc Managerid',
			'userid' => 'Userid',
			'rating' => 'Rating',
			'feedback' => 'Feedback',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('queryid',$this->queryid,true);
		$criteria->compare('feedbackid',$this->feedbackid,true);
		$criteria->compare('ilc_managerid',$this->ilc_managerid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('feedback',$this->feedback,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}