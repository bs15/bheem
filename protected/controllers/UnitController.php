<?php

class UnitController extends Controller
{
	
      public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        
      public function accessRules()
      {

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid';
    	$criteria1->params = array(':rolid'=>'unit');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','ilcmanager','createilcm','getilcm','delilcm'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('unit','teacher','createilcm2','getilcm2','closeint'),
    		'users'=>$modad,
    	),
             array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('query','querylist','queryhistory','createquery','getquery','closequery','getchat','sendchat','getqmy'),
    		'users'=>$modad,
    	),
         array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('getquery3','editfeed','updatefeed','setnoty','checknum','nlist','uploadpost'),
    		'users'=>$modad,
    	),
            
         array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('interview','interview2','closeint','schedulei','intlist1','intlist2','getimy'),
    		'users'=>$modad,
    	),
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
    public function actionIndex()
	{
        $uid = Yii::app()->user->getState("user_id");
        $cr = new CDbCriteria();
        $cr->condition='userid=:u';
        $cr->params=array(':u'=>$uid);
        $user=Users::model()->find($cr);
        $userp=Userphoto::model()->find($cr);
	$this->render('index',array('user'=>$user,'userp'=>$userp));
	
	}
    //////////////////////upload post ///////////////////
    public function actionUploadpost() {
        $model = new QueryChat();
        $userid=Yii::app()->user->getState('user_id');
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
     //   $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
                $model->queryid=trim($_POST['qid']);
                $model -> document = $fileName;
	
                
                $model->chatid=  uniqid();
         $model->senderid=$userid;
         $model->recid='-';
         $model->message='-';
         $model->date1=  strtotime(date('Y-m-d H:i:s'));
         
	if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../chatdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
            }
            
    ///////////////////////////////////////////
    public function actionSchedulei() {
    
        $model = new Interview();
        $userid=Yii::app()->user->getState('user_id');
        $ilcid=Yii::app()->user->getState('ilc_id');
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilcid);
                $ilcinfo= Ilc::model()->find($cri);
        
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
        $fname=$_POST['fname'];
        $datep=$_POST['datep'];
        $h=$_POST['hour'];
        $m=$_POST['min'];
        $da=  explode("-", $datep);
        
      //  $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
	$fileName = "{$timeStamp}-{$uploadedFile}";// random number + Timestamp + file name
        
        $rw=new Report();
        $week=$rw->getweekno($datep);
        
         $model->intid=  uniqid();
         $model->candidateid=uniqid();
         $model->candidatecv=$fileName;
         $model->candidatename=$fname;
         $model->userid=$userid;
         $model->idate= $datep;
         $model->ihour=$h;
         $model->imin=$m;
         $model->week=$week;
         $model->month=  intval($da[1]);
         $model->year=  intval($da[0]);
         $model->istatus='0';
         $model->ilcid=$ilcid;
         $model->state=$ilcinfo->state;
         $model->city=$ilcinfo->city;
         $model->ilcmid='-';
         $model->idate1=strtotime(date('Y-m-d H:i:s'));
         
         if ($model -> save(FALSE)) {
	 move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../cv/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
        $intid=$model->intid;
         $cru=new CDbCriteria(); 
        $cru->condition='intid=:u';
        $cru->params=array(":u"=>$intid);
        $inter= Interview::model()->find($cru);
        
        $mst="An interview for".$inter->candidatename." has been scheduled on ".$inter->idate." at ".$inter->ihour.":".$inter->imin;
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="ilcmanager";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $im=new Nlist();
    $us=$im->getilcmid($ilcid);
    $ilcmid=$us->ilcmid;
    
    $usr=new Nlist();
    $uu=$usr->getuser($ilcmid);
    $uname=$uu->username;
    $name=$uu->name;
    
    
////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk New Interview Scheduled";
            $msg="Dear $name,<br>
                An interview for $inter->candidatename has been scheduled on $inter->idate at $inter->ihour:$inter->imin<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
      
}
            }

   
public function actionQuery() {
    $this->render('query');
}
public function actionQuerylist() {
    $this->render('querylist');
}
public function actionQueryhistory() {
    $this->render('queryhistory');
}
public function actionGetqmy() {
    if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);
    
    $y=intval($yea);
        $m=intval($mon);
        $userid=Yii::app()->user->getState('user_id');
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        if($m!=0 && $y!=0)
        {
        $crq->condition="month=:m and year=:y and userid=:i and query_closed=1";
        $crq->params=array(":m"=>$m,":y"=>$y,":i"=>$userid);
        }
        else if($m==0 && $y!=0)
           {
        $crq->condition="year=:y and userid=:i and query_closed=1";
        $crq->params=array(":y"=>$y,":i"=>$userid);
        } 
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?> <p style="margin-top:8%">No Queries were raised in this month !</p> <?
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <? foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ?>
                <tr>
                <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
                <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
                <td><?=$q->query_topic ?></td>
                <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                <td>
                    <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                </td>
                </tr>
            <?
                
            }
            ?>
                </table>
           </div>
            <?
          }
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
public function actionCreatequery() {
    if(Yii::app()->request->isPostRequest) {
           $topic=trim($_POST['topic']);
           $taggs=trim($_POST['taggs']);

    $uid=Yii::app()->user->getState('user_id');
    $ilcid=Yii::app()->user->getState('ilc_id');
    
    $cri=new CDbCriteria();
    $cri->condition='ilcid=:u';
    $cri->params=array(":u"=>$ilcid);
    $ilcinfo= Ilc::model()->find($cri);
    
    //$t=$h.":".$m;
    $qe=new Query();
    //$n=intval($qe->count('schedule_call_date=:d and schedule_call_time=:t', array(':d'=>$datep,':t'=>$t)));
//    if($n>0) {
//        echo "Sorry this slot is booked! Try again";
//    }
    $rw=new Report();
    $week=$rw->getweekno(date('Y-m-d'));
    $qid=  uniqid();
    $qe->qid= $qid;
    $qe->userid=$uid;
    $qe->tags=$taggs;
    $qe->week=$week;
    $qe->month=intval(date('m'));
    $qe->year=intval(date('Y'));
    $qe->query_topic=$topic;
    $qe->schedule_call_date="";
    $qe->schedule_call_time="";
    $qe->query_date=  intval(strtotime(date('Y-m-d H:i:s')));
    $qe->ilc_id=$ilcid;
    $qe->ilcid=$ilcid;
    $qe->ilc_approved=1;
    $qe->query_closed=0;
    $qe->state=$ilcinfo->state;
    $qe->city=$ilcinfo->city;
    $qe->isNewRecord=TRUE;
    $qe->save(FALSE);
    echo "Your query has been submitted";
   /////////////////////////////tags//////////////////
   if (strpos($taggs, ',') !== false) {
  
        }
        else {
            $taggs.=',';
        }
        $tag=  explode(",", $taggs);
       
        foreach ($tag as $t) {
            if(empty($t) || $t=="")
            {}
            else 
            {
                 $qt=new QueryTags();
                 $qt->queryid=$qid;
                 $qt->tag=trim(strtolower($t));
                 $qt->isNewRecord=TRUE;
                 $qt->save(FALSE);
            }
        }
   //////////////////////////////////////////////
    ////////////////////////notifications//////////////
      $c=new Notifications();
   
     $c->notid=  uniqid();
     $c->role = 'ilcmanager';
     $c->queryid = $qe->qid;
     $c->ilc = $ilcid;
     $c->message="The query with topic ".$qe->query_topic." has been created";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     $im=new Nlist();
    $us=$im->getilcmid($ilcid);
    $ilcmid=$us->ilcmid;
    
    $usr=new Nlist();
    $uu=$usr->getuser($ilcmid);
    $uname=$uu->username;
    $name=$uu->name;
    
    
////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk New Query Raised";
            $msg="Dear $name,<br>
                The query with topic $qe->query_topic has been created<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            
            //////////////////////////////////////////////
}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}

 }
 
 public function actionGetquery() {
     if(Yii::app()->request->isPostRequest) {
            
     $uid=Yii::app()->user->getState('user_id');
        
        $crq=new CDbCriteria();
        $crq->condition='userid=:u and query_closed=0 ';
        $crq->order='query_date desc';
        $crq->params=array(':u'=>$uid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
        ?>
        <p>Raise a query by clicking on the button above</p>
        <?
        }
        else 
        {
        ?>
        <div class="table table-striped table-font">  
                <table>
                    <tr >
                        <td><b>Topic</b></td>
                        <td><b>Created on</b></td>
                        <td><b>Status</b></td>
                        <td><b>Chat</b></td>
                        <td><b>Action</b></td>
                    </tr>
        <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                ?>
                    <tr>
                        <td><?=$q->query_topic ?></td>
                        <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                        <td>
                            <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active":"Closed" ?></span>
                        </td>
                        <td>
                            <input type="button" style="font-size:14px !important; padding:3px;border-radius:5px;" class="btn-dark" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                        </td>
                        <td>
                          
          <? if($q->query_closed==0) { ?>
            <input type="button" class="btn-inverse" style="font-size:14px !important; padding:3px;border-radius:5px;border:1px solid grey" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Close" onclick="closequery(this.id)" />
        <? } ?>
        <br>
        <?
        $qid1=$q->qid;
        $qf= new QueryFeed();
         $qcount=intval($qf->count('queryid=:u',array(":u"=>$qid1)));
         if($q->query_closed==1 && $qcount==0) {
            ?>
        <input type="button" class="btn btn-primary" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Feedback" onclick="editfeed(this.id)" />
         <? } ?>
                        </td>
                    </tr>

                
                    <?
            } ?>
                </table>
        </div>
                <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
 
 public function actionIntlist1() {
        if(Yii::app()->request->isPostRequest) {
         
        $ilcid=Yii::app()->user->getState('ilc_id');
        $userid=Yii::app()->user->getState('user_id');
        $crq=new CDbCriteria();
        $crq->condition='userid=:us and ilcid=:u and iclosed=0';
        $crq->params=array(':u'=>$ilcid,':us'=>$userid);
        $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p>Schedule an interview by clicking on the above button</p> 
            <?
        }
        else 
        {
            ?>
            <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>
            <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
//                $user=$i->ilcid;
//                $cru=new CDbCriteria();
//                $cru->condition='ilc=:u';
//                $cru->params=array(":u"=>$user);
//                $uinfo=Users::model()->find($cru);
                ?>
               <tr>
                    <td><?=$i->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                       
                        
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            
                            if($istatus=='1' && $d<=48){
                              ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" value="Active"  />

                            <?   
                             }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                           ?>
                        </td>
                        <td>
                           <? if($i->iclosed==0) { ?>
                    <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->intid ?>" name="<?=$i->intid ?>" value="Close" onclick="closeint(this.id)" />
                        <? } ?> </td>
                    </tr>     
            
<?
            }
            ?>
                </table>
            </div>
            <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
 
 public function actionIntlist2() {
     if(Yii::app()->request->isPostRequest) {
            
     $ilcid=Yii::app()->user->getState('ilc_id');
        $userid=Yii::app()->user->getState('user_id');

        $crq=new CDbCriteria();
        $crq->condition='ilcid=:u and userid=:us and iclosed=1';
        $crq->params=array(':u'=>$ilcid,':us'=>$userid);
        $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p> No Interviews have been conducted yet</p> <?
        }
        else 
        {
            ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Resume</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
//                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                ?>
            <tr>
                 <td><?=$i->candidatename ?>
                            
                        </td>
                        <td>
                            <a style="font-size:14px !important; padding:4px;border-radius:2px;border:1px solid black" class="btn-light" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a></td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         
                    </tr>

<?
            } ?>
                </table>
                    </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
  public function actionGetimy() {
      if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);

      $y=  intval($yea);
      $m= intval($mon);
      $userid=Yii::app()->user->getState('user_id');
      $ilcid=Yii::app()->user->getState('ilc_id');
        $crq=new CDbCriteria();
        if($y!=0 && $m==0)
        {
           $crq->condition='userid=:u and year=:y and iclosed=1';
        $crq->params=array(':u'=>$userid,':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
         $crq->condition='userid=:u and year=:y and month=:m and iclosed=1';
        $crq->params=array(':u'=>$userid,':m'=>$m,':y'=>$y);
        }
      
        
        $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p> No Interviews have been conducted yet</p> <?
        }
        else 
        {
            ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Resume</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
//                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                ?>
            <tr>
                 <td><?=$i->candidatename ?>
                            
                        </td>
                        <td>
                            <a style="font-size:14px !important; padding:4px;border-radius:2px;border:1px solid black" class="btn-light" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a></td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         
                    </tr>

<?
            } ?>
                </table>
                    </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
 
 public function actionCloseint() {
        if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);


        $ilcid=Yii::app()->user->getState('ilc_id');
        $cru=new CDbCriteria(); 
        $cru->condition='intid=:u';
        $cru->params=array(":u"=>$intid);
        $inter= Interview::model()->find($cru);
        
    $qq=new Interview();
    $qq->updateAll(array("iclosed"=>1,"clodate"=>date('Y-m-d')), 'intid=:q', array(":q"=>$intid));
    
    $qq=new Assignesc();
    $qq->updateAll(array("close"=>1), 'intid=:q', array(":q"=>$intid));
    
    $qq=new Notifications();
    $qq->deleteAll("queryid=:q ", array(":q"=>$intid));
    
    $mst="The interview of ".$inter->candidatename." has been closed";
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="ilcmanager";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $im=new Nlist();
    $us=$im->getilcmid($ilcid);
    $ilcmid=$us->ilcmid;
    
    $usr=new Nlist();
    $uu=$usr->getuser($ilcmid);
    $uname=$uu->username;
    $name=$uu->name;
    
    
////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Interview Closed";
            $msg="Dear $name,<br>
                The interview of $inter->candidatename has been closed<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
  }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
  
    
}
 
 
 
 public function actionEditfeed(){
    if(Yii::app()->request->isPostRequest) {
           $id=trim($_POST['id']);

     $ilcid=Yii::app()->user->getState('ilc_id');
    $cru=new CDbCriteria();
    $cru->condition='ilcid=:u';
    $cru->params=array(":u"=>$ilcid);
    $manager= ManagerIlc::model()->find($cru);
    $ilcmid=$manager->ilcmid;
    
     ?>
         <div class="row">            
            <div class="col-md-12">      
                <div class="input-group mb-3">
                    <input type="hidden" value="<?=$id ?>" id="qqid">
                    <input type="hidden" value="<?=$ilcmid ?>" id="ilcmid">
                    <p>How much satisfied are you with the reply?</p>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rating</span>
                    </div>
                    <select type="number" class="form-control"  id="rating" aria-describedby="basic-addon1" onchange="revshowf()">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                    </select>
                </div>
            </div>
             <div class="col-md-12" id="revshow" >  
                 <br style="clear:both">
               <textarea  style="width:100% !important"  id="review" placeholder="What could have been better?"   aria-describedby="basic-addon1"></textarea>
                <br style="clear:both">
            </div>
            
        </div>
        <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatefeed()"/>
        </div>    
         <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
 
 public function actionUpdatefeed(){
     
     if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $ilcmid=trim($_POST['ilcmid']);
           $rating=trim($_POST['rating']);
           $review=trim($_POST['review']);


     $c=new QueryFeed();
     $nc=intval($c->count('queryid=:q and userid=:u', array(':q'=>$qid, ':u'=>Yii::app()->user->getState('user_id'))));
     if($nc==0){
     $c->feedbackid=uniqid();
     $c->queryid=$qid;
     $c->ilc_managerid=$ilcmid;
     $c->userid = Yii::app()->user->getState('user_id');
     $c->rating =intval($rating);
     $c->feedback=$review;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     $c=new Query();
     $c->updateAll(array("ilcmid"=>$ilcmid,"rating"=>$rating), 'qid=:q', array(":q"=>$qid));
     }
     
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		} 
 }
 
 public function actionGetquery3() {
     if(Yii::app()->request->isPostRequest) {
            
     $uid=Yii::app()->user->getState('user_id');
        $crq=new CDbCriteria();
        $crq->condition='userid=:u and ilc_approved=1 and query_closed=1';
        $crq->order='query_date desc';
        $crq->params=array(':u'=>$uid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        { ?>
        <div class="table table-striped table-font">  
                <table>
                    <tr >
                        <td><b>Topic</b></td>
                        <td><b>Created on</b></td>
                        <td><b>Feedback(out of 5)</b></td>
                        <td><b>Chat</b></td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                ?>
               <tr>
                        <td><?=$q->query_topic ?></td>
                        <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                       
                        <td>
                        <?
                        $qid1=$q->qid;
                        $crq=new CDbCriteria();
                $crq->condition='queryid=:u';
                $crq->params=array(":u"=>$qid1);
                $qinfo= QueryFeed::model()->find($crq);
                         ?>
                            <p><?=$qinfo->rating ?></p>   
                        </td>
                        <td>
                          <input type="button" style="font-size:14px !important; padding:3px;border-radius:5px;" class="btn-dark" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                        </td>
                    </tr> 
                     
    <?
            } ?>
               </table>
        </div>
            <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
 
 public function actionClosequery() {
     if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
   
     $ilcid=Yii::app()->user->getState('ilc_id');
     
        $cru=new CDbCriteria(); 
        $cru->condition='qid=:u';
        $cru->params=array(":u"=>$qid);
        $inter= Query::model()->find($cru);
     
        $qq=new Query();
        $qq->updateAll(array("query_closed"=>1,"clodate"=>date('Y-m-d')), 'qid=:q', array(":q"=>$qid));
    
        $qq=new Notifications();
        $qq->deleteAll("queryid=:q ", array(":q"=>$qid));

        $mst="The query regarding ".$inter->query_topic ." has been closed";
        $qn=new Notifications();
        $qn->queryid=$qid;
        $qn->notid=  uniqid();
        $qn->role="all";
        $qn->ilc=$inter->ilc_id;
        $qn->message=$mst;
        $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
        $qn->count1=3;
        $qn->isNewRecord=true;
        $qn->save(FALSE);
        
    $im=new Nlist();
    $us=$im->getilcmid($ilcid);
    $ilcmid=$us->ilcmid;
    
    $usr=new Nlist();
    $uu=$usr->getuser($ilcmid);
    $uname=$uu->username;
    $name=$uu->name;
    
    
////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Query Closed";
            $msg="Dear $name,<br>
                The query regarding $inter->query_topic has been closed<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////  
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
}

 public function actionGetchat() {
    if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $uid=trim($_POST['uid']);

     $cr=new CDbCriteria();
    $cr->condition='queryid=:q and priv=0';
    $cr->params=array(":q"=>$qid);
    $cr->order='date1 asc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       echo "no chats";
   }
 else {
      $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
    ?>
<br>
<p style="font-size:16px !important" ><b>Topic: </b><?=$topic ?>  </p>
<div id="chatui">
     <?
     foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
         if($uinf->role=='unit'){
           ?>
                <div class="alert alert-success talk" style="float:right">
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='partner'){ ?>
             <div class="alert alert-warning talk" style="float:left">
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Partner ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>     <? 
         }
         if($uinf->role=='ilcmanager'){ ?>
             <div class="alert alert-danger talk" style="float:left">
            <b style="font-size:14px !important"><?=$uinf->name ?>[ ILC Manager ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>  <?    
         }
          if($uinf->role=='academic'){ ?>
             <div class="alert alert-primary talk" style="float:left">
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Academic Head ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>  <?    
         }
          if($uinf->role=='director'){ ?>
             <div class="alert alert-dark talk" style="float:left">
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Director ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>  <?    
         }
         
       }?>
</div>
                <?
   }
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
    
}

 public function actionSendchat() {
  if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $msg=trim($_POST['msg']);

     $uid=Yii::app()->user->getState("user_id");
  
        $cru=new CDbCriteria();
        $cru->condition='userid=:r';
        $cru->params=array(':r'=>$uid);
        $resu=  Users::model()->find($cru);
        $ilcid=$resu->ilcid;
        
        $crt=new CDbCriteria();
        $crt->condition='qid=:r';
        $crt->params=array(':r'=>$qid);
        $rest=  Query::model()->find($crt);
        $qtopic=$rest->query_topic;
        
    $qc=new QueryChat();
    $qc->queryid=$qid;
    $qc->chatid=  uniqid();
    $qc->senderid=$uid;
    $qc->recid='-';
    $qc->message=$msg;
    $qc->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qc->isNewRecord=true;
    $qc->save(FALSE);
    
    $mst="There is a reply for the query '".substr($qtopic, 0, 100)."..'";
    $qn=new Notifications();
    $qn->queryid=$qid;
    $qn->notid=  uniqid();
    $qn->role="all";
    $qn->ilc=$ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
   
} 

  
       
public function actionInterview() {
    $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
     $crt=new CDbCriteria();
        $crt->condition='ilcid=:r';
        $crt->params=array(':r'=>$ilcid);
        $rest= ManagerIlc::model()->find($crt);
        $ilcmid=$rest->ilcmid;       
        Yii::app()->user->setState('man',$ilcmid);   
    $this->render('interview',array('man'=>$ilcmid));
}

public function actionInterview2() {
    $this->render('interview2');
}
 public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }       
     
// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}