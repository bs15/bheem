<?php

/**
 * This is the model class for table "report".
 *
 * The followings are the available columns in table 'report':
 * @property integer $tabby
 */
class Report extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Report the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'report';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tabby', 'required'),
			array('tabby', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tabby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tabby' => 'Tabby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tabby',$this->tabby);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        function getweekno( $date = 'today' ) 
        { 
            return ceil( date( 'j', strtotime( $date ) ) / 7 ); 

        } 

        
        public function ilcreport($il,$yr,$mon,$wee,$ci,$st){
            
           if($il!='0'){
                ////////////////////////set of ifs////////////////
                $rr=new Report();
                $mc=$rr->setofif($yr,$mon,$wee,$st,$ci,$il);
                ///////////////////////////////////////////////////
                
                $criteria1 = new CDbCriteria($mc);
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$il);
                $ilcs = Ilc::model()->find($criteria1);
                $st=$ilcs->state;
                $ci=$ilcs->city;
                
                $ilcname=$ilcs->ins_name;//ilcname
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$il);
                $milc = ManagerIlc::model()->find($criteria1);
                $ilcmid=$milc->ilcmid;
                
                $mname=$milc->managername;//manager name
                
                
                ////////////////////Enquiry//////////////////////
                $re=new Report();
                if($re->checkilc('Enquiry',$il)){
                $ec2=new CDbCriteria($mc);
                $ec2->addCondition('eclosed=0');
                $enqrs2 = Enquiry::model()->findAll($ec2);
                $eopen=sizeof($enqrs2);
                
                $ec=new CDbCriteria($mc);
                $ec->addCondition('eclosed=1');
                $enqrs = Enquiry::model()->findAll($ec);
                $eclosed=  sizeof($enqrs);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Query//////////////////////
                
                $re=new Report();
                if($re->checkilc('Query',$il)){
                    
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('query_closed=0');
                $qry2 = Query::model()->findAll($qc2);
                $qopen=sizeof($qry2);
                
                $qc=new CDbCriteria($mc);
                $qc->addCondition('query_closed=1 ');
                $qry = Query::model()->findAll($qc);
                $qclosed=  sizeof($qry);
                
                $qf=0;
                $qn=0;
                $crqf=new CDbCriteria($mc);
                $crqf->addCondition('query_closed=1');
                $feedq= Query::model()->findAll($crqf);
                $fq=sizeof($feedq);
                if($fq>0){
                foreach($feedq as $f){
                    $qf+=$f->rating;
                    $qn++;
                }
                }
                $qavg=$qf/$qn;
                }
                ///////////////////////////////////////////////
                
                ////////////////////Admission//////////////////////
                $re=new Report();
                if($re->checkilc('Admission',$il)){
                $ac2=new CDbCriteria($mc);
                $ac2->addCondition('aclosed=0');
                $adm2 = Admission::model()->findAll($ac2);
                $aopen=sizeof($adm2);
                
                $ac=new CDbCriteria($mc);
                $ac->addCondition('aclosed=1');
                $adm = Admission::model()->findAll($ac);
                $aclosed=  sizeof($adm);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Interview//////////////////////
                $re=new Report();
                if($re->checkilc('Interview',$il)){
                $ic2=new CDbCriteria($mc);
                $ic2->addCondition('iclosed=0');
                $int2 = Interview::model()->findAll($ic2);
                $iopen=sizeof($int2);
                
                $ic=new CDbCriteria($mc);
                $ic->addCondition('iclosed=1');
                $int = Interview::model()->findAll($ic);
                $iclosed=  sizeof($int);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $re=new Report();
                if($re->checkilc('Call',$il)){
                $cc2=new CDbCriteria($mc);
                $cc2->addCondition('closed=0');
                $cal2 = Call::model()->findAll($cc2);
                $copen=sizeof($cal2);
                
                $cc=new CDbCriteria($mc);
                $cc->addCondition('closed=1');
                $cal = Call::model()->findAll($cc);
                $cclosed=  sizeof($cal);
                
                $cf=0;
                $cn=0;
                $crcf=new CDbCriteria($mc);
                $crcf->addCondition('closed=1');
                $feedc= Call::model()->findAll($crcf);
                $sf=sizeof($feedc);
                if($sf>0){
                foreach($feedc as $c){
                    $cf+=$c->cfeed;
                    $cn++;
                    $callid=$c->callid;
                    $crcf=new CDbCriteria();
                    $crcf->condition='callid=:q and close=1';
                    $crcf->params=array(":q"=>$callid);
                    $fe= Assignesc::model()->find($crcf);
                    $sc=sizeof($fe);
                    if($sc!=0){
                        $cf+=$fe->rating;
                    }
                    }
                }
                $cavg=$cf/$cn;
                }
                $avgg=($cavg+$qavg)/2;
                $avg1=round($avgg,2);
                ///////////////////////////////////////////////
                ?>

<table class="table-striped table-font" id="ilctab" style="margin-left:1%;">
  <? $rolid=Yii::app()->user->getState("rolid"); ?>          
    <tr>
        <td>Ilc Report</td>
        
        <td colspan="12">    
            <span style="float:right;font-size:14px !important;">
                <strong>Generated by</strong>&nbsp;
                <?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                &nbsp;|&nbsp;
                <?=date('Y-m-d H:i:s') ?>
            </span>
        </td>
    </tr>
    <tr style="font-weight: 500">
        
            <th>State</th> 
            <th>City</th>
            <th>ILC</th>
            <th>Manager</th>
            <th>Enquiries</th>
            <th>Admissions</th>
            <th>Queries</th> 
            <th>Interviews</th>
            <th>Calls</th>
            <th>AVG Rating</th> 
       
        </tr>
        <tr>
            <td><?=$st ?></td>
            <td><?=$ci ?></td>
            <td><?=$ilcname ?></td>
            <td><?=$mname ?></td>
            <td>Active:<?=$eopen ?>
                <br>
                Closed:<?=$eclosed ?>
            </td>
            <td>
                Active:<?=$aopen ?>
                <br>
                Closed:<?=$aclosed ?>
            </td>
            <td>
                Active:<?=$qopen ?>
                <br>
                Closed:<?=$qclosed ?>
            </td>
            <td>
                Active:<?=$iopen ?>
                <br>
                Closed:<?=$iclosed ?>
            </td>
            <td>
                Active:<?=$copen ?>
                <br>
                Closed:<?=$cclosed ?>
            </td>
            <td><?=$avg1 ?></td>
        </tr>
    </table> 
         <? 
         $qopen=$qclosed=$aopen=$aclosed=$iopen=$iclosed=$eopen=$eclosed=$copen=$cclosed=0;
         $cn=$cf=$cavg=$qf=$qn=$qavg=$avgg=$avg1=0;
            }
            
           else if($il=='0'){
               //echo("if ilc ==0");
               //die();
               /////////////////////////main//////////////////
                
                
                
               $cr=new CDbCriteria();
               $cr->select="*";
               if($st!='0' && $ci!='0'){
                 $cr->condition='state=:s and city=:c';
                 $cr->params=array(':s'=>$st,':c'=>$ci);  
               }
               else if($st!='0' && $ci=='0'){
                   
                   $cr->condition='state=:s';
                   $cr->params=array(':s'=>$st);
               }
               
               $resilc=Ilc::model()->findAll($cr);
               
               
               ?>
<table class="table-striped table-font" id="ilctab" style="margin-left:1%;">
             <? $rolid=Yii::app()->user->getState("rolid"); ?>          
    <tr>
        <td>Ilc Report</td>
        <td colspan="12">   
            <span style="float:right;font-size:14px !important;">
                <strong>Generated by</strong>&nbsp;<?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                &nbsp;|&nbsp;
                <?=date('Y-m-d H:i:s') ?>
            </span>
        </td>
    </tr>
                   <tr style="font-weight: 500">
        
            <th>State</th> 
            <th>City</th>
            <th>ILC</th>
            <th>Manager</th>
            <th>Enquiries</th>
            <th>Admissions</th>
            <th>Queries</th> 
            <th>Interviews</th>
            <th>Calls</th>
            <th>AVG Rating</th> 
       
        </tr>
               <?
               foreach($resilc as $il2)
               ///////////////////ilc==0//////////////
               {
                   
                $city=$il2->city;
                $state=$il2->state;
                $il=$il2->ilcid;
                $ilcname=$il2->ins_name;//ilcname
                //
                //
                ////////////////////////set of ifs////////////////
                $rr=new Report();
                $mc=$rr->setofif($yr,$mon,$wee,$st,$ci,$il);
                ///////////////////////////////////////////////////
                
                
               
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$il2->ilcid);
                $milc = ManagerIlc::model()->find($criteria1);
                
                $mname=$milc->managername;//manager name
                $ilcmid=$milc->ilcmid;
                
                
               
                
                ////////////////////Enquiry//////////////////////
                $re=new Report();
                if($re->checkilc('Enquiry',$il)){
                $ec2=new CDbCriteria($mc);
                $ec2->addCondition('eclosed=0');
                $enqrs2 = Enquiry::model()->findAll($ec2);
                $eopen=sizeof($enqrs2);
                
                $ec=new CDbCriteria($mc);
                $ec->addCondition('eclosed=1');
                $enqrs = Enquiry::model()->findAll($ec);
                $eclosed=  sizeof($enqrs);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Query//////////////////////
                $re=new Report();
                if($re->checkilc('Query',$il)){
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('query_closed=0');
                $qry2 = Query::model()->findAll($qc2);
                $qopen=sizeof($qry2);
                
                $qc=new CDbCriteria($mc);
                $qc->addCondition('query_closed=1');
                $qry = Query::model()->findAll($qc);
                $qclosed=  sizeof($qry);
                
                $qf=0;
                $qn=0;
                $crqf=new CDbCriteria();
                $crqf->condition='ilcid=:q';
                $crqf->params=array(":q"=>$il);
                $feedq= Query::model()->findAll($crqf);
                $sf=sizeof($feedq);
                if($sf>0){
                foreach($feedq as $f){
                    $qf+=$f->rating;
                    $qn++;
                }
                }
                $qavg=$qf/$qn;
                }
                ///////////////////////////////////////////////
                
                ////////////////////Admission//////////////////////
                $re=new Report();
                if($re->checkilc('Admission',$il)){
                $ac2=new CDbCriteria($mc);
                $ac2->addCondition('aclosed=0');
                $adm2 = Admission::model()->findAll($ac2);
                $aopen=sizeof($adm2);
                
                $ac=new CDbCriteria($mc);
                $ac->addCondition('aclosed=1');
                $adm = Admission::model()->findAll($ac);
                $aclosed=  sizeof($adm);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Interview//////////////////////
                $re=new Report();
                if($re->checkilc('Interview',$il)){
                $ic2=new CDbCriteria($mc);
                $ic2->addCondition('iclosed=0');
                $int2 = Interview::model()->findAll($ic2);
                $iopen=sizeof($int2);
                
                $ic=new CDbCriteria($mc);
                $ic->addCondition('iclosed=1');
                $int = Interview::model()->findAll($ic);
                $iclosed=  sizeof($int);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $re=new Report();
                if($re->checkilc('Call',$il)){
                   
                $cc2=new CDbCriteria($mc);
                $cc2->addCondition('closed=0');
                $cal2 = Call::model()->findAll($cc2);
                $copen=sizeof($cal2);
                
                $cc=new CDbCriteria($mc);
                $cc->addCondition('closed=1');
                $cal = Call::model()->findAll($cc);
                $cclosed=  sizeof($cal);
                
                $cf=0;
                $cn=0;
                $crcf=new CDbCriteria($mc);
                $crcf->addCondition('closed=1');
                $feedc= Call::model()->findAll($crcf);
                foreach($feedc as $c){
                    $cf+=$c->cfeed;
                    $cn++;
                    
                    $callid=$c->callid;
                    $crcf=new CDbCriteria();
                    $crcf->condition='callid=:q and close=1';
                    $crcf->params=array(":q"=>$callid);
                    $fe= Assignesc::model()->find($crcf);
                    $sc=sizeof($fe);
                    if($sc!=0){
                        $cf+=$fe->rating;
                        
                    }
                    
                }
                $cavg=$cf/$cn;
                }
                $avgg=($cavg+$qavg)/2;
                $avg1=round($avgg,2);
                ///////////////////////////////////////////////
                ?>

               
        <tr>
            <td><?=$state ?></td>
            <td><?=$city ?></td>
            <td><?=$ilcname ?></td>
            <td><?=$mname ?></td>
            <td>Active:<?=$eopen ?>
                <br>
                Closed:<?=$eclosed ?>
            </td>
            <td>
                Active:<?=$aopen ?>
                <br>
                Closed:<?=$aclosed ?>
            </td>
            <td>
                Active:<?=$qopen ?>
                <br>
                Closed:<?=$qclosed ?>
            </td>
            <td>
                Active:<?=$iopen ?>
                <br>
                Closed:<?=$iclosed ?>
            </td>
            <td>
                Active:<?=$copen ?>
                <br>
                Closed:<?=$cclosed ?>
            </td>
            <td><?=$avg1 ?></td>
            
        </tr>
    
         <?
         $qopen=$qclosed=$aopen=$aclosed=$iopen=$iclosed=$eopen=$eclosed=$copen=$cclosed=0;
         $cn=$cf=$cavg=$qf=$qn=$qavg=$avgg=$avg1=0;
         $avg1=0;
            }
            ?>
        </table> 
        <?
               ///////////////////////////////////////    
           } 
            
          
        }
        
        public function weeklyreport($il,$yr,$mon,$wee,$ci,$st,$ilcm){
            
           if($ilcm!='0'){
               ?>
<table class="table-striped table-font" id="weektab" style="margin-left:1%;">
             <? $rolid=Yii::app()->user->getState("rolid"); ?>          
    <tr>
        <td>Weekly Report</td>
        <td colspan="12">    
            <span style="float:right;font-size:14px !important;">
                <strong>Generated by</strong>&nbsp;<?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                &nbsp;|&nbsp;
                <?=date('Y-m-d H:i:s') ?>
            </span>
        </td>
    </tr>
    <tr style="font-weight: 500">
        
                    <th>ILC Manager</th>
                    <th>ILC</th>
                    <th>No. of Ratings</th>
                    <th>AVG Rating</th>
                    <th>Query Esc</th>
                    <th>Interview Esc</th>
                    <th>Enquiry Esc</th>
                    <th>Admission Esc</th> 
                    <th>Call Esc</th>
                    <th>Total Escs</th>
                 </tr>
                <?
                $c1 = new CDbCriteria();
                $c1->condition='ilcmid=:s';
                $c1->params=array(':s'=>$ilcm);
                $resilcm = ManagerIlc::model()->findAll($c1);
                
                foreach($resilcm as $ri){
                 $il=$ri->ilcid;
                 $ilcname=$ri->ilcname;
                 
                 ////////////////////////set of ifs////////////////
                $rr=new Report();
                $mc=$rr->setofif($yr,$mon,$wee,$st,$ci,$il);
                ///////////////////////////////////////////////////
                
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s and state=:st and city=:c';
                $criteria1->params=array(':s'=>$il,':st'=>$st,':c'=>$ci);
                $ilcs = Ilc::model()->find($criteria1);
                
                
                //$ilcname=$ilcs->ins_name;//ilcname
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$ri->ilcid);
                $milc = ManagerIlc::model()->find($criteria1);
                
                $ilcmid=$milc->ilcmid;
                $mname=$milc->managername;//manager name
                
               

                ////////////////////Enquiry//////////////////////
                $re=new Report();
                if($re->checkilc('Enquiry',$il)){
                    $ec2=new CDbCriteria($mc);
                    $ec2->addCondition('esc=1');
                    $enqrs2 = Enquiry::model()->findAll($ec2);
                    $eesc=sizeof($enqrs2);
                    
                }
                ///////////////////////////////////////////////
                
                ////////////////////Query//////////////////////
                $re=new Report();
                if($re->checkilc('Query',$il)){
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('esc=1');
                    $qry2 = Query::model()->findAll($qc2);
                    $qesc=sizeof($qry2);
                
                $qf=0;
                $qn=0;
                $crqf=new CDbCriteria($mc);
                $crqf->addCondition('query_closed=1');
                $feedq= Query::model()->findAll($crqf);
                $fq=sizeof($feedq);
                if($fq>0){
                foreach($feedq as $f){
                    $qf+=$f->rating;
                    $qn++;
                }
                }
                $qavg=$qf/$qn;
                }
                ///////////////////////////////////////////////
                
                ////////////////////Admission//////////////////////
                
                    $ac2=new CDbCriteria($mc);
                    $ac2->addCondition('esc=1');
                    $adm2 = Admission::model()->findAll($ac2);
                    $aesc=sizeof($adm2);
                
                ///////////////////////////////////////////////
                
                ////////////////////Interview//////////////////////
                
                $ic2=new CDbCriteria($mc);
                $ic2->addCondition('esc=1');
                $int2 = Interview::model()->findAll($ic2);
                $iesc=sizeof($int2);
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $re=new Report();
                if($re->checkilc('Call',$il)){
                $cc2=new CDbCriteria($mc);
                $cc2->addCondition('esc=1');
                $cal2 = Call::model()->findAll($cc2);
                $cesc=sizeof($cal2);
                
                $cf=0;
                $cn=0;
                $crcf=new CDbCriteria($mc);
                $crcf->addCondition('closed=1');
                $feedc= Call::model()->findAll($crcf);
                $sf=sizeof($feedc);
                if($sf>0){
                foreach($feedc as $c){
                    $cf+=$c->cfeed;
                    $cn++;
                    $callid=$c->callid;
                    $crcf=new CDbCriteria();
                    $crcf->condition='callid=:q and close=1';
                    $crcf->params=array(":q"=>$callid);
                    $fe= Assignesc::model()->find($crcf);
                    $sc=sizeof($fe);
                    if($sc!=0){
                        $cf+=$fe->rating;
                    }
                    }
                }
                $cavg=$cf/$cn;
                }
                $avgg=($qavg+$cavg)/2;
                $avg1=round($avgg,2);
                ///////////////////////////////////////////////
                $totr=$qn+$cn;
                $totesc=$qesc+$eesc+$aesc+$iesc+$cesc;
                
                //echo "<tr><td>$il</td></tr>";
                $ci2=new Report();
                if($ci2->checkilc('Enquiry',$il) || $ci2->checkilc('Query',$il) || $ci2->checkilc('Interview',$il) || $ci2->checkilc('Admission',$il) || $ci2->checkilc('Call',$il))
                {
                ?>
                <tr>
                    <td><?=$mname ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$totr ?></td>
                    <td><?=$avg1 ?></td>
                    <td><?=$qesc ?></td>
                    <td><?=$iesc ?></td>
                    <td><?=$eesc ?></td>
                    <td><?=$aesc ?></td>
                    <td><?=$cesc ?></td>
                    <td><?=$totesc ?></td>
                </tr>
                <?       
                 } 
                $qesc=$eesc=$aesc=$iesc=$cesc=$totesc=0;
                $qf=$qn=$qavg=$cn=$cf=$cavg=$avg1=$avgg=0;
                 
                }
                ?> 
                </table> 
                <?
            
           
                }
            
           else if($ilcm=='0'){
              // echo "ilc==0<br>";
               $cr=new CDbCriteria();
               $cr->select="*";
               if($st!='0' && $ci!='0'){
                 $cr->condition='state=:s and city=:c';
                 $cr->params=array(':s'=>$st,':c'=>$ci);  
               }
               else if($st!='0' && $ci=='0'){
                   
                   $cr->condition='state=:s';
                   $cr->params=array(':s'=>$st);
               }
               $resilc=Ilc::model()->findAll($cr);
               ?>
               
            <table class="table-striped table-font" id="weektab" style="margin-left:1%;">
                <? $rolid=Yii::app()->user->getState("rolid"); ?>          
                <tr>
                    <td>Weekly Report</td>
                    <td colspan="12">
                        <span style="float:right;font-size:14px !important;">
                            <strong>Generated by</strong>&nbsp;<?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                            &nbsp;|&nbsp;
                            <?=date('Y-m-d H:i:s') ?></span></td></tr>
                                <tr style="font-weight: 500">
        
                                    <th>ILC Manager</th>
                                    <th>ILC</th>
                                    <th>No. of Ratings</th>
                                    <th>AVG Rating</th>
                                    <th>Query Esc</th>
                                    <th>Interview Esc</th>
                                    <th>Enquiry Esc</th>
                                    <th>Admission Esc</th> 
                                    <th>Call Esc</th>
                                    <th>Total Escs</th> 
       
                                </tr>
               <?
               foreach($resilc as $il2){
               ///////////////////ilc==0//////////////
               
                $il=$il2->ilcid;  
                //$ilcid=$il2->ilcid;
                $ilcname=$il2->ins_name;//ilcname
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$il);
                $milc = ManagerIlc::model()->find($criteria1);
                
                //$ilcname=$milc->ilcname;
                $mname=$milc->managername;//manager name
                $ilcmid=$milc->ilcmid;
                
                ////////////////////////set of ifs////////////////
                $rr=new Report();
                $mc=$rr->setofif($yr,$mon,$wee,$st,$ci,$il);
                ///////////////////////////////////////////////////
                
                
                ////////////////////Enquiry//////////////////////
                $re=new Report();
                if($re->checkilc('Enquiry',$il)){
                $ec2=new CDbCriteria($mc);
                $ec2->addCondition('esc=1');
                $enqrs2 = Enquiry::model()->findAll($ec2);
                $eesc=sizeof($enqrs2);
                }
                
                ///////////////////////////////////////////////
                
                ////////////////////Query//////////////////////
                $re=new Report();
                if($re->checkilc('Query',$il)){
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('esc=1');
                    $qry2 = Query::model()->findAll($qc2);
                    $qesc=sizeof($qry2);
                
                $qf=0;
                $qn=0;
                $crqf=new CDbCriteria($mc);
                $crqf->addCondition('query_closed=1');
                $feedq= Query::model()->findAll($crqf);
                $fq=sizeof($feedq);
                if($fq>0){
                foreach($feedq as $f){
                    $qf+=$f->rating;
                    $qn++;
                }
                }
                $qavg=$qf/$qn;
                }
                ///////////////////////////////////////////////
                
                ////////////////////Admission//////////////////////
                $re=new Report();
                if($re->checkilc('Admission',$il)){
                $ac2=new CDbCriteria($mc);
                $ac2->addCondition('esc=1');
                $adm2 = Admission::model()->findAll($ac2);
                $aesc=sizeof($adm2);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Interview
                $re=new Report();
                if($re->checkilc('Interview',$il)){
                $ic2=new CDbCriteria($mc);
                $ic2->addCondition('esc=1');
                $int2 = Interview::model()->findAll($ic2);
                $iesc=sizeof($int2);
                }
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $re=new Report();
                if($re->checkilc('Call',$il)){
                $cc2=new CDbCriteria($mc);
                $cc2->addCondition('esc=1');
                $cal2 = Call::model()->findAll($cc2);
                $cesc=sizeof($cal2);
                
                $cf=0;
                $cn=0;
                $crcf=new CDbCriteria($mc);
                $crcf->addCondition('closed=1');
                $feedc= Call::model()->findAll($crcf);
                $sf=sizeof($feedc);
                if($sf>0){
                foreach($feedc as $c){
                    $cf+=$c->cfeed;
                    $cn++;
                    $callid=$c->callid;
                    $crcf=new CDbCriteria();
                    $crcf->condition='callid=:q and close=1';
                    $crcf->params=array(":q"=>$callid);
                    $fe= Assignesc::model()->find($crcf);
                    $sc=sizeof($fe);
                    if($sc!=0){
                        $cf+=$fe->rating;
                    }
                    }
                }
                $cavg=$cf/$cn;
                }
                $avgg=($qavg+$cavg)/2;
                $avg1=round($avgg,2);
                ///////////////////////////////////////////////
                $totr=$qn+$cn;
                $totesc=$qesc+$eesc+$aesc+$iesc+$cesc;
                
                //echo "<tr><td>$il</td></tr>";
                $ci2=new Report();
                if(($ci2->checkilc('Enquiry',$il)) || ($ci2->checkilc('Query',$il)) || ($ci2->checkilc('Admission',$il)) || ($ci2->checkilc('Interview',$il)) || ($ci2->checkilc('Call',$il))){
               ?>

               
        <tr>
            <td><?=$mname ?></td>
            <td><?=$ilcname ?></td>
            <td><?=$totr ?></td>
            <td><?=$avg1 ?></td>
            <td><?=$qesc ?></td>
            <td><?=$iesc ?></td>
            <td><?=$eesc ?></td>
            <td><?=$aesc ?></td>
            <td><?=$cesc ?></td>
            <td><?=$totesc ?></td>
        </tr>
    
         <?       
               } 
               $cn=$qn=$cf=$qf=$cavg=$qavg=$avgg=$avg1=0;
               $qesc=$eesc=$aesc=$iesc=$cesc=$totesc=0;
                }
            ?>
        </table> 
        <?
               ///////////////////////////////////////    
           }
           
            
          
        }
        
        public function ratingreport($il,$yr,$mon,$wee,$ci,$st,$ilcm){
            
           if($ilcm!='0'){
               
                ///////////////////////////main////////////////////////
               ?>
    <table class="table-striped table-font" id="ratetab" style="margin-left:1%;">
        <? $rolid=Yii::app()->user->getState("rolid"); ?>          
                <tr>
                    <td>Rating Report</td>
                    <td colspan="12">    
                        <span style="float:right;font-size:14px !important;">
                            <strong>Generated by</strong>&nbsp;<?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                            &nbsp;|&nbsp;
                            <?=date('Y-m-d H:i:s') ?>
                        </span>
                    </td>
                </tr>
                <tr style="font-weight: 500">
        
                    <th>State</th>
                    <th>City</th>
                    <th>ILC</th>
                    <th>ILC Manager</th>
                    <th>No. of Ratings</th>
                    <th>Average Rating</th>
                    
                 </tr>
                <?
                $c1 = new CDbCriteria();
                $c1->condition='ilcmid=:s';
                $c1->params=array(':s'=>$ilcm);
                $resilcm = ManagerIlc::model()->findAll($c1);
                
                foreach($resilcm as $ri){
                 $il=$ri->ilcid;
                 $ilcname=$ri->ilcname;
                 
                /////////////////////////main//////////////////
                $mc=new CDbCriteria();
                 if($yr=='0' && $mon=='0' && $wee=='0' && $st=='0' && $ci=='0'){
                    $mc->condition='ilcid=:s';
                    $mc->params=array(':s'=>$il);
                }
                if($yr!='0' && $mon=='0'&& $st=='0' && $ci=='0'){
                    $mc->condition='ilcid=:s and year=:y';
                    //$mc->addCondition("year=:y");
                    $mc->params=array(':s'=>$il,':y'=>intval($yr));
                }
                
                if($yr!='0' && $mon!='0'){
                    $mc->condition='ilcid=:s and year=:y and month=:m';
                    //$mc->addCondition("year=:y and month=:m");
                    $mc->params=array(':s'=>$il,':y'=>intval($yr),":m"=>intval($mon));
                }
                
                if($yr!='0' && $mon!='0' && $wee!='0'){
                    $mc->condition='ilcid=:s and year=:y and month=:m and week=:w';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':s'=>$il,':y'=>intval($yr),":m"=>intval($mon),':w'=>intval($wee));
                }
                if($yr!='0' && $mon!='0' && $wee!='0' && $st!='0'){
                    $mc->condition='ilcid=:s and year=:y and month=:m and week=:w';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':s'=>$il,':y'=>intval($yr),":m"=>intval($mon),':w'=>intval($wee),':st'=>$st);
                }
                
                if($yr!='0' && $mon!='0' && $wee!='0' && $st!='0' && $ci!='0'){
                    $mc->condition='ilcid=:s and year=:y and month=:m and week=:w and state=:st and city=:c';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':s'=>$il,':y'=>intval($yr),":m"=>intval($mon),':w'=>intval($wee),':st'=>$st,':c'=>$ci);
                }
                if($yr=='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci!='0'){
                    $mc->condition='ilcid=:s and state=:st and city=:c';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':s'=>$il,':st'=>$st,':c'=>$ci);
                }
                
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$il);
                $ilcs = Ilc::model()->find($criteria1);
                if($st=='0' || $st==''){
                  $st=$ilcs->state;
                 }
                 if($ci=='0' || $ci==''){
                  $ci=$ilcs->city;
                 }
                 
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$ri->ilcid);
                $milc = ManagerIlc::model()->find($criteria1);
                
                $ilcmid=$milc->ilcmid;
                $mname=$milc->managername;//manager name
                
                ////////////////////Query//////////////////////
                $ci2=new Report();
                if($ci2->checkilc('Query',$il)){
                
                $qrating=0;
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('query_closed=1');
                $qry2 = Query::model()->findAll($qc2);
                $qc=sizeof($qry2);
                
                foreach($qry2 as $q){
                    $rating1=$q->rating;
                    $qrating+=$rating1;
                }
                }
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $ci2=new Report();
                if($ci2->checkilc('Call',$il)){
                    
                
                $crating=0;
                $cc2=new CDbCriteria($mc);
                $cc2->addCondition('closed=1');
                $cal2 = Call::model()->findAll($cc2);
                $cc=sizeof($cal2);
                
                foreach($cal2 as $c){
                    $rating2=$c->cfeed;
                    $crating+=$rating2;
                }
                }
                $qavg=$qrating/$qc;
                $cavg=$crating/$cc;
                $totr=$qc+$cc;
                
                $avg1=round(floatval(($qavg+$cavg)/2),2);
                ?>

        <tr>
            
            <td><?=$st ?></td>
            <td><?=$ci ?></td>
            <td><?=$ilcname ?></td>
            <td><?=$mname ?></td>
            <td><?=$totr ?></td>
            <td><?=$avg1 ?></td>
        </tr>
    
         <?       
         $qrating=$crating=$qc=$cc=$qavg=$cavg=$totr=$avg1=0;      
         }
                ?> </table> <?
                }
            
           else if($ilcm=='0'){
               
               // echo "ilc==0<br>";
               $cr=new CDbCriteria();
               $cr->select="*";
               if($st!='0' && $ci!='0'){
                 $cr->condition='state=:s and city=:c';
                 $cr->params=array(':s'=>$st,':c'=>$ci);  
               }
               else if($st!='0' && $ci=='0'){
                   
                   $cr->condition='state=:s';
                   $cr->params=array(':s'=>$st);
               }
               $resilc=Ilc::model()->findAll($cr);
               
               ?>
                <table class="table-striped table-font" id="ratetab" style="margin-left:1%;">
                              <? $rolid=Yii::app()->user->getState("rolid"); ?>          
                    <tr>
                        <td>Rating Report</td>
                        <td colspan="12">
                            <span style="float:right;font-size:14px !important;">
                                <strong>Generated by</strong>&nbsp;<?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                                &nbsp;|&nbsp;
                                <?=date('Y-m-d H:i:s') ?></span>
                        </td>
                    </tr>
                   <tr style="font-weight: 500">
        
                    <th>State</th>
                    <th>City</th>
                    <th>ILC</th>
                    <th>ILC Manager</th>
                    <th>No. of Ratings</th>
                    <th>Average Rating</th>
       
                </tr>
               <?
               foreach($resilc as $il2){
               ///////////////////ilc==0//////////////
               
                $il=$il2->ilcid;  
                //$ilcid=$il2->ilcid;
                $ilcname=$il2->ins_name;//ilcname
                if($st=='0'){
                  $st=$il2->state;
                 }
                 if($ci=='0'){
                  $ci=$il2->city;
                 }
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$il);
                $milc = ManagerIlc::model()->find($criteria1);
                
                //$ilcname=$milc->ilcname;
                $mname=$milc->managername;//manager name
                $ilcmid=$milc->ilcmid;
                /////////////////////////main//////////////////
                
                $mc=new CDbCriteria();
               if($yr=='0' && $mon=='0' && $wee=='0' && $st=='0' && $ci=='0'){
                   $mc->select="*";
                   $mc->condition='ilcid=:il';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':il'=>$il);
                   
               }
               
               else if($yr=='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci=='0'){
                   $mc->condition='state=:st and ilcid=:il';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':st'=>$st,':il'=>$il);
               }
               
               else if($yr=='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci!='0'){
                   $mc->condition='state=:st and city=:c and ilcid=:il';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':st'=>$st,':c'=>$ci,':il'=>$il);
               }
               
               else if($yr!='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci!='0'){
                   $mc->condition='year=:y and state=:st and city=:c and ilcid=:il';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':y'=>intval($yr),':st'=>$st,':c'=>$ci,':il'=>$il);
               }
               
               else if($yr!='0' && $mon!='0' && $wee=='0' && $st!='0' && $ci!='0'){
                   $mc->condition='year=:y and month=:m and state=:st and city=:c and ilcid=:il';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':y'=>intval($yr),":m"=>intval($mon),':st'=>$st,':c'=>$ci,':il'=>$il);
               }
               
               else if($yr!='0' && $mon!='0' && $wee!='0' && $st!='0' && $ci!='0'){
                    $mc->condition='year=:y and month=:m and week=:w and state=:st and city=:c and ilcid=:il';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':il'=>$il,':y'=>intval($yr),":m"=>intval($mon),':w'=>intval($wee),':st'=>$st,':c'=>$ci);
                }
                
                
                ////////////////////Query//////////////////////
                $ci2=new Report();
                if($ci2->checkilc('Query',$il)){
                    
                
                $qrating=0;
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('query_closed=1');
                $qry2 = Query::model()->findAll($qc2);
                $qc=sizeof($qry2);
                
                foreach($qry2 as $q){
                    $rating1=$q->rating;
                    $qrating+=$rating1;
                }
                }
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $ci2=new Report();
                if($ci2->checkilc('Call',$il)){
                    
                
                $crating=0;
                $cc2=new CDbCriteria($mc);
                $cc2->addCondition('closed=1');
                $cal2 = Call::model()->findAll($cc2);
                $cc=sizeof($cal2);
                
                foreach($cal2 as $c){
                    $rating2=$c->cfeed;
                    $crating+=$rating2;
                 }
                }
                $qavg=$qrating/$qc;
                $cavg=$crating/$cc;
                $totr=$qc+$cc;
                
                $avg1=round(floatval(($qavg+$cavg)/2),2);
                ?>

               
        <tr>
            <td><?=$st ?></td>
            <td><?=$ci ?></td>
            <td><?=$ilcname ?></td>
            <td><?=$mname ?></td>
            <td><?=$totr ?></td>
            <td><?=$avg1 ?></td>

        </tr>
    
         <?
         $qrating=$crating=$qc=$cc=$qavg=$cavg=$totr=$avg1=0; 
            }
            ?>
        </table> 
        <?
               ///////////////////////////////////////    
           } 
            
          
        }
        
        public function escalationreport($il,$yr,$mon,$wee,$ci,$st,$ilcm){
            
           if($ilcm!='0'){
               
                ///////////////////////////main////////////////////////
               ?>
<table class="table-striped table-font" id="esctab" style="margin-left:1%;">
     <? $rolid=Yii::app()->user->getState("rolid"); ?>          
    <tr>
        <td>Escalation Report</td>
        <td colspan="12">    
            <span style="float:right;font-size:14px !important;margin-top:2%">
                <strong>Generated by</strong>&nbsp;
                <?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                &nbsp;|&nbsp;
                <?=date('Y-m-d H:i:s') ?>
            </span>
        </td>
    </tr>          
    <tr style="font-weight: 500">
        
                    <th>State</th>
                    <th>City</th>
                    <th>ILC</th>
                    <th>ILC Manager</th>
                    <th>Module</th>
                    <th>Raised Date</th>
                    <th>Closed Date</th>
                    <th>Rating</th>
                    
                 </tr>
                <?
                $c1 = new CDbCriteria();
                $c1->condition='ilcmid=:s';
                $c1->params=array(':s'=>$ilcm);
                $resilcm = ManagerIlc::model()->findAll($c1);
                
                foreach($resilcm as $ri){
                
                 $il=$ri->ilcid;
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$ri->ilcid);
                $ilcs = Ilc::model()->find($criteria1);
                
                $ilcname=$ilcs->ins_name;//ilcname
                $state=$ilcs->state;
                $city=$ilcs->city;
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$ri->ilcid);
                $milc = ManagerIlc::model()->find($criteria1);
                
                $ilcmid=$milc->ilcmid;
                $mname=$milc->managername;//manager name
                
                 ////////////////////////set of ifs////////////////
                $rr=new Report();
                $mc=$rr->setofif($yr,$mon,$wee,$st,$ci,$il);
                ///////////////////////////////////////////////////
                
                ////////////////////Enquiry//////////////////////
                $ec2=new CDbCriteria($mc);
                $ec2->addCondition('esc=1');
                $enqrs2 = Enquiry::model()->findAll($ec2);
                $eesc=sizeof($enqrs2);
                
                if($eesc!=0){
                $ci=new Report();
                if($ci->checkilc('Enquiry',$il)){
                    
                
                foreach($enqrs2 as $enq) {
                ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Enquiry</td>
                    <td><?=$enq->enquiry_date ?></td>
                    <? if($enq->clodate!='0000-00-00'){ ?>
                    <td><?=$enq->clodate ?></td> <? } 
                    else
                    { 
                       ?>
                    <td>Active</td> <? } ?>
                    <td></td>
                </tr> 
                <? } } }///////////////////////////////////////////////
                
                ////////////////////Query//////////////////////
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('esc=1');
                $qry2 = Query::model()->findAll($qc2);
                $qesc=sizeof($qry2);
                
                $ci=new Report();
                if($ci->checkilc('Query',$il)){
                foreach($qry2 as $q3){
                    
                $qf=0;
                $qn=0;
                $crqf=new CDbCriteria();
                $crqf->condition='queryid=:q';
                $crqf->params=array(":q"=>$q3->qid);
                $feedq= QueryFeed::model()->find($crqf);
                
                $feed=$feedq->rating;
                
                ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Query</td>
                    <td><?=date('Y-m-d',$q3->query_date) ?></td>
                    <? if($q3->clodate!='0000-00-00'){ ?>
                    <td><?=$q3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td><?=$feed ?></td>
                </tr>
                <?  } }///////////////////////////////////////////////
                
                ////////////////////Admission//////////////////////
                $ac2=new CDbCriteria($mc);
                $ac2->addCondition('esc=1');
                $adm2 = Admission::model()->findAll($ac2);
                $aesc=sizeof($adm2);
                
                $ci=new Report();
                if($ci->checkilc('Admission',$il)){
                foreach($adm2 as $a3){
                  ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Admission</td>
                    <td><?=date('Y-m-d',$a3->date1) ?></td>
                    <? if($a3->clodate!='0000-00-00'){ ?>
                    <td><?=$a3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td></td>
                </tr>
                <?  
                } }
                ///////////////////////////////////////////////
                
                ////////////////////Interview//////////////////////
                $ic2=new CDbCriteria($mc);
                $ic2->addCondition('esc=1');
                $int2 = Interview::model()->findAll($ic2);
                $iesc=sizeof($int2);
                
                $ci=new Report();
                if($ci->checkilc('Interview',$il)){
                foreach($int2 as $i3){
                  ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Interview</td>
                    <td><?=$i3->idate ?></td>
                    <? if($i3->clodate!='0000-00-00'){ ?>
                    <td><?=$i3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td></td>
                </tr>
                <?    
                } }
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $cc2=new CDbCriteria($mc);
                
                $cc2->addCondition('esc=1');
                $cal2 = Call::model()->findAll($cc2);
                $cesc=sizeof($cal2);
                
                $ci=new Report();
                if($ci->checkilc('Call',$il)){
                foreach($cal2 as $c3){
                  ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Call</td>
                    <td><?=$c3->date1 ?></td>
                    <? if($c3->clodate!='0000-00-00'){ ?>
                    <td><?=$c3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td><?=$c3->cfeed ?></td>
                </tr>
                <?    
                } }
                
                $cf=0;
                $cn=0;
                $crcf=new CDbCriteria($mc);
                $crcf->condition='ilcid=:i and ilcmid=:q';
                $crcf->params=array(":i"=>$ri->ilcid,":q"=>$ilcmid);
                $feedc= Call::model()->findAll($crcf);
                foreach($feedc as $c){
                    $cf+=$c->cfeed;
                    $cn++;
                }
                $cavg=$cf/$cn;
                $avgg=($cavg+$qavg)/2;
                $avg1=round($avgg,2);
                ///////////////////////////////////////////////
                $totr=$qn+$cn;
                $totesc=$qesc+$eesc+$aesc+$iesc+$cesc;
                }
                ?> </table> <?
                }
            
           else if($ilcm=='0'){
               
                
               $cr=new CDbCriteria();
               $cr->select="*";
               if($st!='0' && $ci!='0'){
                 $cr->condition='state=:s and city=:c';
                 $cr->params=array(':s'=>$st,':c'=>$ci);  
               }
               else if($st!='0' && $ci=='0'){
                   
                   $cr->condition='state=:s';
                   $cr->params=array(':s'=>$st);
               }
               $resilc=Ilc::model()->findAll($cr);
               ?>
            <table class="table-striped table-font" id="esctab" style="margin-left:1%;">
              <? $rolid=Yii::app()->user->getState("rolid"); ?>          
                <tr>
                    <td>Escalation Report</td>
                    <td colspan="12"> 
                        <span style="float:right;font-size:14px !important;">
                        <strong>Generated by</strong>&nbsp;<?=Yii::app()->user->getState('name')  ?>[ <?=$rolid ?>]
                &nbsp;|&nbsp;
                <?=date('Y-m-d H:i:s') ?>
                        </span>
                    </td>
                </tr>
                   <tr style="font-weight: 500">
        
                    <th>State</th>
                    <th>City</th>
                    <th>ILC</th>
                    <th>ILC Manager</th>
                    <th>Module</th>
                    <th>Raised Date</th>
                    <th>Closed Date</th>
                    <th>Rating</th>
       
                </tr>
               <?
               foreach($resilc as $il2)
               ///////////////////ilc==0//////////////
               {
                $city=$il2->city;
                $state=$il2->state;
                $il=$il2->ilcid;
                $ilcname=$il2->ins_name;//ilcname
                
                
                $criteria1 = new CDbCriteria();
                $criteria1->condition='ilcid=:s';
                $criteria1->params=array(':s'=>$il2->ilcid);
                $milc = ManagerIlc::model()->find($criteria1);
                
                $ilcname=$milc->ilcname;
                $mname=$milc->managername;//manager name
                $ilcmid=$milc->ilcmid;
                
                ////////////////////////set of ifs////////////////
                $rr=new Report();
                $mc=$rr->setofif($yr,$mon,$wee,$st,$ci,$il);
                ///////////////////////////////////////////////////
                
                
                ////////////////////Enquiry//////////////////////
                $ec2=new CDbCriteria($mc);
                $ec2->addCondition('esc=1');
                $enqrs2 = Enquiry::model()->findAll($ec2);
                $eesc=sizeof($enqrs2);
                
                if($eesc!=0){
                foreach($enqrs2 as $enq) {
                ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Enquiry</td>
                    <td><?=$enq->enquiry_date ?></td>
                    <? if($enq->clodate!='0000-00-00'){ ?>
                    <td><?=$enq->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td></td>
                </tr> 
                <? } } ///////////////////////////////////////////////
                
                ////////////////////Query//////////////////////
                $qc2=new CDbCriteria($mc);
                $qc2->addCondition('esc=1');
                $qry2 = Query::model()->findAll($qc2);
                $qesc=sizeof($qry2);
                
                foreach($qry2 as $q3){
                    
                $qf=0;
                $qn=0;
                $crqf=new CDbCriteria();
                $crqf->condition='queryid=:q';
                $crqf->params=array(":q"=>$q3->qid);
                $feedq= QueryFeed::model()->find($crqf);
                
                $feed=$feedq->rating;
                
                ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Query</td>
                    <td><?=date('Y-m-d',$q3->query_date) ?></td>
                    <? if($q3->clodate!='0000-00-00'){ ?>
                    <td><?=$q3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td><?=$feed ?></td>
                </tr>
                <?  } ///////////////////////////////////////////////
                
                ////////////////////Admission//////////////////////
                $ac2=new CDbCriteria($mc);
                $ac2->addCondition('esc=1');
                $adm2 = Admission::model()->findAll($ac2);
                $aesc=sizeof($adm2);
                
                foreach($adm2 as $a3){
                  ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Admission</td>
                    <td><?=date('Y-m-d',$a3->date1) ?></td>
                    <? if($a3->clodate!='0000-00-00'){ ?>
                    <td><?=$a3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td></td>
                </tr>
                <?  
                }
                ///////////////////////////////////////////////
                
                ////////////////////Interview//////////////////////
                $ic2=new CDbCriteria($mc);
                $ic2->addCondition('esc=1');
                $int2 = Interview::model()->findAll($ic2);
                $iesc=sizeof($int2);
                foreach($int2 as $i3){
                  ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Interview</td>
                    <td><?=$i3->idate ?></td>
                    <? if($i3->clodate!='0000-00-00'){ ?>
                    <td><?=$i3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td></td>
                </tr>
                <?    
                }
                ///////////////////////////////////////////////
                
                ////////////////////Call//////////////////////
                $cc2=new CDbCriteria($mc);
                $cc2->addCondition('esc=1');
                $cal2 = Call::model()->findAll($cc2);
                $cesc=sizeof($cal2);
                foreach($cal2 as $c3){
                  ?>
                <tr>
                    <td><?=$state ?></td>
                    <td><?=$city ?></td>
                    <td><?=$ilcname ?></td>
                    <td><?=$mname ?></td>
                    <td>Call</td>
                    <td><?=$c3->date1 ?></td>
                    <? if($c3->clodate!='0000-00-00'){ ?>
                    <td><?=$c3->clodate ?></td> <? } 
                    else
                    { ?>   <td>Active</td> <? } ?>
                    <td><?=$c3->cfeed ?></td>
                </tr>
                <?    
                }
                $cf=0;
                $cn=0;
                $crcf=new CDbCriteria($mc);
                $crcf->condition='ilcid=:i and ilcmid=:q';
                $crcf->params=array(":i"=>$ri->ilcid,":q"=>$ilcmid);
                $feedc= Call::model()->findAll($crcf);
                foreach($feedc as $c){
                    $cf+=$c->cfeed;
                    $cn++;
                }
                $cavg=$cf/$cn;
                $avgg=($cavg+$qavg)/2;
                $avg1=round($avgg,2);
                ///////////////////////////////////////////////
                $totr=$qn+$cn;
                $totesc=$qesc+$eesc+$aesc+$iesc+$cesc;
                    
             }
            ?>
        </table> 
        <?
               ///////////////////////////////////////    
           } 
            
          
        }
        
        public function checkilc($table,$ilcid){
            $cr=new CDbCriteria();
            $cr->condition="ilcid=:i";
            $cr->params=array(":i"=>$ilcid);
            if($table=='Call'){
                $res=Call::model()->findAll($cr);
                $sizec=  sizeof($res);
                if($sizec>0){
                    return true;
                }
                else {
                    return false;
                }
            }
            
            if($table=='Admission'){
                $res=Admission::model()->findAll($cr);
                $sizea=  sizeof($res);
                if($sizea>0){
                    return true;
                }
                else {
                    return false;
                }
            }
            
            if($table=='Interview'){
                $res=Interview::model()->findAll($cr);
                $sizei=  sizeof($res);
                if($sizei>0){
                    return true;
                }
                else {
                    return false;
                }
            }
            
            if($table=='Query'){
                $res=Query::model()->findAll($cr);
                $sizeq=  sizeof($res);
                if($sizeq>0){
                    return true;
                }
                else {
                    return false;
                }
            }
            
            if($table=='Enquiry'){
                $res=Enquiry::model()->findAll($cr);
                $sizee=  sizeof($res);
                if($sizee>0){
                    return true;
                }
                else {
                    return false;
                }
            }
            
            
        }
        
        public function setofif($yr,$mon,$wee,$st,$ci,$il){
             $mc=new CDbCriteria();
              /////////////////////////bunch of IFs///////////////////////////
               if($yr=='0' && $mon=='0' && $wee=='0' && $st=='0' && $ci=='0'){
                   $mc->condition='ilcid=:i';
                   $mc->params=array(":i"=>$il);
                }
               
                else if($yr!='0' && $mon=='0' && $wee=='0' && $st=='0' && $ci=='0'){
                   $mc->condition='ilcid=:i and year=:y';
                   $mc->params=array(":i"=>$il,":y"=>intval($yr));
                }
               
               else if($yr!='0' && $mon!='0' && $wee=='0' && $st=='0' && $ci=='0'){
                   $mc->condition='ilcid=:i and year=:y and month=:m';
                   $mc->params=array(":i"=>$il,":y"=>intval($yr),":m"=>intval($mon));
               }
               
               else if($yr!='0' && $mon!='0' && $wee!='0' && $st=='0' && $ci=='0'){
                   $mc->condition='ilcid=:i and year=:y and month=:m and week=:w';
                   $mc->params=array(":i"=>$il,":y"=>intval($yr),":m"=>intval($mon),":w"=>intval($wee));
               }
               
               else if($yr!='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci=='0'){
                   $mc->condition='ilcid=:i and year=:y state=:s';
                   $mc->params=array(":i"=>$il,":y"=>intval($yr),":s"=>$st);
               }
              
               
               else if($yr=='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci=='0'){
                   $mc->condition='state=:st and ilcid=:i';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':st'=>$st,":i"=>$il);
               }
               
               else if($yr=='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci!='0'){
                   $mc->condition='state=:st and city=:c and ilcid=:i';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':st'=>$st,':c'=>$ci,":i"=>$il);
               }
               
               else if($yr!='0' && $mon=='0' && $wee=='0' && $st!='0' && $ci!='0'){
                   $mc->condition='year=:y and state=:st and city=:c and ilcid=:i';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':y'=>intval($yr),':st'=>$st,':c'=>$ci,":i"=>$il);
               }
               
               else if($yr!='0' && $mon!='0' && $wee=='0' && $st!='0' && $ci!='0'){
                   $mc->condition='year=:y and month=:m and state=:st and city=:c and ilcid=:i';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':y'=>intval($yr),":m"=>intval($mon),':st'=>$st,':c'=>$ci,":i"=>$il);
               }
               
                else if($yr!='0' && $mon!='0' && $wee!='0' && $st!='0' && $ci=='0'){
                    $mc->condition='year=:y and month=:m and week=:w and state=:st and ilcid=:i';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':i'=>$il,':y'=>intval($yr),":m"=>intval($mon),':w'=>intval($wee),':st'=>$st,":i"=>$il);
                }
               
               else if($yr!='0' && $mon!='0' && $wee!='0' && $st!='0' && $ci!='0'){
                    $mc->condition='year=:y and month=:m and week=:w and state=:st and city=:c and ilcid=:i';
                    //$mc->addCondition("year=:y and month=:m and week=:w");
                    $mc->params=array(':i'=>$il,':y'=>intval($yr),":m"=>intval($mon),':w'=>intval($wee),':st'=>$st,':c'=>$ci,":i"=>$il);
                }
                ///////////////////////////////////////////////////////////////////////
        
                return $mc;
               }
        
        
}