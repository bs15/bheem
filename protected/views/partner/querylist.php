<div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/index">Home</a> >
                Task Scheduler > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/querylist">
                    <span style="color:blue">Post Query</span>
                </a>
            </span>
        </div> 
    </div>
 
<div class="row">
    <div class="col-md-8">
        <h1>Current QUERIES </h1>
        <div id="qer">
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div id="qchat">
            
        </div>
        
        <div class="fchat" id="fchat">
            <form id="sform">
                <input type="hidden" id="query_id" value="-" />
                <div style="display: none">           
                    <input type="file" id="cfile" onchange="uploadfile()" /></div>
                <button onclick="upfile()" type="button" style="padding: 5px"> 
                    <i class="glyphicon glyphicon-paperclip"></i></button>
                <br>
                <!--<input type="button" value="Send File" class="btn btn-success" onclick="uploadfile()"/>-->
                <br>
            </form>
            <!--end widget-->
        </div>
        <div class="chtxt">
            <form id="frmc">
                <input type="hidden" id="qid" value="-" />
                 <input type="hidden" id="uuid" value="-" />
                <div class="form-group">
                    <textarea class="form-control" id="chttext" placeholder="type message..."></textarea>
                </div>
                <div class="form-group">
                    <input type="button" id="chbtn" class="btn btn-outline-dark btn-sm" onclick="sendchat()" value="SEND" />
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
    $("#chttext").hide();
    $("#chbtn").hide();
     $("#fchat").hide();
   // $("#chbtn").prop('disabled','true');
    getq();
//    $("#datepicker").datepicker({
//  dateFormat: "yy-mm-dd"
//});
/////////////////////////////////////////////
setInterval(function(){
    var qid=$("#qid").val();
     var uid=$("#uuid").val();
    if(qid!="-") {
    getchat(qid,uid)  
    }
},10000);
///////////////////////////////////////////////
});

//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var file_data = $('#cfile').prop('files')[0];   
      var qid = $('#query_id').val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('qid', qid);
   // alert(form_data);                             
    $.ajax({
        url: '<? echo $this->createUrl("uploadpost"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
           var uid=$("#uuid").val();
          getchat(qid,uid);
   
        }
     });
}
////////////////////////////////////////////////////////////


function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qer").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getquery2"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function showchat(qid,uid) {
$("#qid").val(qid);
$("#uuid").val(uid);
  //$("#chbtn").prop('disabled','false');
  getchat(qid,uid);
    $("#chttext").show();
    $("#fchat").show();
     $("#query_id").val(qid);
    $("#chbtn").show();
      $('html, body').animate({
        scrollTop: $("#qchat").offset().top
    }, 2000);
}

function getchat(qid,uid) {
     $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   },
                
        success:function(html){
        $("#qchat").empty().append(html);
        var objDiv = document.getElementById("chatui");
        objDiv.scrollTop = objDiv.scrollHeight;
        },
             error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getchat"); ?>',
        data:{
          qid:qid,uid:uid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function sendchat() {
var qid=$("#qid").val();
var msg=$("#chttext").val();
if(msg=="") {
    alert("Enter message...");
}
else {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
  //  $("#frmc").trigger("reset");
  $("#chttext").val('');
  //  document.getElementById('ilcfrm').reset();
    // $("#qchat").empty().append(html);
  var uid=$("#uuid").val();
   getchat(qid,uid);
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("sendchat"); ?>',
        data:{
          qid:qid,msg:msg
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
</script>