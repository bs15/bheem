<?php
/* @var $this CrockController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Crocks',
);

$this->menu=array(
	array('label'=>'Create Crock', 'url'=>array('create')),
	array('label'=>'Manage Crock', 'url'=>array('admin')),
);
?>

<h1>Crocks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
