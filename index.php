<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);


ini_set("post_max_size", "30000M");
ini_set("upload_max_filesize", "30000M");
ini_set("memory_limit", -1 );

//////////////////////////////////////////
define('YII_ENABLE_ERROR_HANDLER', true);
//define('YII_ENABLE_EXCEPTION_HANDLER', false);

// Turn off all error reporting
// error_reporting(0);

// Report all errors except E_NOTICE
// This is the default value set in php.ini
//error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRICATED);
//////////////////////////////////////////
require_once($yii);
Yii::createWebApplication($config)->run();
