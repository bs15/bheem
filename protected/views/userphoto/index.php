<?php
/* @var $this UserphotoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Userphotos',
);

$this->menu=array(
	array('label'=>'Create Userphoto', 'url'=>array('create')),
	array('label'=>'Manage Userphoto', 'url'=>array('admin')),
);
?>

<h1>Userphotos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
