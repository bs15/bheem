<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="row">
   <div class="col-md-12" >
       <br>
       <span class="bread">
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
          Internal > 
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/userpass">
               <span style="color:blue">User Passwords</span>
           </a>
       </span>
    </div> 
</div>
<div class="row">
    <div class="col-md-10" id="ulist">
        <h1>USERS</h1>
           <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Name</b></td>
                        <td><b>Username</b></td>
                        <td><b>Role</b></td>
                        <td><b>Default Password</b></td>
                        <td><b>Action</b></td>
                      
                    </tr>
            <? $qflag=0;
            foreach ($users as $q) {
           $uid=$q->userid;
           $crd=new CDbCriteria();
           $crd->condition="userid=:u";
           $crd->params=array(":u"=>$uid);
           $ud=  Userdef::model()->find($crd);
           if(!empty($ud))
               $dp=$ud->defpass;
           else
               $dp="";
                
                ?>
                <tr>
                    <td><span style="text-transform: uppercase;"><?=$q->name ?></span></td> 
                  <td><span ><?=$q->username ?></span></td> 
                    <td><span ><?=$q->role ?></span></td> 
                    <td><input type="text" id="tp-<?=$q->userid ?>" value="<?=$dp ?>" /></td> 
                    <td>
                        <input type="button" class="btn btn-success" id="xx<?=$q->id ?>" name="<?=$q->userid ?>" value="Set default password" onclick="setpass(this.id,this.name)" />
                    </td>
                </tr>
                <?
                
            }
            ?>
                </table>
           </div>
    </div>
</div> 
<script type="text/javascript">
function setpass(id,uid) {
    var pass=$("#tp-"+uid).val();
    if(pass=="" || pass==null) {
        alert("Please enter password!");
    }
    else if(pass.length < 6) {
        alert("Password should contain 6 or more letters");
    }
    else {
         ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
      
        },
                
        success:function(html){
         alert("Password updated for this user!")
        },
                
        type:'post',
        url:'<? echo $this->createUrl("passreset"); ?>',
        data:{
            uid:uid,pass:pass
        },
    dataType:"html",
    cache:false
});//ajax
                 
                 
                 ///////////////////////////////////////////////////
    }
    
}
</script>