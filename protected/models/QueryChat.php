<?php

/**
 * This is the model class for table "query_chat".
 *
 * The followings are the available columns in table 'query_chat':
 * @property string $queryid
 * @property string $chatid
 * @property string $senderid
 * @property string $recid
 * @property string $message
 * @property string $date1
 * @property string $document
 */
class QueryChat extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return QueryChat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'query_chat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('queryid, chatid, senderid, recid, message, date1', 'required'),
                        array('id', 'integerOnly'=>true),
			array('queryid, chatid, senderid, recid', 'length', 'max'=>100),
			array('date1', 'length', 'max'=>20),
			array('document', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, queryid, chatid, senderid, recid, message, date1, document', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'queryid' => 'Queryid',
			'chatid' => 'Chatid',
			'senderid' => 'Senderid',
			'recid' => 'Recid',
			'message' => 'Message',
			'date1' => 'Date1',
			'document' => 'Document',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('queryid',$this->queryid,true);
		$criteria->compare('chatid',$this->chatid,true);
		$criteria->compare('senderid',$this->senderid,true);
		$criteria->compare('recid',$this->recid,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('date1',$this->date1,true);
		$criteria->compare('document',$this->document,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}