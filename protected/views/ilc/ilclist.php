<?
$criteria1 = new CDbCriteria();
$criteria1->select = '*';
$criteria1->group="state";
$states = Ilc::model()->findAll($criteria1);
?>
<div class="container">
    <div class="row">
    <div class="col-md-12">
        <br>
        <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Master Data Management > Manage > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilc/ilclist">
                    <span style="color:blue">ILC</span>
                </a>
            </span> 
    </div>
</div>
    <div class="row">
        <br style="clear:both">
        <h2 style="margin-left:2%">View all ILCs</h2>
    </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <input type="hidden" id="silc2" value="-">
                        <select id="st" class="enin" onchange="getcity()" style="margin-left:1%">
                            <option value="0">Select State</option>
                            <? foreach($states as $st){
                                ?>
                                <option value="<?=$st->state ?>"><?=$st->state ?></option>
                                <?
                            } ?>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-12" id="loadcity"></div>

                </div>
            
    <div id='enq'>
        <table class="table table-striped table-font">
            <tr>
            <th>Type of ILC</th>
            <th>Institute Details</th>
            <th>Contact Details</th>
            <th>Partner</th>
            <th>Agreement</th>
            <th>Junior Preschool</th>
            <th>Senior Preschool</th>
            <th>Teacher Training</th>
            
            </tr>
            <?
            foreach($ilcs as $il){
            ?>
            <tr>
                <td>
                    
                  <? if($il->preschool==1) { ?> 
                    Preschool<br>
                  <? }
                  if($il->afterschool_activity==1) { ?>
                    Afterschool<br>
                  <? } 
                  if($il->teacher_training==1) { ?>
                    Teachers' Training
                  <? } ?> 
                    <br>
                    <a class="btn-green" href="<?=Yii::app()->request->baseUrl."/index.php/ilc/update/".$il->id ?>">Manage</a>
                </td>
                <td><b><?=$il->ins_name ?></b>
                    <br>
                    <p><?=$il->address ?>, <?=$il->city ?>, <?=$il->state ?>, <?=$il->country ?></p>
                </td>
                <td><?=$il->emails ?>
                    <br>
                <?=$il->phones ?></td>
                <td>
                    <? 
                    $prid=$il->partner_name;
                    $cp = new CDbCriteria();
                    $cp->condition='userid=:u';
                    $cp->params=array(':u'=>$prid);
                    $pname = Users::model()->find($cp); ?>
                    <?=$pname->name ?> <br>
                    <strong>Other Franchise &nbsp;-</strong>&nbsp;<?=$il->othf ?>
                </td>
                <td>
                    <?=$il->agreement_date ?><br>to<br>
                    <?=$il->agreement_valid ?><br> <br>
                    <? if($il->agreement!=''){ ?>
                    <a target="_blank" href="<?=  Yii::app()->request->baseUrl.'/agreement/'.$il->agreement ?>" class="btn-green">Download</a>
                    <? } ?>
                </td>
                <td>
                    <? if($il->junior_pres_prog!='' ){ ?>
                     <?=$il->junior_pres_prog ?><br>
                     valid from<br>
                     <?=$il->preschool_valid_from ?>
                    <? } else{ ?>
                     NIL
                    <? } ?>
                </td>
                <td>
                    <? if($il->senior_pres_prog!='' ){ ?>
                     <?=$il->senior_pres_prog ?><br>
                     valid from<br>
                     <?=$il->senior_pres_valid_from ?>
                    <? } else{ ?>
                     NIL
                    <? } ?>
                </td>
                <td>
                    <? if($il->teacher_training_prog!='' ){ ?>
                     <?=$il->teacher_training_prog ?><br>
                     valid from<br>
                     <?=$il->teacher_training_valid_from ?>
                    <? } else{ ?>
                     NIL
                    <? } ?>
                </td>
                
            </tr>
            <?
            }
            ?>
        </table>
        
    </div>
</div>
<script>
    function getres(){
    var st=$("#st option:selected").val();
    var ci=$("#ci option:selected").val();
    
    if(st=='0' && ci=='0'){
        alert("Select city");
    }
    
   
    else{
        //////////////////////ajax////////////////////////
        //alert("ilc="+il+" year="+yr+" month="+mon+" week="+wee);
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#enq").show().html('loading...........');
        },
        success:function(html){
        $("#enq").empty().append(html);
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getilccity"); ?>',
        data:{
          ci:ci,
        },
    dataType:"html",
    cache:false
})//ajax
    }
 }
function getcity(){
     var st1=$('#st option:selected').val();
     if(st1=='0')
     {
         alert('Select a state ');
     }
     else
     {
         //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#loadcity").show().html('loading...........');
        },
        success:function(html){
        $("#loadcity").empty().append(html);
     
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getcity"); ?>',
        data:{
          st1:st1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
</script>