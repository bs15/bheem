<?php

class UserphotoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','setnoty','checknum','nlist'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Userphoto;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Userphoto']))
		{
			$model->attributes=$_POST['Userphoto'];
                        
                        
                        /////////////////////////////////////////////////
                            $userid=$_POST['Userphoto']['userid'];
                          $Criteria1=new CDbCriteria();
                           $Criteria1->condition="userid=:uid";
                           $Criteria1->params=array(':uid'=>$userid);
                           $row=  Userphoto::model()->find($Criteria1);
                           
                            if(sizeof($row)==0)
                        {
                         //image upload
               $rnd = rand(0,9999);  // generate random number between 0-9999
              $uploadedFile=CUploadedFile::getInstance($model,'uphoto');
              
              if( is_object($uploadedFile) )
              {
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
            $model->uphoto = $fileName;
            $uploadedFile->saveAs(Yii::app()->basePath.'/../userphoto/'.$fileName);  // image will uplode to rootDirectory/banner/
            //end of image upload  
            
             //resize
            $name=Yii::app()->basePath.'/../userphoto/'.$fileName;
            	    $image = Yii::app()->image->load($name);
	    $image->resize(300,500);
	    $image->save();
                        }
                        }
                         else
                        {
                            $rid=$_POST['Userphoto']['userid'];
                            //$comp_id=$_POST['ProfPic']['comp_id'];
                           // $model->attributes=$_POST['ProfPic'];
                           $Criteria=new CDbCriteria();
                           $Criteria->condition="userid=:uid";
                           $Criteria->params=array(':uid'=>$rid);
                           $row= Userphoto::model()->find($Criteria);
                         //unlink image
                      // unlink(getcwd().'/userphoto/'.$row->uphoto);
                         Userphoto::model()->deleteAll('userid=:uid' , array(':uid'=>$rid));
                             //image upload
           $rnd = rand(0,9999);  // generate random number between 0-9999
              $uploadedFile=CUploadedFile::getInstance($model,'uphoto');
              
              if( is_object($uploadedFile) )
              {
            $fileName = "{$rnd}-{$uploadedFile}";  // random number + file name
            $model->uphoto = $fileName;
            $uploadedFile->saveAs(Yii::app()->basePath.'/../userphoto/'.$fileName);  // image will uplode to rootDirectory/banner/
            //end of image upload  
            
             //resize
            $name=Yii::app()->basePath.'/../userphoto/'.$fileName;
            $image = Yii::app()->image->load($name);
	    $image->resize(300, 500);
	    $image->save(); 
              }
                        }
                        //////////////////////////////////////////////////
                        
                        
			if($model->save())
                             $this->redirect(Yii::app()->baseUrl."/index.php/profile/");
				
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Userphoto']))
		{
			$model->attributes=$_POST['Userphoto'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Userphoto');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Userphoto('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Userphoto']))
			$model->attributes=$_GET['Userphoto'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Userphoto the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Userphoto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Userphoto $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='userphoto-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
}
