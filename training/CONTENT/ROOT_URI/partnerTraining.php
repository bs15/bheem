<link rel="stylesheet" href="/training/css/video.css" >
    <style type="text/css">
      .cGap{
        border-top: 1px solid #616C6F;
      }
      
    </style>
    <script
    src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js">
    </script>
   <!--  <script src="/training/js/pdf.min.js"></script>
    <script src="/training/js/pdf.js"></script>
 -->


<?php if ($_SESSION['LoggedIn'] && $_SESSION['role'] == 'partner'): ?>


<div class="container-fluid mt-5" style="background-color: #f8f9fa;">
<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#" style="margin-top: 60px;">
    <i class="fas fa-bars" ></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper" style="margin-top: 65px;">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <h3 style="color: #ffffff;" class="font-weight-bold">Bheem</h3>
        <div id="close-sidebar" class="crossSign" style="margin-left: 110px;">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">

        <div class="user-info">
         <h3>Partner Training</h3>
        </div>
      </div>
      <!-- sidebar-header  -->
      <div class="sidebar-search">

      </div>
      <!-- sidebar-search  -->
      <div class="sidebar-menu" style="margin-bottom: 40px; ">
        <ul>
          <li class="header-menu">
            <div>
               <span style="color: #ffffff;">Lessons</span>
            </div>
          </li>

          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">An ILC - A partners biggest USP!</span>

            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="showText('Your greatest USP. You\'d do well to internalize this!','The short explanation');">The short explanation
                  </a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/fr/ILC-ConceptExplainerInternal.mp4','The short ILC Explainer');">The short ILC Explainer</a>

                </li>
                <li>
                  <!-- https://cdn.preschool-curriculum.in/BeansTalk/videos/About_IIMTT/IIMTT%20explainer%20Video.mp4 -->
                  <a href="#" onclick="showAudio('/fr/aGuide.mp3','An 11 minute Guide to an ILC');">An 11 minute Guide to an ILC</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Partner, not Franchisor!</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="playVideo('/fr/ILCvsFranchising.mp4','Teeny beans vs Everyone');">Teeny beans vs Everyone</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/fr/PartnershipNotFranchisingVideo.mp4','Partner, Not Franchisor!');">Partner, Not Franchisor!</a>
                </li>
                <li>
                  <a href="#" onclick="showText('This is what we were talking about!<br><br><a href=https://teenybeans.in/preschool-franchise-vs-own-preschool-brand>https://teenybeans.in/preschool-franchise-vs-own-preschool-brand</a>','The Blog Article');">The Blog Article</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Decoding Your Financial Plan</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <!-- <a href="#" onclick="initPdf('/Lesson_plan/Basic-Course-Regular-Batch.pdf','Basic Course - Regular Batch');">Basic Course - Regular Batch</a> -->
                  <a href="#" onclick="showPdf('fr-mega-financial.pdf','Mega Tier A Financial Plan');">Mega Tier A Financial Plan</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/fr/Financial-n-BusinessPlanningTraining.mp4','Decoding Financial Metrices For Running A Successful Business');">Decoding Financial Metrices For Running A Successful Business</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Your ILC Design Guide</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <!-- <a href="#" onclick="initPdf('/Lesson_plan/Basic-Course-Regular-Batch.pdf','Basic Course - Regular Batch');">Basic Course - Regular Batch</a> -->
                  <a href="#" onclick="showPdf('fr-designguide.pdf','Design Specifications');">Design Specifications</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">

          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Knowledge Bank</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="showPdf('marketing-ilc.pdf','Marketing Your ILC');">Marketing Your ILC</a>
                </li>
                <li>
                  <a href="#" onclick="showText('Go through these useful e-courses.<br><br><a href=https://www.sba.gov/course/social-media-marketing>https://www.sba.gov/course/social-media-marketing</a><br><a href=https://www.sba.gov/course/marketing-101-guide-winning-customers/>https://www.sba.gov/course/marketing-101-guide-winning-customers/</a>','E-course for Marketing Your Business');">E-course for Marketing Your Business
                  </a>
                </li>
                <li>
                  <a href="#" onclick="showPdf('financial-acc.pdf','Financial Accounting Primar for small business');">Financial Accounting Primar for small business'
                  </a>
                </li>
                <li>
                  <a href="#" onclick="showText('Go through these financial accounting courses is very useful for entrepreneurs engaged in small business\'. Do go through this at leisure.<br><br><a href=https://alison.com/course/fundamentals-of-accounting-revised-2017>https://alison.com/course/fundamentals-of-accounting-revised-2017</a><br><a href=https://www.sba.gov/course/introduction-accounting>https://www.sba.gov/course/introduction-accounting</a>','E-course for Marketing Your Business');">E-course for Financial Accounting
                  </a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">

         


      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->

  </nav>
  <!-- sidebar-wrapper  -->
  <div class="container">
  <main class="page-content mx-auto text-center" style="height: auto;">
    

      <h2 id="title" class="font-weight-bold"></h2>
      <div class="card shadow" id="show" style="border:2px green solid; ">
       <h2 style="font-weight: bold;">Welcome to Partner Training Platform</h2><br>
      </div>
      <div id="pdfShow">
        
      </div>
  </main>
  </div>
  <!-- page-content" -->
</div>
</div>

<?php else: ?>
  <h2 style="text-align: center;">You are not authorized . Please signin first.</h2>

<?php endif; ?>

<script src="/training/js/video.js"></script>
<script src="/training/js/showVideos.js"></script>
<!-- <script src="/JS/pdf.min.js"></script>
<script src="/JS/pdf.js"></script> -->
<!-- <script src="/JS/pdfShow.js"></script> -->

    <script>
    document.addEventListener('contextmenu', event => event.preventDefault());
    // initPDFViewer("https://beanstalks3.s3.ap-south-1.amazonaws.com/Beanstalk-video/frp/Onboarding_Video/HandoverChecklist.pdf");
    // initPDFViewer("/JS/PDF-Wikipedia.pdf");
    </script>

</html>
<style type="text/css">
  #canvas_container {

  width: ;
  height: 450px;
  overflow: auto;
  text-align: center;
  }
  #canvas_container {
    background: #333;
    text-align: center;
    border: solid 3px;
  }
  #my_pdf_viewer{
    border: 3px solid black;
    display: inline-block;
    padding: 5px;
  }
  #zoom_controls{
    margin-top: 5px;
  }
  .btns{
    text-align: center;
    margin-top: 5px;
  }  

</style>
