<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $userid
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $role
 * @property integer $block
 * @property string $org
 * @property string $ilcid
 * @property string $schoolid
 * @property string $country
 * @property string $nationality
 * @property string $address
 * @property string $phone
 * @property string $photo
 */
class Users extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userid, name, username, password,pa, role, block, org, ilcid, schoolid, country, nationality, address, phone, photo', 'required'),
			array('id, block, pa', 'numerical', 'integerOnly'=>true),
			array('userid, ilcid, schoolid', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userid, name, username, password, role, block, org, ilcid, schoolid, country, nationality, address, phone, photo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'name' => 'Name',
			'username' => 'Username',
			'password' => 'Password',
                        'pa' => 'Pa',
			'role' => 'Role',
			'block' => 'Blocked',
			'org' => 'Org',
			'ilcid' => 'Ilcid',
			'schoolid' => 'Schoolid',
			'country' => 'Country',
			'nationality' => 'Nationality',
			'address' => 'Address',
			'phone' => 'Phone',
			'photo' => 'Photo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
                $criteria->compare('pa',$this->pa,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('block',$this->block);
		$criteria->compare('org',$this->org,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('schoolid',$this->schoolid,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('nationality',$this->nationality,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('photo',$this->photo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}