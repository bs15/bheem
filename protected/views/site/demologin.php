<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
//$this->breadcrumbs=array(
//	'Login',
//);
?>
<div class="row">
    <div class="col-md-7 col-lg-7 col-sm-12" style="height:90vh;margin:0 !important;padding:0 !important">
        <div class="bdyy">
            <h1 class="tit">Welcome to BeanStalk MIS</h1>
            <p>This MIS allows you to be a part of BeanStalk Network efficiently.All operations will be at your fingertips!</p>
        </div>   
    </div>
    
   <div class="col-md-5 col-lg-5 col-sm-12">   
       <br style="clear:both"><br style="clear:both"><br style="clear:both">
<div align="right">
<h3>Login</h3>
<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
        <p class="table-font">Please fill out the following form with your login credentials:<br>
        Fields with <span class="required">*</span> are required.</p>
    </div>
  
    <div class="col-md-12">
<div class="form fxform">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

   	<div class="row form-group input-group input-group-lg">
		<?php // echo $form->labelEx($model,'username',array("class"=>"input-group-addon")); ?>
		<?php echo $form->textField($model,'username',array("class"=>"ft","placeholder"=>" Username *")); ?>
		    <div class="row">
                <div class="col-md-12">
                    <?php echo $form->error($model,'username',array("class"=>"er")); ?>
                </div>
            </div>
	</div>

	<div class="row form-group input-group input-group-lg">
		<?php // echo $form->labelEx($model,'password',array("class"=>"input-group-addon")); ?>
		<?php echo $form->passwordField($model,'password',array("class"=>"ft","placeholder"=>" Password *")); ?>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $form->error($model,'password',array("class"=>"er")); ?>
                </div>
            </div>
		
	</div>


    <div class="row buttons" style="margin-left: 33%" align="center">
		<?php echo CHtml::submitButton('Login',array('class'=>'btn btn-ss btn-sm btn-small')); ?>
	</div>

<?php $this->endWidget(); ?>
    <br>
    <a class="forgot" href="<?=Yii::app()->request->baseUrl ?>/index.php/site/forgot">Forgot Password ?</a>
</div><!-- form -->
    </div>
</div>
</div>
   </div>   
</div>
<script>
 $(document).ready(function(){
 
    //checknoti();
       
  $.when(checknoti()).then(function(){ 
  });
});
        
            
 
   
    
</script>
