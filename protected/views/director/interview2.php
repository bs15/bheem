<div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/interview2">
                    <span style="color:blue">Interview Archive</span>
                </a>
            </span>
        </div> 
    </div>
<div class="row">
    <div class="col-md-12">
        <h1>INTERVIEW Archive</h1>
        <div class="row">
                  <div class="col-md-3">
        <select class="enin" id="yea" onchange="searchqmy()">
             <option value="0">Choose Year</option>
             <?
             for($i=2019;$i<=2040;$i++) {
                 ?>
             <option value="<?=$i ?>"><?=$i ?></option>
                     <?
             }
             ?>
        </select>
    <!--<input type="number" min="2019" max="<? //date('Y') ?>" placeholder="Enter year"  class="enin" id="yea" >-->
    </div>
    <div class="col-md-3">
        <select id="mon" class="enin" onchange="searchqmy()">
        <option value="0">Choose Month</option>
        <option value="1">Jan</option> 
        <option value="2">Feb</option> 
        <option value="3">Mar</option> 
        <option value="4">Apr</option> 
        <option value="5">May</option> 
        <option value="6">Jun</option> 
        <option value="7">Jul</option> 
        <option value="8">Aug</option> 
        <option value="9">Sep</option> 
        <option value="10">Oct</option> 
        <option value="11">Nov</option>
        <option value="12">Dec</option> 
    </select>
    </div>
    
  
    
    <div class="col-md-2">
    <button class="btn btn-sm btn-small btn-danger" onclick="reset()">Reset</button>
    </div>
    <div class="col-md-3" >
        <div id="err" style="color:red;font-size:14px;font-weight:500" ></div>
    </div>
</div>
            <div id="intlist2">
                
            </div>
       
    </div>
    
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Share your experience</h4>
      </div>
      <div class="modal-body" id="body1">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default " data-dismiss="modal" id="btn-close">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
    $("#qer").hide();
    //$("#intlist").show();
     // $("#chttext").hide();
   // $("#chbtn").hide();
    //$("#fchat").hide();
    //$("#createq").hide();
   // $("#chbtn").prop('disabled','true');
    
    getq();
   
/////////////////////////////////////////////
//setInterval(function(){
//    var qid=$("#qid").val();
//     var uid=$("#uuid").val();
//    if(qid!="-") {
//   getchat(qid,uid)  
//    }
//},10000);
///////////////////////////////////////////////
});

function showq(){
    if( $("#createq"))
    {
        $("#createq").show(); 
    }
    else{
    $("#createq").show(); 
    }
}
//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var f=$("#cfile").val();
      $("#fn").html(f);
}
////////////////////////////////////////////////////////////

function searchqmy() {
    var mon=$("#mon option:selected").val();
    var yea=$("#yea option:selected").val();
    // alert(yea+" "+mon);     
    if(mon=="0" && yea=="0")
     {
         $("#err").show().html("Choose Month and year");
     }
    else if(mon!="0" && yea=="0")
     {
         $("#err").show().html("Choose Year and then month");
     }
     else 
     {
      ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#intlist2").show().html('Loading..');
        },
                
        success:function(html){
     $("#intlist2").empty().append(html);
  //  document.getElementById('ilcfrm').reset();
   //  getqmy();
     
        //window.location.href=window.location.href;
        //}
        },
                 error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },      
        type:'post',
        url:'<? echo $this->createUrl("getimy"); ?>',
        data:{
          yea:yea,mon:mon,
        },
    dataType:"html",
    cache:false
})//ajax
///////////////////////////////////////////////////
}
}
function editfeed(id){
    
    //alert(id);
    $("#myModal").modal();
    revshowf();
    $("#revshow").hide();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
            
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     revshowf();
    $("#revshow").hide();
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("editfeed"); ?>',
        data:{
          id:id
        },
    dataType:"html",
    cache:false
})//ajax
}
function revshowf(){
 var rat=$("#rating option:selected").val();
 if(rat=="1" || rat=="2" || rat=="3"){
    $("#revshow").show(); 
    }
   else{
     $("#revshow").hide();   
   } 
}
function updatefeed(){
    var qid=$("#qqid").val();
    var ilcmid=$("#ilcmid").val();
    var rating=$("#rating").val();
    var review=$("#review").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     
   $("#btn-close").click();
     getq();
     alert("Your feedback has been submitted");
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
          qid:qid,ilcmid:ilcmid,rating:rating,review:review
        },
    dataType:"html",
    cache:false
})//ajax
}

function reset(){
    window.location.href=window.location.href;
}
function setinterview() {
    var file_data = $('#cfile').prop('files')[0];
    var datep= $('#datepicker').val();
    var f= $("#cfile").val();
    var h=$("#hour option:selected").val();
    var m=$("#min option:selected").val();
    var fname=$("#fullname").val();
    
     if(fname=="" || fname==null)
     {
         $("#err").show().html("Enter candidate name");
     }
     else if(datep=="" || datep==null)
     {
         $("#err").show().html("Enter date..");
     }
     else if(h=="" || h==null)
     {
        $("#err").show().html("Enter hour..");
     }
     else if(m=="" || m==null)
     {
         $("#err").show().html("Enter minuites..");
     }
     else if(f=="" || f==null)
     {
         $("#err").show().html("Enter file..");
     }
      else 
     {
         var form_data = new FormData();  
         form_data.append('fname', fname);
        form_data.append('file', file_data);
        form_data.append('datep', datep);
        form_data.append('hour', h);
        form_data.append('min', m);
    // alert(form_data);                             
    $.ajax({
        url: '<? echo $this->createUrl("schedulei"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Scheduling Interview...');
        },
        success: function(deer){
            $("#qer").show().html('Interview has been scheduled');
            $("#inttogg").click();
         //   alert(php_script_response); // display response from the PHP script, if any
           
   
        }
     });
                 
                 
                 ///////////////////////////////////////////////////
     }
}
function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#intlist2").show().html('Loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#intlist2").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("intlist2"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function closequery(qid) {
var y=confirm("Are you sure?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
   getq();
   
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("closequery"); ?>',
        data:{
          qid:qid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
function showchat(qid,uid) {
$("#qid").val(qid);
$("#uuid").val(uid);
  //$("#chbtn").prop('disabled','false');
  getchat(qid,uid);
    $("#chttext").show();
     $("#fchat").show();
     $("#query_id").val(qid);
    $("#chbtn").show();
    
      $('html, body').animate({
        scrollTop: $("#chatui").offset().top
    }, 2000);
}
function getchat(qid,uid) {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qchat").empty().append(html);
     var objDiv = document.getElementById("chatui");
    objDiv.scrollTop = objDiv.scrollHeight;
   
//      $('html, body').animate({
//        scrollTop: $("#qchat").offset().top
//    }, 2000);
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getchat"); ?>',
        data:{
          qid:qid,uid:uid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function sendchat() {
var qid=$("#qid").val();
var msg=$("#chttext").val();
if(msg=="") {
    alert("Enter message...");
}
else {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
   //  $("#frmc").trigger("reset");
  $("#chttext").val('');
  //  document.getElementById('ilcfrm').reset();
    // $("#qchat").empty().append(html);
  var uid=$("#uuid").val();
   getchat(qid,uid);
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("sendchat"); ?>',
        data:{
          qid:qid,msg:msg
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
</script>