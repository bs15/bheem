<div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/queryhistory">
                    <span style="color:blue">Query Archives</span>
                </a>
            </span>
        </div> 
    </div>
   
<div class="row">
    <div class="col-md-8">
     <h1>QUERY Archive</h1>
            <div class="row">
                  <div class="col-md-3">
        <select class="enin" id="yea" onchange="searchqmy()">
             <option value="0">Choose Year</option>
             <?
             for($i=2019;$i<=2040;$i++) {
                 ?>
             <option value="<?=$i ?>"><?=$i ?></option>
                     <?
             }
             ?>
        </select>
    <!--<input type="number" min="2019" max="<? //date('Y') ?>" placeholder="Enter year"  class="enin" id="yea" >-->
    </div>
    <div class="col-md-3">
        <select id="mon" class="enin" onchange="searchqmy()">
        <option value="0">Choose Month</option>
        <option value="1">Jan</option> 
        <option value="2">Feb</option> 
        <option value="3">Mar</option> 
        <option value="4">Apr</option> 
        <option value="5">May</option> 
        <option value="6">Jun</option> 
        <option value="7">Jul</option> 
        <option value="8">Aug</option> 
        <option value="9">Sep</option> 
        <option value="10">Oct</option> 
        <option value="11">Nov</option>
        <option value="12">Dec</option> 
    </select>
    </div>
    
  
    
    <div class="col-md-2">
    <button class="btn btn-sm btn-small btn-danger" onclick="reset()">Reset</button>
    </div>
    <div class="col-md-3" >
        <div id="err" style="color:red;font-size:14px;font-weight:500" ></div>
    </div>
</div>
    
            
            <div id="qer">
                
            </div>
       
    </div>
    <div class="col-md-4 col-sm-12">
        <div id="qchat">
            
        </div> 
        <div class="chtxt">
            <form id="frmc">
                <input type="hidden" id="qid" value="-" />
                   <input type="hidden" id="uuid" value="-" />
<!--                <div class="form-group">
                    <textarea class="form-control" id="chttext" placeholder="type message..."></textarea>
                </div>
                <div class="form-group">
                    <input type="button" id="chbtn" class="btn btn-outline-dark btn-sm" onclick="sendchat()" value="SEND" />
                </div>-->
            </form>
        </div>
    </div>
</div>





<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
   // $("#chbtn").prop('disabled','true');
    getq();

/////////////////////////////////////////////
setInterval(function(){
    var qid=$("#qid").val();
     var uid=$("#uuid").val();
    if(qid!="-") {
     getchat(qid,uid)  
    }
},200000);
///////////////////////////////////////////////
});
function searchqmy() {
    var mon=$("#mon option:selected").val();
    var yea=$("#yea option:selected").val();
    // alert(yea+" "+mon);     
    if(mon=="0" && yea=="0")
     {
         $("#err").show().html("Choose Month and year");
     }
    else if(mon!="0" && yea=="0")
     {
         $("#err").show().html("Choose Year and then month");
     }
     else 
     {
      ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Loading..');
        },
                
        success:function(html){
     $("#qer").empty().append(html);
  //  document.getElementById('ilcfrm').reset();
    // getqmy();
     
        //window.location.href=window.location.href;
        //}
        },
                 error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },      
        type:'post',
        url:'<? echo $this->createUrl("getqmy"); ?>',
        data:{
          yea:yea,mon:mon,
        },
    dataType:"html",
    cache:false
})//ajax
///////////////////////////////////////////////////
}
}

function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qer").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getquery3"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function showchat(qid,uid) {

$("#qid").val(qid);
$("#uuid").val(uid);
  //$("#chbtn").prop('disabled','false');
  getchat(qid,uid);
    $("#chttext").show();
    $("#fchat").show();
     $("#query_id").val(qid);
    $("#chbtn").show();
      $('html, body').animate({
        scrollTop: $("#qchat").offset().top
    }, 1000);

}
function getchat(qid,uid) {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qchat").empty().append(html);
   var objDiv = document.getElementById("chatui");
    objDiv.scrollTop = objDiv.scrollHeight;
    },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getchat"); ?>',
        data:{
          qid:qid,uid:uid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}


function reset() {
window.location.href=window.location.href;
}
</script>