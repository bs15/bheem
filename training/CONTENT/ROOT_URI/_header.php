<!DOCTYPE html>
<html>
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<!-- 	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script> -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.js"></script>

	<title>Bheem Training Module</title>
	<meta name="description" content="">
	<body>
		<?php if ($_SESSION['LoggedIn']) { ?>
			<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color: #fff; border-bottom: 1px solid #616C6F">
			  	<a class="navbar-brand" style="font-weight: bold;" href="#"><img src="/images/bheem/Bheem_logo_text.png" height="40" width="90"></a>
			  	<?php if ($_SESSION['role'] == 'partner'): ?>
			  		<ul class="navbar-nav mr-auto ">
				     	<li class="nav-item">
					        <a class="btn btn-success" href="partnerTraining" style="font-weight: bold; margin-right: 10px;">Partner Training</a>
					    </li>
					    <li class="nav-item">
					        <a class="btn btn-success" href="teacherTraining" style="font-weight: bold;">Teacher Training</a>
					    </li>
				  	</ul>
			  		<!-- <ul class="navbar-nav mr-auto">
					    <li class="nav-item">
					        <a class="btn btn-success" href="partnerTraining" style="font-weight: bold;">Teacher Training</a>
					    </li>
				  	</ul>
				  	<ul class="navbar-nav mr-auto">
				  		<li class="nav-item">
					        <a class="btn btn-success" href="teacherTraining" style="font-weight: bold;">Teacher Training</a>
					    </li>
				  	</ul> -->
			  	<?php endif ?>
			  	<ul class="navbar-nav ml-auto">
			  		<li class="nav-item">
				        <a class="btn btn-success" href="logout" style="font-weight: bold;">Logout</a>
				    </li>
			  	</ul>
			</nav>
		<? } ?>
		