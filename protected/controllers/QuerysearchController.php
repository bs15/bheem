<?php

class QuerysearchController extends Controller
{
    
      public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        
          public function accessRules()
                        	{

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid or role=:roleid1 or role=:roleid2 or role=:roleid3 or role=:roleid4 or role=:roleid5';
    	$criteria1->params = array(':rolid'=>'ilcmanager',':roleid1'=>'academic',':roleid2'=>'director',':roleid3'=>'partner',':roleid4'=>'unit',':roleid5'=>'teacher');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','ilcmanager','createilcm','getilcm','delilcm','searchbyilc'),
    		'users'=>$modad,
    	),
          array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('query','querylist','queryhistory','createquery','getquery','approvequery','showchat','sendchat'),
    		'users'=>$modad,
    	),
          array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('editquery','searchquery','searchchat','updatequery','getquery3','getchat','setnoty','checknum','nlist','manage','uploadpost'),
    		'users'=>$modad,
    	),
    	
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
       
	public function actionIndex()
	{
             $cr1=new CDbCriteria();
        //   $cr1->condition='ilcmid=:r ';
          //  $cr1->params=array(':r'=>$uid);
            $res1= Ilc::model()->findAll($cr1);
            
		$this->render('index',array('res'=>$res1));
	}
          public function actionSearchbyilc(){
              if(Yii::app()->request->isPostRequest) {
           $silc=trim($_POST['silc']);

       
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ilc_id=:s";
        $crq->params=array(':s'=>$silc);
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?>
            <p style="margin-top: 2%">No queries have been raised for this ILC</p>
                <?
            
        }
        else 
        {
           ?>
            <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Tags</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                
                ?>
                <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td><?=$q->tags ?></td>
    <td>    
        <?
        if($d>=24)
        {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <?
        }
        ?>
        <br>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active ":"Closed " ?></span>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
 <?
                
            }
            ?>
                </table>
            </div>
<?
          
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
        }
        
        public function actionQuery() {
            $model=new QueryChat();
            
            $uid=Yii::app()->user->getState('user_id');
           $cr1=new CDbCriteria();
           $cr1->condition='ilcmid=:r ';
            $cr1->params=array(':r'=>$uid);
            $res1= ManagerIlc::model()->findAll($cr1);
            
             $this->render('query',array('model'=>$model,'res'=>$res1));
            }
     //////////////////////upload post ///////////////////
        public function actionUploadpost() {
        $model = new QueryChat();
        $userid=Yii::app()->user->getState('user_id');
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
      //  $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
                $model->queryid=trim($_POST['qid']);
                $model -> document = $fileName;
	
                
                $model->chatid=  uniqid();
         $model->senderid=$userid;
         $model->recid='-';
         $model->message='-';
         $model->date1=  strtotime(date('Y-m-d H:i:s'));
         
	if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../chatdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
            }
        public function actionGetquery() {
            if(Yii::app()->request->isPostRequest) {
         
        $uid=Yii::app()->user->getState('user_id');
        $ilcs=array();        
        ///////////////////////////////////////
//        $crm=new CDbCriteria();
//        $crm->condition='ilcmid=:u';
//        $crm->params=array(":u"=>$uid);
//        $res=  ManagerIlc::model()->findAll($crm);
//        foreach ($res as $r) {
//            array_push($ilcs, $r->ilcid);
//        }
        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
//        $crq->condition="query_closed=0";
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
           ?>
            <p style="margin-top: -10%">No queries have been raised for this ILC</p>
                <?
        }
        else 
        {
            ?>
            <div class="table table-striped table-font" >   
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Tags</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
//                if(in_array($ilc, $ilcs))
//                {
                ?>
                    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td><?=$q->tags ?></td>
    <td>    
        <?
        if($d>=24)
        {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <?
        }
        ?>
        <br>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active ":"Closed " ?></span>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr> <?


        } ?>
                </table>
            </div>
       <?   
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
        
    }
        public function actionSearchquery() {
            if(Yii::app()->request->isPostRequest) {
           $skey=trim($_POST['skey']);

        $uid=Yii::app()->user->getState('user_id');
        $ilcs=array();     
        $qqq=array();
        
        $f=0;
        ///////////////////////////////////////
//        $crm=new CDbCriteria();
//        $crm->condition='ilcmid=:u';
//        $crm->params=array(":u"=>$uid);
//        $res=  ManagerIlc::model()->findAll($crm);
//        foreach ($res as $r) {
//            array_push($ilcs, $r->ilcid);
//        }
        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
       $crq->condition="query_topic like :q or query_topic like :qq or query_topic like :qqq";
       $crq->params=array(":q"=>$skey.'%',":qq"=>'%'.$skey.'%',":qqq"=>'%'.$skey);
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
          //  echo "No query raised";
            //////////////////////////////////////////////////tag search///////////////////////
            $qrt=new CDbCriteria();
            $qrt->condition="tag like :q or tag like :qq or tag like :qqq";
             $qrt->params=array(":q"=>$skey.'%',":qq"=>'%'.$skey.'%',":qqq"=>'%'.$skey);
             $qrt->group="queryid";
             $qts=  QueryTags::model()->findAll($qrt);
             $nts=  sizeof($qts);
             if($nts>0){
                 $f=1;
                 ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Tags</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
                 foreach ($qts as $qtt) {
                     $qid=$qtt->queryid;
                     $nqq=new CDbCriteria();
                     $nqq->condition="qid=:q";
                     $nqq->params=array(":q"=>$qid);
                     $q=  Query::model()->find($nqq);
                     ///////////////////query card////////////////////
                       $user=$q->userid;
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
//                if(in_array($ilc, $ilcs))
//                {
                ?>

                     <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td><?=$q->tags ?></td>
    <td>    
        <?
        if($d>=24)
        {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <?
        }
        ?>
        <br>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active ":"Closed " ?></span>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr> <?
                     /////////////////////////////////////////////
                 }
                 ?>
                </table>
            </div>
<?
             }
            ///////////////////////////////////////////////////////////////
        }
        else 
        {
            $f=1;
            ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Tags</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $qdate=$q->query_date;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
//                if(in_array($ilc, $ilcs))
//                {
                ?>
 <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td><?=$q->tags ?></td>
    <td>    
        <?
        if($d>=24)
        {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <?
        }
        ?>
        <br>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active ":"Closed " ?></span>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
    <?
     }
     ?>
                </table>
            </div>
     <?     
        }
        if($f==0){
            ?>
            <p style="margin-top: 0%">No queries have been raised with these tags</p>
                <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
        public function actionGetchat() {
            if(Yii::app()->request->isPostRequest) {
          $qid=trim($_POST['qid']);
          $uid=trim($_POST['uid']);

    $cr=new CDbCriteria();
    $cr->condition='queryid=:q';
    $cr->params=array(":q"=>$qid);
    $cr->order='date1 asc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       echo "no chats";
   }
 else {
     $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
    ?>
    
<p style="font-size:16px !important" >Topic: <?=$topic ?>  </p>
<div id="chatui">
     <?
       foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
     if($uinf->role=='unit' && $c->priv==0){
           ?>
                <div class="alert alert-success talk float-left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
          if($uinf->role=='partner' && $c->priv==0){
           ?>
                <div class="alert alert-warning talk float-left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='ilcmanager'){
             ?>
                <div class="alert alert-danger talk float-right" >
                    <?
                    if($c->priv==1)
                    {
                        ?><div class="alert alert-danger">PRIVATE</div>
                            <?
                    }
                    ?>
    <b style="font-size:14px !important"><?=$uinf->name ?>[&nbsp;<?=$uinf->role ?>&nbsp;]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
                 <?
         }
        } 
        ?>
</div>
    <?
   }
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		} 
}
        public function actionSearchchat() {
            if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $uid=trim($_POST['uid']);
           $skey=trim($_POST['skey']);


    $cr=new CDbCriteria();
    $cr->condition='queryid=:que and (message like :q or message like :qq or message like :qqq or document like :q or document like :qq or document like :qqq)';
    $cr->params=array(":que"=>$qid,":q"=>$skey.'%',":qq"=>'%'.$skey.'%',":qqq"=>'%'.$skey);
    $cr->order='date1 asc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       echo "no chats";
   }
 else {
     $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
    ?>
    
<p style="font-size:16px !important" >Topic: <?=$topic ?>  </p>
<div id="chatui">
     <?
       foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
     if($uinf->role=='unit' && $c->priv==0){
           ?>
                <div class="alert alert-success talk float-left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
          if($uinf->role=='partner' && $c->priv==0){
           ?>
                <div class="alert alert-warning talk float-left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='ilcmanager'){
             ?>
                <div class="alert alert-danger talk float-right" >
                    <?
                    if($c->priv==1)
                    {
                        ?><div class="alert alert-danger">PRIVATE</div>
                            <?
                    }
                    ?>
    <b style="font-size:14px !important"><?=$uinf->name ?>[&nbsp;<?=$uinf->role ?>&nbsp;]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
                 <?
         }
        } 
        ?>
</div>
    <?
   }
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
    
}

        public function actionSendchat() {
         if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $msg=trim($_POST['msg']);

   
    $uid=Yii::app()->user->getState("user_id");
  
        $cru=new CDbCriteria();
        $cru->condition='userid=:r';
        $cru->params=array(':r'=>$uid);
        $resu=  Users::model()->find($cru);
        $ilcid=$resu->ilcid;
        
        $crt=new CDbCriteria();
        $crt->condition='qid=:r';
        $crt->params=array(':r'=>$qid);
        $rest=  Query::model()->find($crt);
        $qtopic=$rest->query_topic;
  
    $qc=new QueryChat();
    $qc->queryid=$qid;
    $qc->chatid=  uniqid();
    $qc->senderid=$uid;
    $qc->recid='-';
    $qc->message=$msg;
    $qc->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qc->isNewRecord=true;
    $qc->save(FALSE);
    
    $mst="There is a reply for the query '".substr($qtopic, 0, 100)."..'";
    $qn=new Notifications();
    $qn->queryid=$qid;
    $qn->notid=  uniqid();
    $qn->role="all";
    $qn->ilc=$ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
} 

        public function actionGetquery3($deer) {
        $uid=Yii::app()->user->getState('user_id');
        $ilcs=array();        
        ///////////////////////////////////////
//        $crm=new CDbCriteria();
//        $crm->condition='ilcmid=:u';
//        $crm->params=array(":u"=>$uid);
//        $res=  ManagerIlc::model()->findAll($crm);
//        foreach ($res as $r) {
//            array_push($ilcs, $r->ilcid);
//        }
        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="ilc_approved=1 and query_closed=1";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
            ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Tags</td>
                        <td>Status</td>
                        <td>Chat</td>
                    </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
//                if(in_array($ilc, $ilcs))
//                {
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td><?=$q->tags ?></td>
    <td>    
        <?
        if($d>=24)
        {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <?
        }
        ?>
        <br>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active ":"Closed " ?></span>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
                    <?
//                }
            }
        ?>
                </table>
            <?  
        }
 }
 
    public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
            ///////////////////////////////////////////
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}