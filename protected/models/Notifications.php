<?php

/**
 * This is the model class for table "notifications".
 *
 * The followings are the available columns in table 'notifications':
 * @property string $id
 * @property string $notid
 * @property string $queryid
 * @property string $role
 * @property string $ilc
 * @property string $message
 * @property string $date1
 * @property integer $count1
 */
class Notifications extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Notifications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notifications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('notid, queryid, role, ilc, message, date1, count1', 'required'),
			array('count1', 'numerical', 'integerOnly'=>true),
			array('notid, queryid, ilc', 'length', 'max'=>100),
			array('date1', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, notid, queryid, role, ilc, message, date1, count1', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'notid' => 'Notid',
			'queryid' => 'Queryid',
			'role' => 'Role',
			'ilc' => 'Ilc',
			'message' => 'Message',
			'date1' => 'Date1',
			'count1' => 'Count1',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('notid',$this->notid,true);
		$criteria->compare('queryid',$this->queryid,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('ilc',$this->ilc,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('date1',$this->date1,true);
		$criteria->compare('count1',$this->count1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}