<?php

/**
 * This is the model class for table "calender_date".
 *
 * The followings are the available columns in table 'calender_date':
 * @property string $calid
 * @property string $startd
 * @property string $endd
 * @property string $months
 * @property string $topic
 * @property string $userid
 * @property string $queryid
 * @property integer $approved
 * @property integer $ilcmid
 * @property string $date1
 * @property string $year
 * @property string $mon
 */
class CalenderDate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CalenderDate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'calender_date';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('calid, startd, endd, months, topic, userid, queryid, approved, ilcmid, date1, year, mon', 'required'),
			array('id, approved, ilcmid', 'numerical', 'integerOnly'=>true),
			array('calid, userid, queryid', 'length', 'max'=>100),
			array('date1', 'length', 'max'=>20),
			array('year, mon', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, calid, startd, endd, months, topic, userid, queryid, approved, ilcmid, date1, year, mon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'calid' => 'Calid',
			'startd' => 'Startd',
			'endd' => 'Endd',
			'months' => 'Months',
			'topic' => 'Topic',
			'userid' => 'Userid',
			'queryid' => 'Queryid',
			'approved' => 'Approved',
			'ilcmid' => 'Ilcmid',
			'date1' => 'Date1',
			'year' => 'Year',
			'mon' => 'Mon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('calid',$this->calid,true);
		$criteria->compare('startd',$this->startd,true);
		$criteria->compare('endd',$this->endd,true);
		$criteria->compare('months',$this->months,true);
		$criteria->compare('topic',$this->topic,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('queryid',$this->queryid,true);
		$criteria->compare('approved',$this->approved);
		$criteria->compare('ilcmid',$this->ilcmid);
		$criteria->compare('date1',$this->date1,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('mon',$this->mon,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}