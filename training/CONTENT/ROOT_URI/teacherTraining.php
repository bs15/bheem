<link rel="stylesheet" href="/training/css/video.css" >
    <style type="text/css">
      .cGap{
        border-top: 1px solid #616C6F;
      }
      
    </style>
    <script
    src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js">
    </script>
   <!--  <script src="/training/js/pdf.min.js"></script>
    <script src="/training/js/pdf.js"></script>
 -->


<?php if ($_SESSION['LoggedIn']): ?>


<div class="container-fluid mt-5" style="background-color: #f8f9fa;">
<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#" style="margin-top: 60px;">
    <i class="fas fa-bars" ></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper" style="margin-top: 65px;">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <h3 style="color: #ffffff;" class="font-weight-bold">Bheem</h3>
        <div id="close-sidebar" class="crossSign" style="margin-left: 110px;">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">

        <div class="user-info">
         <h3>Teacher Training</h3>
        </div>
      </div>
      <!-- sidebar-header  -->
      <div class="sidebar-search">

      </div>
      <!-- sidebar-search  -->
      <div class="sidebar-menu" style="margin-bottom: 40px; ">
        <ul>
          <li class="header-menu">
            <div>
               <span style="color: #ffffff;">Lessons</span>
            </div>
          </li>

          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">An Introduction to Teeny Beans and the ILC Approach</span>

            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="showAudio('/teacher/anIntroToILC.mp3','An Introduction');">An Introduction
                  </a>
                </li>
                <li>
                  <a href="#" onclick="showText('An ILC is an institution carrying expertise in early childhood education from birth till preteens. It correctly identifies and administers age appropriate programs for kids aged 6m-9 yrs.<br>For the owners of an ILC, besides the comfort of having invested into a well-diversified business model with muliple value generating streams, it is the goodwill earned in running a highly respectable early learning centre, that strongly differentiates an ILC as a proposition.<br><br><b>AN INTERNATIONAL PRESCHOOL</b><br>A preschool following an internationally recognized curriculum following UK\'s EYFS curriculum guidelines. Lays the foundation stone for early learning with several integrated programs that continue into a child\'s formative years.<br><br><b>TEACHER TRAINING</b><br>A study centre for International Institute of Montessori Teacher Training offering Regular Diploma and Advanced Diploma is Montessori Teaching Practice.<br><br> <b>AFTER-SCHOOL ACTIVITY CENTRE</b><br>Beanstalk 3P, Writo, MaxBrain Abacus, Super Phonics & Cambridge Young Learners English- Programs for all ages and seamlessly integrated with the preschool program.','What is an Integrated Learning Center(ILC)?');">What is an Integrated Learning Center(ILC)?</a>

                </li>
                <li>
                  <!-- https://cdn.preschool-curriculum.in/BeansTalk/videos/About_IIMTT/IIMTT%20explainer%20Video.mp4 -->
                  <a href="#" onclick="playVideo('/teacher/Explainer-ILC-for-Parents.mp4','ILC from a parents perspective');">ILC from a parents perspective</a>
                </li>
                <li>
                  <a href="#" onclick="showHTML('/training/js/more_than_pre.html','So much more than just a preschool!');">So much more than just a preschool!</a>

                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Core Curriculum</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="playVideo('/teacher/CurriculumPrimerVideo.mp4','Core Curriculum Concept');">Core Curriculum Concept</a>
                </li>
                <li>
                  <a href="#" onclick="showPdf('CurriculumPrimer.pdf','Curriculum Presentation');">Curriculum Presentation</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/EYFS-how-it-started.mp4','EYFS - how it started');">EYFS - how it started</a>
                </li>
                 <li>
                  <a href="#" onclick="showPdf('EYFS_Parents.pdf','EYFS from a parents perspective');">EYFS from a parents perspective</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/EYFS-Areas-of-DevelopmentEXPLAINED.mp4','Areas of Development Video');">Areas of Development Video</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Super Phonics Primer</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <!-- <a href="#" onclick="initPdf('/Lesson_plan/Basic-Course-Regular-Batch.pdf','Basic Course - Regular Batch');">Basic Course - Regular Batch</a> -->
                  <a href="#" onclick="showHTML('/training/js/superPhonics.html','Super Phonics');">Super Phonics</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/SuperPhonicsPrimerVideo.mp4','Super Phonics Primar Video');">Super Phonics Primar Video</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Writo Primer</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <!-- <a href="#" onclick="initPdf('/Lesson_plan/Basic-Course-Regular-Batch.pdf','Basic Course - Regular Batch');">Basic Course - Regular Batch</a> -->
                  <a href="#" onclick="showPdf('writoWithTeen.pdf','Writo with Teeny Beans - The model and the Science');">Writo with Teeny Beans - The model and the Science</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/WritoTrainingProgram.mp4','Writo Training Video');">Writo Training Video</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/Writo-in-class-demonstration.mp4','Writo - in class demonstration');">Writo - in class demonstration</a>
                </li>
                <li>
                  <a href="#" onclick="showAudio('/teacher/pencilGripSong.mp3','The pencil grip song');">The pencil grip song</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/Pencil-grip-song.mp4','The penclil grip song in action in kindergarten');">The penclil grip song in action in kindergarten</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/The-Method-Explained.mp4','The-Method-Video');">The-Method-Video</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/CapitalLettersDemonstration.mp4','Capital Letter Demonstration');">Capital Letter Demonstration</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/The-numeral-song.mp4','The numeral song - The Directed way of learning');">The numeral song - The Directed way of learning</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/Basic-Lessons.mp4','Basic Lessons in Handwriting - Finger Exercise, Paper Positioning and The pencil Grip');">Basic Lessons in Handwriting - Finger Exercise, Paper Positioning and The pencil Grip</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/Handwriting-Disorder-and-Remedy.mp4','Handwriting Disorder and Remedial Strategies');">Handwriting Disorder and Remedial Strategies</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">MiniMax Primar</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="playVideo('/teacher/Minimax-Abacus-InClass.mp4','Minimax in class');">Minimax in class</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/BrainDevelopmentResearch.mp4','Research in brain Development in Early Childhood');">Research in brain Development in Early Childhood</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">

          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Toddlers</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="showPdf('motherTodd.pdf','Presentation on Mother toddler Program');">Presentation on Mother toddler Program</a>
                </li>
               <li>
                  <a href="#" onclick="playVideo('/teacher/MTP-Important-Things-to-Ponder.mp4','Considerations for a mother toddler program');">Considerations for a mother toddler program</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Bonjour & Yoga</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="playVideo('/teacher/Yoga-In-Class.mp4','Kids Yoga in Class');">Kids Yoga in Class</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/why_teaching_your_child_a_foreign_language_is_important.mp4','Why teaching your kids a foreign language is important');">Why teaching your kids a foreign language is important</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/The-benefits-of-a-bilingual-brain-MiaNacamulli.mp4','benefits of a multi-lingual brain');">benefits of a multi-lingual brain</a>
                </li>
               <li>
                  <a href="#" onclick="playVideo('/teacher/the_linguistic_genius_of_babies_patricia_kuhl.mp4','Research on the linguistic genius of babies');">Research on the linguistic genius of babies</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Using Various Teacher Resources and Learning Aids</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="playVideo('/teacher/TrainingProgram.mp4','Using Teacher Resources and Learning Aids');">Using Teacher Resources and Learning Aids</a>
                </li>
               <li>
                  <a href="#" onclick="playVideo('/teacher/UsingMontessoriEquipments.mp4','Using Montessori Resources');">Using Montessori Resources</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">General Administrative Guidelines</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="playVideo('/teacher/GeneralAdministrativeGuidelines.mp4','Admin Guidelines');">Admin Guidelines</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">
         <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-circle"></i>
              <span style="">Explainer Videos</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#" onclick="playVideo('/teacher/ILC-Explainer-Generic.mp4','Your Institues ILC Video');">Your Institues ILC Video</a>
                </li>
                <li>
                  <a href="#" onclick="playVideo('/teacher/Beanstalk-3P.mp4','Beanstalk 3P');">Beanstalk 3P</a>
                </li>
              </ul>
            </div>
          </li><hr class="cGap">


      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->

  </nav>
  <!-- sidebar-wrapper  -->
  <div class="container">
  <main class="page-content mx-auto text-center" style="height: auto;">
    

      <h2 id="title" class="font-weight-bold"></h2>
      <div class="card shadow" id="show" style="border:2px green solid; ">
       <h2 style="font-weight: bold;">Welcome to Teacher Training Platform</h2><br>
      </div>
      <div id="pdfShow">
        
      </div>
  </main>
  </div>
  <!-- page-content" -->
</div>
</div>

<?php else: ?>
  <h2 style="text-align: center;">You are not authorized . Please <a href="/training/login">Sign In</a> first.</h2>

<?php endif; ?>

<script src="/training/js/video.js"></script>
<script src="/training/js/showVideos.js"></script>
<!-- <script src="/JS/pdf.min.js"></script>
<script src="/JS/pdf.js"></script> -->
<!-- <script src="/JS/pdfShow.js"></script> -->

    <script>
    document.addEventListener('contextmenu', event => event.preventDefault());
    // initPDFViewer("https://beanstalks3.s3.ap-south-1.amazonaws.com/Beanstalk-video/frp/Onboarding_Video/HandoverChecklist.pdf");
    // initPDFViewer("/JS/PDF-Wikipedia.pdf");
    </script>

</html>
<style type="text/css">
  #canvas_container {

  width: ;
  height: 450px;
  overflow: auto;
  text-align: center;
  }
  #canvas_container {
    background: #333;
    text-align: center;
    border: solid 3px;
  }
  #my_pdf_viewer{
    border: 3px solid black;
    display: inline-block;
    padding: 5px;
  }
  #zoom_controls{
    margin-top: 5px;
  }
  .btns{
    text-align: center;
    margin-top: 5px;
  }  

</style>
