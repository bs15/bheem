<?php

/**
 * This is the model class for table "admission".
 *
 * The followings are the available columns in table 'admission':
 * @property string $adid
 * @property string $parentid
 * @property string $enqid
 * @property string $ilcid
 * @property string $userid
 * @property string $m_tongue
 * @property string $recno
 * @property string $recdate
 * @property string $enrno
 * @property string $enrdate
 * @property string $photo
 * @property string $recphoto
 * @property string $adtype
 * @property string $vaccine
 * @property string $allergy
 * @property string $blood
 * @property string $other_med
 * @property string $date1
 * @property string $child_name
 * @property string $f_name
 * @property string $m_name
 * @property string $m_occupation
 * @property string $f_ofc_add
 * @property string $m_ofc_add
 * @property string $c_dob
 * @property integer $aclosed
 * @property string $ilcfeed
 * @property integer $month
 * @property integer $year
 */
class Admission extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Admission the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admission';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('adid, parentid, enqid, ilcid, userid, m_tongue, recno, recdate, enrno, enrdate, photo, recphoto, adtype, vaccine, allergy, blood, other_med, date1, child_name, f_name, m_name, m_occupation, f_ofc_add, m_ofc_add, c_dob, aclosed, ilcfeed, month, year, clodate,state,city', 'required'),
			array('id,aclosed,week, month, year, esc', 'numerical', 'integerOnly'=>true),
			array('adid, parentid, enqid, ilcid, userid', 'length', 'max'=>100),
			array('blood', 'length', 'max'=>10),
			array('date1', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, adid, parentid, enqid, ilcid, userid, m_tongue, recno, recdate, enrno, enrdate, photo, recphoto, adtype, vaccine, allergy, blood, other_med, date1, child_name, f_name, m_name, m_occupation, f_ofc_add, m_ofc_add, c_dob, aclosed, ilcfeed, week, month, year, esc, clodate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'adid' => 'Adid',
			'parentid' => 'Parentid',
			'enqid' => 'Enqid',
			'ilcid' => 'Ilcid',
			'userid' => 'Userid',
			'm_tongue' => 'M Tongue',
			'recno' => 'Recno',
			'recdate' => 'Recdate',
			'enrno' => 'Enrno',
			'enrdate' => 'Enrdate',
			'photo' => 'Photo',
			'recphoto' => 'Recphoto',
			'adtype' => 'Adtype',
			'vaccine' => 'Vaccine',
			'allergy' => 'Allergy',
			'blood' => 'Blood',
			'other_med' => 'Other Med',
			'date1' => 'Date1',
			'child_name' => 'Child Name',
			'f_name' => 'F Name',
			'm_name' => 'M Name',
			'm_occupation' => 'M Occupation',
			'f_ofc_add' => 'F Ofc Add',
			'm_ofc_add' => 'M Ofc Add',
			'c_dob' => 'C Dob',
			'aclosed' => 'Aclosed',
			'ilcfeed' => 'Ilcfeed',
                        'week' => 'Week',
			'month' => 'Month',
			'year' => 'Year',
                        'esc' => 'Esc',
                        'clodate' => 'Closed Date',
                        'state' => 'State',
                        'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('adid',$this->adid,true);
		$criteria->compare('parentid',$this->parentid,true);
		$criteria->compare('enqid',$this->enqid,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('m_tongue',$this->m_tongue,true);
		$criteria->compare('recno',$this->recno,true);
		$criteria->compare('recdate',$this->recdate,true);
		$criteria->compare('enrno',$this->enrno,true);
		$criteria->compare('enrdate',$this->enrdate,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('recphoto',$this->recphoto,true);
		$criteria->compare('adtype',$this->adtype,true);
		$criteria->compare('vaccine',$this->vaccine,true);
		$criteria->compare('allergy',$this->allergy,true);
		$criteria->compare('blood',$this->blood,true);
		$criteria->compare('other_med',$this->other_med,true);
		$criteria->compare('date1',$this->date1,true);
		$criteria->compare('child_name',$this->child_name,true);
		$criteria->compare('f_name',$this->f_name,true);
		$criteria->compare('m_name',$this->m_name,true);
		$criteria->compare('m_occupation',$this->m_occupation,true);
		$criteria->compare('f_ofc_add',$this->f_ofc_add,true);
		$criteria->compare('m_ofc_add',$this->m_ofc_add,true);
		$criteria->compare('c_dob',$this->c_dob,true);
		$criteria->compare('aclosed',$this->aclosed);
		$criteria->compare('ilcfeed',$this->ilcfeed,true);
                $criteria->compare('week',$this->week,true);
		$criteria->compare('month',$this->month);
		$criteria->compare('year',$this->year);
                $criteria->compare('esc',$this->esc);
                $criteria->compare('clodate',$this->clodate);
                $criteria->compare('state',$this->state);
                $criteria->compare('city',$this->city);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}