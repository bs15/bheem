<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bheem - MIS Portal</title>
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"> -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

    <!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.js"></script> -->

  <style type="text/css">
   body{
    background: #CCCCB2;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #757519, #CCCCB2);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #757519, #CCCCB2); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
   }
   .sliderImg{
    width: 80%;
    height: 600px;
    margin-left: auto;
    margin-right: auto;
   }
  .foot{
    background: #616C6F;
    padding: 10px;
    color: #fff;
  }
  .head{
    display: block;
    border: 2px black solid;
    padding: 10px;
    text-align: center;
    background: #000000;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #434343, #000000);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #434343, #000000); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    border-radius: 80px;
    font-weight: bold;
    color: #fff;
  }
  .aboutS{
    background: #141E30;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #243B55, #141E30);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #243B55, #141E30); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

    color: #fff;
  }
    
    /*.imageG{
      margin-top: 20px;
    }*/
  </style>
</head>
<body>
<script type="text/javascript">
  // $(document).ready(function(){ 
  //   $('.goDown').click(function(){
  //     $("html, body").animate({ scrollTop: $(document).height() }, 2000);
  //   });
  //    $(window).scroll(function() {
  //         // checks if window is scrolled more than 500px, adds/removes solid class
  //         if($(this).scrollTop() > 150) { 
  //           // console.log("hello");
  //             $('.navbar').addClass('solid');
  //             $("#logo").css("display", "inline");
  //         } else {
  //             $('.navbar').removeClass('solid');
  //             $("#logo").css("display", "none");
  //         }
  //       });
  // });
</script>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="/">
      <img src="<?=Yii::app()->request->baseUrl ?>/images/bheem/Bheem_logo_text.png" width="90" height="50" alt="Bheem Logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" style="background: green;" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars" style="color: #ffffff; font-size: 30px;"></i>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
            <li class="nav-item" style="margin-right: 5px;">
              <a class="btn btn-light border-dark" href="login">Login</a>
            </li>
            <li class="nav-item">
              <a class="btn btn-dark border-dark" href="#about">About</a>
            </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container my-4">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block shadow shadow-lg sliderImg" src="<?=Yii::app()->request->baseUrl ?>/images/bheem/Bheem1-min.jpeg" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block shadow shadow-lg sliderImg" src="<?=Yii::app()->request->baseUrl ?>/images/bheem/Bheem2-min.jpeg" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block shadow shadow-lg sliderImg" src="<?=Yii::app()->request->baseUrl ?>/images/bheem/Bheem3-min.jpeg" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block shadow shadow-lg sliderImg" src="<?=Yii::app()->request->baseUrl ?>/images/bheem/Bheem4-min.jpeg" alt="Third slide">
    </div>
    <div class="carousel-item">
      <img class="d-block shadow shadow-lg sliderImg" src="<?=Yii::app()->request->baseUrl ?>/images/bheem/Bheem5-min.jpeg" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

 

    

  
<!-- Page Content -->
<section class="py-5 aboutS">
  <div class="container" id="about">
    <h1 class="head text-center">About Us</h1>
    <div class="col-md-12 col-sm-12 ml-auto mr-auto pt-3">
      <p class="lead text-justify" style="font-size: 24px;">Welcome to Bheem - a one stop solution platform for Teeny Beans partners from all across the globe. Bheem which owes it's etymological roots to Mahabharata, one of the two epic poems of ancient India, is a name that signifies strength of gigantic proportions. Through Bheem we intend to empower our partners to succeed in all aspects of centre management. 
      Bheem provides a full fledged training curriculum for Partners, centre coordinators and teachers. It provides an advanced ticket based query handling system that enables centres to always stay on top of the affairs at their centre by providing a platform for raising and resolving queries quickly and efficiently.
      Bheem also provides an enquiry management and admission management system that is well synchronized with Teeny Bean's Mobile App Sparsh. Partners can also use Bheem to manage their inventory and for order fulfillment using our payments module.</p>
    </div>
  </div>
</section>
<footer class="page-footer foot font-small">
  <div class="container-fluid">
    <strong>Copyright &copy; 2020-2021 <a href="https://beanstalkedu.com/">Beanstalkedu Pvt Ltd.</a>.
    All rights reserved. <a href="https://drive.google.com/file/d/1n8Lc5-OIXZHZjVBdy_EoUNJI52e9e0oF/view">Terms &amp; Conditions</a> </strong>
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 2.0.0
    </div>
  </div>
</footer>
</body>
</html>

<script type="text/javascript">
    
  </script>