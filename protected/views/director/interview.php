<?
$criteria1 = new CDbCriteria();
$criteria1->select = 'state';
$criteria1->group="state";
$states = Ilc::model()->findAll($criteria1);
            
            
?>
<div class="row">
   <div class="col-md-12" >
       <br>
       <span class="bread">
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
           Escalations > 
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/interview">
               <span style="color:blue">Interview Escalations</span>
           </a>
       </span>
    </div> 
</div>
<div class="row">
    <div class="col-md-12">
        
            <?
                $user=Yii::app()->user->getState('user_id');
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc= $uinfo->ilcid;
                
                $cru1=new CDbCriteria();
                $cru1->condition='ilcid=:u';
                $cru1->params=array(":u"=>$ilc);
                $ilcn=  Ilc::model()->find($cru1);
                $iname=$ilcn->ins_name;
                
                $cru2=new CDbCriteria();
                $cru2->condition='ilcid=:u';
                $cru2->params=array(":u"=>$ilc);
                $ilcm= ManagerIlc::model()->find($cru2);
                $mname=$ilcm->managername;
            ?>
        
       <div class="col-md-12">
            <h1 >Escalated INTERVIEWS</h1>
            <div class="row">
            <div class="col-md-3 col-sm-12">
                <input type="hidden" id="silc2" value="-">
                <select id="st" class="enin" onchange="getcity()" style="margin-left:1%">
                    <option value="0">Select State</option>
                    <? foreach($states as $st){
                        ?>
                        <option value="<?=$st->state ?>"><?=$st->state ?></option>
                        <?
                    } ?>
                </select>
            </div>
            <div class="col-md-3 col-sm-12" id="loadcity"></div>

            <div class="col-md-3 col-sm-12" id="loadilc"></div>
        </div>
           
            <div id="qer">
                
            </div>
            
        </div>
    </div>
    
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left">Appoint NEW Manager</h4>
        <button type="button" class="close" id="btn-close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="body1">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
    $("#qer").hide();
    //$("#intlist").show();
     // $("#chttext").hide();
   // $("#chbtn").hide();
    //$("#fchat").hide();
    //$("#createq").hide();
   // $("#chbtn").prop('disabled','true');
    
    getq();
    $("#datepicker").datepicker({
  dateFormat: "yy-mm-dd"
});




/////////////////////////////////////////////
//setInterval(function(){
//    var qid=$("#qid").val();
//     var uid=$("#uuid").val();
//    if(qid!="-") {
//   getchat(qid,uid)  
//    }
//},10000);
///////////////////////////////////////////////
});
function getilc(){
     var ci1=$('#ci option:selected').val();
     if(ci1=='0')
     {
         alert('Select a city ');
     }
     else
     {
        //////////////////////ajax////////////////////////
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#loadilc").show().html('loading...........');
        },
        success:function(html){
        $("#loadilc").empty().append(html);
        getres();
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getilc"); ?>',
        data:{
          ci1:ci1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
 
function getcity(){
     var st1=$('#st option:selected').val();
     if(st1=='0')
     {
         alert('Select a state ');
     }
     else
     {
         //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#loadcity").show().html('loading...........');
        },
        success:function(html){
        $("#loadcity").empty().append(html);
       getres();
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getcity"); ?>',
        data:{
          st1:st1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
 function getres(){
    var il=$("#il option:selected").val();
    var st=$("#st option:selected").val();
    var ci=$("#ci option:selected").val();
    
    if(il=='0' && st=='0' && ci=='0'){
        alert("Select ILC");
    }
    if(il==undefined){
        il='0';
    }
    if(ci==undefined){
        ci='0';
    } 
    //alert(ci+" "+st+" "+il);
    //alert("hii");
   
    else{
        //////////////////////ajax////////////////////////
        //alert("ilc="+il+" year="+yr+" month="+mon+" week="+wee);
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#qer").show().html('loading...........');
        },
        success:function(html){
        $("#qer").empty().append(html);
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getintilc"); ?>',
        data:{
          ci:ci,st:st,il:il
        },
    dataType:"html",
    cache:false
})//ajax
    }
 }
function searchbyilc(){
    var silc=$("#silc2").val();
     //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
    $("#qer").empty().append(html);
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("searchbyilc1"); ?>',
        data:{
          silc:silc
        },
    dataType:"html",
    cache:false
})//ajax
}

function suggestilc(){
    var silc1=$("#silc1").val();
    
     //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#ilclist1").show().html('loading...........');
        },
                
        success:function(html){
    $("#ilclist1").empty().append(html);
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("suggestilc1"); ?>',
        data:{
          silc1:silc1
        },
    dataType:"html",
    cache:false
})//ajax
}

function setilc1(id1){
var ids=id1.split("-");
$("#silc1").val(ids[1]);
$("#silc2").val(ids[0]);
$("#ilclist1").html('');

searchbyilc();
}
function showq(){
    if( $("#createq"))
    {
        $("#createq").show(); 
    }
    else{
    $("#createq").show(); 
    }
}
//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var f=$("#cfile").val();
      $("#fn").html(f);
}
////////////////////////////////////////////////////////////
function editfeed(id){
    
    //alert(id);
    $("#myModal").modal();
    revshowf();
    $("#revshow").hide();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
            
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     revshowf();
    $("#revshow").hide();
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("editfeed"); ?>',
        data:{
          id:id
        },
    dataType:"html",
    cache:false
})//ajax
}
function revshowf(){
 var rat=$("#rating option:selected").val();
 if(rat=="1" || rat=="2" || rat=="3"){
    $("#revshow").show(); 
    }
   else{
     $("#revshow").hide();   
   } 
}
function updatefeed(){
    var qid=$("#qqid").val();
    var ilcmid=$("#ilcmid").val();
    var rating=$("#rating").val();
    var review=$("#review").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     
   $("#btn-close").click();
     getq();
     alert("Your feedback has been submitted");
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
          qid:qid,ilcmid:ilcmid,rating:rating,review:review
        },
    dataType:"html",
    cache:false
})//ajax
}
function setinterview() {
    var file_data = $('#cfile').prop('files')[0];
    var datep= $('#datepicker').val();
    var f= $("#cfile").val();
    var h=$("#hour option:selected").val();
    var m=$("#min option:selected").val();
    var fname=$("#fullname").val();
    
     if(fname=="" || fname==null)
     {
         $("#err").show().html("Enter candidate name");
     }
     else if(datep=="" || datep==null)
     {
         $("#err").show().html("Enter date..");
     }
     else if(h=="" || h==null)
     {
        $("#err").show().html("Enter hour..");
     }
     else if(m=="" || m==null)
     {
         $("#err").show().html("Enter minuites..");
     }
     else if(f=="" || f==null)
     {
         $("#err").show().html("Enter file..");
     }
      else 
     {
         var form_data = new FormData();  
         form_data.append('fname', fname);
        form_data.append('file', file_data);
        form_data.append('datep', datep);
        form_data.append('hour', h);
        form_data.append('min', m);
    // alert(form_data);                             
    $.ajax({
        url: '<? echo $this->createUrl("schedulei"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Scheduling Interview...');
        },
        success: function(deer){
            $("#qer").show().html('Interview has been scheduled');
            $("#inttogg").click();
         //   alert(php_script_response); // display response from the PHP script, if any
           
   
        }
     });
                 
                 
                 ///////////////////////////////////////////////////
     }
}
function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qer").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("intlist1"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function approveint(intid) {
var y=confirm("Do you want to accept the candidate?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
        getq();
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },    
        type:'post',
        url:'<? echo $this->createUrl("approveint"); ?>',
        data:{
          intid:intid
        },
    dataType:"html",
    cache:false
    })//ajax
    }
}
function rescheduleint(intid){
    $("#myModal").modal();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd" ,  minDate: 0 , container: "#myModal"
        });  
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("rescheduleint"); ?>',
        data:{
          intid:intid
        },
    dataType:"html",
    cache:false
})//ajax
}
function manageint(intid){
    $("#myModal").modal();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd" ,  minDate: 0 , container: "#myModal"
        });  
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("manageint"); ?>',
        data:{
          intid:intid
        },
    dataType:"html",
    cache:false
})//ajax
}
function updateint(){
    var intid=$("#qqid").val();
    var idate=$("#datepicker").val();
    var ihour=$("#hour option:selected").val();
    var imin=$("#min option:selected").val();
    var man=$("#man option:selected").val();
    var ac=$("#academiccheck").is(":checked");
    if(ac>0)
    {
        var ac1=1;
    }
    else
    {
        var ac1=0;
    }
    
   
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
   $("#btn-close").click();
     getq();
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updateint"); ?>',
        data:{
          intid:intid,idate:idate,ihour:ihour,imin:imin,man:man,ac1:ac1,
        },
    dataType:"html",
    cache:false
})//ajax
}
function showchat(qid,uid) {
$("#qid").val(qid);
$("#uuid").val(uid);
  //$("#chbtn").prop('disabled','false');
  getchat(qid,uid);
    $("#chttext").show();
     $("#fchat").show();
     $("#query_id").val(qid);
    $("#chbtn").show();
    
      $('html, body').animate({
        scrollTop: $("#chatui").offset().top
    }, 2000);
}
function getchat(qid,uid) {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qchat").empty().append(html);
     var objDiv = document.getElementById("chatui");
    objDiv.scrollTop = objDiv.scrollHeight;
   
//      $('html, body').animate({
//        scrollTop: $("#qchat").offset().top
//    }, 2000);
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getchat"); ?>',
        data:{
          qid:qid,uid:uid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function sendchat() {
var qid=$("#qid").val();
var msg=$("#chttext").val();
if(msg=="") {
    alert("Enter message...");
}
else {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
   //  $("#frmc").trigger("reset");
  $("#chttext").val('');
  //  document.getElementById('ilcfrm').reset();
    // $("#qchat").empty().append(html);
  var uid=$("#uuid").val();
   getchat(qid,uid);
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("sendchat"); ?>',
        data:{
          qid:qid,msg:msg
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
</script>