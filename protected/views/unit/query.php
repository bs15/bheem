<div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Task Scheduler > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/query">
                    <span style="color:blue">Post Query</span>
                </a>
            </span>
        </div> 
    </div>
    
<div class="row">
    <div class="col-md-8">
        <form id="qform" class="table-font">
                <br>
                <a style="margin-left:1%;color:white !important;" id="querybtn" class="btn-dark btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="glyphicon glyphicon-plus"></i>&nbsp;Create Query
                </a>
                
  
                <div  class="card collapse" id="collapseExample" style="padding:0 !important" >
            <?
                $user=Yii::app()->user->getState('user_id');
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc= $uinfo->ilcid;
                
                $cru1=new CDbCriteria();
                $cru1->condition='ilcid=:u';
                $cru1->params=array(":u"=>$ilc);
                $ilcn=  Ilc::model()->find($cru1);
                $iname=$ilcn->ins_name;
                
                $cru2=new CDbCriteria();
                $cru2->condition='ilcid=:u';
                $cru2->params=array(":u"=>$ilc);
                $ilcm= ManagerIlc::model()->find($cru2);
                $mname=$ilcm->managername;
                
                ?>
            <div class="card-header">
                <span style="text-transform: uppercase;font-weight: bold"><?=$iname ?></span>
                <span style="float:right">Manager : <?=$mname ?></span>
                <br>
                <span><?=$ilcn->address.", ".$ilcn->city.", ".$ilcn->state.", ".$ilcn->country."." ?></span> 
                
            </div>
            <div class="card-body">
           
                <div id="err" class="alert alert-danger"></div> 
                <div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Topic</span>
  </div>
                    <textarea class="form-control" id="topic" aria-label="With textarea"></textarea><br>
</div>
                <br style="clear:both">
                     <div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text">Tags</span>
  </div>
                         <textarea class="form-control" id="tagss" placeholder="enter comma saperated words.." aria-label="With textarea"></textarea><br>
</div>
                <br style="clear:both">
<!--                <div class="row">            
                    <div class="col-md-4">      
                <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Date</span>
  </div>
                    <input type="text" class="form-control" id="datepicker" placeholder="Select date" aria-label="Username" aria-describedby="basic-addon1">
</div>
                    </div>
                        <div class="col-md-4">  
<div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">Hour</span>
  </div>
    <select class="form-control" id="hour" >
        <option value="">Choose..</option>
        <?
        for($i=9;$i<=23;$i++)
        {
            if($i<10)
                $d="0".$i;
            else
                $d=$i;
            
        ?>
        <option value="<?//$d ?>"><?//$d ?></option>
            <?
        }
        ?>
    </select>
</div>
                        </div>
                        <div class="col-md-4">  
<div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">Minutes</span>
  </div>
    <select class="form-control" id="min" >
        <option value="">Choose..</option>
        <?
        for($i=0;$i<=59;$i++)
        {
            if($i<10)
                $d="0".$i;
            else
                $d=$i;
            
        ?>
        <option value="<?//$d ?>"><?//$d ?></option>
            <?
        }
        ?>
    </select>
</div>
                </div>
                </div>-->
                <div class="input-group">
                    <input type="button" class="btn btn-success" value="Create" onclick="setquery()"/>
                </div>         
           

            </div>
        </div>    
            </form>
       
        <br style="clear:both">
        <div class="col-md-12">
            <h1>QUERIES</h1>
            <div id="qer">
                
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div id="qchat">
            
        </div> 
        
        <div class="fchat" id="fchat">
            <form id="sform">
                <input type="hidden" id="query_id" value="-" />
                <div style="display: none">           
                    <input type="file" id="cfile" onchange="uploadfile()" /></div>
                <button onclick="upfile()" type="button" style="padding: 5px"> 
                    <i class="glyphicon glyphicon-paperclip"></i></button>
                <br>
                <!--<input type="button" value="Send File" class="btn btn-success" onclick="uploadfile()"/>-->
                <br>
            </form>
            <!--end widget-->
        </div>
        <div class="chtxt">
            <form id="frmc">
                <input type="hidden" id="qid" value="-" />
                 <input type="hidden" id="uuid" value="-" />
                <div class="form-group">
                    <textarea class="form-control" id="chttext" placeholder="type message..."></textarea>
                </div>
                <div class="form-group">
                    <input type="button" id="chbtn" class="btn btn-outline-dark btn-sm" onclick="sendchat()" value="SEND" />
                </div>
            </form>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Share your experience</h4>
      </div>
      <div class="modal-body" id="body1">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default " data-dismiss="modal" id="btn-close">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
    
      $("#chttext").hide();
    $("#chbtn").hide();
    $("#fchat").hide();
    //$("#createq").hide();
   // $("#chbtn").prop('disabled','true');
    getq();
//    $("#datepicker").datepicker({
//  dateFormat: "yy-mm-dd"
//});




/////////////////////////////////////////////
setInterval(function(){
    var qid=$("#qid").val();
     var uid=$("#uuid").val();
    if(qid!="-") {
   getchat(qid,uid)  
    }
},10000);
///////////////////////////////////////////////
 checknoti();

           checknotinum();
               nlist();

      

});

function showq(){
    if( $("#createq"))
    {
        $("#createq").show(); 
    }
    else{
    $("#createq").show(); 
    }
}
//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var file_data = $('#cfile').prop('files')[0];   
      var qid = $('#query_id').val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('qid', qid);
   // alert(form_data);                             
    $.ajax({
        url: '<? echo $this->createUrl("uploadpost"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
           var uid=$("#uuid").val();
          getchat(qid,uid);
   
        }
     });
}
////////////////////////////////////////////////////////////
function editfeed(id){
    
    //alert(id);
    $("#myModal").modal();
    revshowf();
    $("#revshow").hide();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
            
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     revshowf();
    $("#revshow").hide();
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("editfeed"); ?>',
        data:{
          id:id
        },
    dataType:"html",
    cache:false
})//ajax
}
function revshowf(){
 var rat=$("#rating option:selected").val();
 if(rat=="1" || rat=="2" || rat=="3"){
    $("#revshow").show(); 
    }
   else{
     $("#revshow").hide();   
   } 
}
function updatefeed(){
    var qid=$("#qqid").val();
    var ilcmid=$("#ilcmid").val();
    var rating=$("#rating").val();
    var review=$("#review").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     
   $("#btn-close").click();
     getq();
     alert("Your feedback has been submitted");
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatefeed"); ?>',
        data:{
          qid:qid,ilcmid:ilcmid,rating:rating,review:review
        },
    dataType:"html",
    cache:false
})//ajax
}
function setquery() {
    var topic=$("#topic").val();
    var taggs=$("#tagss").val();
//       var datep=$("#datepicker").val();
//          var h=$("#hour").val();
//          var m=$("#min").val();
            if(topic=="" || topic==null)
     {
         $("#err").show().html("Enter topic..");
     }
     else if(taggs=="" || taggs==null)
     {
         $("#err").show().html("Enter tags..");
     }
//     else if(datep=="" || datep==null)
//     {
//         $("#err").show().html("Enter date..");
//     }
//     else if(h=="" || h==null)
//     {
//         
//            $("#err").show().html("Enter hour..");
//     }
//     else if(m=="" || m==null)
//     {
//         $("#err").show().html("Enter minuites..");
//     }
      else 
     {
              ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#err").show().html('creating...........');
        },
                
        success:function(html){
     $("#qform").trigger("reset");
     $("#querybtn").click();
  //  document.getElementById('ilcfrm').reset();
     $("#err").show().html(html);
     getq();
     
        //window.location.href=window.location.href;
        //}
        },
                 error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },      
        type:'post',
        url:'<? echo $this->createUrl("createquery"); ?>',
        data:{
          topic:topic,taggs:taggs
                  },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
     }
}
function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qer").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getquery"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function closequery(qid) {
var y=confirm("Are you sure?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
            editfeed(qid);
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
   getq();
   
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("closequery"); ?>',
        data:{
          qid:qid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
function showchat(qid,uid) {
$("#qid").val(qid);
$("#uuid").val(uid);
  //$("#chbtn").prop('disabled','false');
  getchat(qid,uid);
    $("#chttext").show();
     $("#fchat").show();
     $("#query_id").val(qid);
    $("#chbtn").show();
    
      $('html, body').animate({
        scrollTop: $("#chatui").offset().top
    }, 2000);
}
function getchat(qid,uid) {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qchat").empty().append(html);
     var objDiv = document.getElementById("chatui");
    objDiv.scrollTop = objDiv.scrollHeight;
   
//      $('html, body').animate({
//        scrollTop: $("#qchat").offset().top
//    }, 2000);
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getchat"); ?>',
        data:{
          qid:qid,uid:uid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function sendchat() {
var qid=$("#qid").val();
var msg=$("#chttext").val();
if(msg=="") {
    alert("Enter message...");
}
else {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
   //  $("#frmc").trigger("reset");
  $("#chttext").val('');
  //  document.getElementById('ilcfrm').reset();
    // $("#qchat").empty().append(html);
  var uid=$("#uuid").val();
   getchat(qid,uid);
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("sendchat"); ?>',
        data:{
          qid:qid,msg:msg
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
</script>