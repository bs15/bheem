<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bheem - Forgot Password</title>
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"> -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
  <style type="text/css">
      body{
        background: #4B79A1;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #283E51, #4B79A1);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #283E51, #4B79A1); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      }
      .centerDiv{
        margin-top: 5em;
      }
      .loginsec{
        background: #DAE2F8;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #D6A4A4, #DAE2F8);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #D6A4A4, #DAE2F8); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

      }
  </style>
</head>
<body>
    
    <div class="container">
        <div class="col-md-6 col-sm-12 mx-auto centerDiv">
            <h2 class="text-center font-weight-bold" style="color: #fff;">Reset Your Password</h2>
            <div class="alert alert-danger" id="msg"></div>
            <div class="alert alert-success" id="succ"></div>
            <div class="card shadow loginsec">
                <div class="card-body text-center">
                    <img height="auto" width="auto" class="mb-3" src="<?=Yii::app()->request->baseUrl ?>/images/bheem/bheem_logo.png">
                    <form class="form">
                        <div class="form-group">
                            <input type="email" id="umail" class="form-control" placeholder="Enter valid email id"/>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" onclick="creset()">Get Reset Link</button><br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

<script type="text/javascript">
$(document).ready(function(){
    $("#msg").hide();
    $("#succ").hide();
});
//function validateEmail(sEmail) {
//var filter = /^[w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
//if (filter.test(sEmail)) {
//return true;
//}
//else {
//return false;
//}
//}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}
function creset() {
    $("#succ").hide();
    var umail=$("#umail").val();
       if (umail=="" || umail==null)
             {
                 $("#msg").show().html("Enter email..");
             } 
              else if(umail!="" && !isValidEmailAddress(umail)) {
          $("#msg").show().html("Enter valid email");
     }  
           else  {
                 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#msg").show().html('sending...........');
        },
                
        success:function(html){
            $("#msg").hide();
            $("#succ").show().html(html);
        },
                
        type:'post',
        url:'<? echo $this->createUrl("doreset"); ?>',
        data:{
            umail:umail
        },
    dataType:"html",
    cache:false
});//ajax
                 
                 
                 ///////////////////////////////////////////////////
          
     }
}
</script>