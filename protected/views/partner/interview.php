<div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/index">Home</a> >
                Task Scheduler > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/interview">
                    <span style="color:blue">Schedule Interview </span>
                </a>
            </span>
        </div> 
    </div>
 
<div class="row">
    <div class="col-md-8">
        <br>  
            <form id="qform">
                
                <a style="margin-left:1%;color:white !important;" id="inttogg" class="btn-dark btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
    <i class="glyphicon glyphicon-plus"></i>&nbsp;Schedule Interview
  </a>
          
                
  
        <div  class="card collapse table-font" id="collapseExample" >
            <?
                $user=Yii::app()->user->getState('user_id');
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc= $uinfo->ilcid;
                
                $cru1=new CDbCriteria();
                $cru1->condition='ilcid=:u';
                $cru1->params=array(":u"=>$ilc);
                $ilcn=  Ilc::model()->find($cru1);
                $iname=$ilcn->ins_name;
                
                $cru2=new CDbCriteria();
                $cru2->condition='ilcid=:u';
                $cru2->params=array(":u"=>$ilc);
                $ilcm= ManagerIlc::model()->find($cru2);
                $mname=$ilcm->managername;
                
                ?>
            <div class="card-header"> 
                <span style="float:right">Manager :  
                <?=$mname ?></span>
                <span style="text-transform: uppercase;font-weight: bold"><?=$iname ?></span><br>
    <span><?=$ilcn->address.", ".$ilcn->city.", ".$ilcn->state.", ".$ilcn->country."." ?></span>
            </div>
            <div class="card-body">
                <div id="err" class="alert alert-danger"></div> 
                <div class="row">
                    <div class="col-md-6">
                        
                        <div class="input-group mb-3">
                            <div style="display: none">           
                                <input type="file" id="cfile" onchange="uploadfile()" /></div>
                                <div class="input-group-prepend">&nbsp; 
                                    <span class="input-group-text" id="basic-addon1">  Upload Candidate CV</span></div>
                                    <button onclick="upfile()"  type="button" style="padding: 5px"> 
                                        <i class="glyphicon glyphicon-paperclip"></i>
                                    </button> 
                                    <div id="fn"></div>
                                    
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Full Name</span>
                        </div>
                        <input type="text" class="form-control" id="fullname" placeholder="Candidate name" aria-label="Username" aria-describedby="basic-addon1">
                        </div> 
                    </div>
                </div>
            <br style="clear:both">
            
                <div class="row">            
                    <div class="col-md-4">      
                <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Date</span>
  </div>
  <input type="text" class="form-control" id="datepicker" placeholder="Select date" aria-label="Username" aria-describedby="basic-addon1">
</div>
                    </div>
                    <div class="col-md-2" style="height:45px;background-color:#f4f4f4;border:1px solid lightgrey;border-radius:5px;padding:1px"> 
    <div style="padding-top:15px !important">
    <label >Time</label>
    </div>
</div>
<div class="col-md-3">
    
    <input type="time" class="form-control" id="stime" />
</div>



                    
</div>
<div class="input-group">
    <input type="button" class="btn btn-success" value="Create" onclick="setinterview()"/>
</div>         
<div id="qer" class="alert alert-success"></div>           
</div>

</div>    
</form>

        <div class="col-md-12">
            <h1>Scheduled INTERVIEWS</h1>
            <div id="intlist">
                
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12" align="center">
        <h3>ILC Manager's Calendar</h3>
        <div  id="holcal">
                <ul class="hpal">
                      
                    <li><div class="bred"></div>&nbsp;Holidays</li>
                    <li><div class="bbla"></div>&nbsp;Leaves</li>
                    <li><div class="bora"></div>&nbsp;Enquiry</li>
                    <li><div class="byel"></div>&nbsp;Interview</li>
                    <li><div class="bpur"></div>&nbsp;Calls</li>
                    
                </ul>
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcilcm/?q='.$man), // URL to get event
                /////////////////////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getilcmm/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            //////////////////////////////////////////////////////////////////////////////
                
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
              <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>                  
            </div>

    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Share your experience</h4>
      </div>
      <div class="modal-body" id="body1">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default " data-dismiss="modal" id="btn-close">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
    $("#qer").hide();
    //$("#intlist").show();
     // $("#chttext").hide();
   // $("#chbtn").hide();
    //$("#fchat").hide();
    //$("#createq").hide();
   // $("#chbtn").prop('disabled','true');
    
    getq();
    $("#datepicker").datepicker({
  dateFormat: "yy-mm-dd",
    minDate: 0
  //maxDate:0
});




/////////////////////////////////////////////
//setInterval(function(){
//    var qid=$("#qid").val();
//     var uid=$("#uuid").val();
//    if(qid!="-") {
//   getchat(qid,uid)  
//    }
//},10000);
///////////////////////////////////////////////
});

function showq(){
    if( $("#createq"))
    {
        $("#createq").show(); 
    }
    else{
    $("#createq").show(); 
    }
}
//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var f=$("#cfile").val();
      $("#fn").html(f);
}
////////////////////////////////////////////////////////////

function setinterview() {
    var file_data = $('#cfile').prop('files')[0];
    var datep= $('#datepicker').val();
    var f= $("#cfile").val();
//    var h=$("#hour option:selected").val();
//    var m=$("#min option:selected").val();
 var tt=$("#stime").val();
    var fname=$("#fullname").val();
    
     if(fname=="" || fname==null)
     {
         $("#err").show().html("Enter candidate name");
     }
     else if(datep=="" || datep==null)
     {
         $("#err").show().html("Enter date..");
     }
     else if(tt=="" || tt==null)
     {
        $("#err").show().html("Enter time..");
     }
//     else if(m=="" || m==null)
//     {
//         $("#err").show().html("Enter minuites..");
//     }
     else if(f=="" || f==null)
     {
         $("#err").show().html("Enter file..");
     }
      else 
     {
            var rest=tt.split(":");
         var form_data = new FormData();  
         form_data.append('fname', fname);
        form_data.append('file', file_data);
        form_data.append('datep', datep);
        form_data.append('hour', rest[0]);
        form_data.append('min', rest[1]);
    // alert(form_data);                             
    $.ajax({
        url: '<? echo $this->createUrl("schedulei"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Scheduling Interview...');
        },
        success: function(deer){
            $("#qer").show().html('Interview has been scheduled');
            $("#qform").trigger("reset");
            $("#fn").html('');
            $("#qer").html('');
            $("#qer").hide();
            $("#inttogg").click();
            getq();
         //   alert(php_script_response); // display response from the PHP script, if any
           
   
        }
     });
                 
                 
                 ///////////////////////////////////////////////////
     }
}
function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#intlist").show().html('Loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#intlist").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("intlist1"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function closeint(intid) {
var y=confirm("Are you sure?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
   getq();
   
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("closeint"); ?>',
        data:{
          intid:intid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}

</script>