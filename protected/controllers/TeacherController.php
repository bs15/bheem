<?php

class TeacherController extends Controller
{
  public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        
          public function accessRules()
                        	{

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid';
    	$criteria1->params = array(':rolid'=>'teacher');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','ilcmanager','createilcm','getilcm','delilcm'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('unit','teacher','createilcm2','getilcm2'),
    		'users'=>$modad,
    	),
           array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('query','querylist','queryhistory','createquery','getquery','closequery','getchat','sendchat','getqmy'),
    		'users'=>$modad,
    	),
         array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('getquery2','getquery3','setnoty','checknum','nlist'),
    		'users'=>$modad,
             ),
            
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}	
    public function actionIndex()
	{
	$uid = Yii::app()->user->getState("user_id");
        $cr = new CDbCriteria();
        $cr->condition='userid=:u';
        $cr->params=array(':u'=>$uid);
        $user=Users::model()->find($cr);
        $userp=Userphoto::model()->find($cr);
	$this->render('index',array('user'=>$user,'userp'=>$userp));	
        
	}
public function actionQuerylist() {
    $this->render('querylist');
}


public function actionQueryhistory() {
    $this->render('queryhistory');
}
public function actionGetqmy() {
    if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);

        $y=intval($yea);
        $m=intval($mon);
        $ilcid=Yii::app()->user->getState('ilc_id');
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="month=:m and year=:y and ilc_id=:i and query_closed=1";
        $crq->params=array(":m"=>$m,":y"=>$y,":i"=>$ilcid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?> <p style="margin-top:8%">No Queries were raised in this month !</p> <?
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <? foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ?>
                <tr>
                <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
                <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
                <td><?=$q->query_topic ?></td>
                <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                <td>
                    <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                </td>
                </tr>
            <?
                
            }
            ?>
                </table>
           </div>
            <?
          }
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
public function actionGetquery2() {
    if(Yii::app()->request->isPostRequest) {
             
    $uid=Yii::app()->user->getState('ilc_id');
        $crq=new CDbCriteria();
        $crq->condition='ilc_id=:u and ilc_approved=1 and query_closed=0';
        $crq->order='query_date desc';
        $crq->params=array(':u'=>$uid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?>
            <p>There are no active Queries</p>
            <?
        }
        else 
        {
            ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr >
                        <td><b>Topic</b></td>
                        <td><b>Created on</b></td>
                        <td><b>Status</b></td>
                        <td><b>Chat</b></td>
                        </tr>
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                ?>
<tr>
                        <td><?=$q->query_topic ?></td>
                        <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                        <td>
                            <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active":"Closed" ?></span>
                        </td>
                        <td>
                            <input type="button" style="font-size:14px !important; padding:3px;border-radius:5px;" class="btn-dark" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                        </td>
                       
                    </tr> 
                    <?
            }?>
                </table>
            </div>
                    <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
  public function actionGetquery3() {
      if(Yii::app()->request->isPostRequest) {
         
        $uid=Yii::app()->user->getState('ilc_id');
        $crq=new CDbCriteria();
        $crq->condition='ilc_id=:u and ilc_approved=1 and query_closed=1';
        $crq->order='query_date desc';
        $crq->params=array(':u'=>$uid);
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?>
                    <p>No Queries have been raised from this ILC</p>
            <?
        }
        else 
        {
            ?>
                    <div class="table table-striped table-font">  
                <table>
                    <tr >
                        <td><b>Topic</b></td>
                        <td><b>Created on</b></td>
                        <td><b>Feedback(out of 5)</b></td>
                        <td><b>Chat</b></td>
                    </tr>
                    <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                ?>
<tr>
                        <td><?=$q->query_topic ?></td>
                        <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
                       
                        <td>
                        <?
                        $qid1=$q->qid;
                        $crq=new CDbCriteria();
                $crq->condition='queryid=:u';
                $crq->params=array(":u"=>$qid1);
                $qinfo= QueryFeed::model()->find($crq);
                         ?>
                            <p><?=$qinfo->rating ?></p>   
                        </td>
                        <td>
                          <input type="button" style="font-size:14px !important; padding:3px;border-radius:5px;" class="btn-dark" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
                        </td>
                    </tr>   
                    <?
            }?>
                </table>
            </div>
                    <?
            
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
 }
 public function actionGetchat() {
     if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $uid=trim($_POST['uid']);

    $cr=new CDbCriteria();
    $cr->condition='queryid=:q and priv=0';
    $cr->params=array(":q"=>$qid);
    $cr->order='date1 desc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       echo "no chats";
   }
 else {
     
     $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
    ?>
<p style="font-size:16px !important" >Topic: <?=$topic ?>  </p>
<div id="chatui"> 
    <?
       foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
         if($uinf->role=='unit'){
           ?>
           <div class="alert alert-success talk" style="float:left">
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='partner')
         {
           ?>
            <div class="alert alert-warning talk" style="float:right">
    <b style="font-size:14px !important"><?=$uinf->name ?>[Unit Partner]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
           <?  
         }
         else{
             ?>
                <div class="alert alert-danger talk" style="float:left">
    <b style="font-size:14px !important"><?=$uinf->name ?>[&nbsp;<?=$uinf->role ?>&nbsp;]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
                 <?
         }
         
         
         } ?> 
    </div> 
    <?    
   }
   }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
    
}
public function actionSendchat() {
    if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $msg=trim($_POST['msg']);

  $uid=Yii::app()->user->getState("user_id");
    $qc=new QueryChat();
    $qc->queryid=$qid;
    $qc->chatid=  uniqid();
    $qc->senderid=$uid;
    $qc->recid='-';
    $qc->message=$msg;
    $qc->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qc->isNewRecord=true;
    $qc->save(FALSE);
    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}

    
}
 public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       } 
        

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}