<?php

/**
 * This is the model class for table "ilc".
 *
 * The followings are the available columns in table 'ilc':
 * @property string $id
 * @property string $ilcid
 * @property string $address
 * @property string $partner_name
 * @property string $ins_name
 * @property string $emails
 * @property string $phones
 * @property string $otherf
 * @property string $agreement
 * @property string $agreement_date
 * @property string $agreement_valid
 * @property string $junior_pres_prog
 * @property string $preschool_valid_from
 * @property string $senior_pres_prog
 * @property string $senior_pres_valid_from
 * @property string $teacher_training_prog
 * @property string $teacher_training_valid_from
 * @property string $country
 * @property string $city
 * @property string $state
 * @property string $preschool
 * @property string $afterschool_activity
 * @property string $teacher_training
 */
class Ilc extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ilc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ilc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ilcid, address, partner_name, ins_name, emails, phones, agreement_date, agreement_valid, country, city, state, preschool, afterschool_activity, teacher_training', 'required'),
			array('ilcid', 'length', 'max'=>100),
			// The following rule is used by search().
                     	array('id, ilcid, address, partner_name, ins_name, emails, phones, othf, agreement, agreement_date, agreement_valid, junior_pres_prog, preschool_valid_from, senior_pres_prog, senior_pres_valid_from, teacher_training_prog, teacher_training_valid_from, country, city, state, preschool, afterschool_activity, teacher_training', 'safe'),
		
			// Please remove those attributes that should not be searched.
			array('id, ilcid, address, partner_name, ins_name, emails, phones, othf, agreement, agreement_date, agreement_valid, junior_pres_prog, preschool_valid_from, senior_pres_prog, senior_pres_valid_from, teacher_training_prog, teacher_training_valid_from, country, city, state, preschool, afterschool_activity, teacher_training', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
        public function getPartners() {
            $cr=new CDbCriteria();
           $cr->condition="role=:r and pa=0";
           $cr->params=array(":r"=>'partner');
           $resp=Users::model()->findAll($cr);
           return CHtml::listData($resp, 'userid', 'name');
        }
	public function attributeLabels()
	{
		return array(
                    'ilcid' => 'Ilc id',
                    'address' => 'Address',
                    'partner_name' => 'Partner Name',
                    'ins_name' => 'Institute Name',
                    'emails' => 'Emails',
                    'phones' => 'Phones',
                    'othf' => 'Other Franchise',
                    'agreement_date' => 'Date from',
                    'agreement_valid' => 'Valid till',
                    'junior_pres_prog' => 'Prog name',
                    'preschool_valid_from' => 'Valid from',
                    'senior_pres_prog' => 'Prog name',
                    'senior_pres_valid_from' => 'Valid from',
                    'teacher_training_prog' => 'Prog name',
                    'teacher_training_valid_from' => 'Valid from',
                    'country' => 'Country',
                    'city' => 'City',
                    'state' => 'State',
                    'preschool' => 'Preschool',
                    'afterschool_activity' => 'Afterschool Activity',
                    'teacher_training' => 'Teacher Training',
                                            'agreement' => 'Agreement',
                    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('partner_name',$this->partner_name,true);
		$criteria->compare('ins_name',$this->ins_name,true);
		$criteria->compare('emails',$this->emails,true);
		$criteria->compare('phones',$this->phones,true);
		$criteria->compare('othf',$this->othf,true);
		$criteria->compare('agreement',$this->agreement,true);
		$criteria->compare('agreement_date',$this->agreement_date,true);
		$criteria->compare('agreement_valid',$this->agreement_valid,true);
		$criteria->compare('junior_pres_prog',$this->junior_pres_prog,true);
		$criteria->compare('preschool_valid_from',$this->preschool_valid_from,true);
		$criteria->compare('senior_pres_prog',$this->senior_pres_prog,true);
		$criteria->compare('senior_pres_valid_from',$this->senior_pres_valid_from,true);
		$criteria->compare('teacher_training_prog',$this->teacher_training_prog,true);
		$criteria->compare('teacher_training_valid_from',$this->teacher_training_valid_from,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('preschool',$this->preschool,true);
		$criteria->compare('afterschool_activity',$this->afterschool_activity,true);
		$criteria->compare('teacher_training',$this->teacher_training,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}