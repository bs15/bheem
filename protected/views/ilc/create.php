<?php
/* @var $this IlcController */
/* @var $model Ilc */

//$this->breadcrumbs=array(
//	'Ilcs'=>array('index'),
//	'Create',
//);
//
//$this->menu=array(
//	array('label'=>'List Ilc', 'url'=>array('index')),
//	array('label'=>'Manage Ilc', 'url'=>array('admin')),
//);
?>
<div class="row">
    <div class="col-md-12">
        <br>
        <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Master Data Management > Create > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilc/create">
                    <span style="color:blue">ILC</span>
                </a>
            </span> 
    </div>
</div>
<h1>Create ILC
   &nbsp; <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilc/ilclist" class="btn-green">ILC List</a>
</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>