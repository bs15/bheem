<?php

class CommunicatorController extends Controller
{
     public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        
          public function accessRules()
                        	{

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid or role=:roleid1 or role=:roleid2';
    	$criteria1->params = array(':rolid'=>'ilcmanager',':roleid1'=>'academic',':roleid2'=>'director');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','setnoty','checknum','nlist','uploadpost','uploadpost2','getcomm'),
    		'users'=>$modad,
    	),
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('uploadcomment','uploadcom','commsearch'),
    		'users'=>$modad,
    	),
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        
	public function actionIndex()
	{
		$this->render('index');
	}
        public function actionGetcomm()
        {
          if(Yii::app()->request->isPostRequest) {
            $userid=Yii::app()->user->getState('user_id');
        $ilc=Yii::app()->user->getState('ilc_id');
        $ext=array("jpg","jpeg","png","bmp","gif");
        $crtcm=new CDbCriteria();
        $crtcm->select="*";
        $crtcm->order="date desc";
        $comms=  Communicator::model()->findAll($crtcm);
        $i=0;
        foreach ($comms as $cc) {
            $sender=$cc->sender;
            $cid=$cc->comid;
            $cr1=new CDbCriteria();
            $cr1->condition="userid=:u";
            $cr1->params=array(":u"=>$sender);
            $uinfo=Users::model()->find($cr1);
            $uphoto=  Userphoto::model()->find($cr1);
            $ilcs="";
            switch ($uinfo->role) {
                case "ilcmanager":
                {
                    $cm=new CDbCriteria();
                    $cm->condition="ilcmid=:i";
                    $cm->params=array(":i"=>$sender);
                    $ilcids=  ManagerIlc::model()->findAll($cm);
                    if(sizeof($ilcids)>0)
                    {
                        foreach ($ilcids as $ii) {
                            $ilid=$ii->ilcid;
                            $cm2=new CDbCriteria();
                            $cm2->condition="ilcid=:il";
                            $cm2->params=array(":il"=>$ilid);
                            $ilcr=  Ilc::model()->find($cm2);
                            $ilcs.=$ilcr->ins_name.", ";
                        }
                    }
                    $rr="Ilc Manager";
                    break;
                }
                case "academic":
                    $rr="Academic Head";
                    break;
                case "director":
                    $rr="Director";
                    break;
                default:
                    $rr="User";
                    break;
            }
            $imgf=0;
            $docu=$cc->document;
            if($docu!="-")
            {
                $imgdata=  explode(".", $docu);
                $iext=  strtolower($imgdata[1]);
                if(in_array($iext, $ext))
                        $imgf=1;
            }
            $dd=  strtotime($cc->date);
            $date1=date('d-M-y g:i a',$dd);
            ?>
                <!--/////////////////////card//////////////////////////////////////-->
                <div class="card text-left">
                <div class="card-header">
                    <div class="pull-left">
                       
                        <h3> <img src="<?=Yii::app()->request->baseUrl.'/userphoto/'.$uphoto->uphoto ?>" class="pfoto"/>&nbsp;<strong><?=$uinfo->name ?></strong>&nbsp;(<?=$rr ?>)&nbsp;<?=$ilcs ?></h3>
                    </div>
                    <div class="pull-right">
                        <span class="pdate"><?=$date1 ?></span>
                    </div>
                </div>
                    <?
                    if($imgf==1) 
                    {
                        ?>
                    <div align="center">
                    <img src="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" class="cimg" alt="...">
                    </div>
                            <?
                    } 
                        ?>
                <div class="card-body">
                    
                  <h5 class="card-title ct"><?=$cc->topic ?></h5>
                  <p class="card-text">
                      <?
                      $crt=new CDbCriteria();
                      $crt->condition="comid=:c";
                      $crt->params=array(":c"=>$cid);
                      $tagss=  ComTags::model()->findAll($crt);
                      foreach ($tagss as $tt) {
                         ?>
                      <a class="tt" href="#">#<?=$tt->tag ?></a>       
                      
                             <? 
                      }
                      ?>
                  </p>
                  <? 
                  if(($docu!="-") && ($imgf==0))
                  {
                      ?>
                  <a href="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" target="_blank" class="btn btn-primary btn-sm">Download Attachment</a>
                          <?
                  }
                  ?>
                
                </div>
                <div class="card-footer text-muted">
                    <div class="comfrm">
                        <form id="commfrm">
                            <div class="row">
                                <div class="col-md-7 col-lg-7 col-sm-12">
                                      <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Comment</span>
  </div>
                    <input type="text" id="cmt<?=$cc->comid ?>" class="form-control" placeholder="Type comment..." aria-label="Username" aria-describedby="basic-addon1">
                    &nbsp;<div id="fn2" style="display: block; width: 99%;float: left;margin-top: 2%;font-size: 10px; color: green"></div>
                          </div>   
                                </div>
                                  <div class="col-md-2 col-lg-2 col-sm-12">
                                      
                <div class="form-group" style="display: none">
                    <input type="file" id="cmfile<?=$cc->comid ?>" name="<?=$cc->comid ?>" onchange="comfile(this.id,this.name,<?=$i ?>)" />
                </div>          
                <div class="form-group">
                    <button onclick="ucpfile(this.name)" name="cmfile<?=$cc->comid ?>" type="button" style="padding: 5px"> <i class="glyphicon glyphicon-paperclip"></i></button>
                     
                </div>          
                                </div>
                                  <div class="col-md-3 col-lg-3 col-sm-12">
                                      <div class="form-group">
                                          <button type="button" id="<?=$cc->comid ?>" onclick="dopost2(this.name,this.id,<?=$i ?>)" name="cmt<?=$cc->comid ?>" class="bt btn-primary btn-small">Comment</button>
                </div> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                    <div id="comm<?=$i ?>">
                        <?  
                            $ccom=new CDbCriteria();
                            $ccom->condition="comid=:c";
                            $ccom->params=array(":c"=>$cc->comid);
                            $ccom->order="date desc";
                            $rcomms=  ComComment::model()->findAll($ccom);
                            $rn=  sizeof($rcomms);
                        ?>
               <ul class="list-group list-group-flush">
                   <? if($rn==0) {
                      ?>
                     <li class="list-group-item">No comments yet!</li>
                          <? 
                   } 
                    else {
                       foreach ($rcomms as $ccc) {
                           $csender=$ccc->userid;
                             $ccr1=new CDbCriteria();
            $ccr1->condition="userid=:u";
            $ccr1->params=array(":u"=>$csender);
            $cuinfo=Users::model()->find($ccr1);
            $cuphoto=  Userphoto::model()->find($ccr1);
            $cilcs="";
             switch ($cuinfo->role) {
                case "ilcmanager":
                {
                    $cm=new CDbCriteria();
                    $cm->condition="ilcmid=:i";
                    $cm->params=array(":i"=>$csender);
                    $ilcids=  ManagerIlc::model()->findAll($cm);
                    if(sizeof($ilcids)>0)
                    {
                        $ilcs="";
                        foreach ($ilcids as $ii) {
                            $ilid=$ii->ilcid;
//                            $cm2=new CDbCriteria();
//                            $cm2->condition="ilcid=:il";
//                            $cm2->params=array(":il"=>$ilid);
//                            $ilcr=  Ilc::model()->find($cm2);
                            $ilcs.=$ii->ilcname.", ";
                        }
                    }
                    $rr="Ilc Manager";
                    break;
                }
                case "academic":
                    $rr="Academic Head";
                    break;
                case "director":
                    $rr="Director";
                    break;
                default:
                    $rr="User";
                    break;
            }
             $imgf=0;
            $docu=$ccc->document;
            if($docu!="-")
            {
                $imgdata=  explode(".", $docu);
                $iext=  strtolower($imgdata[1]);
                if(in_array($iext, $ext))
                        $imgf=1;
            }
            $dd=  strtotime($ccc->date);
            $date1=date('d-M-y g:i a',$dd);
                           ?> 
                     <li class="list-group-item">
                      <div class="pull-left">
                       
                          <h5 class="hhh"> <img src="<?=Yii::app()->request->baseUrl.'/userphoto/'.$cuphoto->uphoto ?>" class="pfoto2"/>&nbsp;<strong><?=$cuinfo->name ?></strong>&nbsp;(<?=$rr ?>)&nbsp;<?=$ilcs ?></h5>
                    </div>
                    <div class="pull-right">
                        <span class="pdate"><?=$date1 ?></span>
                    </div> 
                    <?
                    if($imgf==1) 
                    {
                        ?>
                    <div align="center">
                    <img src="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" class="cimg" alt="...">
                    </div>
                            <?
                    } 
                        ?>      
                           <p class="ct ctt"><?=$ccc->reply ?></p>
                     <?
                     if(($docu!="-") && ($imgf==0))
                  {
                      ?>
                  <a href="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" target="_blank" class="btn btn-primary btn-sm">Download Attachment</a>
                          <?
                  }
                  ?>
                     </li>
                              <?
                       }
                    }
                   ?>
              
              </ul>
                    </div>
              </div>
                <br>
                <!--/////////////////////////////////////////////-->
                <?
                $i++;
        } 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        ///////////////////////////////////////comm search/////////////////////////////
         public function actionCommsearch()
        {
           if(Yii::app()->request->isPostRequest) {  
             $stext=trim($_POST['stext']);
             $nc1=0;$nc2=0;
             ////////////////////////////////topic//////////////////////////////
             $userid=Yii::app()->user->getState('user_id');
        $ilc=Yii::app()->user->getState('ilc_id');
        $ext=array("jpg","jpeg","png","bmp","gif");
        $crtcm=new CDbCriteria();
        $crtcm->select="*";
        $crtcm->condition="topic=:t or topic like :u or topic like :v or topic like :w";
        $crtcm->params=array(":t"=>$stext,":u"=>$stext."%",":v"=>"%".$stext,":w"=>"%".$stext."%");
        $crtcm->order="date desc";
        $comms=  Communicator::model()->findAll($crtcm);
        $nc1=  sizeof($comms);
        $i=0;
        foreach ($comms as $cc) {
            $sender=$cc->sender;
            $cid=$cc->comid;
            $cr1=new CDbCriteria();
            $cr1->condition="userid=:u";
            $cr1->params=array(":u"=>$sender);
            $uinfo=Users::model()->find($cr1);
            $uphoto=  Userphoto::model()->find($cr1);
            $ilcs="";
            switch ($uinfo->role) {
                case "ilcmanager":
                {
                    $cm=new CDbCriteria();
                    $cm->condition="ilcmid=:i";
                    $cm->params=array(":i"=>$sender);
                    $ilcids=  ManagerIlc::model()->findAll($cm);
                    if(sizeof($ilcids)>0)
                    {
                        foreach ($ilcids as $ii) {
                            $ilid=$ii->ilcid;
                            $cm2=new CDbCriteria();
                            $cm2->condition="ilcid=:il";
                            $cm2->params=array(":il"=>$ilid);
                            $ilcr=  Ilc::model()->find($cm2);
                            $ilcs.=$ilcr->ins_name.", ";
                        }
                    }
                    $rr="Ilc Manager";
                    break;
                }
                case "academic":
                    $rr="Academic Head";
                    break;
                case "director":
                    $rr="Director";
                    break;
                default:
                    $rr="User";
                    break;
            }
            $imgf=0;
            $docu=$cc->document;
            if($docu!="-")
            {
                $imgdata=  explode(".", $docu);
                $iext=  strtolower($imgdata[1]);
                if(in_array($iext, $ext))
                        $imgf=1;
            }
            $dd=  strtotime($cc->date);
            $date1=date('d-M-y g:i a',$dd);
            ?>
                <!--/////////////////////card//////////////////////////////////////-->
                <div class="card text-left">
                <div class="card-header">
                    <div class="pull-left">
                       
                        <h3> <img src="<?=Yii::app()->request->baseUrl.'/userphoto/'.$uphoto->uphoto ?>" class="pfoto"/>&nbsp;<strong><?=$uinfo->name ?></strong>&nbsp;(<?=$rr ?>)&nbsp;<?=$ilcs ?></h3>
                    </div>
                    <div class="pull-right">
                        <span class="pdate"><?=$date1 ?></span>
                    </div>
                </div>
                    <?
                    if($imgf==1) 
                    {
                        ?>
                    <div align="center">
                    <img src="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" class="cimg" alt="...">
                    </div>
                            <?
                    } 
                        ?>
                <div class="card-body">
                    
                  <h5 class="card-title ct"><?=$cc->topic ?></h5>
                  <p class="card-text">
                      <?
                      $crt=new CDbCriteria();
                      $crt->condition="comid=:c";
                      $crt->params=array(":c"=>$cid);
                      $tagss=  ComTags::model()->findAll($crt);
                      foreach ($tagss as $tt) {
                         ?>
                      <a class="tt" href="#">#<?=$tt->tag ?></a>       
                      
                             <? 
                      }
                      ?>
                  </p>
                  <? 
                  if(($docu!="-") && ($imgf==0))
                  {
                      ?>
                  <a href="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" target="_blank" class="bt btn-primary">Download Attachment</a>
                          <?
                  }
                  ?>
                
                </div>
                <div class="card-footer text-muted">
                    <div class="comfrm">
                        <form id="commfrm">
                            <div class="row">
                                <div class="col-md-7 col-lg-7 col-sm-12">
                                      <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Comment</span>
  </div>
                    <input type="text" id="cmt<?=$cc->comid ?>" class="form-control" placeholder="Type comment..." aria-label="Username" aria-describedby="basic-addon1">
                    &nbsp;<div id="fn2" style="display: block; width: 99%;float: left;margin-top: 2%;font-size: 10px; color: green"></div>
                          </div>   
                                </div>
                                  <div class="col-md-2 col-lg-2 col-sm-12">
                                      
                <div class="form-group" style="display: none">
                    <input type="file" id="cmfile<?=$cc->comid ?>" name="<?=$cc->comid ?>" onchange="comfile(this.id,this.name,<?=$i ?>)" />
                </div>          
                <div class="form-group">
                    <button onclick="ucpfile(this.name)" name="cmfile<?=$cc->comid ?>" type="button" style="padding: 5px"> <i class="glyphicon glyphicon-paperclip"></i></button>
                     
                </div>          
                                </div>
                                  <div class="col-md-3 col-lg-3 col-sm-12">
                                      <div class="form-group">
                                          <button type="button" id="<?=$cc->comid ?>" onclick="dopost2(this.name,this.id,<?=$i ?>)" name="cmt<?=$cc->comid ?>" class="bt btn-primary">Comment</button>
                </div> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                    <div id="comm<?=$i ?>">
                        <?  
                            $ccom=new CDbCriteria();
                            $ccom->condition="comid=:c";
                            $ccom->params=array(":c"=>$cc->comid);
                            $ccom->order="date desc";
                            $rcomms=  ComComment::model()->findAll($ccom);
                            $rn=  sizeof($rcomms);
                        ?>
               <ul class="list-group list-group-flush">
                   <? if($rn==0) {
                      ?>
                     <li class="list-group-item">No comments yet!</li>
                          <? 
                   } 
                    else {
                       foreach ($rcomms as $ccc) {
                           $csender=$ccc->userid;
                             $ccr1=new CDbCriteria();
            $ccr1->condition="userid=:u";
            $ccr1->params=array(":u"=>$csender);
            $cuinfo=Users::model()->find($ccr1);
            $cuphoto=  Userphoto::model()->find($ccr1);
            $cilcs="";
             switch ($cuinfo->role) {
                case "ilcmanager":
                {
                    $cm=new CDbCriteria();
                    $cm->condition="ilcmid=:i";
                    $cm->params=array(":i"=>$csender);
                    $ilcids=  ManagerIlc::model()->findAll($cm);
                    if(sizeof($ilcids)>0)
                    {
                        $ilcs="";
                        foreach ($ilcids as $ii) {
                            $ilid=$ii->ilcid;
//                            $cm2=new CDbCriteria();
//                            $cm2->condition="ilcid=:il";
//                            $cm2->params=array(":il"=>$ilid);
//                            $ilcr=  Ilc::model()->find($cm2);
                            $ilcs.=$ii->ilcname.", ";
                        }
                    }
                    $rr="Ilc Manager";
                    break;
                }
                case "academic":
                    $rr="Academic Head";
                    break;
                case "director":
                    $rr="Director";
                    break;
                default:
                    $rr="User";
                    break;
            }
             $imgf=0;
            $docu=$ccc->document;
            if($docu!="-")
            {
                $imgdata=  explode(".", $docu);
                $iext=  strtolower($imgdata[1]);
                if(in_array($iext, $ext))
                        $imgf=1;
            }
            $dd=  strtotime($ccc->date);
            $date1=date('d-M-y g:i a',$dd);
                           ?> 
                     <li class="list-group-item">
                      <div class="pull-left">
                       
                          <h5 class="hhh"> <img src="<?=Yii::app()->request->baseUrl.'/userphoto/'.$cuphoto->uphoto ?>" class="pfoto2"/>&nbsp;<strong><?=$cuinfo->name ?></strong>&nbsp;(<?=$rr ?>)&nbsp;<?=$ilcs ?></h5>
                    </div>
                    <div class="pull-right">
                        <span class="pdate"><?=$date1 ?></span>
                    </div> 
                    <?
                    if($imgf==1) 
                    {
                        ?>
                    <div align="center">
                    <img src="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" class="cimg" alt="...">
                    </div>
                            <?
                    } 
                        ?>      
                           <p class="ct ctt"><?=$ccc->reply ?></p>
                     <?
                     if(($docu!="-") && ($imgf==0))
                  {
                      ?>
                  <a href="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" target="_blank" class="bt btn-primary">Download Attachment</a>
                          <?
                  }
                  ?>
                     </li>
                              <?
                       }
                    }
                   ?>
              
              </ul>
                    </div>
              </div>
                <br>
                <!--/////////////////////////////////////////////-->
                <?
                $i++;
        }
        //////////////////////end topic search///////////////////////////
        //////////////////////////tag search//////////////////////////////
          ////////////////////////////////topic//////////////////////////////
             $userid=Yii::app()->user->getState('user_id');
        $ilc=Yii::app()->user->getState('ilc_id');
        $ext=array("jpg","jpeg","png","bmp","gif");
        $crtcm=new CDbCriteria();
        $crtcm->select="*";
        $crtcm->condition="tag=:t";
        $crtcm->params=array(":t"=>$stext);
        $crtcm->order="date1 desc";
        $comms= ComTags::model()->findAll($crtcm);
        $nc2=  sizeof($comms);
        $i=0;
        foreach ($comms as $tcc) {
            $cid=$tcc->comid; 
            $tcr=new CDbCriteria();
            $tcr->condition="comid=:cc";
            $tcr->params=array(":cc"=>$cid);
            $cc=  Communicator::model()->find($tcr);
            $sender=$cc->sender;
          
            $cr1=new CDbCriteria();
            $cr1->condition="userid=:u";
            $cr1->params=array(":u"=>$sender);
            $uinfo=Users::model()->find($cr1);
            $uphoto=  Userphoto::model()->find($cr1);
            $ilcs="";
            switch ($uinfo->role) {
                case "ilcmanager":
                {
                    $cm=new CDbCriteria();
                    $cm->condition="ilcmid=:i";
                    $cm->params=array(":i"=>$sender);
                    $ilcids=  ManagerIlc::model()->findAll($cm);
                    if(sizeof($ilcids)>0)
                    {
                        foreach ($ilcids as $ii) {
                            $ilid=$ii->ilcid;
                            $cm2=new CDbCriteria();
                            $cm2->condition="ilcid=:il";
                            $cm2->params=array(":il"=>$ilid);
                            $ilcr=  Ilc::model()->find($cm2);
                            $ilcs.=$ilcr->ins_name.", ";
                        }
                    }
                    $rr="Ilc Manager";
                    break;
                }
                case "academic":
                    $rr="Academic Head";
                    break;
                case "director":
                    $rr="Director";
                    break;
                default:
                    $rr="User";
                    break;
            }
            $imgf=0;
            $docu=$cc->document;
            if($docu!="-")
            {
                $imgdata=  explode(".", $docu);
                $iext=  strtolower($imgdata[1]);
                if(in_array($iext, $ext))
                        $imgf=1;
            }
            $dd=  strtotime($cc->date);
            $date1=date('d-M-y g:i a',$dd);
            ?>
                <!--/////////////////////card//////////////////////////////////////-->
                <div class="card text-left">
                <div class="card-header">
                    <div class="pull-left">
                       
                        <h3> <img src="<?=Yii::app()->request->baseUrl.'/userphoto/'.$uphoto->uphoto ?>" class="pfoto"/>&nbsp;<strong><?=$uinfo->name ?></strong>&nbsp;(<?=$rr ?>)&nbsp;<?=$ilcs ?></h3>
                    </div>
                    <div class="pull-right">
                        <span class="pdate"><?=$date1 ?></span>
                    </div>
                </div>
                    <?
                    if($imgf==1) 
                    {
                        ?>
                    <div align="center">
                    <img src="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" class="cimg" alt="...">
                    </div>
                            <?
                    } 
                        ?>
                <div class="card-body">
                    
                  <h5 class="card-title ct"><?=$cc->topic ?></h5>
                  <p class="card-text">
                      <?
                      $crt=new CDbCriteria();
                      $crt->condition="comid=:c";
                      $crt->params=array(":c"=>$cid);
                      $tagss=  ComTags::model()->findAll($crt);
                      foreach ($tagss as $tt) {
                         ?>
                      <a class="tt" href="#">#<?=$tt->tag ?></a>       
                      
                             <? 
                      }
                      ?>
                  </p>
                  <? 
                  if(($docu!="-") && ($imgf==0))
                  {
                      ?>
                  <a href="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" target="_blank" class="bt btn-primary">Download Attachment</a>
                          <?
                  }
                  ?>
                
                </div>
                <div class="card-footer text-muted">
                    <div class="comfrm">
                        <form id="commfrm">
                            <div class="row">
                                <div class="col-md-7 col-lg-7 col-sm-12">
                                      <div class="input-group mb-3">
  <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon1">Comment</span>
  </div>
                    <input type="text" id="cmt<?=$cc->comid ?>" class="form-control" placeholder="Type comment..." aria-label="Username" aria-describedby="basic-addon1">
                    &nbsp;<div id="fn2" style="display: block; width: 99%;float: left;margin-top: 2%;font-size: 10px; color: green"></div>
                          </div>   
                                </div>
                                  <div class="col-md-2 col-lg-2 col-sm-12">
                                      
                <div class="form-group" style="display: none">
                    <input type="file" id="cmfile<?=$cc->comid ?>" name="<?=$cc->comid ?>" onchange="comfile(this.id,this.name,<?=$i ?>)" />
                </div>          
                <div class="form-group">
                    <button onclick="ucpfile(this.name)" name="cmfile<?=$cc->comid ?>" type="button" style="padding: 5px"> <i class="glyphicon glyphicon-paperclip"></i></button>
                     
                </div>          
                                </div>
                                  <div class="col-md-3 col-lg-3 col-sm-12">
                                      <div class="form-group">
                                          <button type="button" id="<?=$cc->comid ?>" onclick="dopost2(this.name,this.id,<?=$i ?>)" name="cmt<?=$cc->comid ?>" class="bt btn-primary">Comment</button>
                </div> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                    <div id="comm<?=$i ?>">
                        <?  
                            $ccom=new CDbCriteria();
                            $ccom->condition="comid=:c";
                            $ccom->params=array(":c"=>$cc->comid);
                            $ccom->order="date desc";
                            $rcomms=  ComComment::model()->findAll($ccom);
                            $rn=  sizeof($rcomms);
                        ?>
               <ul class="list-group list-group-flush">
                   <? if($rn==0) {
                      ?>
                     <li class="list-group-item">No comments yet!</li>
                          <? 
                   } 
                    else {
                       foreach ($rcomms as $ccc) {
                           $csender=$ccc->userid;
                             $ccr1=new CDbCriteria();
            $ccr1->condition="userid=:u";
            $ccr1->params=array(":u"=>$csender);
            $cuinfo=Users::model()->find($ccr1);
            $cuphoto=  Userphoto::model()->find($ccr1);
            $cilcs="";
             switch ($cuinfo->role) {
                case "ilcmanager":
                {
                    $cm=new CDbCriteria();
                    $cm->condition="ilcmid=:i";
                    $cm->params=array(":i"=>$csender);
                    $ilcids=  ManagerIlc::model()->findAll($cm);
                    if(sizeof($ilcids)>0)
                    {
                        $ilcs="";
                        foreach ($ilcids as $ii) {
                            $ilid=$ii->ilcid;
//                            $cm2=new CDbCriteria();
//                            $cm2->condition="ilcid=:il";
//                            $cm2->params=array(":il"=>$ilid);
//                            $ilcr=  Ilc::model()->find($cm2);
                            $ilcs.=$ii->ilcname.", ";
                        }
                    }
                    $rr="Ilc Manager";
                    break;
                }
                case "academic":
                    $rr="Academic Head";
                    break;
                case "director":
                    $rr="Director";
                    break;
                default:
                    $rr="User";
                    break;
            }
             $imgf=0;
            $docu=$ccc->document;
            if($docu!="-")
            {
                $imgdata=  explode(".", $docu);
                $iext=  strtolower($imgdata[1]);
                if(in_array($iext, $ext))
                        $imgf=1;
            }
            $dd=  strtotime($ccc->date);
            $date1=date('d-M-y g:i a',$dd);
                           ?> 
                     <li class="list-group-item">
                      <div class="pull-left">
                       
                          <h5 class="hhh"> <img src="<?=Yii::app()->request->baseUrl.'/userphoto/'.$cuphoto->uphoto ?>" class="pfoto2"/>&nbsp;<strong><?=$cuinfo->name ?></strong>&nbsp;(<?=$rr ?>)&nbsp;<?=$ilcs ?></h5>
                    </div>
                    <div class="pull-right">
                        <span class="pdate"><?=$date1 ?></span>
                    </div> 
                    <?
                    if($imgf==1) 
                    {
                        ?>
                    <div align="center">
                    <img src="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" class="cimg" alt="...">
                    </div>
                            <?
                    } 
                        ?>      
                           <p class="ct ctt"><?=$ccc->reply ?></p>
                     <?
                     if(($docu!="-") && ($imgf==0))
                  {
                      ?>
                  <a href="<?=Yii::app()->request->baseUrl.'/postdocs/'.$docu ?>" target="_blank" class="btn btn-primary btn-sm">Download Attachment</a>
                          <?
                  }
                  ?>
                     </li>
                              <?
                       }
                    }
                   ?>
              
              </ul>
                    </div>
              </div>
                <br>
                <!--/////////////////////////////////////////////-->
                <?
                $i++;
        }
        
        //////////////////////////////////////////////////end tag search//////////////
        
        if($nc1==0 && $nc2==0)
        {
            echo "No results found!";
        } 
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        //
        /////////////////////////////////////////////////////////////////////////////////
        //////////////////////upload post ///////////////////
            public function actionUploadpost() {
        $model = new Communicator();
        $userid=Yii::app()->user->getState('user_id');
        $ilc=Yii::app()->user->getState('ilc_id');
        $posttext=trim($_POST['ptext']);
        $tags=trim($_POST['tag']);
        $commid=  uniqid();
        $d=date("d");
        $m=date("m");
        $y=date("Y");
        /////////////////////////tag split////////////////
        $tt=  explode(',', $tags);
        foreach ($tt as $t)
        {
            if(!empty($t) && $t!=null && $t!="")
            {
                $ct=new ComTags();
                $ct->comid=$commid;
                $ct->tag=trim(strtolower($t));
                $ct->date1=  strtotime(date("Y-m-d H:i:s"));
                $nt=  intval($ct->count("comid=:c and tag=:t", array(":c"=>$commid,":t"=>trim(strtolower($t)))));
                if($nt==0)
                {
                    $ct->isNewRecord=TRUE;
                    $ct->save(FALSE);
                    
                }
            }
        }
        /////////////////////////////////////////////////
	//$gallery = new UserGallery;
	 if ($_FILES['file']['error'] > 0) {
       // echo 'Error: ' . $_FILES['file']['error'] . '<br>';
            $model -> document = '-';
	
                   $model->comid=$commid;
                $model->sender=$userid;
         $model->rec=$ilc;
         $model->topic=$posttext;
         $model->tags=$tags;
         $model->day=$d;
         $model->mon=$m;
         $model->year=$y;
         $model->isNewRecord=TRUE;  
         $model->save(FALSE);
    }
     else { 
     //   $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
             
                $model -> document = $fileName;
	
                   $model->comid=$commid;
                $model->sender=$userid;
         $model->rec=$ilc;
         $model->topic=$posttext;
         $model->tags=$tags;
         $model->day=$d;
         $model->mon=$m;
         $model->year=$y;
         $model->isNewRecord=TRUE;
         
	if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../postdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
            }
                     public function actionUploadpost2() {
        $model = new Communicator();
        $userid=Yii::app()->user->getState('user_id');
        $ilc=Yii::app()->user->getState('ilc_id');
        $posttext=trim($_POST['ptext']);
        $tags=trim($_POST['tag']);
        $commid=  uniqid();
        $d=date("d");
        $m=date("m");
        $y=date("Y");
        /////////////////////////tag split////////////////
        $tt=  explode(',', $tags);
        foreach ($tt as $t)
        {
            if(!empty($t) && $t!=null && $t!="")
            {
                $ct=new ComTags();
                $ct->comid=$commid;
                $ct->tag=trim(strtolower($t));
                $ct->date1=  strtotime(date("Y-m-d H:i:s"));
                $nt=  intval($ct->count("comid=:c and tag=:t", array(":c"=>$commid,":t"=>trim(strtolower($t)))));
                if($nt==0)
                {
                    $ct->isNewRecord=TRUE;
                    $ct->save(FALSE);
                    
                }
            }
        }
        /////////////////////////////////////////////////
	//$gallery = new UserGallery;
	
       // echo 'Error: ' . $_FILES['file']['error'] . '<br>';
            $model -> document = '-';
	
                   $model->comid=$commid;
                $model->sender=$userid;
         $model->rec=$ilc;
         $model->topic=$posttext;
         $model->tags=$tags;
         $model->day=$d;
         $model->mon=$m;
         $model->year=$y;
         $model->isNewRecord=TRUE;  
         $model->save(FALSE);
   
  
            }
            ///////////////////////////////////////////
            public function actionUploadcomment() {
                 $userid=Yii::app()->user->getState('user_id');
                $commid=substr(trim($_POST['comid']),3);
                $ctext=  trim($_POST['ctext']);
                $cid=  uniqid();
                $d=date("d");
                $m=date("m");
                $y=date("Y");
                $cm=new ComComment();
                $cm->comid=$commid;
                $cm->userid=$userid;
                $cm->replyid=$cid;
                $cm->reply=$ctext;
                $cm->day=$d;
                $cm->mon=$m;
                $cm->year=$y;
                $cm->document="-";
                $cm->isNewRecord=TRUE;
                $cm->save(FALSE);
                
                
            }
    public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
          $nl->setnoty1();
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
        }
          public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {  
  
              $nl= new Nlist();
            $nl->checknum1();
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
        
   public function actionNlist() {
         if(Yii::app()->request->isPostRequest) {  

        $nl= new Nlist();
       $nl->nlist1();
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       public function actionUploadcom()
       {
//           $model = new ComComment();
        $userid=Yii::app()->user->getState('user_id');
        $ilc=Yii::app()->user->getState('ilc_id');
        $comid=trim($_POST['comid']);
     
	 if ($_FILES['file']['error'] > 0) {
       // echo 'Error: ' . $_FILES['file']['error'] . '<br>';
         
    }
     else { 
       // $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
             
            $cid=  uniqid();
                $d=date("d");
                $m=date("m");
                $y=date("Y");
                $cm=new ComComment();
                $cm->comid=$comid;
                $cm->userid=$userid;
                $cm->replyid=$cid;
                $cm->reply=" ";
                $cm->day=$d;
                $cm->mon=$m;
                $cm->year=$y;
                $cm->document=$fileName;
                $cm->isNewRecord=TRUE;
         
	if ($cm -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../postdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
       }
       // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}