<?php
/* @var $this IlcController */
/* @var $model Ilc */

$this->breadcrumbs=array(
	'Ilcs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Ilc', 'url'=>array('index')),
	array('label'=>'Create Ilc', 'url'=>array('create')),
	array('label'=>'View Ilc', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Ilc', 'url'=>array('admin')),
);
?>
<div class="row">
    <div class="col-md-12">
        <br>
        <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Master Data Management > Manage > 
                
                    <span style="color:blue">ILC</span>
                
            </span> 
    </div>
</div>
<h1>Update Ilc <?php echo $model->id; ?>&nbsp; 

<a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilc/ilclist" class="btn-green">ILC List</a>
</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>