
<div class="row">
   <div class="col-md-12" >
       <br>
       <span class="bread">
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
           Internal Management > 
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/mleave">
               <span style="color:blue">Leave Management</span>
           </a>
       </span>
    </div> 
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div>
                <h1>Manage LEAVES</h1>
                <div  id="hol">
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
         <br style="clear:both">   
            <div  id="holcal">
                <ul class="hpal">
                      
                    <li><div class="bred"></div>&nbsp;Holidays</li>
                    <li><div class="bora"></div>&nbsp;Enquiry</li>
                    <li><div class="byel"></div>&nbsp;Interview</li>
                    <li><div class="bbla"></div>&nbsp;Leaves</li>
                    
                </ul>
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcilcm/?q='.$man), // URL to get event
                //////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getilcmm/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            //////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
               <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>                     
            </div>
        </div>
    </div>
</div> 
<script>
 $(document).ready(function(){
  getq();  
 // getwhol();

});
    function getq() {
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#hol").show().html('Loading...........');
        },
                
        success:function(html){
        $("#hol").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("hol"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function setleave(calid,ap)
{
    var y=confirm("Are you sure?");
    if(y) {
         $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#hol").show().html('Loading...........');
        },
                
        success:function(html){
     //   $("#hol").empty().append(html);
        getq();
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("setleave"); ?>',
        data:{
          calid:calid,ap:ap
        },
    dataType:"html",
    cache:false
})//ajax
    }
}
</script>