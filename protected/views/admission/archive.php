<? $rolid=Yii::app()->user->getState("rolid"); ?>
<? if($rolid=='director'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/archive">
                    <span style="color:blue">Admission Archive</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='academic'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/archive">
                    <span style="color:blue">Admission Archive</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='ilcmanager'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/archive">
                    <span style="color:blue">Admission Archive</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='unit'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/archive">
                    <span style="color:blue">Admission Archive</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='partner'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/index">Home</a> >
                Archives > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/admission/archive">
                    <span style="color:blue">Admission Archive</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    
    ?>
<div class="row">
    <div class="col-md-12">
        
    <h1>ADMISSION Archive</h1>
    <div class="row">
                  <div class="col-md-3">
        <select class="enin" id="yea" onchange="searchqmy()">
             <option value="0">Choose Year</option>
             <?
             for($i=2019;$i<=2040;$i++) {
                 ?>
             <option value="<?=$i ?>"><?=$i ?></option>
                     <?
             }
             ?>
        </select>
    <!--<input type="number" min="2019" max="<? //date('Y') ?>" placeholder="Enter year"  class="enin" id="yea" >-->
    </div>
    <div class="col-md-3">
        <select id="mon" class="enin" onchange="searchqmy()">
        <option value="0">Choose Month</option>
        <option value="1">Jan</option> 
        <option value="2">Feb</option> 
        <option value="3">Mar</option> 
        <option value="4">Apr</option> 
        <option value="5">May</option> 
        <option value="6">Jun</option> 
        <option value="7">Jul</option> 
        <option value="8">Aug</option> 
        <option value="9">Sep</option> 
        <option value="10">Oct</option> 
        <option value="11">Nov</option>
        <option value="12">Dec</option> 
    </select>
    </div>
    
  
    
    <div class="col-md-2">
    <button class="btn btn-sm btn-small btn-danger" onclick="reset()">Reset</button>
    </div>
    <div class="col-md-3" >
        <div id="err" style="color:red;font-size:14px;font-weight:500" ></div>
    </div>
</div>
    <div id="admar">

    </div>
        
    </div>
</div>

<div id="myModalai" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left">Additional Information</h4>
        <button type="button" class="close" id="btn-closed" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" id="bodyai">
        <p>Some text in the modal.</p>
      </div>
      
    </div>

  </div>
</div>
<script>
$(document).ready(function(){
    getq();
});
function getq() {
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#admar").show().html('Loading...........');
        },
                
        success:function(html){
        $("#admar").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("admar"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function gethow(){
    var how=$('#how').val();
    $('.form-check-input').each(function(e){
       var howval=$(this).val(); 
       if(how.indexOf(howval) != -1){
           $(this).attr('checked','true');
       }
    });

}
function reset(){
    window.location.href=window.location.href;
}
function viewai(adid){
    $("#myModalai").modal();
    //////////////////////ajax////////////////////////
    $.ajax({
    
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#bodyai").show().html('Loading...');
        },
                
        success:function(html){
        $("#bodyai").empty().append(html);
        $("#datepicker").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        $("#datepicker1").datepicker({
            dateFormat: "yy-mm-dd" ,minDate: 0
        });
        gethow();
        },
        error:function(jqXHR, exception) {
            alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("viewai"); ?>',
        data:{
          adid:adid,
        },
    dataType:"html",
    cache:false
})//ajax
}

function searchqmy() {
    var mon=$("#mon option:selected").val();
    var yea=$("#yea option:selected").val();
    // alert(yea+" "+mon);     
    if(mon=="0" && yea=="0")
     {
         $("#err").show().html("Choose Month and year");
     }
    else if(mon!="0" && yea=="0")
     {
         $("#err").show().html("Choose Year and then month");
     }
     else 
     {
      ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#admar").show().html('Loading..');
        },
                
        success:function(html){
     $("#admar").empty().append(html);
  //  document.getElementById('ilcfrm').reset();
   //  getqmy();
     
        //window.location.href=window.location.href;
        //}
        },
                 error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },      
        type:'post',
        url:'<? echo $this->createUrl("getamy"); ?>',
        data:{
          yea:yea,mon:mon,
        },
    dataType:"html",
    cache:false
})//ajax
///////////////////////////////////////////////////
}
}
</script>