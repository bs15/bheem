<?php

/**
 * This is the model class for table "nlist".
 *
 * The followings are the available columns in table 'nlist':
 * @property string $nid
 */
class Nlist extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Nlist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'nlist';
	}
        
        public function headergap(){
            ?>
            <li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li><li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li><li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a>
        </li>
        <li class="nav-item hidden-sm hidden-xs text-white">
            <a class="nav-link"></a></li>
        <li class="nav-item hidden-sm hidden-xs">
            <a class="nav-link"></a></li>
            <li class="nav-item hidden-sm hidden-xs">
                <a class="nav-link"></a>
            </li>
        
        
        
        <?
        }
        public function getcity($st1)
	{
            $criteria1 = new CDbCriteria();
            $criteria1->select='city';
            $criteria1->condition='state=:s';
            $criteria1->params=array(':s'=>$st1);
            $criteria1->group="city";
            $cities = Ilc::model()->findAll($criteria1);
            
            ?>
            <select id="ci" class="enin" onchange="getilc()">
            <option value="0">Select City</option>
            <? foreach($cities as $ci){
                ?>
                <option value="<?=$ci->city ?>"><?=$ci->city ?></option>
                <?
            } ?>
        </select> 
        <?
	}
        
        public function getcity2($st1)
	{
            $criteria1 = new CDbCriteria();
            $criteria1->condition='state=:s';
            $criteria1->params=array(':s'=>$st1);
            $criteria1->group="city";
            $cities = Ilc::model()->findAll($criteria1);
            
            ?>
            <select id="ci" class="enin" onchange="getres()">
            <option value="0">Select City</option>
            <? foreach($cities as $ci){
                ?>
                <option value="<?=$ci->city ?>"><?=$ci->city ?></option>
                <?
            } ?>
        </select> 
        <?
	}
        
         public function getilc($ci1)
	{
            $criteria1 = new CDbCriteria();
            $criteria1->condition='city=:s';
            $criteria1->params=array(':s'=>$ci1);
            $ilcs = Ilc::model()->findAll($criteria1);
            
            ?>
            <select id="il" class="enin" onchange="getres()">
            <option value="0">Select ILC</option>
            <? foreach($ilcs as $il){
                ?>
                <option value="<?=$il->ilcid ?>"><?=$il->ins_name ?></option>
                <?
            } ?>
            </select> 
        <?
	}
        public function nlist1(){
    $userid=Yii::app()->user->getState('user_id');
    $rolid=Yii::app()->user->getState("rolid");
    $ilcid=Yii::app()->user->getState("ilc_id");
    $n1=$n2=$n3=$n4=$n5=$n6=1;
            
    if($rolid=='unit') {
        $cr=new CDbCriteria();
        $cr->condition='(role=:r or role=:er) and ilc=:i';
        $cr->params=array(':r'=>'unit',':er'=>'all',':i'=>$ilcid);
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n1=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
 <?
        }
    }
    
    if($rolid=='partner') {
        $cr=new CDbCriteria();
        $cr->condition='(role=:r or role=:er) and ilc=:i';
        $cr->params=array(':r'=>'partner',':er'=>'all',':i'=>$ilcid);
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n2=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
            <?
        }
    }
    
    if($rolid=='ilcmanager') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r or role=:er';
        $cr->params=array(':r'=>'ilcmanager',':er'=>'all');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        
        $cr1=new CDbCriteria();
        $cr1->condition='ilcmid=:r ';
        $cr1->params=array(':r'=>$userid);
        $res1= ManagerIlc::model()->findAll($cr1);
        $cc=0;
        $ilcs=array();
        foreach($res1 as $r1){
            array_push($ilcs, $r1->ilcid);
        }
        
        $n3=  sizeof($res);
        foreach($res as $r){
            if($cc>5)
                break;
            
            if(in_array($r->ilc,$ilcs)){
            ?>
             <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
             <hr>
            <?
            $cc++;
        }
      }
    }
  
    if($rolid=='academic') {
        $cr=new CDbCriteria();
         $cr->condition='role=:r or role=:er';
        $cr->params=array(':r'=>'academic',':er'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n4=  sizeof($res);
        foreach($res as $r){
            ?>
         <a class=" cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
         <hr>
            <?
        }
    }
    
    if($rolid=='director') {
        $cr=new CDbCriteria();
         $cr->condition='role=:r or role=:er';
        $cr->params=array(':r'=>'director',':er'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
            <?
        }
    }
    
    if($rolid=='director' || $rolid=='academic' || $rolid=='ilcmanager' || $rolid=='unit' || $rolid=='partner' || $rolid=='teacher'
        ) {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n6=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
            <?
        }
    }
    
        else if($n2==0 || $n3==0 || $n4==0 || $n5==0 || $n6==0){
            ?>
            <div class="alert alert-light" role="alert">
                <p>No Notifications to show !</p>
            </div>
            <?
        }
        }
        
        public function nlist2(){
                       $userid=Yii::app()->user->getState('user_id');
            $rolid=Yii::app()->user->getState("rolid");
            $ilcid=Yii::app()->user->getState("ilc_id");
             $n1=$n2=$n3=$n4=$n5=$n6=1;
            
    if($rolid=='unit') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'unit',':i'=>$ilcid);
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n1=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
 <?
        }
    }
    
    if($rolid=='partner') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'partner',':i'=>$ilcid);
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n2=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
            <?
        }
    }
    
    if($rolid=='ilcmanager') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r ';
        $cr->params=array(':r'=>'ilcmanager');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        
        $cr1=new CDbCriteria();
        $cr1->condition='ilcmid=:r ';
        $cr1->params=array(':r'=>$userid);
        $res1= ManagerIlc::model()->findAll($cr1);
        $cc=0;
        $ilcs=array();
        foreach($res1 as $r1){
            array_push($ilcs, $r1->ilcid);
        }
        
        $n3=  sizeof($res);
        foreach($res as $r){
            if($cc>5)
                break;
            
            if(in_array($r->ilc,$ilcs)){
            ?>
             <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
             <hr>
            <?
            $cc++;
        }
      }
    }
  
    if($rolid=='academic') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'academic');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n4=  sizeof($res);
        foreach($res as $r){
            ?>
         <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
         <hr>
            <?
        }
    }
    
    if($rolid=='director') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'director');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
            <?
        }
    }
     if($rolid=='director' || $rolid=='academic' || $rolid=='ilcmanager' || $rolid=='unit' || $rolid=='partner' || $rolid=='teacher'
        ) {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $cr->limit=5;
        $res=  Notifications::model()->findAll($cr);
        $n6=  sizeof($res);
        foreach($res as $r){
            ?>
           <a class="cn" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/notifications"><?=$r->message ?></a>
           <hr>
            <?
        }
    }
        if($n1==0 || $n2==0 || $n3==0 || $n4==0 || $n5==0 || $n6==0){
            ?>
<!--            <div class="alert alert-light" role="alert">
                <p>No Notifications to show !</p>
            </div>-->
            <?
        }
        }
        
        public function setnoty1(){
        
        $crq=new CDbCriteria();
        $crq->condition="role=:r";
        $crq->params=array(":r"=>'academic');
        $ua=  Users::model()->find($crq);
        $acaname=$ua->name;
        $acaemail=$ua->username;
        
        $crq=new CDbCriteria();
        $crq->condition="role=:r";
        $crq->params=array(":r"=>'director');
        $ud=  Users::model()->find($crq);
        $dirname=$ud->name;
        $diremail=$ud->username;
            
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="query_closed=0";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
            $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                if($nc>0){
                    $qdate=$q->query_date;
                }
                else{
                    $qdate=$q->query_date;
                }
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                  if($d>=24){
                      
                  ////////////////escalated query//////////////
                  $qesc=new Query();
                  $qesc->updateAll(array('esc'=>1), 'qid=:q and esc=0', array(':q'=>$queryid));
                  /////////////////////////////////////////////
                  
                  
                  $msg="The query with topic ".$q->query_topic." has escalated as it has not closed after 24 hours";
                  $mailb="The query with topic $q->query_topic has escalated as it has not closed after 24 hours";
                  
                  
            
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$queryid,':qt'=>$msg,':i'=>$q->ilc_id)));
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $queryid;
     $c->ilc = $q->ilc_id;
     $c->message="The query with topic ".$q->query_topic." has escalated as it has not closed after 24 hours";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$acaemail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Query Escalation";
                    $msg="Dear $acaname,<br>
                        $mailb<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
                }   
                else if($d>=72){
                    
                     ////////////////escalated query//////////////
                  $qesc=new Query();
                  $qesc->updateAll(array('esc'=>1), 'qid=:q and esc=0', array(':q'=>$queryid));
                  /////////////////////////////////////////////
                  $msg="The query with topic ".$q->query_topic." has escalated as it has not closed after 72 hours";
     
                  $mailb="The query with topic $q->query_topic has escalated as it has not closed after 72 hours";
     
                      
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$queryid,':qt'=>$msg,':i'=>$q->ilc_id)));
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'director';
     $c->queryid = $queryid;
     $c->ilc = $q->ilc_id;
     $c->message="The query with topic ".$q->query_topic." has escalated as it has not closed after 72 hours";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$diremail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Query Escalation";
                    $msg="Dear $dirname,<br>
                        $mailb<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
                }   
                else if($d>=96){
                    
                     ////////////////escalated query//////////////
                  $qesc=new Query();
                  $qesc->updateAll(array('esc'=>1), 'qid=:q and esc=0', array(':q'=>$queryid));
                  /////////////////////////////////////////////
                   $msg="The query with topic ".$q->query_topic." has escalated as it has not closed after 24 hours";
     
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$queryid,':qt'=>$msg,':i'=>$q->ilc_id)));
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $queryid;
     $c->ilc = $q->ilc_id;
     $c->message="The query with topic ".$q->query_topic." has escalated as it has not closed after 24 hours";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$acaemail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Query Escalation";
                    $msg="Dear $acaname,<br>
                        $msg<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
                }   
            }
      
        }
        $this->setnotyi($dirname,$diremail,$acaemail,$acaname);
        $this->setnotye($dirname,$diremail,$acaemail,$acaname);
        $this->setnotya($dirname,$diremail,$acaemail,$acaname);
        $this->setnotyc($dirname,$diremail,$acaemail,$acaname);
        }
        
        public function setnotyi($dirname,$diremail,$acaemail,$acaname){
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="iclosed=:is";
        $crq->params=array(":is"=>0);
        $inter=Interview::model()->findAll($crq);
        $n=sizeof($inter);
       //echo $n;
        if($n==0) {
            
        }
        else 
        {
            foreach ($inter as $q) {
                
                $idate=$i->idate;
                $idate1=intval(strtotime($idate));
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
               if($d>=48 && $d<=72){
                   
                
               $msg="Interview scheduled for ".$q->candidatename." has escalated as it has not been approved within 48 hours.";
                $mailb="Interview scheduled for $q->candidatename has escalated as it has not been approved within 48 hours";
               $intid=$q->intid;
               $ilcid=$q->ilcid;
               
               ////////////////escalated interview//////////////
                  $qesc=new Interview();
                  $qesc->updateAll(array('esc'=>1), 'intid=:q and esc=0', array(':q'=>$intid));
                  /////////////////////////////////////////////   
                  
               
      $c=new Notifications();
      $nc=intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$intid,':qt'=>$msg,':i'=>$ilcid)));
      $intid=$q->intid;
       $ilcid=$q->ilcid;
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $intid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$acaemail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Interview Escalation";
                    $msg="Dear $acaname,<br>
                        $mailb<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
      
      
    }   
    else if($d>=72){
       $msg="Interview scheduled for ".$q->candidatename." has escalated as it has not been approved within 72 hours.";
       $mailb="Interview scheduled for $q->candidatename has escalated as it has not been approved within 72 hours";
       
       $intid=$q->intid;
       $ilcid=$q->ilcid;
      
       ////////////////escalated interview//////////////
                  $qesc=new Interview();
                  $qesc->updateAll(array('esc'=>1), 'intid=:q and esc=0', array(':q'=>$intid));
        ///////////////////////////////////////////// 
       
//echo '<script>alert(.'.$ilcid.'.)</script>';
       
                  
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$intid,':qt'=>$msg,':i'=>$ilcid)));
      if($nc==0)
      {
                   
     $c->notid=  uniqid();
     $c->role = 'director';
     $c->queryid = $intid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
      ////////////////send mail//////////////////
                    $to=$diremail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Query Escalation";
                    $msg="Dear $dirname,<br>
                        $mailb<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
     }   
    }
    }
    }
    
        public function setnotye($dirname,$diremail,$acaemail,$acaname){
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="eclosed=:is";
        $crq->params=array(":is"=>0);
        $inter=Enquiry::model()->findAll($crq);
        $n=sizeof($inter);
       // echo $n;
        if($n==0) {
            
        }
        else 
        {
            foreach ($inter as $q) {
                
                $idate1=$q->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $intid=$q->enquiryid;
               $ilcid=$q->ilcid;
                
               if($d>=24 && $d<=48){
               
               ////////////////escalated Enquiry//////////////
                  $qesc=new Enquiry();
                  $qesc->updateAll(array('esc'=>1), 'enquiryid=:q and esc=0', array(':q'=>$intid));
                /////////////////////////////////////////////   
                      
                   
               $msg="Enquiry raised for ".$q->enquiry_date." has escalated as it has not been approved within 24 hours.";
               $mailb="Enquiry raised for $q->enquiry_date has escalated as it has not been approved within 24 hours";
               
      $c=new Notifications();
      $nc=intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$intid,':qt'=>$msg,':i'=>$ilcid)));
      
       
      if($nc==0)
      {
          
          
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $intid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$acaemail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Enquiry Escalation";
                    $msg="Dear $acaname,<br>
                        $mailb<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
      /////////////////////////////////////////////
      }
    }   
    else if($d>=48){
        
        ////////////////escalated Enquiry//////////////
                  $qesc=new Enquiry();
                  $qesc->updateAll(array('esc'=>1), 'enquiryid=:q and esc=0', array(':q'=>$intid));
        /////////////////////////////////////////////   
       $nc=0;
       $msg="Enquiry raised for ".$q->enquiry_date." has escalated as it has not been approved within 24 hours.";
       $mailb="Enquiry raised for $q->enquiry_date has escalated as it has not been approved within 24 hours";
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$intid,':qt'=>$msg,':i'=>$ilcid)));
      if($nc==0)
      {
          
          
     $c->notid=  uniqid();
     $c->role = 'director';
     $c->queryid = $intid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$diremail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Enquiry Escalation";
                    $msg="Dear $dirname,<br>
                        $mailb<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
           /////////////////////////////////////////////
      }
     }   
    }
    }
    }
    
        public function setnotya($dirname,$diremail,$acaemail,$acaname){
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="aclosed=:is";
        $crq->params=array(":is"=>0);
        $inter=Admission::model()->findAll($crq);
        $n=sizeof($inter);
        //echo $n;
        if($n==0) {
            
        }
        else 
        {
            foreach ($inter as $q) {
                
                $idate1=$q->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $adid=$q->adid;
                $ilcid=$q->ilcid;
                
               if($d>=24 && $d<=48){
                  
                   
               ////////////////escalated admission//////////////
                  $qesc=new Admission();
                  $qesc->updateAll(array('esc'=>1), 'adid=:q and esc=0', array(':q'=>$adid));
                  /////////////////////////////////////////////   
                      
               $msg="Admission of son/daughter of ".$q->f_name." has escalated as it has not been approved within 24 hours.";
               
               
      $c=new Notifications();
      $nc=intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$adid,':qt'=>$msg,':i'=>$ilcid)));
      
       
      if($nc==0)
      {
          
         
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $adid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$acaemail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Admission Escalation";
                    $msg="Dear $acaname,<br>
                        $msg<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    ///////////////////////////////////////////// 
      }
    }   
    else if($d>=48){
       
       $msg="Admission of son/daughter of ".$q->f_name." has escalated as it has not been approved within 48 hours.";
       
       ////////////////escalated admission//////////////
                  $qesc=new Admission();
                  $qesc->updateAll(array('esc'=>1), 'adid=:q and esc=0', array(':q'=>$adid));
                  /////////////////////////////////////////////   
       
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$adid,':qt'=>$msg,':i'=>$ilcid)));
      if($nc==0)
      {
          
          
     $c->notid=  uniqid();
     $c->role = 'director';
     $c->queryid = $adid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$diremail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Admission Escalation";
                    $msg="Dear $dirname,<br>
                        $msg<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
     }   
    }
    }
    }
    
        public function setnotyc($dirname,$diremail,$acaemail,$acaname){
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="closed=:is";
        $crq->params=array(":is"=>0);
        $inter=Call::model()->findAll($crq);
        $n=sizeof($inter);
        //echo $n;
        if($n==0) {
            
        }
        else 
        {
            foreach ($inter as $q) {
                
                $idate1=strtotime($q->date1);
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                $callid=$q->callid;
                $ilcid=$q->ilcid;
               
               if($d>=24 && $d<=48){
                   
               
                
                ////////////////escalated call//////////////
                  $qesc=new Call();
                  $qesc->updateAll(array('esc'=>1), 'callid=:q and esc=0', array(':q'=>$callid));
                  /////////////////////////////////////////////   
                     
               $msg="Call scheduled on ".$q->cdate." has escalated as it has not been approved within 24 hours.";
               
               
      $c=new Notifications();
      $nc=intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$callid,':qt'=>$msg,':i'=>$ilcid)));
      
       
      if($nc==0)
      {
          
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $callid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
     ////////////////send mail//////////////////
                    $to=$acaemail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Call Escalation";
                    $msg="Dear $acaname,<br>
                        $msg<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
    }   
    else if($d>=48){
        
        ////////////////escalated call//////////////
                  $qesc=new Call();
                  $qesc->updateAll(array('esc'=>1), 'callid=:q and esc=0', array(':q'=>$callid));
                  /////////////////////////////////////////////   
       $nc=0;
       $msg="Call scheduled on ".$q->cdate." has escalated as it has not been approved within 48 hours.";
                       
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$callid,':qt'=>$msg,':i'=>$ilcid)));
      if($nc==0)
      {
         
     $c->notid=  uniqid();
     $c->role = 'director';
     $c->queryid = $callid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
     
      ////////////////send mail//////////////////
                    $to=$diremail;
                    //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk Call Escalation";
                    $msg="Dear $dirname,<br>
                        $msg<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
      }
     }   
    }
    }
    }
        
        public function setnotyh(){
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="eclosed=:is";
        $crq->params=array(":is"=>0);
        $inter=Enquiry::model()->findAll($crq);
        $n=sizeof($inter);
        echo $n;
        if($n==0) {
            
        }
        else 
        {
            foreach ($inter as $q) {
                
                $idate1=$q->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                $intid=$q->enquiryid;
               $ilcid=$q->ilcid;
                
               if($d>=24 && $d<=48){
                  
               $msg="Enquiry raised for ".$q->enquiry_date." has escalated as it has not been approved within 24 hours.";
               
               
      $c=new Notifications();
      $nc=intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$intid,':qt'=>$msg,':i'=>$ilcid)));
      
       
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $intid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
      }
    }   
    else if($d>=48){
       $nc=0;
       $msg="Enquiry raised for ".$q->enquiry_date." has escalated as it has not been approved within 24 hours.";
       
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$intid,':qt'=>$msg,':i'=>$ilcid)));
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'director';
     $c->queryid = $intid;
     $c->ilc = $ilcid;
     $c->message=$msg;
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
      }
     }   
    }
    }
    }
    
    
    public function setnoty2(){
                  $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="query_closed=0";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
            $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                if($nc>0){
                    $qdate=$qchat->date1;
                }
                else{
                    $qdate=$q->query_date;
                }
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                  if($d>24){
                  $msg="The query with topic ".$q->query_topic." has escalated as it has not closed after 24 hours";
     
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$queryid,':qt'=>$msg,':i'=>$q->ilc_id)));
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $queryid;
     $c->ilc = $q->ilc_id;
     $c->message="The query with topic ".$q->query_topic." has escalated as it has not closed after 24 hours";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
      }
                }   
                else if($d>72){
                      $msg="The query with topic ".$q->query_topic." has escalated as it has not closed after 72 hours";
     
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$queryid,':qt'=>$msg,':i'=>$q->ilc_id)));
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'director';
     $c->queryid = $queryid;
     $c->ilc = $q->ilc_id;
     $c->message="The query with topic ".$q->query_topic." has escalated as it has not closed after 72 hours";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
      }
                }   
                else if($d>96){
                   $msg="The query with topic ".$q->query_topic." has escalated as it has not closed after 96 hours";
     
      $c=new Notifications();
      $nc=  intval($c->count('queryid=:q and message=:qt and ilc=:i', array(':q'=>$queryid,':qt'=>$msg,':i'=>$q->ilc_id)));
      if($nc==0)
      {
     $c->notid=  uniqid();
     $c->role = 'academic';
     $c->queryid = $queryid;
     $c->ilc = $q->ilc_id;
     $c->message="The query with topic ".$q->query_topic." has escalated as it has not closed after 96 hours";
     $c->date1= strtotime(date('Y-m-d H:i:s'));
     $c->count1=3;
     $c->isNewRecord=TRUE;
     $c->save(FALSE);
      }
                }   
            }
      
        }
        }


        public function checknum1(){
            $userid=Yii::app()->user->getState('user_id');
            $rolid=Yii::app()->user->getState("rolid");
            $ilcid=Yii::app()->user->getState("ilc_id");
             $c=0;
            
    if($rolid=='unit') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'unit',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n1=  sizeof($res);
        foreach($res as $r){
           $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
         $cr2=new CDbCriteria();
        $cr2->condition='intid=:r and iclosed=:is';
        $cr2->params=array(':r'=>$qid,':is'=>0);
        $res2=Interview::model()->find($cr2);
        $n41=sizeof($res2);
        
        if($n41>0){
            $c++;
        }
        $cr3=new CDbCriteria();
        $cr3->condition='enquiryid=:r and eclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=Enquiry::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
          $cr3=new CDbCriteria();
        $cr3->condition='adid=:r and aclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Admission::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        } 
        
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n44=sizeof($res3);
        
        if($n44>0){
            $c++;
        } 
        }
    }
    
    if($rolid=='partner') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'partner',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n2=  sizeof($res);
        foreach($res as $r){
            $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
         $cr2=new CDbCriteria();
        $cr2->condition='intid=:r and iclosed=:is';
        $cr2->params=array(':r'=>$qid,':is'=>0);
        $res2=Interview::model()->find($cr2);
        $n41=sizeof($res2);
        
        if($n41>0){
           $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='enquiryid=:r and eclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=Enquiry::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
           $cr3=new CDbCriteria();
        $cr3->condition='adid=:r and aclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Admission::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n44=sizeof($res3);
        
        if($n44>0){
            $c++;
        } 
        }
    }
    
    if($rolid=='ilcmanager') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r ';
        $cr->params=array(':r'=>'ilcmanager');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        
        $cr1=new CDbCriteria();
        $cr1->condition='ilcmid=:r ';
        $cr1->params=array(':r'=>$userid);
        $res1= ManagerIlc::model()->findAll($cr1);
        
        $ilcs=array();
        foreach($res1 as $r1){
            array_push($ilcs, $r1->ilcid);
        }
        
        $n3=  sizeof($res);
        foreach($res as $r){
            if(in_array($r->ilc,$ilcs)){
            $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
         $cr2=new CDbCriteria();
        $cr2->condition='intid=:r and iclosed=:is';
        $cr2->params=array(':r'=>$qid,':is'=>0);
        $res2=Interview::model()->find($cr2);
        $n41=sizeof($res2);
        
        if($n41>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='enquiryid=:r and eclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=Enquiry::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
           $cr3=new CDbCriteria();
        $cr3->condition='adid=:r and aclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Admission::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n44=sizeof($res3);
        
        if($n44>0){
            $c++;
        } 
        }
      }
    }
  
    if($rolid=='academic') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'academic');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n4=  sizeof($res);
        foreach($res as $r){
           $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        
        $cr2=new CDbCriteria();
        $cr2->condition='intid=:r and iclosed=:is';
        $cr2->params=array(':r'=>$qid,':is'=>0);
        $res2=Interview::model()->find($cr2);
        $n41=sizeof($res2);
        
        if($n41>0){
            $c++;
        }
        $cr3=new CDbCriteria();
        $cr3->condition='enquiryid=:r and eclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=Enquiry::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
           $cr3=new CDbCriteria();
        $cr3->condition='adid=:r and aclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Admission::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n44=sizeof($res3);
        
        if($n44>0){
            $c++;
        } 
        }
        
    }
    
    if($rolid=='director') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'director');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        
        foreach($res as $r){
            
        $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr2=new CDbCriteria();
        $cr2->condition='intid=:r and iclosed=:is';
        $cr2->params=array(':r'=>$qid,':is'=>0);
        $res2=Interview::model()->find($cr2);
        $n41=sizeof($res2);
        
        if($n41>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='enquiryid=:r and eclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=Enquiry::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
           $cr3=new CDbCriteria();
        $cr3->condition='adid=:r and aclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Admission::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n44=sizeof($res3);
        
        if($n44>0){
            $c++;
        } 
        }
    }
    
    if($rolid=='director' || $rolid=='academic' || $rolid=='ilcmanager' || $rolid=='unit' || $rolid=='partner' || $rolid=='teacher') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        foreach($res as $r){
            $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        }
    }
//    
    $json=array();
    $json['num']=$c;
    echo json_encode($json);
        }
        
        public function checknum2(){
            $userid=Yii::app()->user->getState('user_id');
            $rolid=Yii::app()->user->getState("rolid");
            $ilcid=Yii::app()->user->getState("ilc_id");
             $c=0;
            
    if($rolid=='unit') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'unit',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n1=  sizeof($res);
        foreach($res as $r){
           $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='intid=:r and iclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Interview::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='enquiryid=:r and eclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Enquiry::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
           $cr3=new CDbCriteria();
        $cr3->condition='adid=:r and aclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Admission::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        } 
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        } 
        }
    }
    
    if($rolid=='partner') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r and ilc=:i';
        $cr->params=array(':r'=>'partner',':i'=>$ilcid);
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n2=  sizeof($res);
        foreach($res as $r){
            $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='intid=:r and iclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Interview::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='enquiryid=:r and eclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Enquiry::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
           $cr3=new CDbCriteria();
        $cr3->condition='adid=:r and aclosed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Admission::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        } 
        }
    }
    
    if($rolid=='ilcmanager') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r ';
        $cr->params=array(':r'=>'ilcmanager');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        
        $cr1=new CDbCriteria();
        $cr1->condition='ilcmid=:r ';
        $cr1->params=array(':r'=>$userid);
        $res1= ManagerIlc::model()->findAll($cr1);
        
        $ilcs=array();
        foreach($res1 as $r1){
            array_push($ilcs, $r1->ilcid);
        }
        
        $n3=  sizeof($res);
        foreach($res as $r){
            if(in_array($r->ilc,$ilcs)){
            $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='intid=:r and iclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Interview::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='enquiryid=:r and eclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Enquiry::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        
        $cr1=new CDbCriteria();
        $cr1->condition='adid=:r and aclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Admission::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        } 
        }
      }
    }
  
    if($rolid=='academic') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'academic');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n4=  sizeof($res);
        foreach($res as $r){
           $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='intid=:r and iclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Interview::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='enquiryid=:r and eclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Enquiry::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        
        $cr1=new CDbCriteria();
        $cr1->condition='adid=:r and aclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Admission::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        } 
        }
        
    }
    
    if($rolid=='director') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'director');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        foreach($res as $r){
            $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='enquiryid=:r and eclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Enquiry::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='intid=:r and iclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Interview::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='adid=:r and aclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Admission::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr3=new CDbCriteria();
        $cr3->condition='callid=:r and closed=:is';
        $cr3->params=array(':r'=>$qid,':is'=>0);
        $res3=  Call::model()->find($cr3);
        $n43=sizeof($res3);
        
        if($n43>0){
            $c++;
        } 
        }
    }
    if($rolid=='director' || $rolid=='academic' || $rolid=='ilcmanager' || $rolid=='unit' || $rolid=='partner' || $rolid=='teacher') {
        $cr=new CDbCriteria();
        $cr->condition='role=:r';
        $cr->params=array(':r'=>'all');
        $cr->order='date1 desc';
        $res=  Notifications::model()->findAll($cr);
        $n5=  sizeof($res);
        foreach($res as $r){
            $qid=$r->queryid;
           
        $cr1=new CDbCriteria();
        $cr1->condition='qid=:r and query_closed=0';
        $cr1->params=array(':r'=>$qid);
        $res1=  Query::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='enquiryid=:r and eclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Enquiry::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        $cr1=new CDbCriteria();
        $cr1->condition='intid=:r and iclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Interview::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        
        $cr1=new CDbCriteria();
        $cr1->condition='adid=:r and aclosed=:c';
        $cr1->params=array(':r'=>$qid,':c'=>0);
        $res1=  Admission::model()->find($cr1);
        $n4=  sizeof($res1);
        
        if($n4>0){
            $c++;
        }
        }
    }
    
    $json=array();
    $json['num']=$c;
    echo json_encode($json);
        }
        
       public function getilcmid($ilcid){
            $cru=new CDbCriteria(); 
            $cru->condition='ilcid=:u';
            $cru->params=array(":u"=>$ilcid);
            $mani= ManagerIlc::model()->find($cru);
            return $mani;
       }
       
       public function getuser($userid){
            $cru=new CDbCriteria(); 
            $cru->condition='userid=:u';
            $cru->params=array(":u"=>$userid);
            $user= Users::model()->find($cru);
            return $user;
       }

        /**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nid', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nid' => 'Nid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nid',$this->nid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function mailsend($to,$subject,$message) {
        
            

$msg=" 
<html>
<head>
<title>$subject</title>
</head>
<body>
<br>$message<br>

</body>
</html>";
$msg = wordwrap($msg,70);

/////////////////////////////////////////////////////////////////////
// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <mailsender@mis.beanstalkedu.com>' . "\r\n";
//$headers .= 'Cc: myboss@example.com' . "\r\n";

//mail($to,$subject,$message,$headers);
try {
	mail($to,$subject,$msg,$headers);
	  ?>
                    <p title="Mail Sent">Mail Sent</p>
                    
                   <script type="text/ecmascript">
                   // alert("Message submited");
				  // window.location.href="index.php";
                   </script> 
     <? 
	}
catch(Exception $e)
{
echo "Message could not be sent. <p>";
echo "Mailer Error: " . $e->getMessage();
//exit;
}
 
////////////////////////////////////////////////////////////////////////////////////

        }
}