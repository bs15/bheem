
<?php 
// session_destroy();
$link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
$link->set_charset("utf8");
if(mysqli_connect_error()){
	die("ERROR: UNABLE TO CONNECT: ".mysqli_connect_error());
}
if ($_SESSION['LoggedIn']) {
	if ($_SESSION['role'] == 'partner') {
		echo '<script>window.location.href="partnerTraining"</script>';
	}else if ($_SESSION['role'] == 'teacher' || $_SESSION['role'] == 'unit') {
		echo '<script>window.location.href="teacherTraining"</script>';
	}else{
		echo '<div>Not Authorized! Please contact Admin</div>';
	}
}else{
	include 'login.php';
}


?>