<?php
/* @var $this UserphotoController */
/* @var $model Userphoto */

$this->breadcrumbs=array(
	'Userphotos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Userphoto', 'url'=>array('index')),
	array('label'=>'Create Userphoto', 'url'=>array('create')),
	array('label'=>'View Userphoto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Userphoto', 'url'=>array('admin')),
);
?>

<h1>Update Userphoto <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>