<div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
                Management > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/interview">
                    <span style="color:blue">Interview</span>
                </a>
            </span>
        </div> 
    </div>
<div class="row">
    <div class="col-md-8">
         <?
                $user=Yii::app()->user->getState('user_id');
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc= $uinfo->ilcid;
                
                $cru1=new CDbCriteria();
                $cru1->condition='ilcid=:u';
                $cru1->params=array(":u"=>$ilc);
                $ilcn=  Ilc::model()->find($cru1);
                $iname=$ilcn->ins_name;
                
                $cru2=new CDbCriteria();
                $cru2->condition='ilcid=:u';
                $cru2->params=array(":u"=>$ilc);
                $ilcm= ManagerIlc::model()->find($cru2);
                $mname=$ilcm->managername;
            ?>
        
       <div class="col-md-12">
            <h1>Scheduled INTERVIEWS</h1>
             <div class="col-md-3">
                <select class="enin table-font" onchange="searchbyilc()" id="silc">
                <option value="Search by ILC">Search by ILC</option>
                <? foreach($res as $r) 
                { ?>
                <option value="<?=$r->ilcid ?>" style="text-transform: uppercase"><?=$r->ilcname ?></option>   
               <? }
                ?>
                </select>
            </div>
            <div id="intlist1">
                
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12" align="center">
        <br>   
 <div  id="holcal">
                <ul class="hpal">
                      
                    <li><div class="bred"></div>&nbsp;Holidays</li>
                    <li><div class="bbla"></div>&nbsp;Leaves</li>
                    <li><div class="bora"></div>&nbsp;Enquiry</li>
                    <li><div class="byel"></div>&nbsp;Interview</li>
                    <li><div class="bpur"></div>&nbsp;Calls</li>
                </ul>
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getcalenderilcm'), // URL to get event
                /////////////////////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getcalenderilcmmodal/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal1").modal();
	    }',
            //////////////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
               <!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>                      
            </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" id="btn-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Interview Time / Date</h4>
      </div>
      <div class="modal-body" id="body1">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="myModali" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">A note for Unit Coordinator</h4>
        <button type="button" class="close" id="btn-closei" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="bodyi">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#err").hide();
    $("#qer").hide();
    //$("#intlist").show();
     // $("#chttext").hide();
   // $("#chbtn").hide();
    //$("#fchat").hide();
    //$("#createq").hide();
   // $("#chbtn").prop('disabled','true');
    
    getq();
    $("#datepicker").datepicker({
  dateFormat: "yy-mm-dd"
});




/////////////////////////////////////////////
//setInterval(function(){
//    var qid=$("#qid").val();
//     var uid=$("#uuid").val();
//    if(qid!="-") {
//   getchat(qid,uid)  
//    }
//},10000);
///////////////////////////////////////////////
});

function showq(){
    if( $("#createq"))
    {
        $("#createq").show(); 
    }
    else{
    $("#createq").show(); 
    }
}
//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var f=$("#cfile").val();
      $("#fn").html(f);
}
////////////////////////////////////////////////////////////

function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#intlist1").show().html('Loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#intlist1").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("intlist1"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function approveint(intid) {
var y=confirm("Do you want to accept the candidate?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
        getq();
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },    
        type:'post',
        url:'<? echo $this->createUrl("approveint"); ?>',
        data:{
          intid:intid
        },
    dataType:"html",
    cache:false
    })//ajax
    }
}
function rescheduleint(intid){
    $("#myModal").modal();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     $( "#datepicker" ).datepicker({
            dateFormat: "yy-mm-dd" ,  minDate: 0 , container: "#myModal"
        });  
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("rescheduleint"); ?>',
        data:{
          intid:intid
        },
    dataType:"html",
    cache:false
})//ajax
}
function icomment(intid){
    $("#myModali").modal();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyi").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#bodyi").empty().append(html);
     
//window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("icomment"); ?>',
        data:{
          intid:intid
        },
    dataType:"html",
    cache:false
})//ajax
}
function updateint(){
    var intid=$("#qqid").val();
    var idate=$("#datepicker").val();
    var tt=$("#stime2").val();
       var rest=tt.split(":");
        var ihour=rest[0];
    var imin=rest[1];
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
   $("#btn-close").click();
     getq();
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updateint"); ?>',
        data:{
          intid:intid,idate:idate,ihour:ihour,imin:imin,
        },
    dataType:"html",
    cache:false
})//ajax
}
function aval(num){
$("#aval").html(num);
}

function cval(num){
$("#cval").html(num);
}

function tval(num){
$("#tval").html(num);
}
function updatecom(){
    var icom=$("#icom").val();
    var intid=$("#qqid").val();
    var inid=$("#inid").val();
    var ack=$("#ack").val();
    var com1=$("#com1").val();
    var acc=$("#att").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#bodyi").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#bodyi").empty().append(html);
   $("#btn-closei").click();
     getq();
      },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatecom"); ?>',
        data:{
          intid:intid,icom:icom,inid:inid,ack:ack,com1:com1,acc:acc,
        },
    dataType:"html",
    cache:false
})//ajax
}

function searchbyilc(){
    var silc=$("#silc option:selected").val();
    
     //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#intlist1").show().html('loading...........');
        },
                
        success:function(html){
    $("#intlist1").empty().append(html);
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("searchbyilcint"); ?>',
        data:{
          silc:silc
        },
    dataType:"html",
    cache:false
})//ajax
}
</script>