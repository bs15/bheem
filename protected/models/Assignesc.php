<?php

/**
 * This is the model class for table "assignesc".
 *
 * The followings are the available columns in table 'assignesc':
 * @property string $asid
 * @property string $intid
 * @property string $callid
 * @property string $ilcmid
 * @property string $ilcid
 * @property integer $week
 * @property integer $month
 * @property integer $year
 * @property integer $close
 * @property string $city
 * @property string $state
 */
class Assignesc extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Assignesc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'assignesc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('asid, ilcmid, ilcid, week, month, year, close,rating', 'required'),
			array('id, week, month, year, close,rating', 'numerical', 'integerOnly'=>true),
			array('asid, intid, callid, ilcmid, ilcid', 'length', 'max'=>100),
			array('city, state', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, asid, intid, callid, ilcmid, ilcid, week, month, year, close, city, state', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'asid' => 'Asid',
			'intid' => 'Intid',
			'callid' => 'Callid',
			'ilcmid' => 'Ilcmid',
			'ilcid' => 'Ilcid',
			'week' => 'Week',
			'month' => 'Month',
			'year' => 'Year',
			'close' => 'Close',
			'city' => 'City',
			'state' => 'State',
                        'rating' => 'Rating',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('asid',$this->asid,true);
		$criteria->compare('intid',$this->intid,true);
		$criteria->compare('callid',$this->callid,true);
		$criteria->compare('ilcmid',$this->ilcmid,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('week',$this->week);
		$criteria->compare('month',$this->month);
		$criteria->compare('year',$this->year);
		$criteria->compare('close',$this->close);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
                $criteria->compare('rating',$this->rating,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}