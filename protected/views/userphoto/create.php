<?php
/* @var $this UserphotoController */
/* @var $model Userphoto */

$this->breadcrumbs=array(
	'Userphotos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Userphoto', 'url'=>array('index')),
	array('label'=>'Manage Userphoto', 'url'=>array('admin')),
);
?>

<h1 style="margin-left:4%" >Upload Profile Photo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>