<?php
/* @var $this IlcController */
/* @var $model Ilc */

$this->breadcrumbs=array(
	'Ilcs'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Ilc', 'url'=>array('index')),
	array('label'=>'Create Ilc', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ilc-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>View all ILCs</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'ilc-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'ilcid',
		'address',
		'partner_name',
		'ins_name',
		'emails',
		/*
		'phones',
		'other_franchise',
		'agreement_date',
		'agreement_valid',
		'junior_pres_prog',
		'preschool_valid_from',
		'senior_pres_prog',
		'senior_pres_valid_from',
		'teacher_training_prog',
		'teacher_training_valid_from',
		'country',
		'city',
		'state',
		'preschool',
		'afterschool_activity',
		'teacher_training',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
