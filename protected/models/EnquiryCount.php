<?php

/**
 * This is the model class for table "enquiry_count".
 *
 * The followings are the available columns in table 'enquiry_count':
 * @property string $enquiryid
 * @property string $ilcid
 * @property integer $year
 * @property string $month
 * @property integer $week
 * @property integer $ecount
 */
class EnquiryCount extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EnquiryCount the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'enquiry_count';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('enquiryid, ilcid, year, month, week, ecount', 'required'),
			array('id, year, week, ecount', 'numerical', 'integerOnly'=>true),
			array('enquiryid, ilcid', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, enquiryid, ilcid, year, month, week, ecount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enquiryid' => 'Enquiryid',
			'ilcid' => 'Ilcid',
			'year' => 'Year',
			'month' => 'Month',
			'week' => 'Week',
			'ecount' => 'Ecount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('enquiryid',$this->enquiryid,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('year',$this->year);
		$criteria->compare('month',$this->month,true);
		$criteria->compare('week',$this->week);
		$criteria->compare('ecount',$this->ecount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}