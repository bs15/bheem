<?
$criteria1 = new CDbCriteria();
$criteria1->select = 'state';
$criteria1->group="state";
$states = Ilc::model()->findAll($criteria1);
            
            
?>
<div class="row">
   <div class="col-md-12" >
       <br>
       <span class="bread">
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
           Escalations > 
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/queryesc">
               <span style="color:blue">Query Escalation</span>
           </a>
       </span>
    </div> 
</div>
<div class="row">
    <div class="col-md-8">
    <h1>Escalated QUERIES</h1>
            <div class="row">
            <div class="col-md-3 col-sm-12">
                <input type="hidden" id="silc2" value="-">
                <select id="st" class="enin" onchange="getcity()" style="margin-left:1%">
                    <option value="0">Select State</option>
                    <? foreach($states as $st){
                        ?>
                        <option value="<?=$st->state ?>"><?=$st->state ?></option>
                        <?
                    } ?>
                </select>
            </div>
            <div class="col-md-3 col-sm-12" id="loadcity"></div>

            <div class="col-md-3 col-sm-12" id="loadilc"></div>
        </div>
            <div id="qer">
                
            </div>
        
    </div>
    <div class="col-md-4 col-sm-12">
        <div id="qchat">
            
        </div>
        
         <div class="fchat" id="fchat">
            <form id="sform">
                <input type="hidden" id="query_id" value="-" />
                <div style="display: none">           
                    <input type="file" id="cfile" onchange="uploadfile()" /></div>
                <button onclick="upfile()" type="button" style="padding: 5px"> <i class="glyphicon glyphicon-paperclip"></i></button>
                <br>
                <!--<input type="button" value="Send File" class="btn btn-success" onclick="uploadfile()"/>-->
                <br>
            </form>
            <!--end widget-->
        </div>
        <div class="chtxt">
            <form id="frmc">
                <input type="hidden" id="qid" value="-" />
                 <input type="hidden" id="uuid" value="-" />
                <div class="form-group">
                    <textarea class="form-control" id="chttext" placeholder="type message..."></textarea>
                </div>
                <div class="form-group">
                    <input type="button" id="chbtn" class="btn btn-outline-dark btn-sm" onclick="sendchat()" value="SEND" />
                </div>
            </form>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" id="btn-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Query Date/Time</h4>
      </div>
      <div class="modal-body" id="body1">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  //  $("#err").hide();
    getq();
      $("#chttext").hide();
      $("#chbtn").hide();
      $("#fchat").hide();
  
});
function getilc(){
     var ci1=$('#ci option:selected').val();
     if(ci1=='0')
     {
         alert('Select a city ');
     }
     else
     {
        //////////////////////ajax////////////////////////
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#loadilc").show().html('loading...........');
        },
        success:function(html){
        $("#loadilc").empty().append(html);
        getres();
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getilc"); ?>',
        data:{
          ci1:ci1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
 function getres(){
    var il=$("#il option:selected").val();
    var st=$("#st option:selected").val();
    var ci=$("#ci option:selected").val();
    
    if(il=='0' && st=='0' && ci=='0'){
        alert("Select ILC");
    }
    if(il==undefined){
        il='0';
    }
    if(ci==undefined){
        ci='0';
    } 
   
   
    else{
        //////////////////////ajax////////////////////////
        //alert("ilc="+il+" year="+yr+" month="+mon+" week="+wee);
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#qer").show().html('loading...........');
        },
        success:function(html){
        $("#qer").empty().append(html);
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getqueryilc"); ?>',
        data:{
          il:il,ci:ci,st:st,
        },
    dataType:"html",
    cache:false
})//ajax
    }
 }
function getcity(){
     var st1=$('#st option:selected').val();
     if(st1=='0')
     {
         alert('Select a state ');
     }
     else
     {
         //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#loadcity").show().html('loading...........');
        },
        success:function(html){
        $("#loadcity").empty().append(html);
        getres();
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getcity"); ?>',
        data:{
          st1:st1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}
function uploadfile() {
      var file_data = $('#cfile').prop('files')[0];   
      var qid = $('#query_id').val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('qid', qid);
   // alert(form_data);                             
    $.ajax({
        url: '<? echo $this->createUrl("uploadpost"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
           var uid=$("#uuid").val();
          getchat(qid,uid);
   
        }
     });
}
function showchat(qid,uid) {
$("#qid").val(qid);
$("#uuid").val(uid);
  //$("#chbtn").prop('disabled','false');
  getchat(qid,uid);
    $("#chttext").show();
    $("#chbtn").show();
     $("#fchat").show();
    $("#query_id").val(qid);
      $('html, body').animate({
        scrollTop: $("#qchat").offset().top
    }, 2000);
}
function searchbyilc(){
    var silc=$("#silc2").val();
    //alert(silc);
     //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
    $("#qer").empty().append(html);
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("searchbyilc"); ?>',
        data:{
          silc:silc
        },
    dataType:"html",
    cache:false
})//ajax
}

function suggestilc(){
    var silc1=$("#silc1").val();
    
     //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#ilclist1").show().html('loading...........');
        },
                
        success:function(html){
    $("#ilclist1").empty().append(html);
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("suggestilc"); ?>',
        data:{
          silc1:silc1
        },
    dataType:"html",
    cache:false
})//ajax
}

function setilc1(id1){
var ids=id1.split("-");
$("#silc1").val(ids[1]);
$("#silc2").val(ids[0]);
$("#ilclist1").html('');

searchbyilc();
}
function getchat(qid,uid) {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        },
                
        success:function(html){
        $("#qchat").empty().append(html);
        var objDiv = document.getElementById("chatui");
        objDiv.scrollTop = objDiv.scrollHeight;
        },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getchat"); ?>',
        data:{
          qid:qid,uid:uid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function sendchat() {
var qid=$("#qid").val();
var msg=$("#chttext").val();
if(msg=="") {
    alert("Enter message...");
}
else {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
   //  $("#frmc").trigger("reset");
  $("#chttext").val('');
  //  document.getElementById('ilcfrm').reset();
    // $("#qchat").empty().append(html);
  var uid=$("#uuid").val();
   getchat(qid,uid);
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("sendchat"); ?>',
        data:{
          qid:qid,msg:msg
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}

function editquery(id){
    $("#myModal").modal();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("editquery"); ?>',
        data:{
          id:id
        },
    dataType:"html",
    cache:false
})//ajax
}

function updatequery(){
    var qid=$("#qqid").val();
    var date1=$("#datepicker").val();
    var hour=$("#hour").val();
    var min=$("#min").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
   $("#btn-close").click();
     getq();
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatequery"); ?>',
        data:{
          qid:qid,date1:date1,hour:hour,min:min,
        },
    dataType:"html",
    cache:false
})//ajax
}

function timediff(date1,date2){
    var res = Math.abs(date1 - date2) / 1000;
    // get total hours between two dates
    var hours = Math.floor(res / 3600) % 24;        
}

function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qer").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("queryescview"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function closequery(qid) {
var y=confirm("Are you sure?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
   getq();
   
     
        //window.location.href=window.location.href;
        //}
        },
                
        type:'post',
        url:'<? echo $this->createUrl("closequery"); ?>',
        data:{
          qid:qid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}

function approvequery(qid) {
var y=confirm("Are you sure?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
   getq();
   
     
        //window.location.href=window.location.href;
        //}
        },
                
        type:'post',
        url:'<? echo $this->createUrl("approvequery"); ?>',
        data:{
          qid:qid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}
</script>
