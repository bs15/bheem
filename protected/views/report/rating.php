<? $rolid=Yii::app()->user->getState("rolid");
   $ilcid=Yii::app()->user->getState("ilc_id");
   $userid=Yii::app()->user->getState("user_id");
   
   if($rolid=='ilcmanager' ){
    $criteria1 = new CDbCriteria();
    $criteria1->condition='ilcmid=:s';
    $criteria1->params=array(':s'=>$userid);
    $milc = ManagerIlc::model()->findAll($criteria1);
   }
   
   if($rolid=='unit' || $rolid=='partner' ){
    $criteria1 = new CDbCriteria();
    $criteria1->condition='ilcid=:s';
    $criteria1->params=array(':s'=>$ilcid);
    $ilc2 = ManagerIlc::model()->find($criteria1);
    $mname=$ilc2->managername;
    $ilcmid=$ilc2->ilcmid;
   }
?>
<? if($rolid=='director'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
                Reports > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/report/rating">
                    <span style="color:blue">Rating Report</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='academic'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Reports > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/report/rating">
                    <span style="color:blue">Rating Report</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='ilcmanager'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
                Reports > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/report/rating">
                    <span style="color:blue">Rating Report</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='unit'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Reports > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/report/rating">
                    <span style="color:blue">Rating Report</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='partner'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/index">Home</a> >
                Reports > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/report/rating">
                    <span style="color:blue">Rating Report</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    ?>
<div class="row">
    <br style="clear:both">
    <h1 style="margin-left:2%;">Rating Report</h1>
</div>
<div class="row">
    <br style="clear:both">
    <? if($rolid=='director' || $rolid=='academic'){ ?>
     <div class="col-md-2">
        <select id="st" class="enin" onchange="getcity()">
            <option value="0">Select State</option>
            <? foreach($states as $st){
                ?>
                <option value="<?=$st->state ?>"><?=$st->state ?></option>
                <?
            } ?>
        </select>
    </div>
    <div class="col-md-2" id="loadcity">
        <select id="ci" class="enin" onchange="getres()">
            <option value="0">Select City</option>
        </select> 
    </div>
     <? } ?>
    <div class="col-md-2">
        <? if($rolid=='director' || $rolid=='academic'){ ?>
        <select id="ilcm" class="enin" onchange="getres()">
            <option value="0">Select ILC Manager</option>
            <? foreach($ilcm as $i){
                ?>
                <option value="<?=$i->userid ?>"><?=$i->name ?></option>
                <? } ?>
        </select>
        <? }
        else if($rolid=='ilcmanager') { ?>
           <select id="ilcm" class="enin" onchange="getres()">
            <option value="<?=$userid ?>"><?=Yii::app()->user->getState('name') ?></option>
           </select>
        <? }
        else if($rolid=='unit' || $rolid=='partner') { ?>
           <select id="ilcm" class="enin" onchange="getres()">
               <option value="0">Select Manager</option>
            <option value="<?=$ilcmid ?>"><?=$mname ?></option>
           </select>
        <? }
        ?>
    </div>
    <div class="col-md-2">
       <select id="yr" class="enin" onchange="getres()">
            <option value="0">Select Year</option>
            <? for($i=2019;$i<=date('Y');$i++){
                ?>
                <option value="<?=$i ?>"><?=$i ?></option>
                <?
            } ?>
        </select> 
    </div>
    <div class="col-md-2">
       <select id="mon" class="enin" onchange="getres()">
            <option value="0">Select Month</option>
            <option value="1">Jan</option>
            <option value="2">Feb</option>
            <option value="3">Mar</option>
            <option value="4">Apr</option>
            <option value="5">May</option>
            <option value="6">Jun</option>
            <option value="7">Jul</option>
            <option value="8">Aug</option>
            <option value="9">Sep</option>
            <option value="10">Oct</option>
            <option value="11">Nov</option>
            <option value="12">Dec</option>
        </select> 
    </div>
    <div class="col-md-2">
       <select id="wee" class="enin" onchange="getres()">
            <option value="0">Select Week</option>
            <option value="1">1st Week</option>
            <option value="2">2nd Week</option>
            <option value="3">3rd Week</option>
            <option value="4">4th Week</option>
            <option value="5">5th Week</option>
        </select> 
    </div>
    
</div>
<div class="row" >
    <div class="col-md-12" align="right" id="showexp">
        <button class="btn-green" onclick="exprint()">Export</button>
    </div>
</div>
<div class="row table-responsive" id="tres">
    
</div>
<script>
$(document).ready(function(){
 $('#showexp').hide();
 getres();
});
    
 function exprint(){
    var il='0';
    var st='0';
    var ci='0';
    var ilcm=$("#ilcm option:selected").val();
    var yr=$("#yr option:selected").val();
    var mon=$("#mon option:selected").val();
    var wee=$("#wee option:selected").val();
    
    
    var url = "<?=Yii::app()->request->baseUrl ?>/index.php/export/index?il="+il+"&yr="+yr+"&mon="+mon+"&wee="+wee+"&st="+st+"&ci="+ci+"&rtype=rating&ilcm="+ilcm;
    window.open(url, '_blank');
 } 
 
 function getcity(){
     var st1=$('#st option:selected').val();
     if(st1=='0')
     {
         alert('Select a state ');
     }
     else
     {
         //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#loadcity").show().html('loading...........');
        },
        success:function(html){
        $("#loadcity").empty().append(html);
        getres();
     
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getcity"); ?>',
        data:{
          st1:st1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
 
 function getilc(){
     var ci1=$('#ci option:selected').val();
     if(ci1=='0')
     {
         alert('Select a city ');
     }
     else
     {
        //////////////////////ajax////////////////////////
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#loadilc").show().html('loading...........');
        },
        success:function(html){
        $("#loadilc").empty().append(html);
        getres();
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("getilc"); ?>',
        data:{
          ci1:ci1,
        },
    dataType:"html",
    cache:false
})//ajax
     }
 } 
 
 function getres(){
   var yr=$("#yr option:selected").val();
    var mon=$("#mon option:selected").val();
    var wee=$("#wee option:selected").val();
    var ilcm=$("#ilcm option:selected").val();
    var il='0';
    var st=$("#st option:selected").val();;
    var ci=$("#ci option:selected").val();
    //alert("state="+st+" city="+ci+" ilcm="+ilcm+" ilc="+il+" year="+yr+" month="+mon+" week="+wee);
    
    if(st==undefined){
        st='0';
    }
    if(ci==undefined){
        ci='0';
    }
    if(ilcm==undefined){
        ilcm='0';
    }
    if(yr==undefined){
        yr='0';
    }
    if(mon==undefined){
        mon='0';
    }
    if(wee==undefined){
        wee='0';
    }
        //////////////////////ajax////////////////////////
        //alert("ilcm="+ilcm+" ilc="+il+" year="+yr+" month="+mon+" week="+wee);
        $.ajax({
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#tres").show().html('loading...........');
        },
        success:function(html){
        $('#showexp').show();    
        $("#tres").empty().append(html);
        },
        error:function(jqXHR, exception) {
        alert(jqXHR.responseText+' '+exception);
        },        
        type:'post',
        url:'<? echo $this->createUrl("tres3"); ?>',
        data:{
          il:il,yr:yr,mon:mon,wee:wee,ci:ci,st:st,ilcm:ilcm
        },
    dataType:"html",
    cache:false
})//ajax
    
 }
 
 
</script>