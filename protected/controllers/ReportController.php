<?php

class ReportController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
	public function actionIlc()
	{
            $criteria1 = new CDbCriteria();
            $criteria1->select = 'state';
            $criteria1->group="state";
            $states = Ilc::model()->findAll($criteria1);
            
            $this->render('ilc',array('states'=>$states));
	}
        
        public function actionWeekly()
	{
            $criteria1 = new CDbCriteria();
            $criteria1->condition = 'role=:r';
            $criteria1->params=array(':r'=>'ilcmanager');
            $ilcm = Users::model()->findAll($criteria1);
            
            $criteria1 = new CDbCriteria();
            $criteria1->select = 'state';
            $criteria1->group="state";
            $states = Ilc::model()->findAll($criteria1);
            
            $this->render('weekly',array('ilcm'=>$ilcm,'states'=>$states));
	}
        
        public function actionRating()
	{
            $criteria1 = new CDbCriteria();
            $criteria1->condition = 'role=:r';
            $criteria1->params=array(':r'=>'ilcmanager');
            $ilcm = Users::model()->findAll($criteria1);
            
            $criteria1 = new CDbCriteria();
            $criteria1->select = 'state';
            $criteria1->group="state";
            $states = Ilc::model()->findAll($criteria1);
            
            $this->render('rating',array('ilcm'=>$ilcm,'states'=>$states));
	}
        
        public function actionEscalation()
	{
            $criteria1 = new CDbCriteria();
            $criteria1->condition = 'role=:r';
            $criteria1->params=array(':r'=>'ilcmanager');
            $ilcm = Users::model()->findAll($criteria1);
            
            $criteria1 = new CDbCriteria();
            $criteria1->select = 'state';
            $criteria1->group="state";
            $states = Ilc::model()->findAll($criteria1);
            
            $this->render('escalation',array('ilcm'=>$ilcm,'states'=>$states));
	}
        
        public function actionGetcity()
	{
            if(Yii::app()->request->isPostRequest) {
            $st1=trim($_POST['st1']);

            $criteria1 = new CDbCriteria();
            $criteria1->condition='state=:s';
            $criteria1->select='city';
            $criteria1->params=array(':s'=>$st1);
            $criteria1->group="city";
            $cities = Ilc::model()->findAll($criteria1);
            
            ?>
            <select id="ci" class="enin" onchange="getilc()">
            <option value="0">Select City</option>
            <? foreach($cities as $ci){
                ?>
                <option value="<?=$ci->city ?>"><?=$ci->city ?></option>
                <?
            } ?>
        </select> 
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
	}
        
         public function actionGetilc()
	{
            if(Yii::app()->request->isPostRequest) {
            $ci1=trim($_POST['ci1']);

            $criteria1 = new CDbCriteria();
            $criteria1->condition='city=:s';
            $criteria1->params=array(':s'=>$ci1);
            $ilcs = Ilc::model()->findAll($criteria1);
            
            ?>
            <select id="il" class="enin" onchange="getres()">
            <option value="0">Select ILC</option>
            <? foreach($ilcs as $il){
                ?>
                <option value="<?=$il->ilcid ?>"><?=$il->ins_name ?></option>
                <?
            } ?>
        </select> 
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}
	}
        
        public function actionTres4(){
            if(Yii::app()->request->isPostRequest) {
           $il=trim($_POST['il']);
           $yr=trim($_POST['yr']);
           $mon=trim($_POST['mon']);
           $wee=trim($_POST['wee']);
           $ci=trim($_POST['ci']);
           $st=trim($_POST['st']);
           $ilcm=trim($_POST['ilcm']);

           $r=new Report();
           $r->escalationreport($il,$yr,$mon,$wee,$ci,$st,$ilcm);
           }
           else
           {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        
        public function actionTres3(){
           if(Yii::app()->request->isPostRequest) {
           $il=trim($_POST['il']);
           $yr=trim($_POST['yr']);
           $mon=trim($_POST['mon']);
           $wee=trim($_POST['wee']);
           $ci=trim($_POST['ci']);
           $st=trim($_POST['st']);
           $ilcm=trim($_POST['ilcm']);
            $r=new Report();
           $r->ratingreport($il,$yr,$mon,$wee,$ci,$st,$ilcm);
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}

        }
        
        public function actionTres2(){
           if(Yii::app()->request->isPostRequest) {
           $il=trim($_POST['il']);
           $yr=trim($_POST['yr']);
           $mon=trim($_POST['mon']);
           $wee=trim($_POST['wee']);
           $ci=trim($_POST['ci']);
           $st=trim($_POST['st']);
           $ilcm=trim($_POST['ilcm']);
            $r=new Report();
           $r->weeklyreport($il,$yr,$mon,$wee,$ci,$st,$ilcm);
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}

        }
        
        public function actionTres(){
            if(Yii::app()->request->isPostRequest) {
           $il=trim($_POST['il']);
           $yr=trim($_POST['yr']);
           $mon=trim($_POST['mon']);
           $wee=trim($_POST['wee']);
           $ci=trim($_POST['ci']);
           $st=trim($_POST['st']);
           
           $r=new Report();
           $r->ilcreport($il,$yr,$mon,$wee,$ci,$st);
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
		}

        }
        
         public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
       
}