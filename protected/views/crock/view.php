<?php
/* @var $this CrockController */
/* @var $model Crock */

$this->breadcrumbs=array(
	'Crocks'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Crock', 'url'=>array('index')),
	array('label'=>'Create Crock', 'url'=>array('create')),
	array('label'=>'Update Crock', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Crock', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Crock', 'url'=>array('admin')),
);
?>

<h1>View Crock #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cname',
	),
)); ?>
