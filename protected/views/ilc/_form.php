<?php
/* @var $this IlcController */
/* @var $model Ilc */
/* @var $form CActiveForm */
$ilcid=uniqid();
Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ilc-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array(
        'enctype' => 'multipart/form-data',)
    
)); ?>

	<p class="table-font">Fields with <span class="required">*</span> are required.</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row">
		<?php 
                if($model->isNewRecord)
                    echo $form->hiddenField($model,'ilcid',array('size'=>60,'maxlength'=>100,'value'=>$ilcid));
                else
                    echo $form->hiddenField($model,'ilcid',array('size'=>60,'maxlength'=>100));
                ?>
            
		<?php echo $form->error($model,'ilcid'); ?>
	</div>
        
        <div class="row " style="margin-left:2%">
            <h5>Basic Information</h5>
        </div>
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'address'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textField($model,'address',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'address'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'partner_name'); ?></div>
                <div class="col-md-7">
                    <div class="input-group input-group-sm">
                        <? if($model->isNewRecord) { ?>
                    <?php echo $form->dropDownList($model,'partner_name',$model->getPartners(),array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'partner_name',array("class"=>"form-control")); ?> 
                        <? }
                        else{
                            ?>
                        <?php echo $form->textField($model,'partner_name',array("class"=>"form-control",'value'=>$model->partner_name,'readonly'=>true)); ?>
                            <?
                        }
                        ?>
                    </div>
                </div>
                </div>
            </div>
        </div>
        
        
        
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'ins_name'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textField($model,'ins_name',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'ins_name'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'emails'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textField($model,'emails',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'emails'); ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'phones'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textField($model,'phones',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'phones'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'othf'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textField($model,'othf',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'othf'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <hr>
        
        
        <div class="row " style="margin-left:2%">
            <h5>Agreement</h5>
        </div>
        
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'agreement_date'); ?></div>
                    <div class="col-md-7 ">
                        <div class="input-group input-group-sm form-control">
                         <?php $this->widget('CJuiDateTimePicker',
                        array('model'=>$model,'attribute'=>'agreement_date', //attribute name
                        'mode'=>'date', 
                        'options'=>array('style'=>array("class"=>"form-control"),
                            'showTime'=>false,
                             'mode'=>'focus',
                             'showAnim' => 'slideDown',),
                              'language'=>''

                ));
        ?>
                        <?php //echo $form->textField($model,'agreement_date',array("class"=>"form-control datepicker")); ?>
                        <?php echo $form->error($model,'agreement_date'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'agreement_valid'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm form-control">
                         <?php $this->widget('CJuiDateTimePicker',
                        array('model'=>$model,'attribute'=>'agreement_valid', //attribute name
                        'mode'=>'date', 
                                'language'=>''
                        ));?>
                        <?php //echo $form->textField($model,'agreement_valid',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'agreement_valid'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'agreement'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm ">
                            <?php     echo CHtml::activeFileField($model,'agreement',array("class"=>"form-control"));  ?>
                        <?php echo $form->error($model,'agreement'); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <hr>
        
        <div class="row " style="margin-left:2%">
            <h5>Junior Preschool Program</h5>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'junior_pres_prog'); ?></div>
                        <div class="col-md-7">
                            <div class="input-group input-group-sm">
                            <?php echo $form->textArea($model,'junior_pres_prog',array("class"=>"form-control")); ?>
                            <?php echo $form->error($model,'junior_pres_prog'); ?>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey1"><?php echo $form->labelEx($model,'preschool_valid_from'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm form-control">
                         <?php $this->widget('CJuiDateTimePicker',
                        array('model'=>$model,'attribute'=>'preschool_valid_from', //attribute name
                        'mode'=>'date', 
                                'language'=>''
                        ));?>
                        <?php //echo $form->textField($model,'preschool_valid_from',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'preschool_valid_from'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        
        
        <div class="row " style="margin-left:2%">
            <h5>Senior Preschool Program</h5>
        </div>
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'senior_pres_prog'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textArea($model,'senior_pres_prog',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'senior_pres_prog'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey1"><?php echo $form->labelEx($model,'senior_pres_valid_from'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm form-control">
                         <?php $this->widget('CJuiDateTimePicker',
                        array('model'=>$model,'attribute'=>'senior_pres_valid_from', //attribute name
                        'mode'=>'date', 
                                'language'=>''
                        ));?>
                        <?php //echo $form->textField($model,'senior_pres_valid_from',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'senior_pres_valid_from'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
       
       <div class="row" style="margin-left:2%">
            <h5>Teachers' Training Program</h5>
        </div> 
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-6">
                <div class="row">
                   <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'teacher_training_prog'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textArea($model,'teacher_training_prog',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'teacher_training_prog'); ?>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-4 back-grey1"><?php echo $form->labelEx($model,'teacher_training_valid_from'); ?></div>
                    <div class="col-md-7">
                         <div class="input-group input-group-sm form-control">
                         <?php $this->widget('CJuiDateTimePicker',
                        array('model'=>$model,'attribute'=>'teacher_training_valid_from', //attribute name
                        'mode'=>'date', 
                                'language'=>''
                        ));?>
                        <?php //echo $form->textField($model,'teacher_training_valid_from',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'teacher_training_valid_from'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        
        <div class="row" style="margin-left:2%">
            <h5>Location</h5>
        </div> 
       <div class="row" style="margin-bottom:15px">
           <div class="col-md-4">
               <div class="row">
                   <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'country'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textArea($model,'country',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'country'); ?>
                        </div>
                    </div>
               </div>
           </div>
            <div class="col-md-4">
                <div class="row">
                   <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'city'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textArea($model,'city',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'city'); ?>
                        </div>
                    </div> 
                </div>
            </div>
           <div class="col-md-4">
               <div class="row">
                   <div class="col-md-4 back-grey"><?php echo $form->labelEx($model,'state'); ?></div>
                    <div class="col-md-7">
                        <div class="input-group input-group-sm">
                        <?php echo $form->textArea($model,'state',array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'state'); ?>
                        </div>
                    </div>
               </div>
           </div>
        </div>
        <hr>
        
             
	 <div class="row" style="margin-left:2%">
            <h5>Type of ILC</h5>
        </div> 
        <div class="row" style="margin-bottom:15px">
            <div class="col-md-4">
                <div class="row">
                   <div class="col-md-7 back-grey"><?php echo $form->labelEx($model,'preschool'); ?></div>
                    <div class="col-md-4">
                        <div class="input-group input-group-sm">
                        <?php echo $form->dropDownList($model,'preschool',array('0'=>'NO','1'=>'YES'),array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'preschool'); ?>
                        </div>
                    </div> 
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-7 back-grey"><?php echo $form->labelEx($model,'afterschool_activity'); ?></div>
                    <div class="col-md-4">
                        <div class="input-group input-group-sm">
                        <?php echo $form->dropDownList($model,'afterschool_activity',array('0'=>'NO','1'=>'YES'),array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'afterschool_activity'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                   <div class="col-md-7 back-grey"><?php echo $form->labelEx($model,'teacher_training'); ?></div>
                    <div class="col-md-4">
                        <div class="input-group input-group-sm">
                        <?php echo $form->dropDownList($model,'teacher_training',array('0'=>'NO','1'=>'YES'),array("class"=>"form-control")); ?>
                        <?php echo $form->error($model,'teacher_training'); ?>
                        </div>
                    </div> 
                </div>
            </div>
            
        </div>
	
	

        <div class="row buttons" style="margin:2%">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array("class"=>"btn btn-dark")); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
    $(document).ready(function(){
        //alert("hiiii");
        $(".datepicker").datepicker();
    });
</script>