<?php

/**
 * This is the model class for table "query".
 *
 * The followings are the available columns in table 'query':
 * @property string $qid
 * @property string $userid
 * @property string $query_topic
 * @property string $schedule_call_date
 * @property string $schedule_call_time
 * @property string $query_date
 * @property integer $ilc_approved
 * @property integer $query_closed
 * @property string $ilc_id
 * @property string $tags
 * @property integer $month
 * @property integer $year
 */
class Query extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Query the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'query';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qid, userid, query_topic, schedule_call_date, schedule_call_time, query_date, ilc_approved, query_closed, ilc_id, ilcmid, rating, ilcid, tags, month, year, clodate,state,city', 'required'),
			array('id,ilc_approved, query_closed,week, month, year, esc, rating', 'numerical', 'integerOnly'=>true),
			array('qid, userid, ilc_id', 'length', 'max'=>100),
			array('query_date', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,qid, userid, query_topic, schedule_call_date, schedule_call_time, query_date, ilc_approved, query_closed, ilc_id, ilcid,tags,week, month, year, esc, clodate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'qid' => 'Qid',
			'userid' => 'Userid',
			'query_topic' => 'Query Topic',
			'schedule_call_date' => 'Schedule Call Date',
			'schedule_call_time' => 'Schedule Call Time',
			'query_date' => 'Query Date',
			'ilc_approved' => 'Ilc Approved',
			'query_closed' => 'Query Closed',
			'ilc_id' => 'Ilc',
                        'ilcmid' => 'Ilc Manager ID',
                        'rating' => 'Rating',
                        'ilcid' => 'Ilcid',
			'tags' => 'Tags',
                        'week' => 'Week',
			'month' => 'Month',
			'year' => 'Year',
                        'clodate' => 'Closed Date',
                        'state' => 'State',
                        'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('qid',$this->qid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('query_topic',$this->query_topic,true);
		$criteria->compare('schedule_call_date',$this->schedule_call_date,true);
		$criteria->compare('schedule_call_time',$this->schedule_call_time,true);
		$criteria->compare('query_date',$this->query_date,true);
		$criteria->compare('ilc_approved',$this->ilc_approved);
		$criteria->compare('query_closed',$this->query_closed);
		$criteria->compare('ilc_id',$this->ilc_id,true);
                $criteria->compare('ilcmid',$this->ilcmid,true);
                $criteria->compare('rating',$this->rating,true);
                $criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('tags',$this->tags,true);
                $criteria->compare('week',$this->week);
		$criteria->compare('month',$this->month);
		$criteria->compare('year',$this->year);
                $criteria->compare('clodate',$this->clodate);
                $criteria->compare('state',$this->state);
                $criteria->compare('city',$this->city);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}