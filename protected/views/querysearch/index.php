<? $rolid=Yii::app()->user->getState("rolid"); ?>
<? if($rolid=='director'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
                Query Search > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/querysearch/index">
                    <span style="color:blue">Search</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='academic'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Query Search > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/querysearch/index">
                    <span style="color:blue">Search</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='ilcmanager'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilcmanager/index">Home</a> >
                Query Search > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/querysearch/index">
                    <span style="color:blue">Search</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='unit'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/unit/index">Home</a> >
                Query Search > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/querysearch/index">
                    <span style="color:blue">Search</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    if($rolid=='partner'){ ?>
    <div class="row">
        <div class="col-md-12" >
            <br>
            <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/partner/index">Home</a> >
                Query Search > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/querysearch/index">
                    <span style="color:blue">Search</span>
                </a>
            </span>
        </div> 
    </div>
    <? }
    ?>
<div class="row">
    <div class="col-md-9">
    <h1>Search QUERIES</h1>
            <div class="row">
                <div class="col-md-3" style="margin-left: 2%">
                <select class="enin table-font" onchange="searchbyilc()" id="silc">
                <option value="Search by ILC">Search by ILC</option>
                <? foreach($res as $r) 
                { ?>
                <option value="<?=$r->ilcid ?>" style="text-transform: uppercase"><?=$r->ins_name ?></option>   
               <? }
                ?>
                
                
            </select>
            </div>
              <div class="col-md-4">
                  <input type="text" id="qsearch" class="enin table-font" placeholder="Keywords / Tags" title="Search by Keywords.." onkeyup="searchbykey()" />
                  <div id="errs" class="alert alert-danger"></div>
              </div>
                    <div class="col-md-4">
                        <input type="button" class="btn-green btn-sm" value="Reset" onclick="rset()" />
              </div>
            </div>
    <div id="qer" >
                
            </div>
        
    </div>
    <div class="col-md-3 col-sm-12">
        <input type="text" id="csearch" onkeyup="searchchat()" placeholder="Type chat search keyword" style="width: 99%; padding: 5px; border: 1px solid black;margin-bottom: 3px" />
        <div id="err3" class="alert alert-danger"></div>
        <div id="qchat">
            
        </div> 
        <div class="fchat" id="fchat">
            <form id="sform">
                <input type="hidden" id="query_id" value="-" />
                <div style="display: none">           
                    <input type="file" id="cfile" onchange="uploadfile()" /></div>
<!--                <button onclick="upfile()" type="button" style="padding: 5px"> <i class="glyphicon glyphicon-paperclip"></i></button>
                <br>-->
                <!--<input type="button" value="Send File" class="btn btn-success" onclick="uploadfile()"/>-->
                <br>
            </form>
            <!--end widget-->
        </div>
        
        <div class="chtxt">
            <form id="frmc">
                <input type="hidden" id="qid" value="-" />
                 <input type="hidden" id="uuid" value="-" />
                <div class="form-group">
                    <textarea class="form-control" id="chttext" placeholder="type message..."></textarea>
                </div>
                <div class="form-group">
                    <input type="button" id="chbtn" class="btn btn-outline-dark btn-sm" onclick="sendchat()" value="SEND" />
                </div>
            </form>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" id="btn-close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change Query Date/Time</h4>
      </div>
      <div class="modal-body" id="body1">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $("#errs").hide();
  $("#err3").hide();
  $("#csearch").hide();
    getq();
    $("#chttext").hide();
    $("#chbtn").hide();
    $("#fchat").hide();
//    setInterval(function(){
//     var qid=$("#qid").val();
//     var uid=$("#uuid").val();
//     if(qid!="-") {
//     getchat(qid,uid)  
//    }
//},10000);
});

//////////////////////////////////////////////////////////'
function upfile() {
    $('#cfile').click();
}

function uploadfile() {
      var file_data = $('#cfile').prop('files')[0];   
      var qid = $('#query_id').val();
    var form_data = new FormData();                  
    form_data.append('file', file_data);
    form_data.append('qid', qid);
   // alert(form_data);                             
    $.ajax({
        url: '<? echo $this->createUrl("uploadpost"); ?>', // point to server-side PHP script 
        dataType: 'html',  // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(deer){
         //   alert(php_script_response); // display response from the PHP script, if any
           var uid=$("#uuid").val();
          getchat(qid,uid);
   
        }
     });
}

function searchbyilc(){
    var silc=$("#silc option:selected").val();
    
     //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
    $("#qer").empty().append(html);
     },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("searchbyilc"); ?>',
        data:{
          silc:silc
        },
    dataType:"html",
    cache:false
})//ajax
}
////////////////////////////////////////////////////////////
function editquery(id){
    $("#myModal").modal();
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
     
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("editquery"); ?>',
        data:{
          id:id
        },
    dataType:"html",
    cache:false
})//ajax
}

function updatequery(){
    var qid=$("#qqid").val();
    var date1=$("#datepicker").val();
    var hour=$("#hour").val();
    var min=$("#min").val();
    
    //////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#body1").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#body1").empty().append(html);
   $("#btn-close").click();
     getq();
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("updatequery"); ?>',
        data:{
          qid:qid,date1:date1,hour:hour,min:min,
        },
    dataType:"html",
    cache:false
})//ajax
}

function getq() {
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qer").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("getquery"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function searchbykey() {

var skey=$("#qsearch").val();
if(skey=="" || skey==null) {
   $("#errs").show().html("Please enter search key");
}
else {
    $("#errs").hide();
 ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
     $("#qer").empty().append(html);
   
     
        //window.location.href=window.location.href;
        //}
        },
         error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },        
        type:'post',
        url:'<? echo $this->createUrl("searchquery"); ?>',
        data:{
          skey:skey
        },
    dataType:"html",
    cache:false
})//ajax
}           
                 
                 ///////////////////////////////////////////////////
}

function approvequery(qid) {
var y=confirm("Are you sure?");
if(y) {
        ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('loading...........');
        },
                
        success:function(html){
 ///    $("#qfrm").trigger("reset");
  //  document.getElementById('ilcfrm').reset();
   getq();
   
     
        //window.location.href=window.location.href;
        //}
        },
                
        type:'post',
        url:'<? echo $this->createUrl("approvequery"); ?>',
        data:{
          qid:qid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}

function showchat(qid,uid) {
$("#qid").val(qid);
$("#uuid").val(uid);
  //$("#chbtn").prop('disabled','false');
  getchat(qid,uid);
//    $("#chttext").show();
//    $("#chbtn").show();
     $("#fchat").show();
     $("#csearch").show();
     $("#query_id").val(qid);
      $('html, body').animate({
        scrollTop: $("#chatui").offset().top
    }, 2000);
}

function getchat(qid,uid) {
        $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
    },
                
        success:function(html){
     $("#qchat").empty().append(html);
     var objDiv = document.getElementById("chatui");
     objDiv.scrollTop = objDiv.scrollHeight;
   },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("getchat"); ?>',
        data:{
          qid:qid,uid:uid
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}

function searchchat() {
  var qid=$("#qid").val();
var uid=$("#uuid").val();
var skey=$("#csearch").val();
if(skey=="" || skey==null)
{
    $("#err3").show().html('Please enter search keyword');
}
else {
 $("#err3").hide();
    $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
    },
                
        success:function(html){
     $("#qchat").empty().append(html);
     var objDiv = document.getElementById("chatui");
     objDiv.scrollTop = objDiv.scrollHeight;
   },
                    error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("searchchat"); ?>',
        data:{
          qid:qid,uid:uid,skey:skey
        },
    dataType:"html",
    cache:false
})//ajax
                 
}           
                 ///////////////////////////////////////////////////
}

function sendchat() {
var qid=$("#qid").val();
var msg=$("#chttext").val();
if(msg=="") {
    alert("Enter message...");
}
else {
     ///////////////////////ajax////////////////////////
                   $.ajax({
        
        beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
   //   $("#qer").show().html('loading...........');
        },
                
        success:function(html){
   //  $("#frmc").trigger("reset");
  $("#chttext").val('');
  //  document.getElementById('ilcfrm').reset();
    // $("#qchat").empty().append(html);
  var uid=$("#uuid").val();
   getchat(qid,uid);
     
        //window.location.href=window.location.href;
        //}
        },
                   error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },    
        type:'post',
        url:'<? echo $this->createUrl("sendchat"); ?>',
        data:{
          qid:qid,msg:msg
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
                 }
}

function rset() {
window.location.href=window.location.href;
}
</script>
