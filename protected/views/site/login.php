<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Bheem - Login</title>
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"> -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
  <style type="text/css">
      body{
        background: #4B79A1;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #283E51, #4B79A1);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #283E51, #4B79A1); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      }
      .centerDiv{
        margin-top: 5em;
      }
      .loginsec{
        background: #DAE2F8;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #D6A4A4, #DAE2F8);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #D6A4A4, #DAE2F8); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

      }
  </style>
</head>
<body>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
            'validateOnSubmit'=>true,
        ),
    )); ?>
    <div class="container">
        <div class="col-md-6 col-sm-12 mx-auto centerDiv">
            <h2 class="text-center font-weight-bold" style="color: #fff;">Login With Your Bheem Account</h2>
            <div class="card shadow loginsec">
                <div class="card-body text-center">
                    <img height="auto" width="auto" class="mb-3" src="<?=Yii::app()->request->baseUrl ?>/images/bheem/bheem_logo.png">
                    <form class="form">
                        <div class="form-group">
                            <?php echo $form->textField($model,'username',array("class"=>"ft form-control","placeholder"=>" Username *")); ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $form->error($model,'username',array("class"=>"er")); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo $form->passwordField($model,'password',array("class"=>"ft form-control","placeholder"=>" Password *")); ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $form->error($model,'password',array("class"=>"er")); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                             <div class="text-center">
                                <?php echo CHtml::submitButton('Login',array('class'=>'btn btn-success')); ?>
                            </div>
                        </div>  
                    </form>
                    <a class="forgot text-center" style="color: black;" href="<?=Yii::app()->request->baseUrl ?>/index.php/site/forgot">Forgot Password ?</a>
                </div>
            </div>
        </div>
    </div>
</body>
<?php $this->endWidget(); ?>