<?php

class AcademicController extends Controller
{
        public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
         public function accessRules()
                        	{

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolid';
    	$criteria1->params = array(':rolid'=>'academic');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
                
    
    	
    	return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','ilcmanager','createilcm','getilcm','delilcm'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('partner','getpartner','createpartner','assignilc','assignm','assignedlist','deleteassigned'),
    		'users'=>$modad,
    	),
            
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('queryhistory','getquery3','getchat,','getquery','getqueryilc','queryesc','queryescview','sendchat','getchat','getqmy'),
    		'users'=>$modad,
    	),
            
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('showfeed','setnoty','checknum','nlist','uploadpost','searchbyilc','suggestilc'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('interview','interview2','getintilc','intlist1','intlist2','getimy','manageint','updateint','approveint','searchbyilc1','suggestilc1'),
    		'users'=>$modad,
    	),
            array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('holiday','mleave','hol','setleave','mcal','managers','getcity','getilc','tres'),
    		'users'=>$modad,
    	),
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        public function actionInterview() {
            $this->render('interview');
        }
        public function actionTres($il,$ci,$st){
           $r=new Nlist();
           $r->queryesc($il,$ci,$st);
        }
        public function actionGetcity()
        {
            if(Yii::app()->request->isPostRequest) {
            $st1=trim($_POST['st1']);

            $nl= new Nlist();
            $nl->getcity($st1);
            
	}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
         public function actionGetilc()
	{
            if(Yii::app()->request->isPostRequest) {
             $ci1=trim($_POST['ci1']);
             $nl= new Nlist();
             $nl->getilc($ci1);
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
	}
        public function actionMcal() {
            $man=$_REQUEST['q'];
            Yii::app()->user->setState('man',$man); 
            $this->render('mcal',array('man'=>$man));
        }
        public function actionManagers() {
            $this->render("managers");
        }

        public function actionInterview2() {
            $this->render('interview2');
        }
        
        public function actionMleave() {
            $uid=$_REQUEST['q'];
            
            $this->render('mleave',array('man'=>$uid));
        }
        
        public function actionGetimy() {
         if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);
   
        $y=  intval($yea);
        $m= intval($mon);
      
      
        $crq=new CDbCriteria();
        if($y!=0 && $m==0 )
        {
           $crq->condition='year=:y and iclosed=1';
        $crq->params=array(':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
         $crq->condition='year=:y and month=:m and iclosed=1';
        $crq->params=array(':m'=>$m,':y'=>$y);
        }
      $crq->order='idate1 desc';
        $interviews= Interview::model()->findAll($crq);
        $n=sizeof($interviews);
        if($n==0) {
            ?> <p> No Interviews have been conducted yet</p> <?
        }
        else 
        {
            ?>
                <div class="table table-striped table-font"  >  
                <table>
                    <tr class="bold">
                        <td><b>Candidate</b></td>
                        <td><b>Resume</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Conducted by</b></td>
                        <td><b>Manager's Feedback</b></td>
                    </tr>
                    <?
            foreach ($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
//                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                ?>
            <tr>
                 <td><?=$i->candidatename ?>
                            
                        </td>
                        <td>
                            <a style="font-size:14px !important; padding:4px;border-radius:2px;border:1px solid black" class="btn-light" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a></td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? } ?>                           
                        </td>
                         
                    </tr>

<?
            } ?>
                </table>
                    </div><?
                    
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
        public function actionGetqmy() {
         if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);
   
        $y=intval($yea);
        $m=intval($mon);
        
        $crq=new CDbCriteria();
        if($y!=0 && $m==0 )
        {
           $crq->condition='year=:y and query_closed=1';
        $crq->params=array(':y'=>$y);   
        }
        else if($y!=0 && $m!=0)
        {
         $crq->condition='year=:y and month=:m and query_closed=1';
        $crq->params=array(':m'=>$m,':y'=>$y);
        }
      $crq->order='query_date desc';
        $queries= Query::model()->findAll($crq);
        $n=sizeof($queries);
        if($n==0) {
            ?> <p> No Queries were raised in this month !</p> <?
        }
        
        
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>
           <? foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr>
                    <?
                }
            
            ?>
                </table>
           </div>
            <?
          }
      }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }    
 }
        public function actionGetintilc() {
            if(Yii::app()->request->isPostRequest) {
           $ci=trim($_POST['ci']);
           $st=trim($_POST['st']);
           $il=trim($_POST['il']);
                //echo $ci." ".$st." ".$il;
                $crq=new CDbCriteria();
               if($ci=="0" && $il=="0")
               {
                  $crq->condition='iclosed=0 and state=:s';
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($il=="0"){
                   $crq->condition='iclosed=0 and state=:s and city=:c';
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                    $crq->condition='iclosed=0 and ilcid=:i';
                    $crq->params=array(':i'=>$il);
               }
                
                $crq->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq);
                $n=sizeof($interviews);
                
                if($n==0) {
                    
                    ?>
            <p>No interviews have escalated from this ILC </p>
            <?
                 }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                    </tr>
            <?
                foreach($interviews as $i){
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);  
                    
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                
                $crid=$i->userid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$crid);
                $user2= Users::model()->find($crq1);
                $uname1=$user2->name;
                $rolex=$user2->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                 if($rolex=="unit"){
                    $rolex="Coordinator";
                }
                else if($rolex=="partner"){
                    $rolex="Partner";
                }
                
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                $crq=new CDbCriteria();
                $crq->condition='ilcid=:u';
                $crq->params=array(':u'=>$ilc);
                $ilcs= ManagerIlc::model()->find($crq);
                
                
                $idate=$i->idate;
                $idate1=intval(strtotime($idate));
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>48 || $d==48)
                {
                   
                ?>
                    <td><span style="text-transform:uppercase"><?=$ilcs->ilcname ?></span></td>
                        <td><?=$uname1 ."[".$rolex."]" ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br>
                            
                            <a style="margin-top:3%;font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? 
                            if($is->communication=='' && $is->academic_knowledge=='' && $is->attitude=='' && $i->icomment==''  ){
                                
                                ?>
                            <button id="<?=$intid ?>" onclick="manageint(this.id)" >Assign Manager</button> 
                                <? } 
                            if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { 
                             
                                ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td> <?
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if ($istatus=='1' && $d>=48) {
                             ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            ?>
                        </td>
                    
                    </tr>
            

<?
 }
                
            } ?>
                </table>
            </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        public function actionIntlist1() {
            if(Yii::app()->request->isPostRequest) {
           

                $crq1=new CDbCriteria();
                $crq1->condition='iclosed=0';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                if($n==0) {
                 }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                    </tr>
            <?
               
            
                foreach($interviews as $i){
                //echo $d;  
                $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri);  
                    
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                $uname=$user1->name;
                $role=$user1->role;
                
                $crid=$i->userid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$crid);
                $user2= Users::model()->find($crq1);
                $uname1=$user2->name;
                $rolex=$user2->role;
                
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                if($rolex=="unit"){
                    $rolex="Coordinator";
                }
                else if($rolex=="partner"){
                    $rolex="Partner";
                }
                
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                $crq=new CDbCriteria();
                $crq->condition='ilcid=:u';
                $crq->params=array(':u'=>$ilc);
                $ilcs= ManagerIlc::model()->find($crq);
                
                
                $idate=$i->idate;
                $idate1=intval(strtotime($idate));
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$idate1; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                //echo $d.", ".$n."<br>";
                if($d>48 || $d==48 )
                {
                    
                ?>
                    
                    <tr>
                        <td><span style="text-transform:uppercase"><?=$ilcs->ilcname ?></span></td>
                        <td><?=$uname1 ."[".$rolex."]" ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br>
                            
                            <a style="margin-top:3%;font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? 
                            if($is->communication=='' || $is->academic_knowledge=='' || $is->attitude=='' ){ ?>
                            <button id="<?=$intid ?>" onclick="manageint(this.id)" >Assign Manager</button> 
                           <? }
                            if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { 
                             
                                ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td>
                           <?
                           $crq=new CDbCriteria();
                            $crq->condition='intid=:c and close=0';
                            $crq->params=array(':c'=>$intid);
                            $ri= Assignesc::model()->find($crq);
                            $ni=sizeof($ri);
                            if($ni!=0){
                                ?><button class="btn-danger" style="margin-bottom:5px">Reassigned</button><? 
                            }
                            $istatus=$i->istatus;
                            $idate=$i->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            if ($istatus=='1' && $d>=48) {
                             ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            if ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            ?>
                        </td>
                    
                    </tr>
            

<?
 }
                
            } ?>
                </table>
            </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        
    }
    
        public function actionIntlist2() {
             if(Yii::app()->request->isPostRequest) {
               

                
                $crq1=new CDbCriteria();
                $crq1->condition='iclosed=1';
                $crq1->order='idate1 desc';
                $interviews= Interview::model()->findAll($crq1);
                $n=sizeof($interviews);
                
                if($n==0) {
                ?> <p style="text-align:center;font-size: 12px !important"> <? echo "No scheduled interviews"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        
                    </tr>
            <?
                foreach($interviews as $i){
                 $intid=$i->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
                
                $role1="";
                //$role="";
                $inid=$i->interviewerid;
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                
                $uname=$user1->name;
                $role=$user1->role;
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                
                $ilc=$i->ilcid;
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role="unit"';
                $cru->params=array(":u"=>$ilc);
                $uinfo=Users::model()->find($cru);
                
                $crq=new CDbCriteria();
                $crq->condition='ilcid=:u';
                $crq->params=array(':u'=>$ilc);
                $ilcs= ManagerIlc::model()->find($crq);
                
                ?>
            <tr>
                        <td><span style="text-transform:uppercase"><?=$ilcs->ilcname ?></span></td>
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$i->candidatename ?>
                            <br>
                            
                            <a style="margin-top:3%;font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($i->idate)) ?>
                            <br>
                        <?=$i->ihour.":".$i->imin ?>
                        </td>
                        <td>
                           <?=$i->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span>&nbsp;
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> &nbsp;
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($i->icomment!=""){
                    
                } ?>
                            
                        </td>
                        
                    
                    </tr>

<?              
            
                
            } ?>
                </table>
                    </div><?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
 
        public function actionManageint(){
            if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);
            $cru=new CDbCriteria(); 
            $cru->condition='intid=:u';
            $cru->params=array(":u"=>$intid);
            $interview= Interview::model()->find($cru);
        
     ?>
         <div class="row">            
            <div class="col-md-12">      
                <div class="input-group mb-3">
                    <input type="hidden" value="<?=$intid ?>" id="qqid">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Date</span>
                        </div>
                        <input type="text" class="form-control" value="<?=$interview->idate ?>" id="datepicker" placeholder="Select date" aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </div>
             
            <div class="col-md-12">  
                <div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">Hour</span>
  </div>
    <select class="form-control" id="hour" >
        <option value="<?=$interview->ihour ?>" selected><?=$interview->ihour ?></option>
        <?
        for($i=9;$i<=23;$i++)
        {
            if($i<10)
                $d="0".$i;
            else
                $d=$i;
        ?>
        <option value="<?=$d ?>"><?=$d ?></option>
            <?
        }
        ?>
    </select>
</div>
                        </div>
             
                        <div class="col-md-12">  
<div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">Minutes</span>
  </div>
    <select class="form-control" id="min" >
        <option value="<?=$interview->imin ?>" selected><?=$interview->imin ?></option>
        <?
        for($i=0;$i<=59;$i++)
        {
            if($i<10)
                $d="0".$i;
            else
                $d=$i;
            
        ?>
        <option value="<?=$d ?>"><?=$d ?></option>
        <?
        }
        ?>
    </select>
</div>
                </div>
             
<div class="col-md-12">  
<div class="input-group mb-3">
   <div class="input-group-prepend">
    <span class="input-group-text" id="basic-addon2">NEW Manager</span>
  </div>
    <select class="form-control" id="man" >
        <option value="0" selected>Choose NEW Manager</option>
        <?
        $crq=new CDbCriteria(); 
        $crq->select='*';
        $crq->condition="role=:r";
        $crq->params=array(':r'=>'ilcmanager');
        $ilc=Users::model()->findAll($crq);
        
        foreach($ilc as $i){
           $iname=$i->name;
           $iid=$i->userid;
           ?>
           <option value="<?=$iid ?>"><?=$iname ?></option>
           <?
        }
        ?>
    </select>
</div>
</div>
             
<div class="col-md-12">  
<div class="input-group mb-3">
   <input type="checkbox" style="font-size:22px" id="academiccheck">&nbsp;Appoint myself as Manager
</div>
</div>
                </div>
                <div class="input-group">
                    <input type="button" class="btn btn-success" value="Update" onclick="updateint()"/>
                </div>    
         <?
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
 
        public function actionUpdateint(){
        if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']); 
           $idate=trim($_POST['idate']);
           $ihour=trim($_POST['ihour']); 
           $imin=trim($_POST['imin']);  
           $man=trim($_POST['man']);  
           $ac1=trim($_POST['ac1']);
           
         $cru=new CDbCriteria();
         $cru->condition='intid=:u';
         $cru->params=array(":u"=>$intid);
         $inter=Interview::model()->find($cru);
         $userid=$inter->userid;
         
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$userd);
         $uu=Users::model()->find($cru);
         $uname=$uu->username;
         $name=$uu->name;
         
         $cru=new CDbCriteria();
         $cru->condition='ilcid=:u';
         $cru->params=array(":u"=>$inter->ilcid);
         $mi=  ManagerIlc::model()->find($cru);
         
         $cru=new CDbCriteria();
         $cru->condition='ilcmid=:u';
         $cru->params=array(":u"=>$man);
         $mm=  ManagerIlc::model()->find($cru);
         $mmilc= $mm->ilcid;
         
        $rw=new Report(); 
        $qq=new Interview();
        $acaid = Yii::app()->user->getState("user_id");
        if($ac1==1){
           $qq->updateAll(array('idate'=>$idate,'ihour'=>$ihour,'imin'=>$imin,'ilcmid'=>'-','interviewerid'=>$acaid,'istatus'=>1), "intid=:q", array(':q'=>$intid)); 
        }
        if($ac1==0){
           if($mi->ilcmid==$man){
               $qq->updateAll(array('idate'=>$idate,'ihour'=>$ihour,'imin'=>$imin,'interviewerid'=>$man,'ilcmid'=>$man,'istatus'=>1), "intid=:q", array(':q'=>$intid)); 
           
           }
           else{
            $q2=new Assignesc();
            $q2->asid=uniqid();
            $q2->intid=$intid;
            $q2->callid='-';
            $q2->ilcmid=$man;
            $q2->ilcid=$inter->ilcid;
            $q2->week=$rw->getweekno(date('Y-m-d'));
            $q2->month= intval(date('m'));
            $q2->year=intval(date('Y'));
            $q2->close=0;
            $q2->city=$inter->city;;
            $q2->state=$inter->state;
            $q2->isNewRecord=true;
            $q2->save(FALSE);
           
            $qq->updateAll(array('idate'=>$idate,'ihour'=>$ihour,'imin'=>$imin,'interviewerid'=>$man,'istatus'=>1), "intid=:q", array(':q'=>$intid)); 
           
            $mst="Academic head has assigned you as the new Manager of the interview of ".$inter->candidatename." on ".$inter->idate." at ".$inter->ihour.":".$inter->imin;
            $qn=new Notifications();
            $qn->queryid=$intid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$mmilc;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            }
        }
        
         
    $mst="Academic head has changed the ILC Manager conducting the interview of ".$inter->candidatename." on ".$inter->idate." at ".$inter->ihour.":".$inter->imin;
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="unit";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    $qn=new Notifications();
    $qn->queryid=$intid;
    $qn->notid=  uniqid();
    $qn->role="partner";
    $qn->ilc=$inter->ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk New Manager assigned";
            $msg="Dear $name,<br>
                Academic head has changed the ILC Manager conducting the interview of $inter->candidatename on $inter->idate at $inter->ihour $inter->imin<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
    } 
        
        public function actionApproveint() {
            if(Yii::app()->request->isPostRequest) {
           $intid=trim($_POST['intid']);

         $userid=Yii::app()->user->getState('user_id');
         $qq=new Interview();
         $qq->updateAll(array("istatus"=>1,"ilcmid"=>$userid,"interviewerid"=>$userid), 'intid=:q', array(":q"=>$intid));
}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
         
        }

        public function actionIndex()
	{
	$uid = Yii::app()->user->getState("user_id");
        $cr = new CDbCriteria();
        $cr->condition='userid=:u';
        $cr->params=array(':u'=>$uid);
        $user=Users::model()->find($cr);
        $userp=Userphoto::model()->find($cr);
	$this->render('index',array('user'=>$user,'userp'=>$userp));		
        
	}
        
        //////////////////////upload post ///////////////////
        public function actionUploadpost() {
        $model = new QueryChat();
        $userid=Yii::app()->user->getState('user_id');
	//$gallery = new UserGallery;
	 if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
     else { 
       // $rnd = rand(0123456789, 9876543210); 	// generate random number between 0123456789-9876543210
	$timeStamp = time(); 	// generate current timestamp
        $uploadedFile = $_FILES['file']['name'];
     
		$fileName = "{$timeStamp}-{$uploadedFile}"; 	// random number + Timestamp + file name
                $model->queryid=trim($_POST['qid']);
                $model -> document = $fileName;
	
                
                $model->chatid=  uniqid();
         $model->senderid=$userid;
         $model->recid='-';
         $model->message='-';
         $model->date1=  strtotime(date('Y-m-d H:i:s'));
         
	if ($model -> save(FALSE)) {
	move_uploaded_file($_FILES['file']['tmp_name'], Yii::app() -> basePath . '/../chatdocs/' . $fileName);	
	//$uploadedFile -> saveAs(Yii::app() -> basePath . '/../chatdocs/' . $fileName); // save images in given destination folder
                echo "uploaded";
        }
      
}
            }
            
            ///////////////////////////////////////////
        public function actionIlcmanager() {
            $this->render('ilcmanager');
        }
        
        public function actionPartner() {
            $this->render('partner');
        }
        
        public function actionAssignilc() {
            $cr=new CDbCriteria();
            $cr->condition='role=:r';
            $cr->params=array(":r"=>'ilcmanager');
            $managers=  Users::model()->findAll($cr);
              $cr2=new CDbCriteria();
   
            $ilcs= Ilc::model()->findAll($cr2);
            $this->render('assignilc',array('managers'=>$managers,'ilcs'=>$ilcs));
        }
        
        public function actionAssignedlist() {
            if(Yii::app()->request->isPostRequest) {
           
             ?><h4 style="text-align: center">Assigned ILCs</h4> <?
             $cr=new CDbCriteria();
             $cr->group='managername';
             $result=  ManagerIlc::model()->findAll($cr);
             foreach ($result as $r ) {
                 $name=$r->managername;
                 $mid=$r->ilcmid;
            
            $cr1 = new CDbCriteria();
            $cr1->condition='userid=:u';
            $cr1->params=array(':u'=>$mid);
            $userp=Userphoto::model()->find($cr1);
	   
                 ?>

<div style="padding:10px;background-color:#f4f4f4;margin:0 15px 10px 15px">
    <h5>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/userphoto/<?=$userp->uphoto ?>" style="width:30px;height:30px;border-radius:30px" >
            <span style="text-transform:uppercase"> <?=$name ?></span>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <? 
             $cr2=new CDbCriteria();
             $cr2->condition='ilcmid = :i';
             $cr2->params=array(':i'=>$mid);
             $result2=  ManagerIlc::model()->findAll($cr2);
             foreach($result2 as $r2){
           ?>
            <span><?=$r2->ilcname ?>
            <button class="btn-light" id="<?=$r2->itemid ?>" onclick="delete2(this.id)" >
                <ion-icon name="close" style="font-size:20px;color:black;margin-top:7%!important"></ion-icon></button>
            </span> &nbsp;&nbsp;
            <?
             }
           ?> 
        </h5>
    </div>
              <?php
                 
             }
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
         }
        
        public function actionCreateilcm() {
            if(Yii::app()->request->isPostRequest) {
           $uname=trim($_POST['uname']);
           $name=trim($_POST['name']);
           $org=trim($_POST['org']);
           $nation=trim($_POST['nation']);
           $address=trim($_POST['address']);
           $country=trim($_POST['country']);
           $phone=trim($_POST['phone']);
           
        $uid = uniqid();
        $uu = new Users();
        $n = intval($uu->count('username=:u or phone=:p', array(":u" => $uname, ":p" => $phone)));
        if ($n > 0) {
            echo "User already exists......";
        } else {
            
            ////////////////send mail//////////////////
            $to=$uname;
            $url="http://tmp-mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Manager Registration";
            $msg="Dear $name,<br>
                You have been registered as a beanstalk manager.<br>
                Please click on this <a target=\"_blank\" href=\"$url\">LINK</a> to reset password and complete registration process.<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            $uu->isNewRecord = TRUE;
            $uu->userid = $uid;
            $uu->name = $name;
            $uu->username = $uname;
            $uu->password = md5('-');
            $uu->role = 'ilcmanager';
            $uu->block = 0;
            $uu->org = $org;
            $uu->address = $address;
            $uu->country = $country;
            $uu->nationality = $nation;
            $uu->phone = $phone;
            $uu->save(FALSE);
            echo "Profile created.";
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
    }
    
        public function actionCreatepartner() {
        if(Yii::app()->request->isPostRequest) {
           $uname=trim($_POST['uname']);
           $name=trim($_POST['name']);
           $org=trim($_POST['org']);
           $nation=trim($_POST['nation']);
           $address=trim($_POST['address']);
           $country=trim($_POST['country']);
           $phone=trim($_POST['phone']);
        $uid = uniqid();
        $uu = new Users();
        $n = intval($uu->count('username=:u or phone=:p', array(":u" => $uname, ":p" => $phone)));
        if ($n > 0) {
            echo "User already exists......";
        } else {
            $uu->isNewRecord = TRUE;
            $uu->userid = $uid;
            $uu->name = $name;
            $uu->username = $uname;
            $uu->password = md5('-');
            $uu->role = 'partner';
            $uu->block = 0;
            $uu->org = $org;
            $uu->address = $address;
            $uu->country = $country;
            $uu->nationality = $nation;
            $uu->phone = $phone;
            $uu->save(FALSE);
            echo "Profile created.";
            ////////////////send mail//////////////////
            $to=$uname;
            $url="http://tmp-mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Partner Registration";
            $msg="Dear $name,<br>
                You have been registered as a beanstalk partner.<br>
                Please click on this <a target=\"_blank\" href=\"$url\">LINK</a> to reset password and complete registration process.<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
        }
       }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
    }

        public function actionGetilcm() {
            if(Yii::app()->request->isPostRequest) {
           
                $cr=new CDbCriteria();
                    $cr->condition="role=:r";
                    $cr->params=array(":r"=>'ilcmanager');
                    $ilcms=  Users::model()->findAll($cr);
                    if(sizeof($ilcms)==0) {
                        echo "No entries..";
                    }
                    else {

                    ?>
                     <div class="table-font">
                         <table class="table table-striped">
                             <tr>
                                 <td><b>Photo</b></td>
                                 <td><b>Username</b></td>
                                 <td><b>Password</b></td>
                                 <td><b>Country</b></td>
                                 <td><b>Organization</b></td>
                                 <td><b>Phone</b></td>
                                 <td><b>Nationality</b></td>
                                 <td><b>Reg URL</b></td>
                                 <td></td>
                             </tr>
                 <?
                        foreach ($ilcms as $mm) {
                            ?>
                             <tr>
                                <td>
                                    <img src="<?=Yii::app()->request->baseUrl ?>/ilcmimg/<?=$mm->photo ?>" class="table-img"  alt="no photo">
                                </td>
                                <td>
                                   <?=$mm->username ?> 
                                </td>
                                <td>
                                   <?=$mm->password ?> 
                                </td>
                                <td>
                                   <?=$mm->country ?> 
                                </td>
                                <td>
                                    <?=$mm->org ?>
                                </td>
                                <td>
                                    <?=$mm->phone ?>
                                </td>
                                <td>
                                    <?=$mm->nationality ?>
                                </td>

                                <td>
                                   <?=Yii::app()->request->baseUrl ?>/index.php/newregister/?p=<?=$mm->username ?>&q=<?=$mm->userid ?> 
                                </td>
                                <td>
                                    <a id="<?=$mm->userid ?>" onclick="delilcm(this.id)" class="btn-light">
                                       <ion-icon name="close" style="font-size:20px;color:black;margin-top:7%!important"></ion-icon></button>
                                     </a>
                                </td>
                            </tr>
                            <?
                        } ?>
                        </table>
                     </div>
                         <?
                    }
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionGetpartner() {
         if(Yii::app()->request->isPostRequest) {
           
   
        $cr=new CDbCriteria();
            $cr->condition="role=:r";
            $cr->params=array(":r"=>'partner');
            $ilcms=  Users::model()->findAll($cr);
            if(sizeof($ilcms)==0) {
                echo "No entries..";
            }
 else {
     ?>
             <div class="table-font">
                 <table class="table table-striped">
                     <tr>
                         <td><b>Photo</b></td>
                         <td><b>Name</b></td>
                         <td><b>Phone</b></td>
                         <td><b>Nationality</b></td>
                         <td><b>Organization</b></td>
                         <td><b>Email ID</b></td>
                         <td><b>Password</b></td>
                         <td><b>Reg URL</b></td>
                         <td></td>
                     </tr>
         <?
                foreach ($ilcms as $mm) {
                    ?>
                     <tr>
                        <td>
                         <img src="<?=Yii::app()->request->baseUrl ?>/ilcmimg/<?=$mm->photo ?>" class="table-img" alt="no photo">
                        </td>
                        <td>
                            <?=$mm->name ?>
                        </td>
                        <td>
                           <?=$mm->phone ?> 
                        </td>
                        <td>
                           <?=$mm->nationality ?> 
                        </td>
                        <td>
                            <?=$mm->org ?>
                        </td>
                        
                        <td>
                           <?=$mm->username ?> 
                        </td>
                        <td>
                           <?=$mm->password ?> 
                        </td>
                        <td>
                          <?=Yii::app()->request->baseUrl ?>/index.php/newregister/?p=<?=$mm->username ?>&q=<?=$mm->userid ?>  
                        </td>
                        <td>
                            <a id="<?=$mm->userid ?>" onclick="delilcm(this.id)" class="btn-light">
                               <ion-icon name="close" style="font-size:20px;color:black;margin-top:7%!important"></ion-icon></button>
                             </a>
                        </td>
                        
                    </tr>
  
                    <?
                    } ?></table>
             </div> <?
      }
    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionDelilcm() {
           if(Yii::app()->request->isPostRequest) {
            $id=trim($_POST['id']); 
            $uu=new Users();
            $uu->deleteAll('userid=:u', array(":u"=>$id));
            
	}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionDeleteassigned() {
            if(Yii::app()->request->isPostRequest) {
            $id=trim($_POST['id']);

            $uu=new ManagerIlc();
            $uu->deleteAll('itemid=:u', array(":u"=>$id));
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionAssignm() {
            if(Yii::app()->request->isPostRequest) {
             $ilcs=trim($_POST['ilcs']);
             $mans=trim($_POST['mans']);
             $ilcsname=trim($_POST['ilcsname']);
             $mansname=trim($_POST['mansname']);
            $m=new ManagerIlc();
            $n=intval($m->count('ilcid=:x and ilcmid=:y', array(':x'=>$ilcs,':y'=>$mans)));
            if($n>0)
            {
                echo "Already Assigned to Manager !";
            }
            else
            {
                   $nm=intval($m->count('ilcid=:x', array(':x'=>$ilcs)));
                   if($nm>0) {
                       echo "Ilc has assigned manager..";
                   }
                   else {
                 $cr=new CDbCriteria();
                 $cr->condition="userid=:u";
                 $cr->params=array(":u"=>$mans);
                 $res= Users::model()->find($cr);
                 $uname=$res->username;
                 $name=$mansname;
                 ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="New Manager Assignment";
            $msg="Dear $name,<br>
                You have been assigned as the manager of $ilcsname<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////      
                $itemid=  uniqid();
                $m->isNewRecord=true;
                $m->itemid=$itemid;
                $m->ilcid=$ilcs;
                $m->ilcname=$ilcsname;
                $m->ilcmid=$mans;
                $m->managername=$mansname;
                $m->save(false);
                echo "Manager Assigned";
                   }
            }
          
	}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }  
        }
        
        public function actionQueryhistory() {
            $this->render('queryhistory');
        }
        
        public function actionGetqueryilc(){
        if(Yii::app()->request->isPostRequest) {
           $il=trim($_POST['il']);
           $ci=trim($_POST['ci']);
           $st=trim($_POST['st']);
        $crq=new CDbCriteria();
               if($ci=="0" && $il=="0")
               {
                    $crq->condition="query_closed=0 and state=:s";
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($il=="0"){
                    $crq->condition="query_closed=0 and state=:s and city=:c";
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition="query_closed=0 and ilc_id=:i";
                   $crq->params=array(':i'=>$il);
               }    
        $crq->order="query_date desc";
        $queries=  Query::model()->findAll($crq);
        $n= sizeof($queries);
        
        if($n==0) {
            echo "No queries have been raised from this ILC";
        }
        else 
        {
           ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Address</b></td>
                        <td><b>Query Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Status</b></td>
                        <td><b>Chat</b></td>
                    </tr>
            <? $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $ilcid=$q->ilc_id;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilcid);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                $qdate=$q->query_date;
                
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>48){
                    $qflag=1;
                
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td><? if($d<72 || $d==72) 
       { ?>
        <input type="button" class="btn-warning" value="Active">
    <? } 
    else if($d>72) 
       { ?> 
        <input type="button" class="btn-danger" value="Escalated">
    <? } ?>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr> 
 <?
                }   
            }
            ?>
                </table>
           </div>
            <?
            if($qflag==0){
                ?><p>No queries have been escalated yet!</p><?
                //echo "<br>".$d."hours";
            }
          
        }
       }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
 }
        public function actionGetquery($deer) {
        
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        {
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                
                ?>
    <div class="card">
    <div class="card-header">
        <strong><?=$ilcn ?></strong>
        <span style="float:right">
            <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">
                        <?=($q->query_closed==0)?"Active Query":"Closed Query" ?>
            </span>
        </span>
        
    </div>
    <div class="card-body">
        <p><?=$q->query_topic ?></p>   
    </div>
        
    <div class="card-footer">
        <span style="float:left">
             <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
        </span>
        <span style="float:right"><strong >Created on :</strong>&nbsp;<?=date('d-M-y H:i:s',$q->query_date) ?></span>
        
    </div>
</div>   
<br>
                    <?
                
            }
          
        }
 }
        
        public function actionGetquery3() {
        if(Yii::app()->request->isPostRequest) {
          
        //////////////////////////////////////////
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="ilc_approved=1 and query_closed=1";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            echo "No query raised";
        }
        else 
        { ?>
            <div class="table table-striped table-font" >  
                
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Address</td>
                        <td>Query Topic</td>
                        <td>Date</td>
                        <td>Chat</td>
                    </tr>  
            <?
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                
           
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr> 

                    <?
                
            }
            ?>
                </table>
            </div>
<?
          
        }
      }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }  
 }
 
        public function actionSuggestilc(){
        if(Yii::app()->request->isPostRequest) {
           $silc1=trim($_POST['silc1']);    
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ins_name=:s or ins_name like :s1 or ins_name like :s2";
        $crq->params=array(':s'=>$silc1,':s1'=>$silc.'%',':s2'=>'%'.$silc.'%');
        $queries=  Ilc::model()->findAll($crq);
        ?>
        <ul class="slist">
            <? foreach($queries as $q){
                ?><li name="<?=$q->ins_name ?>" id="<?=$q->ilcid ?>-<?=$q->ins_name ?>" onclick="setilc1(this.id)">
                        <?=$q->ins_name ?>
                </li><?
            } ?>
        </ul>
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionSuggestilc1($silc1){
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ins_name=:s or ins_name like :s1 or ins_name like :s2";
        $crq->params=array(':s'=>$silc1,':s1'=>$silc.'%',':s2'=>'%'.$silc.'%');
        $queries=  Ilc::model()->findAll($crq);
        ?>
        <ul class="slist">
            <? foreach($queries as $q){
                ?><li name="<?=$q->ins_name ?>" id="<?=$q->ilcid ?>-<?=$q->ins_name ?>" onclick="setilc1(this.id)">
                        <?=$q->ins_name ?>
                </li><?
            } ?>
        </ul>
        <?
        }
        
        public function actionSearchbyilc(){
       if(Yii::app()->request->isPostRequest) {
           $silc=trim($_POST['silc']);

        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ilc_id=:s and query_closed=0";
        $crq->params=array(':s'=>$silc);
        $crq->order="query_date desc";
      
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        
        if($n==0) {
            echo "No queries have been raised from this ILC";
        }
        else 
        { ?>
<div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Address</b></td>
                        <td><b>Query Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Status</b></td>
                        <td><b>Chat</b></td>
                    </tr> 
          <?
            $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                if($nc>0){
                    $qdate=$qchat->date1;
                }
                else{
                    $qdate=$q->query_date;
                }
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>=24){
                    $qflag=1;
                
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td>    
        <?
        if($d>=24)
        {
        ?>
        <span class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">ESCALATED</span>
        <?
        }
        ?>
        <span class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;"><?=($q->query_closed==0)?"Active Query":"Closed Query" ?></span>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr> 
<?
                }   
            }?>
                </table></div>
<?
            if($qflag==0){
                ?><p>No queries have escalated yet!</p><?
                //echo "<br>".$d."hours";
            }
          
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionSearchbyilc1(){
       if(Yii::app()->request->isPostRequest) {
           $silc=trim($_POST['silc']);
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="ilcid=:s and iclosed=0";
        $crq->params=array(':s'=>$silc);
        $crq->order="idate1 desc";
        $queries=  Interview::model()->findAll($crq);
        $n=  sizeof($queries);
        if($n==0) {
            ?>
                <p>No Interviews have escalated from this ILC</p>    
            <?
        }
        else 
        {
          ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td> 
                        <td><b>Coordinator</b></td>
                        <td><b>Candidate</b></td>
                        <td><b>Interview Date</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                    </tr>
            <? 
            foreach ($queries as $q) {
                $qdate=$q->idate1;
                $ilc=$q->ilcid;
                
                $intid=$q->intid;
                $cri=new CDbCriteria();
                $cri->condition='intid=:u';
                $cri->params=array(":u"=>$intid);
                $is=InterviewScore::model()->find($cri); 
                
                $role1="";
                //$role="";
                $inid=$q->interviewerid;
                
                $crq1=new CDbCriteria();
                $crq1->condition='userid=:u';
                $crq1->params=array(':u'=>$inid);
                $user1= Users::model()->find($crq1);
                
                $uname=$user1->name;
                $role=$user1->role;
                
                if($role=="ilcmanager")
                {   $role1='ILC Manager';
                
                }
                else if($role=="academic")
                {   $role1="Academic Head";
                
                }
                
                $cru=new CDbCriteria();
                $cru->condition='ilcid=:u and role=:r';
                $cru->params=array(":u"=>$ilc,":r"=>'unit');
                $uinfo=  Users::model()->find($cru);
                $unit=$uinfo->name;
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                
                if($d>48){
                    
                //echo $d;
                ?>
     <tr>
                        <td><span style="text-transform:uppercase"><?=$ilcn ?></span></td>
                        <td><?=$uinfo->name ?>
                        <?
                    if($inid!=""){
                        ?>
                    <p><strong>Conducted by:&nbsp;</strong><?=$uname.' ['.$role1.'] ' ?></p>
                        <?
                    } ?>
                        </td>
                        <td><?=$q->candidatename ?>
                            <br><br>
                            <a style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" target="_blank" href="<?=Yii::app()->request->baseUrl.'/cv/'.$i->candidatecv ?>">
                                Resume
                            </a>
                        </td>
                        <td><?=date('d-M-y ', strtotime($q->idate)) ?>
                            <br>
                        <?=$q->ihour.":".$q->imin ?>
                        </td>
                        <td>
                           <?=$q->icomment ?>
                            <? if($is->communication!='' || $is->academic_knowledge!='' || $is->attitude!='' )
                            { ?>
                            <p>
                                <span class="badge badge-warning" style="margin-bottom:4px">Communication&nbsp; <?=$is->communication ?></span><br>
                                <span class="badge badge-danger" style="margin-bottom:4px">Academic&nbsp;<?=$is->academic_knowledge ?></span> <br>
                    <span class="badge badge-success">Attitude&nbsp; <?=$is->attitude ?> </span></p>
                            <? }
                     if($q->icomment!=""){
                    
                } ?>
                            
                        </td>
                        <td> <?
                            $istatus=$q->istatus;
                            $idate=$q->idate;
                            $idate1=intval(strtotime($idate));
                            /////time difference calculation////////
                            $currtime=  strtotime(date('Y-m-d H:i:s'));
                            $datediff = $currtime-$idate1; 
                            $d=round($datediff/(60 * 60 * 24))*24;
                            ///////////////////////////////////////
                            
                             if ($istatus=='1' && $d>=48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            elseif ($istatus=='0' && $d>48) {
                                ?>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-danger" value="Escalated"  />

                            <?
                            }
                            ?>
                        </td>
                    
                    </tr>
                    
              
            <?
           } 
            
            
            
           } ?></table></div> <?
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        public function actionQueryesc() {
            
           $cr1=new CDbCriteria();
           $res1= Ilc::model()->findAll($cr1);
            
             
            $this->render('queryesc',array('res'=>$res1));
        }
        
        public function actionShowfeed($id){
            
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->condition="queryid=:u";
        $crq->params=array(":u"=>$id);
        $feed= QueryFeed::model()->find($crq);
        
    
    
     ?>
         <div class="row">            
            <div class="col-md-12">      
                <p><strong>Rating &nbsp;</strong>
                    <? $stars=intval($feed->rating); 
                    while($stars>0)
                    {
                        ?><i class="glyphicon glyphicon-star" style="color:yellow"></i>&nbsp; <?
                          $stars--;  
                        
                    } ?>
                 </p>
            </div>
            <div class="col-md-12">  
                <p><strong>Review</strong>&nbsp;<?=$feed->feedback ?></p> 
            </div>
          </div>
        
        
         <?
 }
 
 
        public function actionQueryescview(){
         if(Yii::app()->request->isPostRequest) {
           
   
        $crq=new CDbCriteria();
        $crq->select="*";
        $crq->order="query_date desc";
        $crq->condition="query_closed=0";
        $queries=  Query::model()->findAll($crq);
        $n=  sizeof($queries);
        
        if($n==0) {
            echo "No queries have been raised from this ILC";
        }
        else 
        {
           ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>ILC</b></td>
                        <td><b>Address</b></td>
                        <td><b>Query Topic</b></td>
                        <td><b>Date</b></td>
                        <td><b>Status</b></td>
                        <td><b>Chat</b></td>
                    </tr>
            <? $qflag=0;
            foreach ($queries as $q) {
                $user=$q->userid;
                $cru=new CDbCriteria();
                $cru->condition='userid=:u';
                $cru->params=array(":u"=>$user);
                $uinfo=  Users::model()->find($cru);
                $ilc=$uinfo->ilcid;
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ////checking entry in chat table////
                $queryid=$q->qid;
                $qc=new CDbCriteria();
                $qc->condition='queryid=:u ';
                $qc->params=array(":u"=>$queryid);
                $qc->order="date1 asc";
                $qchat= QueryChat::model()->find($qc);
                $nc=  sizeof($qchat);
                $qdate=$q->query_date;
                
                
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$qdate; 
		$d=round($datediff/(60 * 60 * 24))*24;
                ///////////////////////////////////////
                //echo $d.',' ;
                if($d>48 ){
                    $qflag=1;
                
                ?>
    <tr>
    <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
    <td><span><?=$ilcinfo->address.", ".$ilcinfo->city.", ".$ilcinfo->state.", ".$ilcinfo->country."." ?></span></td>
    <td><?=$q->query_topic ?></td>
    <td><?=date('d-M-y H:i:s',$q->query_date) ?></td>
    <td><? if($d<72 || $d==72) 
       { ?>
        <input type="button" class="btn-warning" value="Active">
    <? } 
    else if($d>72) 
       { ?> 
        <input type="button" class="btn-danger" value="Escalated">
    <? } ?>
    </td>
    <td>
        <input type="button" class="btn btn-success" id="<?=$q->qid ?>" name="<?=$q->userid ?>" value="Chat" onclick="showchat(this.id,this.name)" />
    </td>
</tr> 
 <?
                }   
            }
            ?>
                </table>
           </div>
            <?
            if($qflag==0){
                ?><p>No queries have been escalated yet!</p><?
                //echo "<br>".$d."hours";
            }
          
        }
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
 }
 
        public function actionGetchat() {
        if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']); 
           $uid=trim($_POST['uid']); 
    $cr=new CDbCriteria();
    $cr->condition='queryid=:q';
    $cr->params=array(":q"=>$qid);
    $cr->order='date1 asc';
    $chats=  QueryChat::model()->findAll($cr);
   $nc=  sizeof($chats);
   if($nc==0)
   {
       echo "no chats";
   }
 else {
     $crq=new CDbCriteria();
    $crq->condition='qid=:q';
    $crq->params=array(":q"=>$qid);
    $qr=  Query::model()->find($crq);
    $topic=$qr->query_topic; 
     ?>
                <br style="clear:both">
                <p style="font-size:16px !important" ><b>Topic:</b> <?=$topic ?>  </p>
        <div id="chatui">
                <?
       foreach ($chats as $c) {
         $cru=new CDbCriteria();
         $cru->condition='userid=:u';
         $cru->params=array(":u"=>$c->senderid);
         $uinf=  Users::model()->find($cru);
         
         $durl=Yii::app()->request->baseUrl.'/chatdocs/'.$c->document;
         
         if($uinf->role=='unit'){
           ?>
                <div class="alert alert-success talk " style="float:left" >
            <b style="font-size:14px !important"><?=$uinf->name ?>[ Unit Coordinator ]</b>
            <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
            <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
           </div>      
           <?  
         }
         if($uinf->role=='partner')
         {
           ?>
            <div class="alert alert-warning talk" style="float:left">
    <b style="font-size:14px !important"><?=$uinf->name ?>[Unit Partner]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
           <?  
         }
          if($uinf->role=='academic')
         {
           ?>
            <div class="alert alert-primary talk" style="float:right">
    <b style="font-size:14px !important"><?=$uinf->name ?>[Academic Head]</b>
    <p style="font-size:14px !important"><? echo ($c->document==NULL)?$c->message:'<a href="'.$durl.'" target=\"_blank\">'.$c->document .'</a>' ?></p>
      <small style="font-size:12px !important"><?=date('d M y H:i:s',$c->date1) ?></small>
</div>  
           <?  
         }
         
         
       } ?>
        </div>
            <?
   }
  }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }  
}

        public function actionSendchat() {
            if(Yii::app()->request->isPostRequest) {
           $qid=trim($_POST['qid']);
           $msg=trim($_POST['msg']);
  $uid=Yii::app()->user->getState("user_id");
  
  $cru=new CDbCriteria();
        $cru->condition='userid=:r';
        $cru->params=array(':r'=>$uid);
        $resu=  Users::model()->find($cru);
        $ilcid=$resu->ilcid;
        
        
        $crt=new CDbCriteria();
        $crt->condition='qid=:r';
        $crt->params=array(':r'=>$qid);
        $rest=  Query::model()->find($crt);
        $qtopic=$rest->query_topic;
        $quser=$rest->userid;
        
        $crt=new CDbCriteria();
        $crt->condition='userid=:r';
        $crt->params=array(':r'=>$quser);
        $rest= Users::model()->find($crt);
        $name=$rest->name;
        $uname=$rest->username;
        
        
        
    $qc=new QueryChat();
    $qc->queryid=$qid;
    $qc->chatid=  uniqid();
    $qc->senderid=$uid;
    $qc->recid='-';
    $qc->message=$msg;
    $qc->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qc->isNewRecord=true;
    $qc->save(FALSE);
    
    $mst="There is a reply for the query '".substr($qtopic, 0, 100)."..'";
    $qn=new Notifications();
    $qn->queryid=$qid;
    $qn->notid=  uniqid();
    $qn->role="all";
    $qn->ilc=$ilcid;
    $qn->message=$mst;
    $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
    $qn->count1=3;
    $qn->isNewRecord=true;
    $qn->save(FALSE);
    
    ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Query Chat Reply";
            $msg="Dear $name,<br>
                Academic head has replied to your query <b>".substr($qtopic, 0, 100)."</b><br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
} 
    
        public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
       
       public function actionHol(){
            if(Yii::app()->request->isPostRequest) {
           


            $rolid= Yii::app()->user->getState("rolid");
           $userid=Yii::app()->user->getState('user_id');
           $ilcid=Yii::app()->user->getState('ilcid');
           
            $crq=new CDbCriteria();
            $crq->condition='queryid=:u and approved=0';
            $crq->params=array(':u'=>'ilc');
            $crq->order='startd asc';
            $en= CalenderDate::model()->findAll($crq);
            $approve=$en->approved;
            ?>
            <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Date</b></td>
                        <td><b>Reason</b></td>
                        <td><b>Manager Details</b></td>
                        <td><b>Applied on</b></td>
                        <td><b>Calender</b></td>
                        <td><b>Action</b></td>
                        
                    </tr>
                    <?
            foreach($en as $e){
                $uid=$e->userid;
                $cru=new CDbCriteria();
                $cru->condition='ilcmid=:u';
                $cru->params=array(':u'=>$uid);
                $ui= ManagerIlc::model()->find($cru);
                
                $dd=date('Y-m-d H:i',$e->date1);
            ?>
                    <tr>
                        <td><?=date('d-M-y' ,strtotime($e->startd)) ?></td>
                        <td><?=$e->topic ?></td>
                        <td><?=$ui->managername ?> <br><b><?=$ui->ilcname ?></b></td>
                        <td><?=$dd ?></td>
                        <td><a id="<?=$e->calid ?>" href="<?=Yii::app()->request->baseUrl.'/index.php/academic/mleave/?q='.$uid ?>" class="btn-dark" style="padding:4px;">
                           View
                            </a>
                        </td>
                        <td><? if($approve==0){ ?>
                            <button  type="button" id="<?=$e->calid ?>" onclick="setleave(this.id,1)" class="btn-success but1" style="padding:4px;">
                            <ion-icon name="checkmark"></ion-icon>
                            </button>
                            &nbsp;
                            <button type="button" id="<?=$e->calid ?>" onclick="setleave(this.id,2)" class="btn-danger but1" style="padding:4px;">
                              <ion-icon name="close"></ion-icon>   
                            </button>
                        <? }  ?>
                        </td>
                    </tr>
                <?
            } ?>
                </table>
            </div>
                    <?
                    }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       public function actionSetleave() {
           if(Yii::app()->request->isPostRequest) {
           $calid=trim($_POST['calid']);
            $ap=trim($_POST['ap']);

           $af=  intval($ap);
           $cd=new CalenderDate();
           $cd->updateAll(array('approved'=>$af), 'calid=:c', array(':c'=>$calid));
           
           $crq=new CDbCriteria();
            $crq->condition='calid=:u';
            $crq->params=array(':u'=>$calid);
            $en= CalenderDate::model()->find($crq);
            $userid=$en->userid;
            $startd=date('Y-m-d',strtotime($en->startd));
            
            $crq=new CDbCriteria();
            $crq->condition='userid=:u';
            $crq->params=array(':u'=>$userid);
            $uu= Users::model()->find($crq);
            $username=$uu->username;
            $name=$uu->name;
            
            $crq1=new CDbCriteria();
            $crq1->condition='ilcmid=:u';
            $crq1->params=array(':u'=>$userid);
            $en1= ManagerIlc::model()->find($crq1);
            $ilc=$en1->ilcid;
           
           if($af==1){
                $mst="Your leave on ".$startd." has been granted";
                $mailb="Academic head has approved your leave on $startd ";
           }
           if($af==2){
               $mst="Your leave on ".$startd." has been denied";
               $mailb="Academic head has disapproved your leave on $startd ";
           }
           
            $n=new Notifications();
            $n->deleteAll('ilc=:i',array(':i'=>$calid));
           
            $qn=new Notifications();
            $qn->queryid=$userid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$ilc;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            ////////////////send mail//////////////////
            $to=$username;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Holiday Approval";
            $msg="Dear $name,<br>
                $mailb<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
             }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       }
       // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}