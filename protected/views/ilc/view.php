<?php
/* @var $this IlcController */
/* @var $model Ilc */

$this->breadcrumbs=array(
	'Ilcs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Ilc', 'url'=>array('index')),
	array('label'=>'Create Ilc', 'url'=>array('create')),
	array('label'=>'Update Ilc', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Ilc', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Ilc', 'url'=>array('admin')),
);
?>

<h1>View Ilc #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'ilcid',
		'address',
		'partner_name',
		'ins_name',
		'emails',
		'phones',
		'other_franchise',
		'agreement_date',
		'agreement_valid',
		'junior_pres_prog',
		'preschool_valid_from',
		'senior_pres_prog',
		'senior_pres_valid_from',
		'teacher_training_prog',
		'teacher_training_valid_from',
		'country',
		'city',
		'state',
		'preschool',
		'afterschool_activity',
		'teacher_training',
	),
)); ?>
