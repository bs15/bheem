<?php

/**
 * This is the model class for table "com_tags".
 *
 * The followings are the available columns in table 'com_tags':
 * @property string $comid
 * @property string $tag
 * @property string $date1
 */
class ComTags extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ComTags the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'com_tags';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comid, tag, date1', 'required'),
                        array('id', 'integerOnly'=>true),
			array('comid', 'length', 'max'=>100),
			array('date1', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,comid, tag, date1', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comid' => 'Comid',
			'tag' => 'Tag',
			'date1' => 'Date1',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('comid',$this->comid,true);
		$criteria->compare('tag',$this->tag,true);
		$criteria->compare('date1',$this->date1,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}