<? $rolid=Yii::app()->user->getState("rolid"); ?>
<? if($rolid=='director'){ ?>
<div class="row">
   <div class="col-md-12" >
       <br>
       <span class="bread">
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/director/index">Home</a> >
           Internal > 
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/holiday/index">
               <span style="color:blue">Holiday Management</span>
           </a>
       </span>
    </div> 
</div>
<? } 
if($rolid=='academic'){ ?>
<div class="row">
   <div class="col-md-12" >
       <br>
       <span class="bread">
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
           Internal > 
           <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/holiday/index">
               <span style="color:blue">Holiday Management</span>
           </a>
       </span>
    </div> 
</div>
<? } 
?>
<div class="row">
    <div class="col-md-12" >
        <h1>Manage HOLIDAYS</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <a style="margin-left:1%;color:white !important;" id="addhol2" class="btn-dark btn-sm" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample">
            <i class="glyphicon glyphicon-plus"></i>&nbsp;Add weekend Holidays
        </a>
        &nbsp;&nbsp;
        <a style="margin-left:1%;color:white !important;" id="addhol" class="btn-dark btn-sm" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            <i class="glyphicon glyphicon-plus"></i>&nbsp;Add Holidays
        </a>
        
        <br>
            
        <div class="card collapse" id="collapseExample2" style="padding:6px">
            <div id="qer2" class="alert alert-danger"></div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="enin" id="year" onkeyup="gethmon()" placeholder="Year">
                        <div id="hmon"> <input type="hidden" id="smon" value="-" /></div>
                    </div>
                    <div class="col-md-12">
                        <div id="mons">
                            <input class="chk" type="checkbox" value="01" />&nbsp;January&nbsp;
                          <input class="chk" type="checkbox" value="02" />&nbsp;February&nbsp;   
                            <input class="chk" type="checkbox" value="03" />&nbsp;March&nbsp;   
                               <input class="chk" type="checkbox" value="04" />&nbsp;April&nbsp;   
                       <input class="chk" type="checkbox" value="05" />&nbsp;May&nbsp;
                          <input class="chk" type="checkbox" value="06" />&nbsp;June&nbsp;   
                            <input class="chk" type="checkbox" value="07" />&nbsp;July&nbsp;   
                               <input class="chk" type="checkbox" value="08" />&nbsp;August&nbsp;   
                                   <input class="chk" type="checkbox" value="09" />&nbsp;September&nbsp;
                          <input class="chk" type="checkbox" value="10" />&nbsp;October&nbsp;   
                            <input class="chk" type="checkbox" value="11" />&nbsp;November&nbsp;   
                               <input class="chk" type="checkbox" value="12" />&nbsp;December&nbsp;                 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="button" onclick="setwhol()" class="enin btn-success">SET</button>
                    </div>
                </div>
            </div>
        <br style="clear:both">
        <div class="card collapse" id="collapseExample" style="padding:6px">
                <div id="qer" class="alert alert-danger"></div>
                <div class="row">
                    <div class="col-md-5">
                        <input type="text" class="enin" id="datepicker" placeholder="Choose Date">
                    </div>
                    <div class="col-md-5">
                        <input type="text" class="enin" id="htopic" placeholder="Holiday Detail">
                    </div>
                    <div class="col-md-2">
                        <button type="button" onclick="sethol()" class="enin btn-success">SET</button>
                    </div>
                </div>
            </div>
        
        
            <div  id="hol">
                
            </div>
        </div>
    
        <div class="col-md-4">
            <div  id="holcal">
               <?php 
                //////////////////////ca;lender///////////////////////
 
         $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
//	'themeCssFile'=> '<?php echo Yii::app()->request->baseUrl."/css/main.css" ',
	'options'=>array(
		'header'=>array(
			'left'=>'prev',
			'center'=>'title',
			'right'=>'next',
		),
		'events'=>$this->createUrl('calender/getweekend'), // URL to get event
                //////////////////////////////////////////click//////////////////
            'eventClick'=> 'js:function(calEvent, jsEvent, view) {
	        $("#myModalHeader").html(calEvent.title);
	        $("#myModalBody").load("'.$this->createUrl("calender/getweekendm/?id=").'"+calEvent.id+"&asModal=true");
	        $("#myModal").modal();
	    }',
            /////////////////////////////////////////////////////////////////////
          //  'events'=>$ddays,
	)));
           
           
           
           ///////////////////////////////////////////////////////
               
               ?> 
                <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header bg-dark text-white text-light">
        <h4 id="myModalHeader">Modal header</h4>
        <a class="close" data-dismiss="modal">&times;</a>
        
    </div>
 
    <div class="modal-body" id="myModalBody">
        <p>One fine body...</p>
    </div>
 
<!--    <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  
    </div>-->
 
    </div>
  </div>
</div>
            </div>
            <div class="gap"></div>
            
            
        </div>
    
    <!--<div class="col-md-1"></div>-->
        
    </div>
</div>
<script>
$(document).ready(function(){
  $("#qer").hide(); $("#qer2").hide();
  $("#datepicker").datepicker({
  dateFormat: "yy-mm-dd" ,minDate: 0
    });
  getq();  
 // getwhol();
 $('.fc-day-content').css('font-size','12px');
});

function getq() {
    $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#hol").show().html('Loading...........');
        },
                
        success:function(html){
        $("#hol").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("hol"); ?>',
        data:{
          deer:'deer'
        },
    dataType:"html",
    cache:false
})//ajax
                 
                 
                 ///////////////////////////////////////////////////
}
function sethol() {
    var htopic=$("#htopic").val();
    var hdate= $("#datepicker").val();
    
     if(htopic=="" || htopic==null)
     {
         $("#qer").show().html("Enter HOLIDAY Text");
     }
     else if(hdate=="" || hdate==null)
     {
         $("#qer").show().html("Select HOLIDAY Date");
     }
     else 
     {
        $.ajax({
         beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
      $("#qer").show().html('Setting Holiday');
        },
                
        success:function(html){
        //$("#qer").empty().append(html);
        $("#addhol").click();
        getq();
        window.location.href=window.location.href;
        
   },
              error:function(jqXHR, exception) {
             alert(jqXHR.responseText+' '+exception);
         },   
        type:'post',
        url:'<? echo $this->createUrl("sethol"); ?>',
        data:{
          htopic:htopic,hdate:hdate,
        },
    dataType:"html",
    cache:false
})//ajax
            
            
                 
                 
                 ///////////////////////////////////////////////////
     }
}    
function delhol(hid)
{
    var y=confirm("Are you sure?");
    if(y)
    {
            $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#hol").show().html('Loading...........');
        },
                
        success:function(html){
    getq();
      window.location.href=window.location.href;
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("delhol"); ?>',
        data:{
          hid:hid
           },
    dataType:"html",
    cache:false
})//ajax
                 
    }
}
function setwhol() {
var mons="";
var year=$("#year").val();
    
    $('.chk').each(function(e){
        if($(this).is(':checked')){
            mons+=$(this).val()+',';
        }
    });
    if(year=="" || year==null)
    {
        $("#qer2").show().html('Enter year..');
    }
    else if(mons=="" || mons==null)
    {
        $("#qer2").show().html("Select month");
    }
    else {
           $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#holcal").show().html('Loading...........');
        },
                
        success:function(html){
        $("#holcal").empty().append(html);
          window.location.href=window.location.href;
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("setwhol"); ?>',
        data:{
          mons:mons,year:year
           },
    dataType:"html",
    cache:false
})//ajax
    }
}
function getwhol() {

           $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#holcal").show().html('Loading...........');
        },
                
        success:function(html){
        $("#holcal").empty().append(html);
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("getwhol"); ?>',
        data:{
          deer:'deer'
           },
    dataType:"html",
    cache:false
})//ajax
  
}
function gethmon() {
var y=$("#year").val();
if(y!=""){
     $.ajax({
    beforeSend:function(){
        var url='<? echo Yii::app()->baseUrl."/images/ajl2.gif"; ?>';
        $("#hmon").show().html('Loading...........');
        },
                
        success:function(html){
        $("#hmon").empty().append(html);
          var how=$('#smon').val();
    $('.chk').each(function(e){
       var howval=$(this).val(); 
       if(how.indexOf(howval) != -1){
           $(this).attr('checked','true');
       }
    });
        },
        error:function(jqXHR, exception){
          alert(jqXHR.responseText+' '+exception);
        },   
        type:'post',
        url:'<? echo $this->createUrl("gethmon"); ?>',
        data:{
          y:y
           },
    dataType:"html",
    cache:false
})//ajax 
}
}
</script>