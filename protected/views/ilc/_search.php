<?php
/* @var $this IlcController */
/* @var $model Ilc */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ilcid'); ?>
		<?php echo $form->textField($model,'ilcid',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'partner_name'); ?>
		<?php echo $form->textArea($model,'partner_name',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ins_name'); ?>
		<?php echo $form->textArea($model,'ins_name',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emails'); ?>
		<?php echo $form->textArea($model,'emails',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phones'); ?>
		<?php echo $form->textArea($model,'phones',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'other_franchise'); ?>
		<?php echo $form->textArea($model,'other_franchise',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agreement_date'); ?>
		<?php echo $form->textField($model,'agreement_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'agreement_valid'); ?>
		<?php echo $form->textField($model,'agreement_valid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'junior_pres_prog'); ?>
		<?php echo $form->textArea($model,'junior_pres_prog',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'preschool_valid_from'); ?>
		<?php echo $form->textField($model,'preschool_valid_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'senior_pres_prog'); ?>
		<?php echo $form->textArea($model,'senior_pres_prog',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'senior_pres_valid_from'); ?>
		<?php echo $form->textField($model,'senior_pres_valid_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'teacher_training_prog'); ?>
		<?php echo $form->textArea($model,'teacher_training_prog',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'teacher_training_valid_from'); ?>
		<?php echo $form->textField($model,'teacher_training_valid_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'country'); ?>
		<?php echo $form->textArea($model,'country',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'city'); ?>
		<?php echo $form->textArea($model,'city',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'state'); ?>
		<?php echo $form->textArea($model,'state',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'preschool'); ?>
		<?php echo $form->textArea($model,'preschool',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'afterschool_activity'); ?>
		<?php echo $form->textArea($model,'afterschool_activity',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'teacher_training'); ?>
		<?php echo $form->textArea($model,'teacher_training',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->