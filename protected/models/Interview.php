<?php

/**
 * This is the model class for table "interview".
 *
 * The followings are the available columns in table 'interview':
 * @property string $intid
 * @property string $candidateid
 * @property string $candidatecv
 * @property string $candidatename
 * @property string $userid
 * @property string $idate
 * @property string $ihour
 * @property string $imin
 * @property string $icomment
 * @property string $istatus
 * @property integer $iduration
 * @property integer $iclosed
 * @property string $interviewerid
 * @property string $ilcid
 * @property string $ilcmid
 * @property string $idate1
 * @property integer $month
 * @property integer $year
 */
class Interview extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Interview the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'interview';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('intid, candidateid, candidatecv, candidatename, userid, idate, ihour, imin, icomment, istatus, iduration, iclosed, interviewerid, ilcid, ilcmid, idate1, month, year, clodate,state,city', 'required'),
			array('id, iduration, iclosed,week, month, year, esc', 'numerical', 'integerOnly'=>true),
			array('intid, candidateid, userid, interviewerid, ilcid, ilcmid', 'length', 'max'=>100),
			array('idate1', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, intid, candidateid, candidatecv, candidatename, userid, idate, ihour, imin, icomment, istatus, iduration, iclosed, interviewerid, ilcid, ilcmid, idate1,week, month, year, esc, clodate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'intid' => 'Intid',
			'candidateid' => 'Candidateid',
			'candidatecv' => 'Candidatecv',
			'candidatename' => 'Candidatename',
			'userid' => 'Userid',
			'idate' => 'Idate',
			'ihour' => 'Ihour',
			'imin' => 'Imin',
			'icomment' => 'Icomment',
			'istatus' => 'Istatus',
			'iduration' => 'Iduration',
			'iclosed' => 'Iclosed',
			'interviewerid' => 'Interviewerid',
			'ilcid' => 'Ilcid',
			'ilcmid' => 'Ilcmid',
			'idate1' => 'Idate1',
                        'week' => 'Week',
			'month' => 'Month',
			'year' => 'Year',
                        'esc' => 'Esc',
                        'clodate' => 'Closed Date',
                        'state' => 'State',
                        'city' => 'City',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('intid',$this->intid,true);
		$criteria->compare('candidateid',$this->candidateid,true);
		$criteria->compare('candidatecv',$this->candidatecv,true);
		$criteria->compare('candidatename',$this->candidatename,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('idate',$this->idate,true);
		$criteria->compare('ihour',$this->ihour,true);
		$criteria->compare('imin',$this->imin,true);
		$criteria->compare('icomment',$this->icomment,true);
		$criteria->compare('istatus',$this->istatus,true);
		$criteria->compare('iduration',$this->iduration);
		$criteria->compare('iclosed',$this->iclosed);
		$criteria->compare('interviewerid',$this->interviewerid,true);
		$criteria->compare('ilcid',$this->ilcid,true);
		$criteria->compare('ilcmid',$this->ilcmid,true);
		$criteria->compare('idate1',$this->idate1,true);
                $criteria->compare('week',$this->week);
		$criteria->compare('month',$this->month);
		$criteria->compare('year',$this->year);
                $criteria->compare('esc',$this->esc);
                $criteria->compare('clodate',$this->clodate);
                $criteria->compare('state',$this->state);
                $criteria->compare('city',$this->city);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}