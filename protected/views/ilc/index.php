<?php
/* @var $this IlcController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ilcs',
);

$this->menu=array(
	array('label'=>'Create Ilc', 'url'=>array('create')),
	array('label'=>'Manage Ilc', 'url'=>array('admin')),
);
?>
<div class="row">
    <div class="col-md-12">
        <br>
        <span class="bread">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/academic/index">Home</a> >
                Master Data Management > Manage > 
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/ilc/create">
                    <span style="color:blue">ILC</span>
                </a>
            </span> 
    </div>
</div>
<h1>ILCS</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
