<?
$rolid=Yii::app()->user->getState("rolid");
$crmm=new CDbCriteria();
$crmm->condition='ilcmid=:q';
$crmm->params=array(":q"=>$uid);
$manilcs= ManagerIlc::model()->findAll($crmm);

$qf=0;
$qn=0;
$crqf=new CDbCriteria();
$crqf->condition='ilcmid=:q and query_closed=1';
$crqf->params=array(":q"=>$uid);
$feedq= Query::model()->findAll($crqf);
foreach($feedq as $f){
    $qf+=$f->rating;
    $qn++;
}
$qavg=$qf/$qn;

$cf=0;
$cn=0;
$crcf=new CDbCriteria();
$crcf->condition='ilcmid=:q and closed=1';
$crcf->params=array(":q"=>$uid);
$feedc= Call::model()->findAll($crcf);
foreach($feedc as $c){
    $callid=$c->callid;
    $cf+=$c->cfeed;
    $cn++;
    
    $crcf=new CDbCriteria();
    $crcf->condition='callid=:q and close=1';
    $crcf->params=array(":q"=>$callid);
    $rc= Assignesc::model()->find($crcf);
    $rn=sizeof($rc);
    if($rn!=0){
        $cn++;
        $cf+=$rc->rating;
    }
    
}
$cavg=$cf/$cn;

 $avgg=($cavg+$qavg)/2;

$avg1=round($avgg,2);


?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br style="clear:both">
            <div class="row">
                
                <div class="col-md-2">
                   <img src="<?= Yii::app()->request->baseUrl.'/userphoto/'.$uinfop->uphoto ?>" class="up"> 
                </div>
                <div class="col-md-10">
                    
                    <? if($rolid=='ilcmanager')
                    { ?>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/profile/" class="pull-right prof">Edit Profile</a>
                    <?  } ?>
                    
                    <button class="pull-right btn-green">
                        <?=$avg1 ?>&nbsp; <i class="glyphicon glyphicon-star"></i>
                    </button>
                    <h2><?=$uinfo->name ?>, 
                        <span class="font16"><?=$uinfo->nationality ?>&nbsp;|&nbsp;<?=$uinfo->org ?></span></h2>
                    <?
                    if($rolid=='ilcmanager' || $rolid=='academic' || $rolid=='director')
                    {
                     ?>
                         <p class="font14">
                         <?=$uinfo->address ?>
                         &nbsp;| &nbsp;
                         <?=$uinfo->phone ?>
                         </p>
                     <?   
                    }
                    ?>
                         
                         <p class="font18">
                             <strong>ILCs</strong>
                         </p> 
                             
                         <div class="row">
                             <?
                             foreach($manilcs as $m)
                            {
                                $ilcid=$m->ilcid;
                                $cri=new CDbCriteria();
                                $cri->condition='ilcid=:q';
                                $cri->params=array(":q"=>$ilcid);
                                $ilcs= Ilc::model()->find($cri);
                                ?>
                             <div class="br">
                                 <p class="font16"><?=$ilcs->ins_name ?><br>
                                 <span class="font-14" style="color:grey"><?=$ilcs->address ?></span>
                                 </p>
                                 
                             </div>
                            <? } ?>
                          </div>   
                        </div>
               
            </div>
            <br>
            <div class="row">
                <hr>
                <div class="col-md-2">
                    <div class="box box-red">
                        <h4 class="text-white">Query</h4>
                        <p><span style="font-size:20px !important"><?=$nq ?></span> Solved </p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="box box-lav">
                        <h4 class="text-white">Call</h4>
                        <p><span style="font-size:20px !important"><?=$nc ?></span> Completed</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="box box-green">
                        <h4 class="text-white">Enquiry</h4>
                        <p><span style="font-size:20px !important"><?=$ne ?></span> Closed</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="box box-blue">
                        <h4 class="text-white">Interview</h4>
                        <p><span style="font-size:20px !important"><?=$ni ?></span> Taken</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="box box-pink">
                        <h4 class="text-white">Admission</h4>
                        <p><span style="font-size:20px !important"><?=$na ?></span> Admitted</p>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>