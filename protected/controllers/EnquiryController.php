<?php

class EnquiryController extends Controller
{
    public function filters()
    	{
    		return array(
    			'accessControl', // perform access control for CRUD operations
    			'postOnly + delete', // we only allow deletion via POST request
    		);
    	}
        public function accessRules()
        {

    	$criteria1 = new CDbCriteria();
    	$criteria1->select = '*';
    	$criteria1->condition = 'role=:rolida or role=:rolidd or role=:rolidi or role=:rolidu or role=:rolidp';
    	$criteria1->params = array(':rolida'=>'academic',':rolidd'=>'director',':rolidi'=>'ilcmanager',':rolidu'=>'unit',':rolidp'=>'partner');
    	$model1 = Users::model()->findAll($criteria1);
    	$modad=array();
    	$i=0;
    	
    	foreach($model1 as $mod1)
    	{
    	$modad[$i]=$mod1->username;
        	$i++;
    	}
        
        return array(
    	array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('index','setnoty','checknum','nlist','setenquiry','enq','setst','updatest','newm','updatem'),
    		'users'=>$modad,
    	),
        
        array('allow',  // allow all users to perform 'index' and 'view' actions
    		'actions'=>array('archive','enqa','closeenq','setdetail','updatedetail','setai','getemy','getcity','getilc','getenqilc'),
    		'users'=>$modad,
    	),
            
    
    			array('deny',  // deny all users
    				'users'=>array('*'),
    			),
    		);
    	}
        
        public function actionGetcity()
        {
            if(Yii::app()->request->isPostRequest) {
            $st1=trim($_POST['st1']);
            $nl= new Nlist();
            $nl->getcity($st1);
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
         public function actionGetilc()
	{
            if(Yii::app()->request->isPostRequest) {
                $ci1=trim($_POST['ci1']);
             $nl= new Nlist();
            $nl->getilc($ci1);
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
	}
	public function actionIndex()
	{
            $rolid = Yii::app()->user->getState("rolid");
            $ilcid=Yii::app()->user->getState('ilc_id');
            $userid=Yii::app()->user->getState('user_id');
           
     $crt=new CDbCriteria();
        $crt->condition='ilcid=:r';
        $crt->params=array(':r'=>$ilcid);
        $rest= ManagerIlc::model()->find($crt);
        $ilcmid=$rest->ilcmid;       
           
    //$this->render('interview',array('man'=>$ilcmid));
            $cr1=new CDbCriteria();
            $res1= Ilc::model()->findAll($cr1);
            if($rolid=='unit' || $rolid=='partner'){
            Yii::app()->user->setState('man',$ilcmid);
            }
            $this->render('index',array('res'=>$res1,'man'=>$ilcmid));
	}
        
        public function actionArchive()
	{
            $cr1=new CDbCriteria();
            $res1= Ilc::model()->findAll($cr1);
            $this->render('archive',array('res'=>$res1));
	}
        
        public function actionSetnoty() {
            if(Yii::app()->request->isPostRequest) {
           $sn= new Nlist();
           $sn->setnoty1(); 
           }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
        public function actionChecknum(){
            if(Yii::app()->request->isPostRequest) {
                $cn= new Nlist();
                $cn->checknum1();
                }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
        }
    
        public function actionNlist() {
            if(Yii::app()->request->isPostRequest) {
           $nl= new Nlist();
          $nl->nlist1();
         }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
            }
       }
       public function actionGetemy() {
           if(Yii::app()->request->isPostRequest) {
           $yea=trim($_POST['yea']);
           $mon=trim($_POST['mon']);

        $y=intval($yea);
        $m=intval($mon);
        $userid=Yii::app()->user->getState('user_id');
        $rolid = Yii::app()->user->getState("rolid");
        
        if($rolid=='unit' || $rolid=='partner'){
            
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            if($m!=0 && $y!=0)
            {
            $crq->condition="month=:m and year=:y and enquired_by=:i and eclosed=1";
            $crq->params=array(":m"=>$m,":y"=>$y,":i"=>$userid);
            }
            else if($m==0 && $y!=0)
               {
            $crq->condition="year=:y and enquired_by=:i and eclosed=1";
            $crq->params=array(":y"=>$y,":i"=>$userid);
            } 
            $queries=  Enquiry::model()->findAll($crq);
            $n=  sizeof($queries);  
        }
        
        if($rolid=='ilcmanager'){
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            if($m!=0 && $y!=0)
            {
            $crq->condition="month=:m and year=:y and ilcmid=:i and eclosed=1";
            $crq->params=array(":m"=>$m,":y"=>$y,":i"=>$userid);
            }
            else if($m==0 && $y!=0)
               {
            $crq->condition="year=:y and ilcmid=:i and eclosed=1";
            $crq->params=array(":y"=>$y,":i"=>$userid);
            } 
            $queries=  Enquiry::model()->findAll($crq);
            $n=  sizeof($queries);   
            
        }
        
        if($rolid=='academic' || $rolid=='director'){
            $crq=new CDbCriteria();
            $crq->select="*";
            $crq->order="date1 desc";
            if($m!=0 && $y!=0)
            {
            $crq->condition="month=:m and year=:y and eclosed=1";
            $crq->params=array(":m"=>$m,":y"=>$y);
            }
            else if($m==0 && $y!=0)
               {
            $crq->condition="year=:y and eclosed=1";
            $crq->params=array(":y"=>$y);
            } 
            $queries=  Enquiry::model()->findAll($crq);
            $n=  sizeof($queries);   
        }
        
        if($n==0) {
            ?> <p >No Enquiries were raised in this month !</p> <?
        }
        else 
        {
           ?>

           <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td>ILC</td>
                        <td>Type</td>
                        <td>Child Details</td>
                        <td>Parent Details</td>
                        <td>Date & Time</td>
                        <td>Created</td>
                        <td>More Details</td>
                    </tr>
           <? foreach ($queries as $q) {
               
               $ilc=$q->ilcid;
               $par=$q->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                
                
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilc);
                $ilcinfo= Ilc::model()->find($cri);
                $ilcn=$ilcinfo->ins_name;
                
                ?>
                <tr>
                <td><span style="text-transform: uppercase;"><?=$ilcn ?></span></td> 
                <td><?=$q->enquiry_type ?></td>
                <td><?=$ep->child_name ?>
                    <br>
                    Born on <?=$ep->child_dob ?>
                    <br>
                    <?=$ep->gender ?>
                </td>
                <td>
                    <?=$ep->parent_f_name ?>
                    <br>
                    <?=$ep->parent_f_phone ?>
                </td>
                <td><?=$q->enquiry_date ?>
                    <br>
                    <?=$q->enquiry_time ?>
                </td>
                <td><?=$q->cdate ?></td>
                <td>
                    <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$q->enquiryid ?>" name="<?=$q->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                </td>
                </tr>
            <?
                
            }
            ?>
                </table>
           </div>
            <?
          }
       }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
        
 }
       
        public function actionSetenquiry(){
            if(Yii::app()->request->isPostRequest) {
            $enqtype=trim($_POST['enqtype']);
            $datep=trim($_POST['datep']);
            $h=trim($_POST['h']);
            $m=trim($_POST['m']);
            $chi1=trim($_POST['chi1']);
            $gen1=trim($_POST['gen1']);
            $dob=trim($_POST['dob']);
            $par1=trim($_POST['par1']);
            $email1=trim($_POST['email1']);
            $phn1=trim($_POST['phn1']);
            $add1=trim($_POST['add1']);
            $city1=trim($_POST['city1']);
            $pf=trim($_POST['pf']);
            $uid=Yii::app()->user->getState("user_id");
            $ecount=0;
            
            $cru=new CDbCriteria();
            $cru->condition='userid=:r';
            $cru->params=array(':r'=>$uid);
            $resu=  Users::model()->find($cru);
            $ilcid=$resu->ilcid;
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilcid);
                $ilcinfo= Ilc::model()->find($cri);
            
            $crum=new CDbCriteria();
            $crum->condition='ilcid=:r';
            $crum->params=array(':r'=>$ilcid);
            $resum= ManagerIlc::model()->find($crum);
            $ilcmid=$resum->ilcmid;
            
            $enid=uniqid();
            $parid=uniqid();
            $yy=intval(date('Y'));
            $mm=intval(date('m'));
            $ww=intval(date('W'));
            
            $ec=new EnquiryCount();
            
            $ecc=$ec->count("ilcid=:ilc and year=:y and week=:w", array(":ilc"=>$ilcid,":y"=>$yy,":w"=>$ww));
            if($ecc<=5)
            {
                
            $ec->enquiryid=$enid;
            $ec->ilcid=$ilcid;
            $ec->year= $yy;
            $ec->month=$mm;
            $ec->week=$ww;
            $ec->ecount=$ecount++;
            $ec->isNewRecord=true;
            $ec->save(FALSE);
            
            $pa=new EnquiryParent();
            $pa->parentid=$parid;
            $pa->parent_f_name=$par1;
            $pa->child_name=$chi1;
            $pa->child_dob=$dob;
            $pa->gender=$gen1;
            $pa->parent_f_email=$email1;
            $pa->parent_f_phone=$phn1;
            $pa->parent_add=$add1;
            $pa->city=$city1;
            $pa->ilc_id=$ilcid;
            $pa->isNewRecord=true;
            $pa->save(FALSE);
            
            
            ///////////////////////week no////////////
            $r=new Report();
            $week=$r->getweekno(date('Y-m-d'));
            
            $en=new Enquiry();
            $en->enquiryid=  $enid;
            $en->enquiry_type=$enqtype;
            $en->enquired_by= $uid;
            $en->week=intval($week);
            $en->month=intval(date('m'));
            $en->year=intval(date('Y'));
            $en->ilcid=$ilcid;
            $en->ilcmid=$ilcmid;
            $en->parentid= $parid;
            $en->enquiry_date= $datep;
            $en->enquiry_time=$h.':'.$m;
            $en->date1=strtotime(date('d-m-Y h:i:s'));
            $en->pref=  intval($pf);
            $en->cdate=date('Y-m-d');
            $en->state=$ilcinfo->state;
            $en->city=$ilcinfo->city;
            $en->isNewRecord=true;
            $en->save(FALSE);
            
            
            
            $mst="A new enquiry has been raised for ".$chi1." on ".$datep." at ".$h.':'.$m;
            $mailb="A new enquiry has been raised for $chi1 on $datep at $h:$m";
            $qn=new Notifications();
            $qn->queryid=$enid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            $u=new Nlist();
            $uu=$u->getuser($ilcmid);
            $username=$uu->username;
            $name=$uu->name;
            ////////////////send mail//////////////////
                    $to=$username;
//                    $url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
                    $subject="Beanstalk New Enquiry";
                    $msg="Dear $name,<br>
                        $mailb<br>
                        Regards,<br>
                        Beanstalk Team.
                        ";

                    $nl=new Nlist();
                    $nl->mailsend($to, $subject, $msg);
                    /////////////////////////////////////////////
            
            }
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
       
        }
        
        public function actiongetenqilc(){
            if(Yii::app()->request->isPostRequest) {
            $st=trim($_POST['st']);
            $ci=trim($_POST['ci']);
            $ilc1=trim($_POST['ilc1']);
            
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit'  )
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and eclosed=0 and enquired_by=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                    <tr>
                        <td><?=$i->enquiry_type ?>
                            <br>
                            <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;margin-top:2%" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Add Details" onclick="setdetail(this.id)" />
                           <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                           
                           <? if($i->eclosed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Close" onclick="closeenq(this.id,0)" />
                            
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Admission" onclick="closeenq(this.id,1)" />
                               
                        <? } ?>
                        </td>
                    </tr><?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     
     if($rolid=='partner'){
         {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and eclosed=0 and enquired_by=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                    <tr>
                        <td><?=$i->enquiry_type ?>
                            <br>
                        <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>     
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;margin-top:2%" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Add Details" onclick="setdetail(this.id)" />
                            <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                           
                           <? if($i->eclosed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Close" onclick="closeenq(this.id,0)" />
                            
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Admission" onclick="closeenq(this.id,1)" />
                               
                        <? } ?>
                        </td>
                    </tr>
        <?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     }
     
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
               $crq=new CDbCriteria();
               $crq->condition='eclosed=0 and ilcid=:i';
               $crq->params=array(':i'=>$ilc1);
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p > <? echo "No Current Enquiries from this ILC"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                ?>
                    <tr>
                        <td><?=$i->enquiry_type ?>
                         <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                             <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td><? if($d<=24) { ?>
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            <br><br>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                            <? }
                             else { ?>
                             <button class="btn-danger" style="margin-bottom:4px;font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <? } ?>
                             
                             <button type="button" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" class="btn-dark" style="font-size:14px !important; padding:4px;border-radius:5px;" onclick="setst(this.id)">Status/Task</button>
                         
                        </td>
                    </tr>
                <? }
               }
               ?>
                </table>
                </div>
             <?
            }
            
        }
        
           if($rolid=='academic'){
               $crq=new CDbCriteria();
               if($ci=="0" && $ilc1=="0")
               {
                   $crq->condition='eclosed=0 and state=:s';
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($ilc1=="0"){
                    $crq->condition='eclosed=0 and state=:s and city=:c';
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition='eclosed=0 and ilcid=:i';
                    $crq->params=array(':i'=>$ilc1);
               }
              
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p > <? echo "No Enquiries have been raised from this ILC"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>24 ) {
                ?>
                <tr>
                        <td><?=$i->enquiry_type ?>
                         <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                             <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p> 
                             <? } 
                             
                             if($i->estatus=='' || $i->etask==''){
                                ?>
                            <button type="button" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" style="text-align:left;background-color:transparent;border:0;font-size:14px !important; padding:4px;border-radius:5px;text-decoration:underline;color:black" onclick="newm(this.id)">Appoint NEW Manager</button>
                             <? } ?>
                               
                        </td>
                        <td><? if($d>24 && $d<=48 ) {  ?>
                            
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            &nbsp;
                            <? } 
                            else if ($d>48){ ?>
                             <button class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <? } ?>
                             <br>
                             <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                        </td>
                    </tr><? 
                
                } } ?>
                </table>
                </div>
                <?
           }
        }
        
           if($rolid=='director'){
            
               $crq=new CDbCriteria();
               if($ci=="0" && $ilc1=="0")
               {
                   $crq->condition='eclosed=0 and state=:s';
                    $crq->params=array(':s'=>$st);
                   
               }
               else if($ilc1=="0"){
                    $crq->condition='eclosed=0 and state=:s and city=:c';
                    $crq->params=array(':s'=>$st,':c'=>$ci);
               }
               else
               {
                   $crq->condition='eclosed=0 and ilcid=:i';
                    $crq->params=array(':i'=>$ilc1);
               }
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p > <? echo "No Enquiries has been raised from this ILC"; ?></p> <?
                }
                else 
                { ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr> 
                <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>48 ) {
                ?>
                <tr>
                        <td><?=$i->enquiry_type ?>
                             <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                             <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td><? if($d<=24) { ?>
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            <br><br>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                            <? }
                             else { ?>
                             <button class="btn-danger" style="margin-bottom:4px;font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <? } ?>
                             
                             
                        </td>
                    </tr>
                
                <? 
                }
               }
               ?>
                </table>
             </div>
             <?
            }
        }
       }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
        
     }
        public function actionEnq(){
            if(Yii::app()->request->isPostRequest) {
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit'  )
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and eclosed=0 and enquired_by=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                    <tr>
                        <td><?=$i->enquiry_type ?>
                            <br>
                            <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;margin-top:2%" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Add Details" onclick="setdetail(this.id)" />
                           <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                           
                           <? if($i->eclosed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Close" onclick="closeenq(this.id,0)" />
                            
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Admission" onclick="closeenq(this.id,1)" />
                               
                        <? } ?>
                        </td>
                    </tr><?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     
     if($rolid=='partner'){
         {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and eclosed=0 and enquired_by=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                   ?>
               <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                    <tr>
                        <td><?=$i->enquiry_type ?>
                            <br>
                        <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>     
                        <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;margin-top:2%" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Add Details" onclick="setdetail(this.id)" />
                            <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                           
                           <? if($i->eclosed==0 ) { ?>
                            <input type="button" style="margin-bottom:3%;font-size:14px !important; padding:4px;border-radius:5px;border:1px solid grey" class="btn-inverse" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Close" onclick="closeenq(this.id,0)" />
                            
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-warning" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="Admission" onclick="closeenq(this.id,1)" />
                               
                        <? } ?>
                        </td>
                    </tr>
        <?
        }
        ?>
          </table>
        </div>
        <?
       }
     }
     }
     
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
               $crq=new CDbCriteria();
               $crq->condition='eclosed=0';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                ?>
                    <tr>
                        <td><?=$i->enquiry_type ?>
                         <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                             <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td><? if($d<=24) { ?>
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            <br><br>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                            <? }
                             else { ?>
                             <button class="btn-danger" style="margin-bottom:4px;font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <? } ?>
                             
                             <button type="button" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" class="btn-dark" style="font-size:14px !important; padding:4px;border-radius:5px;" onclick="setst(this.id)">Status/Task</button>
                         
                        </td>
                    </tr>
                <? }
               }
               ?>
                </table>
                </div>
             <?
            }
            
        }
        
           if($rolid=='academic'){
            $crq=new CDbCriteria();
               $crq->condition='eclosed=0';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>24 ) {
                ?>
                <tr>
                        <td><?=$i->enquiry_type ?>
                         <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                             <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>  
                            <?
                            } if($i->estatus=='' || $i->etask==''){
                                ?>
                            <button type="button" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" style="text-align:left;background-color:transparent;border:0;font-size:14px !important; padding:4px;border-radius:5px;text-decoration:underline;color:black" onclick="newm(this.id)">Appoint NEW Manager</button>
                             <? } ?>
                        </td>
                        <td><? if($d>24 && $d<=48 ) {  ?>
                            
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            &nbsp;
                            <? } 
                            else if ($d>48){ ?>
                             <button class="btn-danger" style="font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <? } ?>
                             <br>
                             <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                        </td>
                    </tr><? 
                
                } } ?>
                </table>
                </div>
                <?
           }
        }
        
           if($rolid=='director'){
            $crq=new CDbCriteria();
               $crq->condition='eclosed=0';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                { ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>Status</b></td>
                        
                    </tr> 
                <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                if($d>48 ) {
                ?>
                <tr>
                        <td><?=$i->enquiry_type ?>
                             <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                            <br>
                             <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td><? if($d<=24) { ?>
                            <button class="btn-warning" style="font-size:14px !important; padding:4px;border-radius:5px;">Active</button>
                            <br><br>
                            <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                            <? }
                             else { ?>
                             <button class="btn-danger" style="margin-bottom:4px;font-size:14px !important; padding:4px;border-radius:5px;">Escalated</button>
                            <? } ?>
                             
                             
                        </td>
                    </tr>
                
                <? 
                }
               }
               ?>
                </table>
             </div>
             <?
            }
        }
        
       }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  } 
     }
     
        public function actionEnqa(){
            if(Yii::app()->request->isPostRequest) {
           
           $rolid = Yii::app()->user->getState("rolid");
           $ilcid=Yii::app()->user->getState('ilc_id');
           $userid=Yii::app()->user->getState('user_id');
           
           if($rolid=='unit'  )
           {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and eclosed=1 and enquired_by=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                    ?>
               <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                <tr>
                        <td><?=$i->enquiry_type ?>
                         <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                           <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                             <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                             
                        </td>
                    </tr>
        <?
        }
        ?>
            </table>
                </div><?
       }
     }
            
            if($rolid=='partner')
                {
               $crq=new CDbCriteria();
               $crq->condition='ilcid=:u and eclosed=1 and enquired_by=:us';
               $crq->params=array(':u'=>$ilcid,':us'=>$userid);
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                    ?>
               <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>  
                 <?
                foreach ($en as $i){
                    
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                ?>
                <tr>
                        <td><?=$i->enquiry_type ?> <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                          <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                             <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                             
                        </td>
                    </tr>
           
        <?
        }
        ?>
            </table>
                </div><?
       }
     }
           if($rolid=='ilcmanager')
           {
               $crqm=new CDbCriteria();
               $crqm->condition='ilcmid=:u';
               $crqm->params=array(':u'=>$userid);
               $ilcs= ManagerIlc::model()->findAll($crqm);
               $ii=array();
               foreach($ilcs as $il){
                    array_push($ii,$il->ilcid);
                }
               $crq=new CDbCriteria();
               $crq->condition='eclosed=1';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                if(in_array($ilid,$ii)){
                
                $edate1=$i->date1;
                /////time difference calculation////////
                $currtime=  strtotime(date('Y-m-d H:i:s'));
                $datediff = $currtime-$edate1;
		$d=round($datediff/(60 * 60 * 24))*24;
                ?>
                <tr>
                        <td><?=$i->enquiry_type ?> <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                        <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        
                    </tr>
                
               
                <? }
               }
               ?>
                </table>
              </div>
                <?
            }
            
        }
        
           if($rolid=='academic'){
            $crq=new CDbCriteria();
               $crq->condition='eclosed=1';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font" >  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                $edate1=$i->date1;
                
                ?>
                
                <tr>
                        <td><?=$i->enquiry_type ?> <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                             <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                             <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                             
                        </td>
                    </tr>
                 <? 
               
               }
               ?>
                </table>
                </div>
                <?
            }
        }
        
           if($rolid=='director'){
            $crq=new CDbCriteria();
               $crq->condition='eclosed=1';
               $crq->order='date1 desc';
               $en= Enquiry::model()->findAll($crq);
               $n=sizeof($en);
               if($n==0) {
                ?> <p style="text-align:center"> <? echo "No Current Enquiries"; ?></p> <?
                }
                else 
                {
                    ?>
                <div class="table table-striped table-font">  
                <table>
                    <tr class="bold">
                        <td><b>Type</b></td>
                        <td><b>Date</b></td>
                        <td><b>Child Info</b></td>
                        <td><b>Parent Info</b></td>
                        <td><b>Contact</b></td>
                        <td><b>Manager's Feedback</b></td>
                        <td><b>More Info</b></td>
                        
                    </tr>        
               <?
                foreach ($en as $i){
                
                $ilid=$i->ilcid;
                $par=$i->parentid;
                $cri=new CDbCriteria();
                $cri->condition='parentid=:u';
                $cri->params=array(":u"=>$par);
                $ep=EnquiryParent::model()->find($cri); 
                
                $edate1=$i->date1;
                
                ?>
                
                <tr>
                        <td><?=$i->enquiry_type ?> <? 
                            if($i->pref==1) {
                                ?>
                            <div style="width: 100px; background-color: red; color: white; font-weight: 600; padding: 5px">PREFERRED</div>      
                             <?
                            }
                            ?>
                          <br>
                           Created on &nbsp;<strong><?=date('Y-m-d',$i->date1) ?></strong>
                        </td>
                        <td><?=$i->enquiry_date ?><br><strong><?=$i->enquiry_time ?></strong></td>
                        <td><?=$ep->child_name ?>,
                        <?=$ep->gender ?> <br>born on <?=$ep->child_dob ?> </td>
                        <td><?=$ep->parent_f_name ?><br>
                        <?=$ep->parent_f_email ?><br>
                        <?=$ep->parent_f_phone ?></td>
                        <td>
                            <?=$ep->parent_add.', '.$ep->city ?><br>
                            
                        </td>
                        <td>
                            <? if($i->estatus!='' || $i->etask!=''){
                                ?>
                            <p><?=$i->estatus ?><br>
                                <b>Task:</b><?=$i->etask ?>
                            </p>   
                                <?
                            }?>
                        </td>
                        <td>
                             <input type="button" style="font-size:14px !important; padding:4px;border-radius:5px;" class="btn-dark" id="<?=$i->enquiryid ?>" name="<?=$i->enquiryid ?>" value="View Details" onclick="setai(this.id)" />
                            
                             
                        </td>
                    </tr>
                
                <? 
                
               }
               ?>
                </table>
                    </div>
                        <?
            }
        }
      
	}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}  
        
     }
   
        public function actionSetst(){
        if(Yii::app()->request->isPostRequest) {
           $enqid=trim($_POST['enqid']);    
        $cru=new CDbCriteria(); 
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $en= Enquiry::model()->find($cru);
        
     ?>
         <div class="row">            
            <div class="col-md-12"> 
                <p>Set status for this enquiry</p>
                <input type="hidden" id="enqid" value="<?=$enqid ?>">   
               <textarea class="form-control" id="st1" name="st1" rows="3" placeholder="Add some text"></textarea>
            </div>
             <br>
             <div class="col-md-12">  
                 <p>Set a task for Unit Coordinator</p>
               <textarea class="form-control" id="ta1" name="ta1" rows="3" placeholder="Add some text"></textarea>
            </div>
         </div>
         <br>
         <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatest()"/>
         </div>
            
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionSetdetail(){
        if(Yii::app()->request->isPostRequest) {
           $enqid=trim($_POST['enqid']);
           $rolid=Yii::app()->user->getState('rolid');    
        
        $cru=new CDbCriteria(); 
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $en= Enquiry::model()->find($cru);
        $parid=$en->parentid;
        
        $crp=new CDbCriteria(); 
        $crp->condition='parentid=:u';
        $crp->params=array(":u"=>$parid);
        $pa= EnquiryParent::model()->find($crp);
        
        
     ?>
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Father's Details</strong>
         </div>
         <div class="row">
            <div class="col-md-6"> 
                <input type="hidden" id="parid1" value="<?=$parid ?>">
                <input type="text" class="enin" id="femail" value="<?=($pa->parent_f_email=='')?'':$pa->parent_f_email ?>"  placeholder="Email ID">  
            </div>
             <div class="col-md-6"> 
                <input type="text" class="enin" id="focc" value="<?=($pa->f_occupation=='')?'':$pa->f_occupation ?>" placeholder="Occupation">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="fphn" value="<?=($pa->parent_f_phone=='')?'':$pa->parent_f_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                 <select type="text" class="enin" id="fcall"  placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?>">
                    <?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?> </option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Call at your own risk">Call at your own risk</option>
                 </select> 
            </div>
         </div>
         <br style="clear:both">
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Mother's Details</strong>
         </div>
         <div class="row">
             <div class="col-md-6"> 
                <input type="text" class="enin" id="mname" value="<?=($pa->parent_m_name=='')?'':$pa->parent_m_name ?>" placeholder="Name">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="memail" value="<?=($pa->parent_m_email=='')?'':$pa->parent_m_email ?>" placeholder="Email ID">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="mphn" value="<?=($pa->parent_m_phone=='')?'':$pa->parent_m_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                <select type="text" class="enin" id="mcall" placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?>"><?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?></option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Mother's busy call father">Mother's busy call father.</option>
                 </select>  
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
            <div class="col-md-12"> 
              <textarea  class="enin" id="add" placeholder="Parent Address"><?=(trim($pa->parent_add)=='')?'':trim($pa->parent_add) ?></textarea>
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                 <strong style="margin-left:1%;font-size:16px !important">
                     How have parents heard about Edify Kids Baguiati?</strong>
                 <div id="hhear">
                 <input type="hidden" value="<?=$pa->how_hear ?>" id="how">    
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Hoardings">
                    <span style="font-size:16px!important">Hoardings</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Pamphlets" >
                    <span style="font-size:16px!important">Pamphlets</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Newspaper insert">
                    <span style="font-size:16px!important">Newspaper insert</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Just Dial Services">
                    <span style="font-size:16px!important">Just Dial Services</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Cable TV Ad">
                  <span style="font-size:16px!important">Cable TV Ad</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Internet">
                   <span style="font-size:16px!important">Internet</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Word of mouth">
                    <span style="font-size:16px!important">Word of mouth</span>&nbsp;
                </div>
                </div>
             </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                <input type="text" class="enin" id="hoarding" value="<?=($pa->hoarding=='')?'':$pa->hoarding ?>" placeholder="Where have you seen the hoarding?">
              </div>
         </div>
         
         <div class="row">
             <div class="col-md-12">
                 <textarea class="enin" id="feed" rows="3" placeholder="Parent Feedback"><?=(trim($pa->parent_feedback)=='')?'':trim($pa->parent_feedback) ?></textarea>
              </div>
         </div>
         
         
          <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatedetail()"/>
         </div>
            
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
        }
        
        public function actionSetai(){
            if(Yii::app()->request->isPostRequest) {
           $enqid=trim($_POST['enqid']);


        $rolid=Yii::app()->user->getState('rolid');    
        
        $cru=new CDbCriteria(); 
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $en= Enquiry::model()->find($cru);
        $parid=$en->parentid;
        
        $crp=new CDbCriteria(); 
        $crp->condition='parentid=:u';
        $crp->params=array(":u"=>$parid);
        $pa= EnquiryParent::model()->find($crp);
        
        ?>
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Father's Details</strong>
         </div>
         <div class="row">
            <div class="col-md-6"> 
                <input type="hidden" id="parid1" value="<?=$parid ?>">
                <input type="text" class="enin" id="femail" value="<?=($pa->parent_f_email=='')?'':$pa->parent_f_email ?>"  placeholder="Email ID">  
            </div>
             <div class="col-md-6"> 
                <input type="text" class="enin" id="focc" value="<?=($pa->f_occupation=='')?'':$pa->f_occupation ?>" placeholder="Occupation">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="fphn" value="<?=($pa->parent_f_phone=='')?'':$pa->parent_f_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                 <select type="text" class="enin" id="fcall"  placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?>">
                    <?=($pa->parent_f_calltime=='' || $pa->parent_f_calltime=='0')?'Convenient time to call':$pa->parent_f_calltime ?> </option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Call at your own risk">Call at your own risk</option>
                 </select> 
            </div>
         </div>
         <br style="clear:both">
         <div class="row"> 
             <strong style="margin-left:3%;font-size:16px !important">Mother's Details</strong>
         </div>
         <div class="row">
             <div class="col-md-6"> 
                <input type="text" class="enin" id="mname" value="<?=($pa->parent_m_name=='')?'':$pa->parent_m_name ?>" placeholder="Name">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="memail" value="<?=($pa->parent_m_email=='')?'':$pa->parent_m_email ?>" placeholder="Email ID">  
            </div>
            <div class="col-md-6"> 
                <input type="text" class="enin" id="mphn" value="<?=($pa->parent_m_phone=='')?'':$pa->parent_m_phone ?>" placeholder="Phone No.">  
            </div>
             <div class="col-md-6"> 
                <select type="text" class="enin" id="mcall" placeholder="Preferable Call time">
                     <option value="<?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?>"><?=($pa->parent_m_calltime=='' || $pa->parent_m_calltime=='0')?'Convenient time to call':$pa->parent_m_calltime ?></option>
                     <option value="8am to 12noon">8am to 12noon</option>
                     <option value="12noon to 6pm">12noon to 6pm</option>
                     <option value="6pm to 9pm">6pm to 9pm</option>
                     <option value="Mother's busy call father">Mother's busy call father.</option>
                 </select>  
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
            <div class="col-md-12"> 
              <textarea  class="enin" id="add" placeholder="Parent Address"><?=(trim($pa->parent_add)=='')?'':trim($pa->parent_add) ?></textarea>
            </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                 <strong style="margin-left:1%;font-size:16px !important">
                     How have parents heard about Edify Kids Baguiati?</strong>
                 <div id="hhear">
                 <input type="hidden" value="<?=$pa->how_hear ?>" id="how">    
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Hoardings">
                    <span style="font-size:16px!important">Hoardings</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Pamphlets" >
                    <span style="font-size:16px!important">Pamphlets</span>&nbsp;
                </div>
                 
                 <div class="form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Newspaper insert">
                    <span style="font-size:16px!important">Newspaper insert</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Just Dial Services">
                    <span style="font-size:16px!important">Just Dial Services</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Cable TV Ad">
                  <span style="font-size:16px!important">Cable TV Ad</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Internet">
                   <span style="font-size:16px!important">Internet</span>&nbsp;
                </div>
                 
                 <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="Word of mouth">
                    <span style="font-size:16px!important">Word of mouth</span>&nbsp;
                </div>
                </div>
             </div>
         </div>
         <br style="clear:both">
         <div class="row">
             <div class="col-md-12">
                <input type="text" class="enin" id="hoarding" value="<?=($pa->hoarding=='')?'':$pa->hoarding ?>" placeholder="Where have you seen the hoarding?">
              </div>
         </div>
         
         <div class="row">
             <div class="col-md-12">
                 <textarea class="enin" id="feed" rows="3" placeholder="Parent Feedback"><?=(trim($pa->parent_feedback)=='')?'':trim($pa->parent_feedback) ?></textarea>
              </div>
         </div>
         
            
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
        }
        
        public function actionNewm(){
        if(Yii::app()->request->isPostRequest) {
           $enqid=trim($_POST['enqid']);

        $cru=new CDbCriteria(); 
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $en= Enquiry::model()->find($cru);
        
     ?>
        <div class="row">            
            <div class="col-md-12">  
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon2">NEW Manager</span>
                    </div>
                    <input type="hidden" id="enqid" value="<?=$enqid ?>">
                    <select class="form-control" id="ilcmid" >
                        <option value="0" selected>Choose NEW Manager</option>
                        <?
                        $crq=new CDbCriteria(); 
                        $crq->select='*';
                        $crq->condition="role=:r";
                        $crq->params=array(':r'=>'ilcmanager');
                        $ilc=Users::model()->findAll($crq);
        
                        foreach($ilc as $i){
                            $iname=$i->name;
                            $iid=$i->userid;
                            ?>
                            <option value="<?=$iid ?>"><?=$iname ?></option>
                            <? } ?>
                    </select>
                </div>
</div>
         </div>
         <br>
         <div class="input-group">
            <input type="button" class="btn btn-success" value="Update" onclick="updatem()"/>
         </div>
            
        <?
        }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUpdatest(){
            if(Yii::app()->request->isPostRequest) {
            $enqid=trim($_POST['enqid']);
            $st1=trim($_POST['st1']);
            $ta1=trim($_POST['ta1']);
            $qq=new Enquiry();
            $qq->updateAll(array('estatus'=>$st1,'etask'=>$ta1), "enquiryid=:q", array(':q'=>$enqid));
        
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        }
        
        public function actionUpdatedetail(){
            if(Yii::app()->request->isPostRequest) {
           $parid=trim($_POST['parid']);
           $femail=trim($_POST['femail']);
           $fphn=trim($_POST['fphn']);
           $focc=trim($_POST['focc']);
           $fcall=trim($_POST['fcall']);
           $mname=trim($_POST['mname']);
           $mphn=trim($_POST['mphn']);
           $memail=trim($_POST['memail']);
           $mcall=trim($_POST['mcall']);
           $add=trim($_POST['add']);
           $hhear=trim($_POST['hhear']);
           $hoarding=trim($_POST['hoarding']);
           $feed=trim($_POST['feed']);

            $qq=new EnquiryParent();
            $qq->updateAll(array(
                'f_occupation'=>$focc,
                'parent_f_phone'=>$fphn,
                'parent_f_email'=>$femail,
                'parent_f_calltime'=>$fcall,
                'parent_m_name'=>$mname,
                'parent_m_email'=>$memail,
                'parent_m_calltime'=>$mcall,
                'parent_m_phone'=>$mphn,
                'parent_add'=>$add,
                'how_hear'=>$hhear,
                'hoarding'=>$hoarding,
                'parent_feedback'=>$feed,
                ),
                    "parentid=:q", array(':q'=>$parid));
          }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}  
        }
        
        public function actionUpdatem(){
            if(Yii::app()->request->isPostRequest) {
           $enqid=trim($_POST['enqid']);
           $ilcmid=trim($_POST['ilcmid']);
           $ilcmname=trim($_POST['ilcmname']);
        $qq=new Enquiry();
        $acaid = Yii::app()->user->getState("user_id");
        
        $qq->updateAll(array('ilcmid'=>$ilcmid), "enquiryid=:q", array(':q'=>$enqid)); 
        
        $cru=new CDbCriteria();
        $cru->condition='enquiryid=:u';
        $cru->params=array(":u"=>$enqid);
        $enq=Enquiry::model()->find($cru);
        $parid=$enq->parentid;
        $uid=$enq->userid;
        //$ilcmid=$enq->ilcmid;
        
        $usr=new Nlist();
        $uu=$usr->getuser($uid);
        $uname=$uu->username;
        $name=$uu->name;
        
        $crum=new CDbCriteria();
        $crum->condition='ilcmid=:u';
        $crum->params=array(":u"=>$ilcmid);
        $enqm= ManagerIlc::model()->find($crum);
        $nm=  sizeof($enqm);
        
        $crup=new CDbCriteria();
        $crup->condition='parentid=:u';
        $crup->params=array(":u"=>$parid);
        $enqp=  EnquiryParent::model()->find($crup);
        $cname=$enqp->child_name;
         
        $mst="Academic head has assigned a new ILC Manager ".$ilcmname." for the enquiry of ".$cname." on ".$enq->enquiry_date." at ".$enq->enquiry_time;
        $mailb="Academic head has assigned a new ILC Manager $ilcmname for the enquiry of $cname on $enq->enquiry_date at $enq->enquiry_time";
        $qn=new Notifications();
        $qn->queryid=$enqid;
        $qn->notid=  uniqid();
        $qn->role="unit";
        $qn->ilc=$enq->ilcid;
        $qn->message=$mst;
        $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
        $qn->count1=3;
        $qn->isNewRecord=true;
        $qn->save(FALSE);
    
        $qn=new Notifications();
        $qn->queryid=$enqid;
        $qn->notid=  uniqid();
        $qn->role="partner";
        $qn->ilc=$enq->ilcid;
        $qn->message=$mst;
        $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
        $qn->count1=3;
        $qn->isNewRecord=true;
        $qn->save(FALSE);
        
        
        if($nm!=0){
        $mailb="Academic head has assigned you as new ILC Manager $ilcmname for the enquiry of $cname on $enq->enquiry_date at $enq->enquiry_time";
            
        $qn=new Notifications();
        $qn->queryid=$enqid;
        $qn->notid=  uniqid();
        $qn->role="ilcmanager";
        $qn->ilc=$enqm->ilcid;
        $qn->message=$mst;
        $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
        $qn->count1=3;
        $qn->isNewRecord=true;
        $qn->save(FALSE);
        
        $usr=new Nlist();
        $uu=$usr->getuser($ilcmid);
        $uname=$uu->username;
        $name=$uu->name;
        
        }
             ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Manager reassigned";
            $msg="Dear $name,<br>
                $mailb<br>
                Regards,<br>
                Beanstalk Team.
                ";
            
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            
	}
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  }
        } 
        
        public function actionCloseenq() {
            if(Yii::app()->request->isPostRequest) {
            $enqid=trim($_POST['enqid']);
            $sfa=trim($_POST['sfa']);


            $cru=new CDbCriteria(); 
            $cru->condition='enquiryid=:u';
            $cru->params=array(":u"=>$enqid);
            $inter= Enquiry::model()->find($cru);
            $sfa=  intval($sfa);
            $ilcid=$inter->ilcid;
            $ilcm=$inter->ilcmid;
            $parid= $inter->parentid;
            
            $crp=new CDbCriteria(); 
            $crp->condition='parentid=:u';
            $crp->params=array(":u"=>$parid);
            $intep= EnquiryParent::model()->find($crp);
             
            $qq=new Notifications();
            $qq->deleteAll("queryid=:q ", array(":q"=>$enqid));
            
            $qq=new Enquiry();
            if($sfa==0)
            {
            $qq->updateAll(array("eclosed"=>1,"clodate"=>date('Y-m-d')), 'enquiryid=:q', array(":q"=>$enqid));
            
            $mst="The enquiry on ".$inter->enquiry_date." has been closed";
            $qn=new Notifications();
            $qn->queryid=$enqid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$inter->ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            $mailb="The enquiry on $inter->enquiry_date has been closed";
            
            
            }
            else {
               $qq->updateAll(array("eclosed"=>1,"sfa"=>1), 'enquiryid=:q', array(":q"=>$enqid));
                     $mst="The enquiry on ".$inter->enquiry_date." has been closed and selected for admission";
            $qn=new Notifications();
            $qn->queryid=$enqid;
            $qn->notid=  uniqid();
            $qn->role="ilcmanager";
            $qn->ilc=$inter->ilcid;
            $qn->message=$mst;
            $qn->date1=  intval(strtotime(date('Y-m-d H:i:s')));
            $qn->count1=3;
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            
            $mailb="The enquiry on $inter->enquiry_date has been closed and selected for admission";
            
            $rw=new Report();
            $week=$rw->getweekno(date('Y-m-d'));
            
                $cri=new CDbCriteria();
                $cri->condition='ilcid=:u';
                $cri->params=array(":u"=>$ilcid);
                $ilcinfo= Ilc::model()->find($cri);
            
            $qn=new Admission();
            $qn->adid= uniqid();
            $qn->parentid=$inter->parentid;
            $qn->enqid=$enqid;
            $qn->week=$week;
            $qn->month=intval(date('m'));
            $qn->year=intval(date('Y'));
            $qn->userid=$inter->enquired_by;
            $qn->ilcid=$inter->ilcid;
            $qn->f_name=$intep->parent_f_name;
            $qn->m_name=$intep->parent_m_name;
            $qn->state=$ilcinfo->state;
            $qn->city=$ilcinfo->city;
            $qn->date1=intval(strtotime(date('Y-m-d H:i:s')));
            $qn->isNewRecord=true;
            $qn->save(FALSE);
            }
           
            $usr=new Nlist();
            $uu=$usr->getuser($ilcm);
            $uname=$uu->username;
            $name=$uu->name;
            
            ////////////////send mail//////////////////
            $to=$uname;
            //$url="http://mis.beanstalkedu.com/index.php/newregister/?q=".$uid;
            $subject="Beanstalk Enquiry Closed";
            $msg="Dear $name,<br>
                $mailb<br>
                Regards,<br>
                Beanstalk Team.
                   ";
            $nl=new Nlist();
            $nl->mailsend($to, $subject, $msg);
            /////////////////////////////////////////////
            }
             else
             {throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');  
}
            }
            
        }