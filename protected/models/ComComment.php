<?php

/**
 * This is the model class for table "com_comment".
 *
 * The followings are the available columns in table 'com_comment':
 * @property string $comid
 * @property string $userid
 * @property string $replyid
 * @property string $reply
 * @property string $date
 * @property string $day
 * @property string $mon
 * @property string $year
 * @property string $document
 */
class ComComment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ComComment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'com_comment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comid, userid, replyid, reply, date, day, mon, year, document', 'required'),
                        array('id', 'integerOnly'=>true),
			array('comid, userid, replyid', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, comid, userid, replyid, reply, date, day, mon, year, document', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'comid' => 'Comid',
			'userid' => 'Userid',
			'replyid' => 'Replyid',
			'reply' => 'Reply',
			'date' => 'Date',
			'day' => 'Day',
			'mon' => 'Mon',
			'year' => 'Year',
			'document' => 'Document',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('id',$this->id,true);
		$criteria->compare('comid',$this->comid,true);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('replyid',$this->replyid,true);
		$criteria->compare('reply',$this->reply,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('day',$this->day,true);
		$criteria->compare('mon',$this->mon,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('document',$this->document,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}